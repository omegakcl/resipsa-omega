Option Strict Off
Option Explicit On
Imports LegalObjectsModel
Module LegalEntityTypes
	
	'Don't want to dictate how legal dlls should use errorhandling, so no reference to _
	'LRoot here.
	
	Public Enum EntityType
		PLC = 1
		Limited = 2
		LLP = 3
	End Enum
	
	Public Sub UnanticipatedError(ByRef s As String)
		
		On Error Resume Next
		System.Diagnostics.Debug.WriteLine(VB6.TabLayout(Err.Number, Err.Description, s))
		
	End Sub
	
    Public Sub AddEntityType(ByRef EType As EntityType, ByRef C As List(Of String))

        On Error GoTo ErrorHandler

        With C
            Select Case EType

                Case EntityType.PLC
                    .Add("p.l.c.")
                    .Add("plc")
                    .Add("Plc")
                    .Add("PLC")
                    .Add("Public Limited Company")
                Case EntityType.Limited
                    .Add("Limited")
                    .Add("Ltd.")
                Case EntityType.LLP
                    .Add("Limited Liability Partnership")
                    .Add("LLP")
            End Select

        End With

        Exit Sub '******

ErrorHandler:

        UnanticipatedError("EntityTypeCol of Module LegalEntityType")
        Resume Next

    End Sub
	
    Public Function fnEntityType(ByRef A As Entity) As EntityType

        'Thought about putting this in JClass as there should already be a JClass existing if this is _
        'to apply. But the syntax for calling it gets a lot longer.

        Dim C As New List(Of String)
        Dim m As EntityType

        If TypeOf A.Incorp Is JClass Then

            For m = EntityType.PLC To EntityType.LLP
                C = New List(Of String)
                AddEntityType(m, C)
                For Each s As String In C
                    If s = A.IncorpSuffix Then Return m
                Next
            Next

        End If

    End Function
End Module