Option Strict Off
Option Explicit On

'Remember there's a build event for this project which copies the dll to the same folder as the exe.
'The wording is copy "C:\Res Ipsa\Legal\bin\EnglishLaw.dll" "C:\Res Ipsa\Res Ipsa\Bin\Debug\EnglishLaw.dll"
'I suppose I could build it straight into the debug folder but I need it for setup too.
REM I need this for debugging so the dll is in the same folder as the exe
'If it fails I'll get an error relating to this. Access the build event through Project/Compile/Build Events.

Imports LegalObjectsModel     'This is the Root Namespace for Res Ipsa.
Imports System.ComponentModel

<Serializable()> Public NotInheritable Class JClass

    Implements IJurisdiction

    'Note importance of marking this as Serializable.

    Private Const HTMLFolderName As String = "html"

    Private ReadOnly Property IJurisdiction_AdjectivalName() As String Implements IJurisdiction.AdjectivalName, IPropGridListNameGiver.PropGridName
        Get

            Return "English"

            Exit Property '******

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_ContentsPage() As String Implements IJurisdiction.ContentsPage

        Get

            'To generate an html file that can show a contents page, use the HTML ActiveX Control _
            'Wizard which shows as a wizard's hat button in the HTML Help editor program. Very easy.
            'Only slight problem with this is that it occurs in a frame within the browser, with a border round it _
            'and I don't really want this. I could enlarge the WebBrowser control to hide this, but _
            'that would be a problem for any other contents pages that weren't produced this way. _
            'Best if I can find a way to alter the html page to fix this. Index page might be harder though _
            'cos it has extra controls.
            'One thought, if it's an ActiveX Control can you use it directly in a VB project? No, if you try to add _
            'a reference through Project/Components you get a message that it's not registrable as an _
            'ActiveX component.

            'Deploying this on the net reveals another problem. You can't use the hhctrl control over the internet. _
            'For some nonsense security reason. So the best fix seems to be to have a local html page and hhc/k file with _
            'all references in the hhc/k file to files adapted to include the full web address.

            Return My.Application.Info.DirectoryPath & "\" & "Contents" & IIf(UseLocalFiles, "", "www") & ".htm"

        End Get
    End Property

    Private ReadOnly Property Currency() As Currency Implements IJurisdiction.Currency
        Get

            Return CurrencySpace.Currencies.Item(PDCurrency.GBP)

        End Get
    End Property

    Private pBsoc As Boolean
    Public Property BSoc() As Boolean
        Get
            Return pBsoc
        End Get
        Set(ByVal value As Boolean)
            pBsoc = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return "England/Wales"
    End Function

    Private ReadOnly Property IJurisdiction_EntityTypes() As List(Of String) Implements IJurisdiction.EntityTypes
        Get

            IJurisdiction_EntityTypes = New List(Of String)
            Dim n As LegalEntityTypes.EntityType
            For n = LegalEntityTypes.EntityType.PLC To LegalEntityTypes.EntityType.LLP
                AddEntityType(n, IJurisdiction_EntityTypes)
            Next

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_EU() As Boolean Implements IJurisdiction.EU
        Get
            Return True

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_FinancialCentres() As List(Of String) Implements IJurisdiction.FinancialCentres
        Get

            Dim p As New List(Of String)
            p.Add("London")
            Return p

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_IndexPage() As String Implements IJurisdiction.IndexPage
        Get


            'See comments under ContentsPage.

            Return My.Application.Info.DirectoryPath & "\" & "Index" & IIf(UseLocalFiles, "", "www") & ".htm"

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_Name() As String Implements IJurisdiction.Name
        Get

            Return "England And Wales"

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_PredefinedJur() As PDJ Implements IJurisdiction.PredefinedJur
        Get

            Return PDJ.UKEngWales


        End Get
    End Property

    Private ReadOnly Property IJurisdiction_RegulatoryTypes() As List(Of String) Implements IJurisdiction.RegulatoryTypes
        Get


            IJurisdiction_RegulatoryTypes = New List(Of String)
            With IJurisdiction_RegulatoryTypes
                .Add("&Bank")
                .Add("&Securities Firm")
            End With

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_RootFolder() As String Implements IJurisdiction.RootFolder
        Get

            Return TopFolder() '& IIf(UseLocalFiles, "\", "/")

        End Get
    End Property

    Private ReadOnly Property IJurisdiction_ZoneA() As Boolean Implements IJurisdiction.ZoneA
        Get

            Return True

        End Get
    End Property

    Private Function TopFolder() As String

        'If files are on the current computer, look there, otherwise look on the Internet.
        'Contents and Index files must be in the same folder as this dll or the top ftp folder, html files in a folder called html.
        If UseLocalFiles() Then
            TopFolder = My.Application.Info.DirectoryPath & "\"
        Else
            TopFolder = "http://finance.practicallaw.com/"
        End If


    End Function

    Private Function UseLocalFiles() As Boolean

        On Error GoTo ErrorHandler

        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        UseLocalFiles = False   'TODO Dir(My.Application.Info.DirectoryPath & "\" & HTMLFolderName, FileAttribute.Directory) <> ""

        Exit Function '******

ErrorHandler:

        UnanticipatedError("UseLocalFiles of Class Module JClass")
        Resume Next

    End Function

    Public Sub DepositTaking(ByVal objX As CashPayment, ByVal THandler As ITopicsHandler) Implements ICash.DepositTaking

        Dim t As Type = objX.Consideration.GetType
        Select Case True

            Case t Is GetType(Deposit)

            Case t Is GetType(Loan)

            Case Else

                'Can't be debt so what's this?     If TypeOf objX Is Debt Then THandler.AddTopic("debt0hkf")

        End Select

    End Sub

    Public Sub CollateralAssetsStamp(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralAssetsStamp
        'If you take shares as collateral, is there stamp duty payable?

        If TypeOf SelSec Is FixedCharge Then

        ElseIf TypeOf SelSec Is Repo Or TypeOf SelSec Is StockLoan Then

        End If


    End Sub

    Public Sub CollateralInsolvency(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralInsolvency
        With SelSec

            If TypeOf SelSec Is FixedCharge Then

                '     If .OwnObln Then

                'LawID = 3

                'End If
                If TypeOf .SecuredObligation Is EToEInstrument Then

                End If

            Else

                '

            End If

        End With
    End Sub

    Public Sub CollateralShareDisc(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralShareDisc

    End Sub

    Public Sub CredSupporterAccounting(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterAccounting


    End Sub

    Public Sub CredSupporterCapReq(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterCapReq

        If objX.FromEntity.Bank Then

        End If

    End Sub

    Public Sub CredSupporterInsolvency(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterInsolvency
    End Sub

    Public Sub CredSupporterStampDuty(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterStampDuty

    End Sub

    Public Sub CredSupporterWHT(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterWHT

    End Sub

    Public Sub AccountsParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AccountsParentConsolidation
    End Sub

    Public Sub AccountsSubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AccountsSubsidiaryConsolidation
    End Sub

    Public Sub AdditionalBorrowerRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AdditionalBorrowerRelated
        If Not objX.FromEntity.Bank Then Exit Sub

        Dim P As Cash
        P = objX.NominalAmount

        If P.Curr Is CurrencySpace.Currencies.Item(PDCurrency.GBP) Or P.Curr Is Nothing Then

            Dim t As Type = objX.GetType
            Select Case True

                Case t Is GetType(Deposit)

                Case t Is GetType(Loan)

                    'BUG - think I've lost a topic here.
                Case t Is GetType(Bonds), t Is GetType(CP), t Is GetType(PromNotes)


                    THandler.AddTopic("crdl3flf")
                Case t Is GetType(Shares), t Is GetType(PrefShares)


                Case True

            End Select

        Else

        End If

    End Sub

    Public Sub AdditionalInvestorRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AdditionalInvestorRelated

        If Not objX.ToEntity.Bank Then Exit Sub

        Dim P As Cash
        P = objX.NominalAmount

        If P.Curr Is CurrencySpace.Currencies.Item(PDCurrency.GBP) Or P.Curr Is Nothing Then

            Dim t As Type = objX.GetType  'simplest way to handle this cos = not defined for System.Type

            Select Case True

                Case t Is GetType(Deposit), t Is GetType(Loan), t Is GetType(CP)

                Case t Is GetType(Bonds), t Is GetType(PromNotes), t Is GetType(PrefShares)
                Case t Is GetType(Shares)


                    'NodeAdd 4

                Case True




            End Select

        Else

        End If

    End Sub

    Public Sub BorrowerAccounting(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerAccounting
        If TypeOf objX Is Shares Then

        Else

        End If
    End Sub

    Public Sub BorrowerInsolvency(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerInsolvency
        If TypeOf objX Is Shares Then

        Else

        End If

    End Sub

    Public Sub BorrowerStampDuty(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerStampDuty
        If TypeOf objX Is Shares Then  'should only apply for depositary really.
        End If

    End Sub

    Public Sub FundingCapitalCriteria(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.FundingCapitalCriteria

        Dim t As Type = objX.GetType

        If objX.FromEntity.Bank Then

            Select Case True

                Case t Is GetType(Bonds), t Is GetType(CP), t Is GetType(Loan), t Is GetType(PromNotes), t Is GetType(Deposit)
                Case t Is GetType(PrefShares)

                Case t Is GetType(Shares)

            End Select



        End If
    End Sub

    Public Sub InvestorCapitalRequirement(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.InvestorCapitalRequirement
        If objX.ToEntity.Bank Then

            Dim t As Type = objX.GetType
            Select Case True
                Case t Is GetType(Bonds), t Is GetType(CP), t Is GetType(PromNotes), t Is GetType(Deposit)

                Case t Is GetType(Loan)

            End Select



            'Give trading book info.

            If objX.FromEntity.Bank And (TypeOf objX Is Shares Or TypeOf objX Is Loan) Then


            ElseIf TypeOf objX Is Shares Then

            ElseIf TypeOf objX Is Deposit Then

            Else 'bonds, loans, deposits

            End If

        End If

        '  End If

    End Sub

    Public Sub InvestorSalesRestrictions(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.InvestorSalesRestrictions

        '    If objX.ToParty.PType = Intermediary Then
        '        THandler.AddTopic  "sale3isn"
        '    End If

    End Sub

    Public Sub RegulatoryParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.RegulatoryParentConsolidation
        If objX.FromEntity.Bank Then



        End If
    End Sub

    Public Sub RegulatorySubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.RegulatorySubsidiaryConsolidation
        If objX.FromEntity.Bank Then

            '  THandler.AddTopic


        End If
    End Sub

    Public Sub ShareHoldingDisclosure(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.ShareHoldingDisclosure
        If TypeOf objX Is PrefShares Then

        End If

        'Dim p As Type = objX.ToParty.GetType

        'Select Case True
        '    Case p Is GetType(Depositary)

        '        THandler.AddTopic("shar7fou")

        '    Case p Is GetType(Intermediary)

        '        THandler.AddTopic("shar29wy")

        '    Case True

        ' End Select
    End Sub

    Public Sub WHT(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.WHT

        If TypeOf objX Is Loan Then
            If objX.ToEntity.Bank Then

            Else
            End If
        ElseIf TypeOf objX Is Instrument.Securities Then

            If TypeOf objX Is Shares Then

            Else

            End If

        ElseIf TypeOf objX Is Deposit Then

            THandler.AddTopic("8-201-9956?q=&qp=&qo=&qe#a557196")

            '    ElseIf TypeOf objX Is IntProp Then
            'TODO
            '      THandler.AddTopic("whti1q9c")

        End If

    End Sub

    Public Sub InvestorAccounting(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.InvestorAccounting

        If Not BaseFunding Is Nothing Then
            For Each i As EToEInstrument In BaseFunding
                If i.FromEntity Is objX.ToEntity And TypeOf BaseFunding Is Shares Then

                End If
            Next
        End If

    End Sub

    Public Sub OwnSharePurchase(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.OwnSharePurchase


    End Sub

    Public Sub Recharacterisation(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.Recharacterisation

    End Sub

    Public Sub SellerAccountingOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerAccountingOffset


    End Sub

    Public Sub SellerInsolvency(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerInsolvency

        Dim t As Type = objX.GetType
        Select Case True

            Case t Is GetType(Sale)

            Case t Is GetType(Asstw)

            Case t Is GetType(Asstwo)

            Case t Is GetType(Novation)

            Case t Is GetType(Subpart)


            Case t Is GetType(CredLink)


            Case t Is GetType(TRSPass)



            Case t Is GetType(FlawedAsset) 'need to move!!



            Case t Is GetType(Trust)


            Case t Is GetType(Subrogation)


        End Select
    End Sub

    Public Sub SellerRegulatoryOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerRegulatoryOffset

    End Sub

    Public Sub ShareHoldingDisclosure1(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.ShareHoldingDisclosure

        Dim t As Type = objX.GetType
        Select Case True

            Case t Is GetType(Sale), t Is GetType(Asstw), t Is GetType(Asstwo)

                '  FLawShares 'no legal significance to the fact that purchase is not primary.

            Case t Is GetType(Trust)


            Case True


        End Select

    End Sub

    Public Sub SwapAccounting(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapAccounting

    End Sub

    Public Sub SwapCapReq(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapCapReq
        If objX.ToEntity.Bank Then

        End If
    End Sub

    Public Sub SwapInsolvency(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapInsolvency

    End Sub

    Public Sub SwapStampDuty(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapStampDuty

    End Sub

    Public Sub SwapWHT(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapWHT

    End Sub

    Public Sub StampDuty(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.StampDuty

        Dim t As Type = BaseFunding.Item(0).GetType

        Select Case True

            Case t Is GetType(Shares)


            Case t Is GetType(PrefShares)


            Case t Is GetType(PromNotes)


            Case t Is GetType(DepRecs)



            Case t Is GetType(Trade), t Is GetType(CreditCard)



            Case t Is GetType(IntProp)



            Case Else 'loan could be from an individual, bonds couldn't.

        End Select
    End Sub

    Public ReadOnly Property StockExchanges() As System.Collections.Generic.List(Of String) Implements IJurisdiction.StockExchanges
        Get
            StockExchanges = New List(Of String)
            StockExchanges.Add("London")
        End Get
    End Property

    Public Sub CollateralAdditional(ByVal SelSec As LegalObjectsModel.SecurityBase, ByVal THandler As LegalObjectsModel.ITopicsHandler) Implements LegalObjectsModel.ICollateralLaw.CollateralAdditional
        '    'PLCs can't take security over own shares, cos would lead to reduction in own share capital if enforced.

        '    With SelSec
        '        '   If .Collateral.FromParty.LEntity Is .SecuredObligation.ToParty.LEntity And LegalEntityTypes.EntityType(.SecuredObln.LinkedCon.FromParty.LegalEntity) = LegalEntityTypes.EntityType.PLC And (TypeOf .Collateral Is Shares) Then
        '        'System.Diagnostics.Debug.WriteLine("s. 150 Companies Act - public companies may not take charges over own shares!")
        '        '     End If
        '    End With

    End Sub

    Public ReadOnly Property TrustRecognition() As LegalObjectsModel.IJurisdiction.TrustRecognitionStatus Implements LegalObjectsModel.IJurisdiction.TrustRecognition
        Get
            Return IJurisdiction.TrustRecognitionStatus.TrustsRecognised
        End Get
    End Property

    Public Function GetRegsObject() As LegalObjectsModel.Regs Implements LegalObjectsModel.IJurisdiction.GetRegsObject
        Return New EngRegs
    End Function

    Public Function CanHoldPropertyInTrust(ByVal E As Entity) As Boolean Implements LegalObjectsModel.IJurisdiction.CanHoldPropertyInTrust
        Return True
    End Function

    Public Function CanTakeDeposits(ByVal E As Entity) As Boolean Implements LegalObjectsModel.IJurisdiction.CanTakeDeposits
        If TypeOf E.MyRegs Is EngRegs Then Return CType(E.MyRegs, EngRegs).Bank
    End Function

    Public Sub HandlePropID(ByVal v As LegalObjectsModel.PropIDAttribute, ByVal THandler As ITopicsHandler) Implements LegalObjectsModel.IJurisdiction.HandlePropID


        If Not v Is Nothing Then

            Select Case v.ID

                Case 1
                    THandler.AddTopic("8-505-1655#a956852")       'Regulated activities: accepting deposits

                Case 2
                    THandler.AddTopic("4-506-8506?source=pspopup")      'Building Societies Act 1986

                Case 3
                    THandler.AddTopic("8-505-1655#a974169")     'Insurers - regulated activities order 2001 added them in, not listed under FSMA 2000.

            End Select

        End If

    End Sub

    Public Sub ShareHoldingRestrictions(ByVal objX As LegalObjectsModel.Shares, ByVal THandler As LegalObjectsModel.ITopicsHandler) Implements LegalObjectsModel.IFundingLaw.ShareHoldingRestrictions

        If TypeOf objX Is OrdShares Then
            Dim s As OrdShares = objX
            If s.FromEntity.Incorp Is Me Then
                If CType(s.FromEntity.MyRegs, EngRegs).FSA Then
                    THandler.AddTopic("3-506-5037")
                End If

            End If
        End If

    End Sub

    Public Sub BankDepositProtection(ByVal objX As LegalObjectsModel.Deposit, ByVal THandler As LegalObjectsModel.ITopicsHandler) Implements LegalObjectsModel.IFundingLaw.BankDepositProtection

    End Sub
End Class

<Serializable()> Public Class EngRegs

    Inherits LegalObjectsModel.Regs

    Private bFSA As Boolean
    <DisplayName("FSA Authorised")> Public Property FSA() As Boolean
        Get
            Return bFSA
        End Get
        Set(ByVal value As Boolean)
            bFSA = value
            If Not value Then
                Bank = False
                BSoc = False
                Insurer = False
            End If
        End Set
    End Property

    Private bBank As Boolean
    <PropID(1), DisplayName("Deposit-Taking Licence")> Public Property Bank() As Boolean
        Get
            Return bBank
        End Get
        Set(ByVal value As Boolean)
            bBank = value
            If value Then FSA = True 'must be.
        End Set
    End Property

    Private bBSoc As Boolean
    <PropID(2), System.ComponentModel.DisplayName("Building Society")> Public Property BSoc() As Boolean
        Get
            Return bBSoc
        End Get
        Set(ByVal value As Boolean)
            bBSoc = value
            If value Then FSA = True 'must be.
        End Set
    End Property

    Private bInsurer As Boolean
    <PropID(3), System.ComponentModel.DisplayName("Insurance Company")> Public Property Insurer() As Boolean
        Get
            Return bInsurer
        End Get
        Set(ByVal value As Boolean)
            bInsurer = value
            If value Then FSA = True 'must be.
        End Set
    End Property

End Class