<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmBase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer      'this holds things like context menus which don't appear in the designer.

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnsMain = New System.Windows.Forms.MenuStrip
        Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSaveAs = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilePasswordProtection = New System.Windows.Forms.ToolStripMenuItem
        Me.zspUIH = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFilePageSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilePrintArea = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilePrintPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilePrint = New System.Windows.Forms.ToolStripMenuItem
        Me.zspQW = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFileGeneratePost = New System.Windows.Forms.ToolStripMenuItem
        Me.zspIU = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFileSendTo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSendToMail = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileSendToPowerPoint = New System.Windows.Forms.ToolStripMenuItem
        Me.zstProp = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFileProperties = New System.Windows.Forms.ToolStripMenuItem
        Me.tstMain = New System.Windows.Forms.ToolStrip
        Me.tsbSave = New System.Windows.Forms.ToolStripButton
        Me.tsbPrintArea = New System.Windows.Forms.ToolStripButton
        Me.tsbPrintPreview = New System.Windows.Forms.ToolStripButton
        Me.tsbPrint = New System.Windows.Forms.ToolStripButton
        Me.tsbCopy = New System.Windows.Forms.ToolStripButton
        Me.zstWop = New System.Windows.Forms.ToolStripSeparator
        Me.tsbGeneratePost = New System.Windows.Forms.ToolStripButton
        Me.zstP = New System.Windows.Forms.ToolStripSeparator
        Me.tsbEmail = New System.Windows.Forms.ToolStripButton
        Me.tsbPowerPoint = New System.Windows.Forms.ToolStripButton
        Me.zstOIH = New System.Windows.Forms.ToolStripSeparator
        Me.tsbAlign = New System.Windows.Forms.ToolStripButton
        Me.zstOIJ = New System.Windows.Forms.ToolStripSeparator
        Me.PageSetupDialog1 = New System.Windows.Forms.PageSetupDialog
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog
        Me.picCanvas = New LegalObjectsModel.PartyCanvas
        Me.mnsMain.SuspendLayout()
        Me.tstMain.SuspendLayout()
        CType(Me.picCanvas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnsMain
        '
        Me.mnsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEdit, Me.mnuFile})
        Me.mnsMain.Location = New System.Drawing.Point(0, 0)
        Me.mnsMain.Name = "mnsMain"
        Me.mnsMain.Size = New System.Drawing.Size(751, 24)
        Me.mnsMain.TabIndex = 3
        Me.mnsMain.Text = "MenuStrip1"
        Me.mnsMain.Visible = False
        '
        'mnuEdit
        '
        Me.mnuEdit.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuEdit.MergeIndex = 1
        Me.mnuEdit.Name = "mnuEdit"
        Me.mnuEdit.Size = New System.Drawing.Size(37, 20)
        Me.mnuEdit.Text = "&Edit"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileClose, Me.mnuFileSave, Me.mnuFileSaveAs, Me.mnuFilePasswordProtection, Me.zspUIH, Me.mnuFilePageSetup, Me.mnuFilePrintArea, Me.mnuFilePrintPreview, Me.mnuFilePrint, Me.zspQW, Me.mnuFileGeneratePost, Me.zspIU, Me.mnuFileSendTo, Me.zstProp, Me.mnuFileProperties})
        Me.mnuFile.MergeAction = System.Windows.Forms.MergeAction.MatchOnly
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(35, 20)
        Me.mnuFile.Text = "&File"
        Me.mnuFile.Visible = False
        '
        'mnuFileClose
        '
        Me.mnuFileClose.Image = Global.LegalObjectsModel.My.Resources.Resources.Close
        Me.mnuFileClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuFileClose.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileClose.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileClose.MergeIndex = 2
        Me.mnuFileClose.Name = "mnuFileClose"
        Me.mnuFileClose.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileClose.Text = "&Close"
        '
        'mnuFileSave
        '
        Me.mnuFileSave.Image = Global.LegalObjectsModel.My.Resources.Resources.Save
        Me.mnuFileSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuFileSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileSave.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileSave.MergeIndex = 3
        Me.mnuFileSave.Name = "mnuFileSave"
        Me.mnuFileSave.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileSave.Text = "&Save"
        '
        'mnuFileSaveAs
        '
        Me.mnuFileSaveAs.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileSaveAs.MergeIndex = 4
        Me.mnuFileSaveAs.Name = "mnuFileSaveAs"
        Me.mnuFileSaveAs.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileSaveAs.Text = "Save &As"
        '
        'mnuFilePasswordProtection
        '
        Me.mnuFilePasswordProtection.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFilePasswordProtection.MergeIndex = 5
        Me.mnuFilePasswordProtection.Name = "mnuFilePasswordProtection"
        Me.mnuFilePasswordProtection.Size = New System.Drawing.Size(251, 22)
        Me.mnuFilePasswordProtection.Text = "Pass&word Protection..."
        '
        'zspUIH
        '
        Me.zspUIH.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zspUIH.MergeIndex = 6
        Me.zspUIH.Name = "zspUIH"
        Me.zspUIH.Size = New System.Drawing.Size(248, 6)
        '
        'mnuFilePageSetup
        '
        Me.mnuFilePageSetup.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFilePageSetup.MergeIndex = 7
        Me.mnuFilePageSetup.Name = "mnuFilePageSetup"
        Me.mnuFilePageSetup.Size = New System.Drawing.Size(251, 22)
        Me.mnuFilePageSetup.Text = "Page Set&up"
        '
        'mnuFilePrintArea
        '
        Me.mnuFilePrintArea.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFilePrintArea.MergeIndex = 8
        Me.mnuFilePrintArea.Name = "mnuFilePrintArea"
        Me.mnuFilePrintArea.Size = New System.Drawing.Size(251, 22)
        Me.mnuFilePrintArea.Text = "Prin&t Area"
        '
        'mnuFilePrintPreview
        '
        Me.mnuFilePrintPreview.Image = Global.LegalObjectsModel.My.Resources.Resources.Preview
        Me.mnuFilePrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFilePrintPreview.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFilePrintPreview.MergeIndex = 9
        Me.mnuFilePrintPreview.Name = "mnuFilePrintPreview"
        Me.mnuFilePrintPreview.Size = New System.Drawing.Size(251, 22)
        Me.mnuFilePrintPreview.Text = "Print Pre&view"
        '
        'mnuFilePrint
        '
        Me.mnuFilePrint.Image = Global.LegalObjectsModel.My.Resources.Resources.Print
        Me.mnuFilePrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuFilePrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFilePrint.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFilePrint.MergeIndex = 10
        Me.mnuFilePrint.Name = "mnuFilePrint"
        Me.mnuFilePrint.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuFilePrint.Size = New System.Drawing.Size(251, 22)
        Me.mnuFilePrint.Text = "&Print..."
        '
        'zspQW
        '
        Me.zspQW.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zspQW.MergeIndex = 11
        Me.zspQW.Name = "zspQW"
        Me.zspQW.Size = New System.Drawing.Size(248, 6)
        '
        'mnuFileGeneratePost
        '
        Me.mnuFileGeneratePost.Image = Global.LegalObjectsModel.My.Resources.Resources.GeneratePost
        Me.mnuFileGeneratePost.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mnuFileGeneratePost.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileGeneratePost.MergeIndex = 12
        Me.mnuFileGeneratePost.Name = "mnuFileGeneratePost"
        Me.mnuFileGeneratePost.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileGeneratePost.Text = "&Generate Post-completion Diagram"
        '
        'zspIU
        '
        Me.zspIU.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zspIU.MergeIndex = 13
        Me.zspIU.Name = "zspIU"
        Me.zspIU.Size = New System.Drawing.Size(248, 6)
        '
        'mnuFileSendTo
        '
        Me.mnuFileSendTo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileSendToMail, Me.mnuFileSendToPowerPoint})
        Me.mnuFileSendTo.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileSendTo.MergeIndex = 14
        Me.mnuFileSendTo.Name = "mnuFileSendTo"
        Me.mnuFileSendTo.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileSendTo.Text = "Sen&d to"
        '
        'mnuFileSendToMail
        '
        Me.mnuFileSendToMail.Image = Global.LegalObjectsModel.My.Resources.Resources.EMail
        Me.mnuFileSendToMail.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileSendToMail.Name = "mnuFileSendToMail"
        Me.mnuFileSendToMail.Size = New System.Drawing.Size(231, 22)
        Me.mnuFileSendToMail.Text = "M&ail Recipient (as Attachment)"
        '
        'mnuFileSendToPowerPoint
        '
        Me.mnuFileSendToPowerPoint.Image = Global.LegalObjectsModel.My.Resources.Resources.PP
        Me.mnuFileSendToPowerPoint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileSendToPowerPoint.Name = "mnuFileSendToPowerPoint"
        Me.mnuFileSendToPowerPoint.Size = New System.Drawing.Size(231, 22)
        Me.mnuFileSendToPowerPoint.Text = "Microsoft &PowerPoint"
        '
        'zstProp
        '
        Me.zstProp.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zstProp.MergeIndex = 15
        Me.zstProp.Name = "zstProp"
        Me.zstProp.Size = New System.Drawing.Size(248, 6)
        '
        'mnuFileProperties
        '
        Me.mnuFileProperties.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuFileProperties.MergeIndex = 16
        Me.mnuFileProperties.Name = "mnuFileProperties"
        Me.mnuFileProperties.Size = New System.Drawing.Size(251, 22)
        Me.mnuFileProperties.Text = "P&roperties"
        '
        'tstMain
        '
        Me.tstMain.AllowItemReorder = True
        Me.tstMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbSave, Me.tsbPrintArea, Me.tsbPrintPreview, Me.tsbPrint, Me.tsbCopy, Me.zstWop, Me.tsbGeneratePost, Me.zstP, Me.tsbEmail, Me.tsbPowerPoint, Me.zstOIH, Me.tsbAlign, Me.zstOIJ})
        Me.tstMain.Location = New System.Drawing.Point(0, 0)
        Me.tstMain.Name = "tstMain"
        Me.tstMain.Size = New System.Drawing.Size(751, 25)
        Me.tstMain.TabIndex = 9
        Me.tstMain.Text = "ToolStrip"
        Me.tstMain.Visible = False
        '
        'tsbSave
        '
        Me.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSave.Image = Global.LegalObjectsModel.My.Resources.Resources.Save
        Me.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSave.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbSave.MergeIndex = 2
        Me.tsbSave.Name = "tsbSave"
        Me.tsbSave.Size = New System.Drawing.Size(23, 22)
        Me.tsbSave.Text = "Save"
        Me.tsbSave.ToolTipText = "Save"
        '
        'tsbPrintArea
        '
        Me.tsbPrintArea.CheckOnClick = True
        Me.tsbPrintArea.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrintArea.Enabled = False
        Me.tsbPrintArea.Image = Global.LegalObjectsModel.My.Resources.Resources.PrintArea
        Me.tsbPrintArea.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrintArea.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbPrintArea.MergeIndex = 4
        Me.tsbPrintArea.Name = "tsbPrintArea"
        Me.tsbPrintArea.Size = New System.Drawing.Size(23, 22)
        Me.tsbPrintArea.Text = "Show/Hide Print Area"
        '
        'tsbPrintPreview
        '
        Me.tsbPrintPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrintPreview.Image = Global.LegalObjectsModel.My.Resources.Resources.Preview
        Me.tsbPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrintPreview.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbPrintPreview.MergeIndex = 5
        Me.tsbPrintPreview.Name = "tsbPrintPreview"
        Me.tsbPrintPreview.Size = New System.Drawing.Size(23, 22)
        Me.tsbPrintPreview.Text = "ToolStripButton11"
        Me.tsbPrintPreview.ToolTipText = "Print Preview"
        '
        'tsbPrint
        '
        Me.tsbPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrint.Image = Global.LegalObjectsModel.My.Resources.Resources.Print
        Me.tsbPrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrint.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbPrint.MergeIndex = 6
        Me.tsbPrint.Name = "tsbPrint"
        Me.tsbPrint.Size = New System.Drawing.Size(23, 22)
        Me.tsbPrint.Text = "Print"
        Me.tsbPrint.ToolTipText = "Print"
        '
        'tsbCopy
        '
        Me.tsbCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbCopy.Image = Global.LegalObjectsModel.My.Resources.Resources.Copy
        Me.tsbCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbCopy.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbCopy.MergeIndex = 7
        Me.tsbCopy.Name = "tsbCopy"
        Me.tsbCopy.Size = New System.Drawing.Size(23, 22)
        Me.tsbCopy.Text = "ToolStripButton2"
        Me.tsbCopy.ToolTipText = "Copy Image"
        '
        'zstWop
        '
        Me.zstWop.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zstWop.MergeIndex = 8
        Me.zstWop.Name = "zstWop"
        Me.zstWop.Size = New System.Drawing.Size(6, 25)
        '
        'tsbGeneratePost
        '
        Me.tsbGeneratePost.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbGeneratePost.Image = Global.LegalObjectsModel.My.Resources.Resources.GeneratePost
        Me.tsbGeneratePost.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbGeneratePost.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbGeneratePost.MergeIndex = 9
        Me.tsbGeneratePost.Name = "tsbGeneratePost"
        Me.tsbGeneratePost.Size = New System.Drawing.Size(23, 22)
        Me.tsbGeneratePost.Text = "ToolStripButton1"
        Me.tsbGeneratePost.ToolTipText = "Generate Post-Completion Diagram"
        '
        'zstP
        '
        Me.zstP.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zstP.MergeIndex = 10
        Me.zstP.Name = "zstP"
        Me.zstP.Size = New System.Drawing.Size(6, 25)
        '
        'tsbEmail
        '
        Me.tsbEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbEmail.Image = Global.LegalObjectsModel.My.Resources.Resources.EMail
        Me.tsbEmail.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEmail.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbEmail.MergeIndex = 11
        Me.tsbEmail.Name = "tsbEmail"
        Me.tsbEmail.Size = New System.Drawing.Size(23, 22)
        Me.tsbEmail.Text = "Send as e-mail Attachment"
        '
        'tsbPowerPoint
        '
        Me.tsbPowerPoint.Image = Global.LegalObjectsModel.My.Resources.Resources.PP
        Me.tsbPowerPoint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPowerPoint.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbPowerPoint.MergeIndex = 12
        Me.tsbPowerPoint.Name = "tsbPowerPoint"
        Me.tsbPowerPoint.Size = New System.Drawing.Size(84, 22)
        Me.tsbPowerPoint.Text = " &PowerPoint"
        '
        'zstOIH
        '
        Me.zstOIH.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zstOIH.MergeIndex = 13
        Me.zstOIH.Name = "zstOIH"
        Me.zstOIH.Size = New System.Drawing.Size(6, 25)
        '
        'tsbAlign
        '
        Me.tsbAlign.Image = Global.LegalObjectsModel.My.Resources.Resources.Align
        Me.tsbAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbAlign.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.tsbAlign.MergeIndex = 14
        Me.tsbAlign.Name = "tsbAlign"
        Me.tsbAlign.Size = New System.Drawing.Size(67, 22)
        Me.tsbAlign.Text = " Ali&gn All"
        '
        'zstOIJ
        '
        Me.zstOIJ.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.zstOIJ.MergeIndex = 15
        Me.zstOIJ.Name = "zstOIJ"
        Me.zstOIJ.Size = New System.Drawing.Size(6, 25)
        '
        'PageSetupDialog1
        '
        Me.PageSetupDialog1.EnableMetric = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'picCanvas
        '
        Me.picCanvas.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.picCanvas.BackColor = System.Drawing.Color.Teal
        Me.picCanvas.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.picCanvas.Location = New System.Drawing.Point(0, 0)
        Me.picCanvas.Name = "picCanvas"
        Me.picCanvas.Size = New System.Drawing.Size(751, 419)
        Me.picCanvas.TabIndex = 12
        Me.picCanvas.TabStop = False
        Me.picCanvas.Visible = False
        '
        'frmBase
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 419)
        Me.Controls.Add(Me.mnsMain)
        Me.Controls.Add(Me.tstMain)
        Me.Controls.Add(Me.picCanvas)
        Me.MainMenuStrip = Me.mnsMain
        Me.Name = "frmBase"
        Me.ShowInTaskbar = False
        Me.Text = "Form1"
        Me.mnsMain.ResumeLayout(False)
        Me.mnsMain.PerformLayout()
        Me.tstMain.ResumeLayout(False)
        Me.tstMain.PerformLayout()
        CType(Me.picCanvas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mnsMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSaveAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFilePasswordProtection As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFilePageSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFilePrintPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFilePrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSendTo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSendToMail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSendToPowerPoint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileProperties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspIU As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFilePrintArea As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tstMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbEmail As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPrintArea As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPrintPreview As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPrint As System.Windows.Forms.ToolStripButton
    Friend WithEvents zstP As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbPowerPoint As System.Windows.Forms.ToolStripButton
    Friend WithEvents zstOIH As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents zstOIJ As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents picCanvas As PartyCanvas
    Friend WithEvents zstProp As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents zspUIH As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PageSetupDialog1 As System.Windows.Forms.PageSetupDialog
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents tsbCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents zspQW As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileGeneratePost As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zstWop As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbGeneratePost As System.Windows.Forms.ToolStripButton

End Class

