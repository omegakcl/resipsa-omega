Public Enum PDCurrency
    ARS 'Argentine Peso
    AUD
    EUR
    USD
    BRL 'Brazilian Real
    CAD
    GBP
    CHR 'Chinese Renminbi
    CLP 'Chilean Peso
    CZK 'Czech Koruna
    DKK 'Danish Currencies
    EEK 'Estonian Kroon
    HKD
    HUF 'Hungarian Forint
    ICE 'Icelandic Something
    INR 'Indian Rupee
    IDR 'Indonesian Rupiah
    ILS 'Israeli Shekel
    LBP 'Lebanese Pound
    JPY
    MYR  'Malaysian Ringgit
    MXP 'Mexican Peso
    NZD 'NZ Dollar
    NOK 'Norwegian Kronor
    PHP 'Philippine Peso
    PLZ 'Polish Zloty
    RUR 'Russian Rouble
    SAR 'Saudi-Arabian Riyal
    SGD 'Singapore Dollar
    SKK 'Slovak Koruna
    ZAR 'South African Rand
    KWR 'Korean Won
    SEK 'Swedish Krona
    CHF 'Swiss Franc
    TWD 'Taiwan Dollar
    THB 'Thai Baht
    TRL 'Turkish Lira
End Enum

<Serializable()> Public Class Currency
    Implements IPropGridListNameGiver
    'Worth separating this out from Jurisdiction since obviously some currencies are shared between jurisdictions (like euro).

    Private pAcronym As String
    Private pIntOptions() As String

    Sub New(ByVal sAcronym As String, Optional ByVal sIntOptions() As String = Nothing)
        pAcronym = sAcronym
        pIntOptions = sIntOptions
    End Sub

    Public ReadOnly Property Acronym() As String Implements IPropGridListNameGiver.PropGridName
        Get
            Return pAcronym
        End Get
    End Property

    Public ReadOnly Property IntOptions() As String()
        Get
            Return pIntOptions
        End Get
    End Property

End Class

Public Class CurrencySpace

    Public Shared Currencies As New System.Collections.Generic.Dictionary(Of PDCurrency, Currency)

    Shared Sub New()

        RefreshCurrencies()
    End Sub
End Class

Module FillCurrencies
    Friend Sub RefreshCurrencies()

        With CurrencySpace.Currencies
            .Clear()
            .Add(PDCurrency.AUD, New Currency("AUD", New String() {"AUD-BBR-ISDC", "AUD-BBR-BBSW", "AUD-BBR-BBSY (BID)", "AUD-LIBOR-BBA", "AUD-LIBOR-Reference Banks"}))
            .Add(PDCurrency.BRL, New Currency("BRL"))
            .Add(PDCurrency.CAD, New Currency("CAD", New String() {"CAD-BA-ISDD", "CAD-BA-CDOR", "CAD-BA-Telerate", "CAD-BA-Reference Banks", "CAD-TBILL-ISDD", "CAD-TBILL-Telerate", "CAD-TBILL-Reference Banks", "CAD-LIBOR-BBA", "CAD-LIBOR-Reference Banks", "CAD-REPO-CORRA"}))
            .Add(PDCurrency.CHF, New Currency("CHF", New String() {"CHF-LIBOR-ISDA", "CHF-LIBOR-BBA", "CHF-LIBOR-Reference Banks", "CHF-TOIS-OIS-COMPOUND", "CHF-Annual Swap Rate", "CHF-Annual Swap Rate-Reference Banks"}))
            .Add(PDCurrency.CHR, New Currency("CHR"))
            .Add(PDCurrency.ARS, New Currency("ARS"))
            .Add(PDCurrency.CLP, New Currency("CLP"))
            .Add(PDCurrency.CZK, New Currency("CZX", New String() {"CZK-PRIBOR-PRBO", "CZK-PRIBOR-Reference Banks"}))
            .Add(PDCurrency.DKK, New Currency("DKK", New String() {"DKK-CIBOR-DKNA13"}))
            .Add(PDCurrency.EEK, New Currency("EEK"))
            .Add(PDCurrency.EUR, New Currency("EUR", New String() {"EUR-EURIBOR-Telerate", "EUR-EURIBOR-Act/365", "EUR-EURIBOR-Reference Banks", "EUR-LIBOR-BBA", "EUR-LIBOR-Reference Banks", "EUR-EONIA-OIS-COMPOUND", "EUR-EURONIA-OIS-COMPOUND", "EUR-TAM-CDC", "EUR-TMM-CDC-COMPOUND", "EUR-EONIA-AVERAGE", "EUR-TEC10-CNO", "EUR-TEC10-Reference Banks", "EUR-TEC5-CNO", "EUR-TEC5-Reference Banks", "EUR-Annual Swap Rate-10:00", "EUR-Annual Swap Rate-11:00", "EUR-Annual Swap Rate-11:00", "EUR-Annual Swap Rate-3 Month", "EUR-ISDA-EURIBOR Swap Rate-11:00", "EUR-ISDA-EURIBOR Swap Rate-12:00", "EUR-ISDA-LIBOR Swap Rate-10:00", "EUR-ISDA-LIBOR Swap Rate-11:00", "EUR-Annual Swap Rate-Reference Banks"}))
            .Add(PDCurrency.GBP, New Currency("GBP", New String() {"GBP-LIBOR-ISDA", "GBP-LIBOR-BBA", "GBP-LIBOR-Reference Banks", "GBP-Semi-Annual Swap Rate", "GBP-Semi-Annual Swap Rate-Reference Banks", "GBP-WMBA-SONIA-COMPOUND"}))
            .Add(PDCurrency.HKD, New Currency("HKD", New String() {"HKD-HIBOR-ISDC", "HKD-HIBOR-HIBOR=", "HKD-HIBOR-HKAB", "HKD-HIBOR-Reference Banks"}))
            .Add(PDCurrency.HUF, New Currency("HUF", New String() {"HUF-BUBOR-Reuters", "HUF-BUBOR-Reference Banks"}))
            .Add(PDCurrency.ICE, New Currency("ICE"))
            .Add(PDCurrency.IDR, New Currency("IDR", New String() {"IDR-SOR-Telerate", "IDR-SOR-Reference Banks"}))
            .Add(PDCurrency.ILS, New Currency("ILS"))
            .Add(PDCurrency.INR, New Currency("INR"))
            .Add(PDCurrency.JPY, New Currency("JPY", New String() {"JPY-LIBOR-ISDA", "JPY-LIBOR-BBA", "JPY-LIBOR-Reference Banks", "JPY-TIBOR-TIBM (10 Banks)", "JPY-TIBOR-TIBM (5 Banks)", "JPY-TIBOR-TIBM(All Banks)", "JPY-TIBOR-ZTIBOR", "JPY-TIBOR-TIBM-Reference Banks", " JPY-TSR-Telerate-10:00", "JPY-ISDA-Swap Rate-10:00", "JPY-TSR-Telerate-15:00", "JPY-ISDA-Swap Rate-15:00", "JPY-TSR-Reference Banks"}))
            .Add(PDCurrency.KWR, New Currency("KWR"))
            .Add(PDCurrency.LBP, New Currency("LBP"))
            .Add(PDCurrency.MXP, New Currency("MXP"))
            .Add(PDCurrency.MYR, New Currency("MYR", New String() {"MYR-KLIBOR-BNM", "MYR-KLIBOR-Reference Banks"}))
            .Add(PDCurrency.NOK, New Currency("NOK", New String() {"NOK-NIBOR-NIBR", "NOK-NIBOR-Reference Banks"}))
            .Add(PDCurrency.NZD, New Currency("NZD", New String() {"NZD-BBR-ISDC", "NZD-BBR-FRA", "NZD-BBR-Telerate", "NZD-BBR-Reference Banks"}))
            .Add(PDCurrency.PHP, New Currency("PHP"))
            .Add(PDCurrency.PLZ, New Currency("PLZ", New String() {"PLZ-WIBOR-WIBO", "PLZ-WIBOR-Reference Banks"}))
            .Add(PDCurrency.RUR, New Currency("RUR"))
            .Add(PDCurrency.SAR, New Currency("SAR", New String() {"SAD-SRIOR-SUAA", "SAR-SRIOR-Reference Banks"}))
            .Add(PDCurrency.SEK, New Currency("SEK", New String() {"SEK-STIBOR-SIDE", "SEK-STIBOR-Reference Banks"}))
            .Add(PDCurrency.SGD, New Currency("SGD", New String() {"SGD-SIBOR-Telerate", "SGD-SIBOR-Reference Banks", "SGD-SOR-Telerate", "SGD-SOR-Reference Banks"}))
            .Add(PDCurrency.SKK, New Currency("SKK", New String() {"SKK-BRIBOR-BRBO", "SKK-BRIBOR-Bloomberg", "SKK-BRIBOR-Reference Banks"}))
            .Add(PDCurrency.THB, New Currency("THB", New String() {"THB-SOR-Telerate", "THB-SOR-Reference Banks"}))
            .Add(PDCurrency.TRL, New Currency("TRL"))
            .Add(PDCurrency.TWD, New Currency("TWD"))
            .Add(PDCurrency.USD, New Currency("USD", New String() {"USD-BA-H.15", "USD-BA-Reference Dealers", "USD-CD-H.15", "USD-CD-Reference Dealers", "USD-CMS-Telerate", "USD-ISDA-Swap Rate", "USD-CMS-Reference Banks", "USD-CMT-T7051", "USD-CMT-T7052", "USD-COF11-Telerate", "USD-COF11-Telerate", "USD-COF11-FHLBSF", "USD-CP-H.15", "USD-CP-Reference Dealers", "USD-Federal Funds-H.15", "USD-Federal Funds-Reference Dealers", "USD-FFCB-DISCO", "USD-LIBOR-BBA", "USD-LIBOR-ISDA", "USd-LIBOR-LIBO", "USD-LIBOR-Reference Banks", "USD-Prime-H.15", "USD-Prime-Reference Banks", "USD-SIBOR-SIBO", "USD-SIBOR-Reference Banks", "USD-TBILL-H.15", "USD-TBILL-Secondary Market", "USD-TIBOR-ISDC", "USD-TIBOR-Reference Banks", "USD-Treasury Rate-T500", "USD-Treasury Rate-T19901"}))
            .Add(PDCurrency.ZAR, New Currency("ZAR", New String() {"ZAR-JIBAR-SAFEX", "ZAR-JIBAR-Reference Banks", "ZAR-PRIME-AVERAGE", "ZAR-PRIME-AVERAGE-Reference Banks", "ZAR-DEPOSIT-SAFEX", "ZAR-DEPOSIT-Reference Banks"}))
        End With
    End Sub

End Module