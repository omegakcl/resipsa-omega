<Serializable, Editor(GetType(EntityDisplayer), GetType(System.Drawing.Design.UITypeEditor))> Public Class OneLineEntity

    'Have this so that I can just show the name where the Entity is a property to be shown in the propertygrid. But it shows the dropdown for the 
    'monthview for some reason.

    Inherits SerializableObject
    Protected pName As String

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Public Overrides Function ToString() As String
        Return pName
    End Function

    Public Sub New()
        MyBase.New()
    End Sub

End Class

<System.ComponentModel.TypeConverter(GetType(EntityConverter)), ClassDisplayName("Entity"), Serializable()> Public Class Entity

    Inherits OneLineEntity
    Implements INameObject
    ' Add the default and de-serialization constructors

    'I can't combine this with EntityControl because I don't want to expose the properties of TableLayoutPanel. Also Serialization issues.
    <NonSerialized()> Friend Parent As EntityControl

    Friend bNameModified As Boolean    'whether the user has changed the name from the default. I need to store this for serialization.

    Friend ControlPoint As Point    'necessary for serialization.

    Event JurnChanged()

    Public IncorpSuffix As String

    <MergableProperty(False), Category("Entity Properties")> Public Property Name() As String Implements INameObject.Name
        Get
            Name = pName
        End Get
        Set(ByVal value As String)
            If InApp Then
                pName = value
                If Me.Parent.rtbName.Text <> pName Then Me.Parent.rtbName.Text = pName
            End If
        End Set
    End Property

    Private pIncorp As IJurisdiction
    <DisplayName("Jurisdiction"), TypeConverter(GetType(Instrument.JurConverter)), Category("Entity Properties")> Public Property Incorp() As IJurisdiction

        'Can I adjust the JurConverter so that names of jurisdictions are returned here rather than adjectives?
        Get
            Return pIncorp
        End Get
        Set(ByVal value As IJurisdiction)
            If InApp Then
                pIncorp = value
                MyRegs = pIncorp.GetRegsObject
                RaiseEvent JurnChanged()
            End If
        End Set
    End Property

    Private pRegs As Regs
    <DisplayName("Regulatory Type(s)"), Category("EntityProperties")> Public Property MyRegs() As Regs
        Get
            Return pRegs
        End Get
        Set(ByVal value As Regs)
            pRegs = value
        End Set
    End Property

    Private pBank As Boolean
    <DisplayName("Deposit-taking"), Category("Entity Properties")> Public Property Bank() As Boolean
        Get
            Return pBank
        End Get
        Set(ByVal value As Boolean)
            pBank = value
        End Set
    End Property

    Private pFiduciary As Boolean
    <Category("Entity Properties"), FiduciaryAttribute()> Public Property Fiduciary() As Boolean
        'Relevant in jurisdictions where only certain institutions can act as fiduciaries, i.e. where a trust isn't recognised.
        'I suppose the concept is whether it can own assets on behalf of someone else, so that they won't be part of its estate on a 
        'winding up. Need to have something in each dll to indicate whether this is relevant, and if not, to hide it.
        Get
            Return pFiduciary
        End Get
        Set(ByVal value As Boolean)
            pFiduciary = value
        End Set
    End Property

    Friend Function GetClone() As Entity
        Return MemberwiseClone()
    End Function

    Public Function HasCapReq() As Boolean
        HasCapReq = (Bank)
    End Function

    Friend Class RegCheckBoxEditor
        Inherits CheckedListBoxEditor
        Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object
            MyBase.ListItems = New List(Of String)
            For Each s As String In JurisdictionSpace.Jurisdictions.Values.ToString

                MyBase.ListItems.Add(s)
            Next
            Return MyBase.EditValue(context, provider, value)
        End Function
    End Class

    Friend Function CanTakeDeposits() As Boolean
        Return Me.Incorp.CanTakeDeposits(Me)
    End Function

    Public Sub New()
        MyBase.New()
    End Sub
End Class

Public Class FiduciaryAttribute
    Inherits System.Attribute

End Class

Public Class EntityConverter

    Inherits CustomExpandableObjectConverter
    Friend Overrides Function RemoveProperty(ByVal converterobject As Object, ByVal pd As System.ComponentModel.PropertyDescriptor) As Boolean

        Dim E As Entity = TryCast(converterobject, Entity)
        If pd.Attributes.Item(GetType(FiduciaryAttribute)) IsNot Nothing Then
            If Not E Is Nothing Then
                If E.Incorp Is Nothing Then
                    Return True
                Else
                    Return E.Incorp.TrustRecognition <> IJurisdiction.TrustRecognitionStatus.TrustsRecognisedByFiduciaryOnly
                End If
            End If
        End If
        Return MyBase.RemoveProperty(converterobject, pd)
    End Function

End Class

<System.ComponentModel.TypeConverter(GetType(CustomExpandableObjectConverter)), Serializable()> Public MustInherit Class Regs

    'A class to be inherited and completed for regulatory types by dlls.

End Class

Friend Class EntityDisplayer

    'For selecting Entities through the grid.
    Inherits System.Drawing.Design.UITypeEditor

    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return System.Drawing.Design.UITypeEditorEditStyle.Modal  'Show the ellipsis. I'd rather show an arrow or something if I could, but I can't figure out how to do it.
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        Dim ent As Entity = TryCast(value, Entity)
        If Not ent Is Nothing Then    'shouldn't be

            '    CurrentBL = value

        End If

    End Function

End Class