
Imports System.Reflection
Imports System.IO

Public Enum PDJ 'Pre-defined jurisdictions.
    Argentina
    Australia
    Austria
    Belgium
    Bermuda
    Brazil
    BVI
    Canada
    CaymanIslands
    Chile
    China
    CzechRepublic
    Denmark
    UKEngWales
    Estonia
    Finland
    France
    Germany
    Gibraltar
    Greece
    Guernsey
    HongKong
    Hungary
    Iceland
    India
    Indonesia
    Ireland
    IsleMan
    Israel
    Italy
    Japan
    Jersey
    Lebanon
    Luxembourg
    Malaysia
    Mexico
    Netherlands
    NewZealand
    Norway
    Phillipines
    Poland
    Portugal
    Russia
    SaudiArabia
    UKScotland
    Slovakia
    Singapore
    SouthAfrica
    SouthKorea
    Spain
    Sweden
    Switzerland
    Taiwan
    Thailand
    Turkey
    USANewYork
End Enum

Public Class JurisdictionSpace  'more like a namespace than a class really. I seem to have not used a Module here so that I don't have to 
    'call RefreshJurisdictions at start of the programme.

    'Could I have this inherit ListOf or CollectionBase so that I can call .Add or .New relating to 
    'this class rather than the Jurisdictions object? I don't think so because you need an instance 
    'to call the methods of the base class (try it). "MyBase is only valid within an instance method."

    Public Shared Jurisdictions As New System.Collections.Generic.Dictionary(Of PDJ, IJurisdiction)  'The order of the Jurisdictions is important, but I can't 
    'very well use a SortedDictionary with a key because I have the combination of implemented jurisdictions and 
    'pre-defined ones.

    Shared Sub New()    'will be called the first time JurisdictionSpace is referred to and not thereafter.
        RefreshJurisdictions()
    End Sub


    Shared Sub SetItems(ByVal Items As System.Windows.Forms.ComboBox.ObjectCollection)

        'Don't call this from the New event of the jcombo - will get all the objects in a resource file attached to 
        'wherever I use the jcombo and compilation will fail whenever I make any change to IJurisdiction.
        For Each j As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
            If Not TypeOf j Is AppJur Then Items.Add(j) 'ToString override will be shown.
        Next
        For Each j As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
            If TypeOf j Is AppJur Then Items.Add(j) 'ToString override will be shown.
        Next
    End Sub

End Class

Friend Module FillJurisdictions
    Dim WithEvents s As JurisdictionFinder
    Friend Sub RefreshJurisdictions()
        With JurisdictionSpace.Jurisdictions
            'This was horrible in VB6 when I couldn't use constructors.
            'TODO - check Zone A/B Countries
            'Stock Exchanges are from World Federation of Exchanges.
            'Strange in LOMNew I was allowed to just import PDJ and PDCurrency into the module and avoid repeating the identifiers every time.
            .Clear()
            .Add(PDJ.Argentina, New AppJur(PDJ.Argentina, "Argentina", "Argentinian", PDCurrency.ARS, False, False, New String() {"Buenos Aires"}, New String() {"Buenos Aires"}))
            .Add(PDJ.Australia, New AppJur(PDJ.Australia, "Australia", "Australian", PDCurrency.AUD, True, False, New String() {"Sydney"}, New String() {"Australia"}))
            .Add(PDJ.Austria, New AppJur(PDJ.Austria, "Austria", "Austrian", PDCurrency.EUR, True, True, New String() {"Vienna"}, New String() {"Wiener B�rse"}))
            .Add(PDJ.Belgium, New AppJur(PDJ.Belgium, "Belgium", "Belgian", PDCurrency.EUR, True, True, New String() {"Brussels"}, New String() {"Euronext Brussels"}))
            .Add(PDJ.Bermuda, New AppJur(PDJ.Bermuda, "Bermuda", "Bermudan", PDCurrency.USD, True, False, New String() {"Bermuda"}))
            .Add(PDJ.Brazil, New AppJur(PDJ.Brazil, "Brazil", "Brazilian", PDCurrency.BRL, False, False, New String() {"Sao Paulo"}, New String() {"Sao Paulo"}))
            .Add(PDJ.BVI, New AppJur(PDJ.BVI, "BVI", "BVI", PDCurrency.USD, True, False, New String() {"Tortola"}))
            .Add(PDJ.Canada, New AppJur(PDJ.Canada, "Canada", "Canadian", PDCurrency.CAD, True, False, New String() {"Toronto"}, New String() {"Toronto"}))
            .Add(PDJ.CaymanIslands, New AppJur(PDJ.CaymanIslands, "The Cayman Islands", "Cayman Islands", PDCurrency.GBP, False, False, New String() {"Cayman Islands"}, , "Cayman Islands"))
            .Add(PDJ.Chile, New AppJur(PDJ.Chile, "Chile", "Chilean", PDCurrency.CLP, False, False, New String() {"Santiago"}, New String() {"Santiago"}))
            .Add(PDJ.China, New AppJur(PDJ.China, "China", "Chinese", PDCurrency.CHR, False, False, New String() {"Beijing"}, New String() {"Shanghai", "Shenzen"}))
            .Add(PDJ.CzechRepublic, New AppJur(PDJ.CzechRepublic, "The Czech Republic", "Czech", PDCurrency.CZK, True, False, New String() {"Prague"}, , "Czech Republic"))
            .Add(PDJ.Denmark, New AppJur(PDJ.Denmark, "Denmark", "Danish", PDCurrency.DKK, True, True, New String() {"Copenhagen"}, New String() {"OMX Copenhagen"}))
            .Add(PDJ.UKEngWales, New AppJur(PDJ.UKEngWales, "England/Wales", "English", PDCurrency.GBP, True, True, New String() {"London"}, New String() {"London"}))
            .Add(PDJ.Estonia, New AppJur(PDJ.Estonia, "Estonia", "Estonian", PDCurrency.EEK, False, True, New String() {"Tallinn"}, New String() {"OMX Tallinn"}))
            .Add(PDJ.Finland, New AppJur(PDJ.Finland, "Finland", "Finnish", PDCurrency.EUR, True, True, New String() {"Helsinki"}))
            .Add(PDJ.France, New AppJur(PDJ.France, "France", "French", PDCurrency.EUR, True, True, New String() {"Paris"}, New String() {"Euronext Paris"}))
            .Add(PDJ.Germany, New AppJur(PDJ.Germany, "Germany", "German", PDCurrency.EUR, True, True, New String() {"Frankfurt"}, New String() {"Deutsche B�rse"}))
            .Add(PDJ.Gibraltar, New AppJur(PDJ.Gibraltar, "Gibraltar", "Gibraltan", PDCurrency.GBP, True, False, New String() {"Gibraltar"}))
            .Add(PDJ.Greece, New AppJur(PDJ.Greece, "Greece", "Greek", PDCurrency.EUR, True, True, New String() {"Athens"}, New String() {"Athens"}))
            .Add(PDJ.Guernsey, New AppJur(PDJ.Guernsey, "Guernsey", "Guernsey", PDCurrency.GBP, True, False, New String() {"Guernsey"}))
            .Add(PDJ.HongKong, New AppJur(PDJ.HongKong, "Hong Kong", "Hong Kong", PDCurrency.HKD, False, False, New String() {"Hong Kong"}, New String() {"Hong Kong"}))
            .Add(PDJ.Hungary, New AppJur(PDJ.Hungary, "Hungary", "Hungarian", PDCurrency.HUF, True, False, New String() {"Budapest"}, New String() {"Budapest"}))
            .Add(PDJ.Iceland, New AppJur(PDJ.Iceland, "Iceland", "Icelandic", PDCurrency.ICE, True, False, New String() {"Reykjavik"}))
            .Add(PDJ.India, New AppJur(PDJ.India, "India", "Indian", PDCurrency.INR, False, False, New String() {"Bombay"}, New String() {"Bombay", "National Stock Exchange of India"}))
            .Add(PDJ.Indonesia, New AppJur(PDJ.Indonesia, "Indonesia", "Indonesian", PDCurrency.IDR, False, False, New String() {"Jakarta"}, New String() {"Jakarta"}))
            .Add(PDJ.Israel, New AppJur(PDJ.Israel, "Israel", "Israeli", PDCurrency.ILS, False, False, New String() {"Tel Aviv"}, New String() {"Tel Aviv"}))
            .Add(PDJ.Ireland, New AppJur(PDJ.Ireland, "The Republic of Ireland", "Irish", PDCurrency.EUR, True, True, New String() {"Dublin"}, New String() {"Irish Stock Exchange"}, "Rep. of Ireland"))
            .Add(PDJ.IsleMan, New AppJur(PDJ.IsleMan, "The Isle of Man", "Manx", PDCurrency.GBP, True, False, New String() {"Isle of Man"}, , "Isle of Man"))
            .Add(PDJ.Italy, New AppJur(PDJ.Italy, "Italy", "Italian", PDCurrency.EUR, True, True, New String() {"Rome"}, New String() {"Borsa Italiana"}))
            .Add(PDJ.Japan, New AppJur(PDJ.Japan, "Japan", "Japanese", PDCurrency.JPY, True, False, New String() {"Tokyo"}, New String() {"Tokyo", "Osaka"}))
            .Add(PDJ.Jersey, New AppJur(PDJ.Jersey, "Jersey", "Jersey", PDCurrency.GBP, True, False, New String() {"Jersey"}))
            .Add(PDJ.Lebanon, New AppJur(PDJ.Lebanon, "Lebanon", "Lebanese", PDCurrency.LBP, False, False, New String() {"Beirut"}))
            .Add(PDJ.Luxembourg, New AppJur(PDJ.Luxembourg, "Luxembourg", "Luxembourg", PDCurrency.EUR, True, True, New String() {"Luxembourg"}, New String() {"Luxembourg"}))
            .Add(PDJ.Malaysia, New AppJur(PDJ.Malaysia, "Malaysia", "Malaysian", PDCurrency.MYR, False, False, New String() {"Kuala Lumpur"}, New String() {"Malaysia"}))
            .Add(PDJ.Mexico, New AppJur(PDJ.Mexico, "Mexico", "Mexican", PDCurrency.MXP, True, False, New String() {"Mexico City"}, New String() {"Bolsa Mexicana"}))
            .Add(PDJ.Netherlands, New AppJur(PDJ.Netherlands, "The Netherlands", "Dutch", PDCurrency.EUR, True, True, New String() {"Amsterdam"}, New String() {"Euronext Amsterdam"}, "Netherlands"))
            .Add(PDJ.NewZealand, New AppJur(PDJ.NewZealand, "New Zealand", "New Zealand", PDCurrency.NZD, True, False, New String() {"Aukland", "Wellington"}, New String() {"New Zealand"}))
            .Add(PDJ.Norway, New AppJur(PDJ.Norway, "Norway", "Norwegian", PDCurrency.NOK, True, False, New String() {"Oslo"}, New String() {"Oslo B�rs "}))
            .Add(PDJ.Phillipines, New AppJur(PDJ.Phillipines, "Phillipines", "Phillipine", PDCurrency.PHP, False, False, New String() {"Manila"}, New String() {"Phillipine Stock Exchange"}))
            .Add(PDJ.Poland, New AppJur(PDJ.Poland, "Poland", "Polish", PDCurrency.PLZ, True, False, New String() {"Warsaw"}, New String() {"Warsaw"}))
            .Add(PDJ.Portugal, New AppJur(PDJ.Portugal, "Portugal", "Portuguese", PDCurrency.EUR, True, True, New String() {"Lisbon"}, New String() {"Euronext Lisbon"}))
            .Add(PDJ.Russia, New AppJur(PDJ.Russia, "Russia", "Russian", PDCurrency.RUR, False, False, New String() {"Moscow"}))
            .Add(PDJ.SaudiArabia, New AppJur(PDJ.SaudiArabia, "Saudi Arabia", "Saudi Arabian", PDCurrency.SAR, True, False, New String() {"Riyadh"}))
            .Add(PDJ.UKScotland, New AppJur(PDJ.UKScotland, "Scotland", "Scottish", PDCurrency.GBP, True, True, New String() {"Edinburgh"}))
            .Add(PDJ.Singapore, New AppJur(PDJ.Singapore, "Singapore", "Singaporean", PDCurrency.SGD, False, False, New String() {"Singapore"}, New String() {"Singapore"}))
            .Add(PDJ.Slovakia, New AppJur(PDJ.Slovakia, "Slovakia", "Slovakian", PDCurrency.SKK, False, False, New String() {"Bratislava"}))
            .Add(PDJ.SouthAfrica, New AppJur(PDJ.SouthAfrica, "South Africa", "South African", PDCurrency.ZAR, False, False, New String() {"Johannesburg"}))
            .Add(PDJ.SouthKorea, New AppJur(PDJ.SouthKorea, "South Korea", "South Korean", PDCurrency.KWR, True, False, New String() {"Seoul"}, New String() {"Korea Exchange"}))
            .Add(PDJ.Spain, New AppJur(PDJ.Spain, "Spain", "Spanish", PDCurrency.EUR, True, True, New String() {"Madrid"}, New String() {"BME Spanish"}))
            .Add(PDJ.Sweden, New AppJur(PDJ.Sweden, "Sweden", "Swedish", PDCurrency.SEK, True, True, New String() {"Stockholm"}))
            .Add(PDJ.Switzerland, New AppJur(PDJ.Switzerland, "Switzerland", "Swiss", PDCurrency.CHF, True, False, New String() {"Zurich"}, New String() {"Swiss Exchange"}))
            .Add(PDJ.Taiwan, New AppJur(PDJ.Taiwan, "Taiwan", "Taiwanese", PDCurrency.TWD, False, False, New String() {"Taipei"}, New String() {"Taiwan"}))
            .Add(PDJ.Thailand, New AppJur(PDJ.Thailand, "Thailand", "Thai", PDCurrency.THB, False, False, New String() {"Bangkok"}, New String() {"Stock Exchange of Thailand"}))
            .Add(PDJ.Turkey, New AppJur(PDJ.Turkey, "Turkey", "Turkish", PDCurrency.TRL, True, False, New String() {"Istanbul"}, New String() {"Istanbul"}))
            .Add(PDJ.USANewYork, New AppJur(PDJ.USANewYork, "New York", "New York", PDCurrency.USD, True, False, New String() {"New York"}, New String() {"NYSE", "American Stock Exchange", "NASD"}, "US - New York"))

            s = New JurisdictionFinder
            s.GetJurs()
            s = Nothing

        End With
    End Sub

    Private Sub s_GotJur(ByVal JJ As IJurisdiction, ByVal objDLL As System.Reflection.Assembly, ByVal sPath As String) Handles s.GotJur

        If Not JJ Is Nothing Then
            'Remove any AppJur with a matching predefined jurisdiction.
            With JurisdictionSpace.Jurisdictions
                If .ContainsKey(JJ.PredefinedJur) Then .Remove(JJ.PredefinedJur)
                .Add(JJ.PredefinedJur, JJ)  'does this insert JJ or replace what was there?

                'Need to think about adding jurns with a predefined jur.

                Dim B As IJurisdiction
                For Each B In .Values
                    If TypeOf B Is AppJur And B.PredefinedJur = JJ.PredefinedJur Then .Remove(B.PredefinedJur)
                Next
            End With
        End If

    End Sub
End Module

Friend Class JurisdictionFinder

    'Goes through all the dlls in the application folder and returns instances of the jurisdiction they contain and 
    'the assembly.

    Event GotJur(ByVal JJ As IJurisdiction, ByVal objDLL As Assembly, ByVal sPath As String)

    Friend Sub GetJurs()

        Dim strDLLs() As String, intIndex As Integer
        Dim objDLL As Assembly

        'Go through all DLLs in the application's directory. This is how I want things set up on the user's computer. 
        'To mirror this on my computer I have a batch file which copies the dll into the directory after compilation. Or did have...
        strDLLs = Directory.GetFileSystemEntries(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "*.dll")
        Dim JJ As IJurisdiction
        For intIndex = 0 To strDLLs.Length - 1
            Try
                objDLL = Assembly.LoadFrom(strDLLs(intIndex))

                JJ = Nothing

                'Loop through each type in the DLL
                For Each objType As Type In objDLL.GetTypes
                    'Only look at public types
                    If objType.IsPublic Then
                        'Ignore abstract classes - "MustInherit" classes since they can't be instantiated.
                        If Not ((objType.Attributes And TypeAttributes.Abstract) = TypeAttributes.Abstract) Then

                            'See if this type implements our interface

                            If Not objType.GetInterface(GetType(IJurisdiction).Name, True) Is Nothing Then _
                            RaiseEvent GotJur(objDLL.CreateInstance(objType.FullName, True), objDLL, strDLLs(intIndex))

                        End If
                    End If

                Next
            Catch e As Exception
                'Error loading DLL, we don't need to do anything special
            End Try
        Next
    End Sub

End Class
