Public Interface IPropGridListNameGiver
    ReadOnly Property PropGridName() As String
End Interface

Friend Interface INameObject
    'Things to be added to a collection and given a unique name in it.
    Property Name() As String
End Interface

Public Interface ITopicsHandler

    'Implemented by frmMDI so I have a callback procedure. Makes it easy for a plug-in to add topics. 
    'Alternative would be (a) for all subs to be functions which return an array/collection of strings or 
    '(b) to send a collection ByRef to the functions for filling. But I find this simpler for the dll writer to use.

    Sub AddTopic(ByRef TopicID As String, Optional ByRef Title As String = "", Optional ByRef Priority As Boolean = False)

    'TopicID is the address of the document to be referenced. It would be good to be able to extract the heading for the topic, but 
    'I'm leaving the option open by allowing a title to be specified.

End Interface

Public Interface IFundingLaw
    'Need to add financial assistance.

    Sub BorrowerAccounting(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub BorrowerInsolvency(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub BorrowerStampDuty(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub WHT(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub InvestorSalesRestrictions(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub ShareHoldingDisclosure(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub FundingCapitalCriteria(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub InvestorCapitalRequirement(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub AccountsParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub AccountsSubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub RegulatoryParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub RegulatorySubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub AdditionalBorrowerRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub AdditionalInvestorRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler)
    Sub ShareHoldingRestrictions(ByVal objX As Shares, ByVal THandler As ITopicsHandler)
    Sub BankDepositProtection(ByVal objX As Deposit, ByVal THandler As ITopicsHandler)

End Interface

Public Interface ICash

    Sub DepositTaking(ByVal objX As CashPayment, ByVal THandler As ITopicsHandler)

End Interface

Public Interface ICollateralLaw

    Sub CollateralInsolvency(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler)
    Sub CollateralAssetsStamp(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler)
    Sub CollateralShareDisc(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler)
    Sub CollateralAdditional(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler)

End Interface

Public Interface IPassThroughLaw

    'Need to add in stamp duty!!
    'Would be better if the dll writer could access the underlyings from objX. But objX refers to its underlying BaseLines so that I 
    'can expose them in the Property Grid and not get a whole list of properties. So they have to be Public.
    Sub SellerInsolvency(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub SellerAccountingOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub SellerRegulatoryOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub Recharacterisation(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub InvestorAccounting(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub ShareHoldingDisclosure(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub OwnSharePurchase(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)
    Sub StampDuty(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler)

End Interface

Public Interface ISwapLaw

    Sub SwapInsolvency(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler)
    Sub SwapAccounting(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler)
    Sub SwapWHT(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler)
    Sub SwapCapReq(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler)
    Sub SwapStampDuty(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler)

End Interface

Public Interface ICreditSupportLaw

    Sub CredSupporterInsolvency(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler)
    Sub CredSupporterAccounting(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler)
    Sub CredSupporterWHT(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler)
    Sub CredSupporterCapReq(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler)
    Sub CredSupporterStampDuty(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler)

End Interface

Public Interface IJurisdiction

    'Combining all interfaces through inheritance is beneficial in that it forces them all to be implemented by the same class
    'so I'm not just assuming that the first IFunding I find in a dll relates to the first IJurisdiction I find.
    'A dll can now provide two plug-ins it the writer wants it to.
    'Continuing to split them up in sub-interfaces is helpful so that the detail doesn't look overwhelming.

    'Having sub interfaces within an interface doesn't really help - if a class implements the parent interface, 
    'there's no compulsion on it to implement the child interface. The only significance of such an arrangement 
    'seems to be to establish a hierarchy of interfaces.

    'Re Serializiation, an Interface can't be marked Serializable. So the implementing classes will have to be.
    'Which is another good reason for requiring everything to be done in one place - the dll writer only has to 
    'remember to add the Serializable attribute in one place.
    Inherits IPropGridListNameGiver
    Inherits ICash
    Inherits IFundingLaw
    Inherits IPassThroughLaw
    Inherits ICollateralLaw
    Inherits ICreditSupportLaw
    Inherits ISwapLaw

    ReadOnly Property RootFolder() As String

    ReadOnly Property IndexPage() As String

    ReadOnly Property ContentsPage() As String

    ReadOnly Property Name() As String

    ReadOnly Property AdjectivalName() As String

    ReadOnly Property Currency() As Currency

    ReadOnly Property EU() As Boolean

    ReadOnly Property ZoneA() As Boolean

    ReadOnly Property PredefinedJur() As PDJ

    ReadOnly Property EntityTypes() As List(Of String)

    ReadOnly Property RegulatoryTypes() As List(Of String)

    ReadOnly Property FinancialCentres() As List(Of String)

    ReadOnly Property StockExchanges() As List(Of String)

    ReadOnly Property TrustRecognition() As TrustRecognitionStatus

    Function GetRegsObject() As Regs

    Function CanTakeDeposits(ByVal E As Entity) As Boolean      'this is duplicative?
    Function CanHoldPropertyInTrust(ByVal E As Entity) As Boolean

    Sub HandlePropID(ByVal v As PropIDAttribute, ByVal THandler As ITopicsHandler)

    Enum TrustRecognitionStatus
        TrustsRecognised
        TrustsRecognisedByFiduciaryOnly
        TrustsNotRecognised
    End Enum

    'Also a dropdown name is given for the combo by overriding ToString. But
    'I don't think I can oblige this to be implemented.

End Interface