Friend Class JCombo
    Inherits ComboBox

    Public Sub New()

        MyBase.DrawMode = Windows.Forms.DrawMode.OwnerDrawFixed
        MyBase.Sorted = False

    End Sub

    Private Sub JCombo_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles Me.DrawItem

        With e

            .DrawBackground()

            If (.State And DrawItemState.Selected) = DrawItemState.Selected Then
                Dim selRect As New Rectangle(.Bounds.X, .Bounds.Y, .Bounds.Width - 1, .Bounds.Height - 1)
                Dim br As New SolidBrush(Color.FromArgb(206, 223, 255))
                .Graphics.FillRectangle(br, selRect)
                .Graphics.DrawRectangle(New Pen(Color.FromArgb(30, 108, 253), 1), selRect)
                br.Dispose()
            Else
                If Not TypeOf MyBase.Items(.Index) Is AppJur Then
                    If .Index < MyBase.Items.Count - 1 Then
                        If TypeOf MyBase.Items(.Index + 1) Is AppJur Then
                            .Graphics.DrawLine(Pens.Black, .Bounds.X + 1, .Bounds.Height - 1, .Bounds.Width - 2, .Bounds.Height - 1)
                        End If
                    End If
                End If
            End If

            Dim p As SolidBrush
            p = IIf(TypeOf MyBase.Items(.Index) Is AppJur, Brushes.Gray, Brushes.Black)
            .Graphics.DrawString(MyBase.Items(.Index).ToString, MyBase.Font, p, .Bounds)

        End With

    End Sub

    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'JCombo
        '
        Me.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.TabStop = False
        Me.ResumeLayout(False)

    End Sub

    Private Sub JCombo_DropDownClosed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DropDownClosed

    End Sub

    Private Sub JCombo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        ClearSelection()
    End Sub

    Friend Sub ClearSelection()
        'Don't like the fact that the whole thing gets selected by default. Putting caret at the end is better.
        MyBase.Select(MyBase.Text.Length, 0)

    End Sub

    Private Sub JCombo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        If Me.FlatStyle = Windows.Forms.FlatStyle.Flat Then Refresh() 'seem to get a bit left over in 3D otherwise. Don't know why - doesn't normally happen for a combo box.

    End Sub
End Class
