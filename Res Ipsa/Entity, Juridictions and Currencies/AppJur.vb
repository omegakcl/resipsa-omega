<Serializable()> Public Class AppJur

    'Application defined jurisdictions.

    Implements IJurisdiction


    Dim pAdjName As String
    Friend ReadOnly Property AdjectivalName() As String Implements IJurisdiction.AdjectivalName, IPropGridListNameGiver.PropGridName
        Get
            Return pAdjName
        End Get

    End Property

    Dim pContentsPage As String
    Friend ReadOnly Property ContentsPage() As String Implements IJurisdiction.ContentsPage
        Get
            Return pContentsPage
        End Get

    End Property

    Dim pCCY As Currency
    Friend ReadOnly Property CurrencyAcronym() As Currency Implements IJurisdiction.Currency
        Get
            Return pCCY
        End Get

    End Property

    Dim pDDN As String
    Public Overrides Function ToString() As String      'for showing in the drop down.
        Return pDDN
    End Function

    Friend ReadOnly Property EntityTypes() As List(Of String) Implements IJurisdiction.EntityTypes
        Get
            Return Nothing
        End Get

    End Property

    Dim pEU As Boolean
    Friend ReadOnly Property EU() As Boolean Implements IJurisdiction.EU
        Get
            Return pEU
        End Get

    End Property

    Dim pFinCent As List(Of String)
    Friend ReadOnly Property FinancialCentres() As List(Of String) Implements IJurisdiction.FinancialCentres
        Get
            Return pFinCent
        End Get

    End Property

    Friend ReadOnly Property IndexPage() As String Implements IJurisdiction.IndexPage
        Get
            Return ""
        End Get

    End Property

    Dim pName As String
    Friend ReadOnly Property Name() As String Implements IJurisdiction.Name
        Get
            Return pName
        End Get

    End Property

    Dim pPDJ As PDJ
    Friend ReadOnly Property PredefinedJur() As PDJ Implements IJurisdiction.PredefinedJur
        Get
            Return pPDJ
        End Get

    End Property

    Friend ReadOnly Property RegulatoryTypes() As List(Of String) Implements IJurisdiction.RegulatoryTypes
        Get
            Return Nothing
        End Get

    End Property

    Friend ReadOnly Property RootFolder() As String Implements IJurisdiction.RootFolder
        Get
            Return ""
        End Get

    End Property

    Dim pZoneA As Boolean
    Friend ReadOnly Property ZoneA() As Boolean Implements IJurisdiction.ZoneA
        Get
            Return pZoneA
        End Get

    End Property

    Public Sub New(ByVal PDJ As PDJ, ByVal sName As String, ByVal AdjName As String, ByVal Ccy As PDCurrency, _
    ByVal bZoneA As Boolean, ByVal bEU As Boolean, ByVal sFinancialCentres As String(), Optional ByVal sStockExchanges() As String = Nothing, Optional ByVal sDropDownName As String = "")

        pPDJ = PDJ
        pName = sName
        pAdjName = AdjName
        pCCY = CurrencySpace.Currencies.Item(Ccy)
        pZoneA = bZoneA
        pEU = bEU
        If Not sFinancialCentres Is Nothing Then pFinCent = New List(Of String)(sFinancialCentres)
        If Not sStockExchanges Is Nothing Then pStockExchanges = New List(Of String)(sStockExchanges)
        pDDN = IIf(sDropDownName = "", sName, sDropDownName)    'Optional keyword doesn't seem to work.

    End Sub

    Private pStockExchanges As List(Of String)
    Friend ReadOnly Property StockExchanges() As System.Collections.Generic.List(Of String) Implements IJurisdiction.StockExchanges
        Get
            Return pStockExchanges
        End Get
    End Property
    'Rest is superfluous.

    Public Sub DepositTaking(ByVal objX As CashPayment, ByVal THandler As ITopicsHandler) Implements ICash.DepositTaking

    End Sub

    Public Sub CollateralAdditional(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralAdditional

    End Sub

    Public Sub CollateralAssetsStamp(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralAssetsStamp

    End Sub

    Public Sub CollateralInsolvency(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralInsolvency

    End Sub

    Public Sub CollateralShareDisc(ByVal SelSec As SecurityBase, ByVal THandler As ITopicsHandler) Implements ICollateralLaw.CollateralShareDisc

    End Sub

    Public Sub CredSupporterAccounting(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterAccounting

    End Sub

    Public Sub CredSupporterCapReq(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterCapReq

    End Sub

    Public Sub CredSupporterInsolvency(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterInsolvency

    End Sub

    Public Sub CredSupporterStampDuty(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterStampDuty

    End Sub

    Public Sub CredSupporterWHT(ByVal objX As CreditSupport, ByVal THandler As ITopicsHandler) Implements ICreditSupportLaw.CredSupporterWHT

    End Sub

    Public Sub AccountsParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AccountsParentConsolidation

    End Sub

    Public Sub AccountsSubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AccountsSubsidiaryConsolidation

    End Sub

    Public Sub AdditionalBorrowerRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AdditionalBorrowerRelated

    End Sub

    Public Sub AdditionalInvestorRelated(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.AdditionalInvestorRelated

    End Sub

    Public Sub BorrowerAccounting(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerAccounting

    End Sub

    Public Sub BorrowerInsolvency(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerInsolvency

    End Sub

    Public Sub BorrowerStampDuty(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BorrowerStampDuty

    End Sub

    Public Sub FundingCapitalCriteria(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.FundingCapitalCriteria

    End Sub

    Public Sub InvestorCapitalRequirement(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.InvestorCapitalRequirement

    End Sub

    Public Sub InvestorSalesRestrictions(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.InvestorSalesRestrictions

    End Sub

    Public Sub RegulatoryParentConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.RegulatoryParentConsolidation

    End Sub

    Public Sub RegulatorySubsidiaryConsolidation(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.RegulatorySubsidiaryConsolidation

    End Sub

    Public Sub ShareHoldingDisclosure(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.ShareHoldingDisclosure

    End Sub

    Public Sub WHT(ByVal objX As Funding, ByVal THandler As ITopicsHandler) Implements IFundingLaw.WHT

    End Sub

    Public Sub InvestorAccounting(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.InvestorAccounting

    End Sub

    Public Sub OwnSharePurchase(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.OwnSharePurchase

    End Sub

    Public Sub Recharacterisation(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.Recharacterisation

    End Sub

    Public Sub SellerAccountingOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerAccountingOffset

    End Sub

    Public Sub SellerInsolvency(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerInsolvency

    End Sub

    Public Sub SellerRegulatoryOffset(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.SellerRegulatoryOffset

    End Sub

    Public Sub ShareHoldingDisclosure1(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.ShareHoldingDisclosure

    End Sub

    Public Sub StampDuty(ByVal objX As PassThrough, ByVal BaseFunding As List(Of EToEInstrument), ByVal THandler As ITopicsHandler) Implements IPassThroughLaw.StampDuty

    End Sub

    Public Sub SwapAccounting(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapAccounting

    End Sub

    Public Sub SwapCapReq(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapCapReq

    End Sub

    Public Sub SwapInsolvency(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapInsolvency

    End Sub

    Public Sub SwapStampDuty(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapStampDuty

    End Sub

    Public Sub SwapWHT(ByVal objX As SwapLeg, ByVal THandler As ITopicsHandler) Implements ISwapLaw.SwapWHT

    End Sub

    Public ReadOnly Property TrustRecognition() As IJurisdiction.TrustRecognitionStatus Implements IJurisdiction.TrustRecognition
        Get
            'I suppose it's a reasonable assumption that trusts are only OK for fiduciaries in countries that I don't know about. 
            Return IJurisdiction.TrustRecognitionStatus.TrustsRecognisedByFiduciaryOnly
        End Get
    End Property

    Public Function GetRegsObject() As Regs Implements IJurisdiction.GetRegsObject
        Return New AppRegs
    End Function

    Public Function CanHoldPropertyInTrust(ByVal E As Entity) As Boolean Implements IJurisdiction.CanHoldPropertyInTrust
        Return False
    End Function

    Public Function CanTakeDeposits(ByVal E As Entity) As Boolean Implements IJurisdiction.CanTakeDeposits

    End Function

    <Serializable()> Public Class AppRegs
        Inherits Regs

        Private bDepositTaking As Boolean
        Public Property DepositTaking() As Boolean
            Get
                Return bDepositTaking
            End Get
            Set(ByVal value As Boolean)
                bDepositTaking = value
            End Set
        End Property

        Private bCanBeTrustee As Boolean
        Public Property CanBeTrustee() As Boolean   'could either be because trusts are allowed in the jurisdiction or cos entity is a fiduciary.
            Get
                Return bCanBeTrustee
            End Get
            Set(ByVal value As Boolean)
                bCanBeTrustee = value
            End Set
        End Property

    End Class

    Public Sub HandlePropID(ByVal v As PropIDAttribute, ByVal THandler As ITopicsHandler) Implements IJurisdiction.HandlePropID

    End Sub

    Public Sub ShareHoldingRestrictions(ByVal objX As Shares, ByVal THandler As ITopicsHandler) Implements IFundingLaw.ShareHoldingRestrictions

    End Sub

    Public Sub BankDepositProtection(ByVal objX As Deposit, ByVal THandler As ITopicsHandler) Implements IFundingLaw.BankDepositProtection

    End Sub
End Class
