Module BaseLineModule

    Friend BLDrawing As BaseLine 'A temporary object, the BaseLine being drawn by the user.

    Friend MouseOverBL As BaseLine  'A store of the baseline which the cursor is over at any given time. Have to have a store because I need to know what it is when
    'the context menus are opening so I can fill them, but I can only work out what it is in mousemove and mousedown events.

    'Flag for the control the mouse went down on. Otherwise when a drop down in the propgrid 
    'is showing and the user clicks on the canvas instead of an item in that grid, collateral drawing will begin.
    'Similarly if user presses Esc while drawing a baseline - I need this to prevent drawing from starting again.
    Friend MouseDownControl As Control

    Private pDisplayedBL As BaseLine 'I only have one treeview so I can only have one of these.

    Friend Property CurrentBL() As BaseLine

        'The baseline for which law is shown. Identified by slightly larger red boxes.

        Get
            Return pDisplayedBL
        End Get

        Set(ByVal value As BaseLine)
            If Not pDisplayedBL Is value Then

                If Not pDisplayedBL Is Nothing Then pDisplayedBL.HideRedEnds()

                pDisplayedBL = value

                If Not pDisplayedBL Is Nothing Then
                    pDisplayedBL.SetRedEnds()
                    If Not value.Parent.SelectedBLs.Contains(value) Then value.Parent.SelectedBLs.add(value, True)
                End If

                frmMDI.SetDrawButtonEnablement()
                frmMDI.FillTreeView()
            End If
        End Set

    End Property

End Module

