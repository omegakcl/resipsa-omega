<Serializable()> <TypeConverter(GetType(PaymentConverter))> Public Class Cash

    'Represents a monetary amount. This is almost the same as the CashPayment Instrument, but I need it to 
    'describe things like nominal amount, which aren't really something that is 
    'exchanged between two parties.

    Friend UpdateCanvasForChanges As Boolean
    Friend Parent As Instrument

    Private pCurr As Currency

    Event Changed(ByVal OldValue As Cash)

    <System.ComponentModel.NotifyParentProperty(True), DisplayName("Currency"), Currency(), Order(1), _
    TypeConverter(GetType(CurrencyConverter))> Public Property Curr() As Currency

        'NotifyParentProperty causes the parent grid item to be refreshed, i.e. the one that displays the whole currency.

        Get
            Return pCurr
        End Get
        Set(ByVal value As Currency)
            Dim f As Cash = GetCopy()
            pCurr = value
            RaiseEvent Changed(f)
        End Set

    End Property
    Private Class CurrencyConverter
        Inherits ObjectListConverter

        Public Sub New()

            ReDim MyBase.ListItems(CurrencySpace.Currencies.Count - 1)
            Dim n As Long
            For Each v As Currency In CurrencySpace.Currencies.Values   'CopyTo method doesn't work.
                MyBase.ListItems(n) = v
                n += 1
            Next
        End Sub
    End Class

    Private pAmount As Nullable(Of Decimal)
    <System.ComponentModel.NotifyParentProperty(True), Order(2)> Public Property Amount() As Nullable(Of Decimal)

        Get
            Return pAmount
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            Dim f As Cash = GetCopy()
            pAmount = value
            RaiseEvent Changed(f)
        End Set

    End Property

    Friend Function FullAmount() As Decimal

        If Amount.HasValue Then Return Amount.Value * SuffixVal(pSfx)

    End Function

    Shared Function SuffixVal(ByVal esfx As eSuffix) As Long
        Return 10 ^ (esfx * 3)
    End Function

    Private pSfx As eSuffix
    <System.ComponentModel.NotifyParentProperty(True), Order(3), System.ComponentModel.TypeConverter(GetType(SuffixConverter))> Public Property Suffix() As eSuffix

        'Hard to have this as an enumeration, because then I have to convert the string to the right enumeration when
        'the user sets the value in the propertygrid. But this approach could be a problem because sometimes I need to _
        'access info in code. Don't want to have to do if x.RegBearer = "Bearer" for instance.
        Get
            Return pSfx
        End Get
        Set(ByVal value As eSuffix)
            Dim f As Cash = GetCopy()
            pSfx = value
            'value = UCase(value)  'if user types in "m", will change to "M"
            'If value.Length > 1 Then value = value.Substring(0, 1) 'no reason for it to be more than one character.

            'Dim f As New SuffixConverter
            'If f.ListItems.Contains(value) Then
            '    pSfx = value
            'Else
            '    pSfx = ""
            'End If
            RaiseEvent Changed(f)
        End Set
    End Property

    Public Enum eSuffix
        sNone
        sK
        sM
        sB
        sT
    End Enum

    Friend Class SuffixConverter
        Inherits EnumStringConverter

        Public Sub New()
            With MyBase.ListItems
                .Add("")
                .Add("K")
                .Add("M")
                .Add("B")
                .Add("T")
            End With
            MyBase.ValuesExclusive = True
        End Sub

        Friend Overrides Function ConvertIntToEnum(ByVal x As Integer) As Object
            Return CType(x, eSuffix)
        End Function
    End Class

    Public Overrides Function ToString() As String

        ToString = ""
        If Not Curr Is Nothing Then ToString = Curr.Acronym
        If Amount.HasValue Then If Amount.Value <> 0 Then ToString = ToString & Amount.Value
        Dim f As New SuffixConverter
        ToString = ToString & f.ListItems(pSfx)

    End Function

    Shared Operator =(ByVal a As Cash, ByVal b As Cash) As Boolean
        With a
            If .Amount.HasValue And b.Amount.HasValue Then
                Return AreTheSame(.Curr, b.Curr) And (.Amount.Value = b.Amount.Value) And (.Suffix = b.Suffix)
            ElseIf Not .Amount.HasValue And Not b.Amount.HasValue Then
                Return (.Curr Is b.Curr) And (.Suffix = b.Suffix)
            Else
                Return False
            End If
        End With
    End Operator

    Shared Operator <>(ByVal a As Cash, ByVal b As Cash) As Boolean
        Return Not a = b
    End Operator

    Shared Operator +(ByVal a As Cash, ByVal b As Cash) As Cash
        If Not AreTheSame(a.Curr, b.Curr) Then Return a
        Dim p As New Cash
        p.Curr = a.Curr
        p.Suffix = Math.Max(a.Suffix, b.Suffix)
        p.Amount = (a.FullAmount + b.FullAmount) / SuffixVal(p.Suffix)
        Return p
    End Operator

    Shared Operator -(ByVal a As Cash, ByVal b As Cash) As Cash
        If Not AreTheSame(a.Curr, b.Curr) Then Return a
        Dim p As New Cash
        p.Curr = a.Curr
        p.Suffix = Math.Max(a.Suffix, b.Suffix)
        p.Amount = (a.FullAmount - b.FullAmount)
        p.Suffix = Math.Truncate(Math.Log10(p.Amount.Value) / 3)
        p.Amount = p.Amount.Value / SuffixVal(p.Suffix)
        Return p
    End Operator

    Friend Function GetCopy() As Cash
        Return MemberwiseClone()   'this procedure copies all properties. Properties which refer to objects 
        'will refer to the same object instance in the clone.
    End Function

    Private Sub RefreshCanvas()
        If Not Parent Is Nothing And UpdateCanvasForChanges Then
            Parent.Parent.Parent.Refresh()
        End If
    End Sub

End Class


