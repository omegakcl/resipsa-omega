Friend Class PartyABConverter

    'A class to handle the content of all drop down lists.
    'In sample code this inherited from StringConverter (which inherits from TypeConverter).
    'But it works just the same with TypeConverter.
    'StringConverter just has a CanConvertFrom and a ConvertFrom overload.

    'Doesn't fill the ArrayList itself, so must be inherited.

    'If standard values are exclusive then I want to use an enumeration for the property since _
    'will make it easy to use that property in code. But if they're not, e.g. with currency, 
    'then I can't use an enumeration. So I need to be able to handle both possibilities.

    'Note that a property which holds an enumerated value will be shown with a dropdown listing _
    'all the enumerations using their declared names. But that's not much help where I want the / or space symbols to appear.

    Inherits System.ComponentModel.TypeConverter

    Shared Function GetItemText(ByVal s As SwapLeg, ByVal value As Boolean) As String

        If value Then
            Return (s.ToEntity.Name)
        Else
            Return (s.FromEntity.Name)
        End If

    End Function

    Public Overloads Overrides Function GetStandardValues(ByVal context As System.ComponentModel.ITypeDescriptorContext) As StandardValuesCollection

        Dim a As New ArrayList
        a.Add(GetItemText(context.Instance, True))
        a.Add(GetItemText(context.Instance, False))

        Return New StandardValuesCollection(a)

    End Function

    Public Overloads Overrides Function GetStandardValuesExclusive(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True 'no other entries allowed.
    End Function
    Public Overloads Overrides Function GetStandardValuesSupported(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True 'No dropdown shown unless you do this.
    End Function

    Public Overloads Overrides Function CanConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal destinationType As Type) As Boolean
        If (destinationType Is GetType(Integer) Or destinationType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, destinationType)
    End Function

    Public Overloads Overrides Function ConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, _
    ByVal destinationType As System.Type) As Object

        If destinationType Is GetType(String) Then
            If VarType(value) = VariantType.Boolean Then
                Return GetItemText(context.Instance, value)
            ElseIf VarType(value) = VariantType.String Then
                Return value
            End If
        End If

        Return MyBase.ConvertTo(context, culture, value, destinationType)

    End Function

    Public Overloads Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal sourceType As System.Type) As Boolean

        If (sourceType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, sourceType)

    End Function

    Public Overloads Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object

        If VarType(value) = VariantType.String Then
            Return ConvertFrom(context.Instance, value)
        End If

        Return MyBase.ConvertFrom(context, culture, value)

    End Function

    Public Overloads Function ConvertFrom(ByVal Instance As SwapLeg, ByVal value As Object) As Object

        If GetItemText(Instance, False) = value Then Return False
        If GetItemText(Instance, True) = value Then Return True

    End Function

End Class

