
Friend Class IntegerConverter
    'If user is required to insert an integer, I don't want him to get the cryptic "Cannot convert to Int32" message.

    Inherits TypeConverter
    Public Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal sourceType As System.Type) As Boolean

        Return True

    End Function

    Public Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object
        If IsNumeric(value) Then
            If CType(value, Integer) >= 0 Then
                Return CType(value, Integer)
            Else
                value = Nothing
            End If
        Else
            value = Nothing
        End If
    End Function
End Class

