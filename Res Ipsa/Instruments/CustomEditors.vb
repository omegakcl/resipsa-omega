'Editors are the things that drop down from property grid cells to allow a selection to be made.
Imports System.Drawing.Design
Imports System.Windows.Forms.Design

Friend Class CheckedListBoxEditor
    'Provides a checkbox drop down. StringConverter won't allow multiselect, so need this. Not very hard.
    'Sample code had some override of the PropertyDescriptor's GetEditor function, but doesn't seem to be necessary.

    'To add a picture in, override the GetPaintValueSupported and PaintValue functions.

    Inherits UITypeEditor

    Friend ListItems As List(Of String)

    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        'Ensure that the dropdown is shown.
        Return UITypeEditorEditStyle.DropDown
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        'n.b. currently grid item is editable by user directly. This may be undesirable.

        Dim ui As New System.Windows.Forms.CheckedListBox   'can use a usercontrol if it gets more complicated.

        Dim s2 As String = ""

        With ui
            .Sorted = True
            For Each s As String In ListItems
                .Items.Add(s)
            Next
            .CheckOnClick = True
            .BorderStyle = Windows.Forms.BorderStyle.None   'One gets provided by the editor, don't want two.

            Dim sv As String = TryCast(value, String)
            If Not sv Is Nothing Then  'can't be surely.


                If sv <> "" Then
                    Dim f() As String = sv.Split(",")
                    For Each s As String In f
                        If .Items.IndexOf(s) <> -1 Then .SetItemChecked(.Items.IndexOf(s), True)
                    Next
                End If
            End If

            GetEditorService(provider).DropDownControl(ui)
            For Each s3 As String In .CheckedItems
                s2 = s2 & s3 & ","
            Next

            If Right$(s2, 1) = "," Then s2 = Left$(s2, Len(s2) - 1)

            .Dispose()
        End With

        Return MyBase.EditValue(context, provider, s2)

    End Function

End Class

Friend Class TextBoxFormEditor
    'Provides a simple form with a textbox to enter text in. Any line returns 
    'appear as a black square in the grid, but tooltip shows text perfectly.

    Inherits UITypeEditor

    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        'Ensure that the ellipsis is shown.
        Return UITypeEditorEditStyle.Modal
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        'Grid item is editable by user directly. Fine here.

        Dim g As New frmAddTerms
        Dim p As Instrument = context.Instance
        g.TextBox1.Text = p.AddTerms
        Dim s2 As String = p.AddTerms
        GetEditorService(provider).ShowDialog(g)    'g.ShowDialog seems to work just as well but since this facility is provided, I guess I should use it.
        If g.DialogResult = Windows.Forms.DialogResult.OK Then
            Return MyBase.EditValue(context, provider, g.TextBox1.Text)
        Else
            Return s2
        End If

    End Function

End Class

Friend Class MonthViewEditor
    Inherits UITypeEditor
    'Having my own month view editor, rather than just what is automatically provided for a date 
    'allows me to set min dates and to set the ShowToday property to False.
    Protected WithEvents g As Windows.Forms.MonthCalendar
    Private fr As IWindowsFormsEditorService
    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle

        Return UITypeEditorEditStyle.DropDown
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        With g
            .ShowToday = False  'today will be circled but not mentioned at foot.
            .MaxSelectionCount = 1  'Prevents the badly implemented facility for the user to be able to select a range of dates.
            .SelectionStart = value
            fr = GetEditorService(provider)

            'Small problem here. If there's more than enough room for the monthview, the dropdown fills 
            'the whole width available, and leaves white space to its right. If I simply declare a property 
            'as Date and give it no customeditor the same happens. But in the Properties window for a monthview 
            'control's maxdate property this doesn't happen. So there must be some way to fix this.
            fr.DropDownControl(g)
            Return .SelectionStart
            .Dispose()

        End With

    End Function

    Private Sub g_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles g.DateSelected
        fr.CloseDropDown()
    End Sub

    Public Sub New()
        g = New Windows.Forms.MonthCalendar
    End Sub
End Class

Friend Class IssueDateEditor
    Inherits MonthViewEditor

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        Dim dbt As Debt = TryCast(context.Instance, Debt)
        If Not dbt Is Nothing Then


            Dim d() As Date = {dbt.MaturityDate}
            g.BoldedDates = d

        End If
        Return MyBase.EditValue(context, provider, value)
    End Function
End Class

Friend Class MatDateEditor
    Inherits MonthViewEditor
    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        Dim dbt As Debt = TryCast(context.Instance, Debt)
        If Not dbt Is Nothing Then

            With g
                .MinDate = dbt.IssueDate
                Dim d() As Date = {.MinDate}
                .BoldedDates = d
            End With
        End If
        Return MyBase.EditValue(context, provider, value)
    End Function
End Class

Friend Class PeriodicCommencementEditor
    Inherits MonthViewEditor
    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        Dim ds As DatesSelection = TryCast(context.Instance, DatesSelection)
        If Not ds Is Nothing Then


            Dim d() As Date = {ds.Parent.IssueDate, ds.Parent.MaturityDate}
            With g
                .BoldedDates = d
                .MinDate = ds.Parent.IssueDate
                .MaxDate = ds.Parent.MaturityDate
            End With

        End If

        Return MyBase.EditValue(context, provider, value)
    End Function
End Class

Friend Class MultiSelectMonthViewEditor
    Inherits UITypeEditor
    'Having my own month view editor, rather than just what is automatically provided for a date 
    'allows me to set min dates and to set the ShowToday property to False.
    Private WithEvents g As Windows.Forms.MonthCalendar
    Private fr As IWindowsFormsEditorService
    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle

        Return UITypeEditorEditStyle.DropDown
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        g = New Windows.Forms.MonthCalendar

        With g
            .ShowToday = False  'today will be circled but not mentioned at foot.
            .MaxSelectionCount = 1  'Prevents the badly implemented facility for the user to be able to select a range of dates.

            fr = GetEditorService(provider)

            Dim ds As DatesSelection = TryCast(context.Instance, DatesSelection)

            If Not ds Is Nothing Then   'should be


                .BoldedDates = value    'should be an array
                .MinDate = ds.Parent.IssueDate
                If ds.Parent.MaturityDate > .MinDate Then .MaxDate = ds.Parent.MaturityDate
            End If

            fr.DropDownControl(g)

            If TypeOf value Is Date() Then  'will be this unless it's Nothing
                Dim dd() As Date = value

                ReDim Preserve dd(UBound(dd) + 1)
                dd(UBound(dd)) = .SelectionStart
                .BoldedDates = dd
            Else
                .BoldedDates = New Date() {.SelectionStart}
            End If

            Return .BoldedDates

        End With

    End Function

    Private Sub g_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles g.DateSelected
        fr.CloseDropDown()
    End Sub
End Class

Module CustomEditors
    Friend Function GetEditorService(ByVal provider As System.IServiceProvider) As IWindowsFormsEditorService

        GetEditorService = provider.GetService(GetType(IWindowsFormsEditorService))

    End Function
End Module