
Friend Class CustomPropertyDescriptor

    'PropertyDescriptor is marked "MustInherit". So I need this, even if doesn't do very much.

    Inherits System.ComponentModel.PropertyDescriptor

    Private MyPD As PropertyDescriptor  'Seems a bit odd to be holding a reference to this,
    'but properties are declared as "MustOverride" so can't use MyBase, even though I've given 
    'it the attributes.
    Private pIsReadOnly As Boolean

    Friend ParentCol As PropertyDescriptorCollection

    Public Sub New(ByVal f As PropertyDescriptor, ByVal aa() As Attribute, ByVal IsReadOnly As Boolean)

        MyBase.New(f.DisplayName, aa) 'Can change display name here if I want.
        MyPD = f
        pIsReadOnly = IsReadOnly

    End Sub

    Public Overrides Function CanResetValue(ByVal component As Object) As Boolean
        Return MyPD.CanResetValue(component)
    End Function

    Public Overrides ReadOnly Property ComponentType() As System.Type
        Get
            Return MyPD.ComponentType
        End Get
    End Property

    Public Overrides Function GetValue(ByVal component As Object) As Object
        Return MyPD.GetValue(component)
    End Function

    Public Overrides ReadOnly Property IsReadOnly() As Boolean
        Get
            If pIsReadOnly Then
                Return True
            Else
                Return MyPD.IsReadOnly
            End If

        End Get
    End Property

    Public Overrides ReadOnly Property PropertyType() As System.Type
        Get
            Return MyPD.PropertyType
        End Get
    End Property

    Public Overrides Sub ResetValue(ByVal component As Object)
        MyPD.ResetValue(component)
    End Sub

    Public Overrides Sub SetValue(ByVal component As Object, ByVal value As Object)

        'Need to make the following conversions for custom converters because the ConvertFrom function 
        'doesn't get called when the user changes the selection by double-clicking. Probably there's a better way 
        'to do this, but I don't know how.

        'n.b. this is only called when the value is changed in the property grid, not in code.

        Try
            'Had a go at using Copier and ProviderAttributes to handle this. But it won't work if I set 
            'a value in code then.
            MyPD.SetValue(component, value) 'this will call the Property Set method.
        Catch
            'Following will be necessary if the user double clicks on the cell to select the next item. 
            'ConvertFrom won't have been called in such circumstances.
            Dim pabc As PartyABConverter = TryCast(MyPD.Converter, PartyABConverter)
            If Not pabc Is Nothing Then
                MyPD.SetValue(component, pabc.ConvertFrom(component, value))
            Else
                MyPD.SetValue(component, MyPD.Converter.ConvertFrom(value))
            End If
        End Try

    End Sub

    Public Overrides Function ShouldSerializeValue(ByVal component As Object) As Boolean

        'Show things in bold if they aren't what's expected, like interest commencement date not being issue date.

        Dim p As CopierAttribute = MyPD.Attributes.Item(GetType(CopierAttribute))
        If Not p Is Nothing Then
            For Each h As CustomPropertyDescriptor In Me.ParentCol
                Dim j As ProviderAttribute = h.MyPD.Attributes.Item(GetType(ProviderAttribute))
                If Not j Is Nothing Then
                    If j.ID = p.ID Then
                        If MyPD.GetValue(component) Is Nothing Then Return True
                        Return MyPD.GetValue(component) <> h.MyPD.GetValue(component)
                    End If

                End If
            Next
        End If

        Return False    'This prevents values from ever appearing in bold. Doesn't seem to have any impact on serialization.
        'Can use this to indicate if an inappropriate value is showing, like wrong DCF for a rate option.
    End Function

End Class
