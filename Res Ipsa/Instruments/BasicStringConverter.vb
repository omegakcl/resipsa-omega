Public Class RateOptionsStringConverter

    'Where the user is to select from an array of objects implementing IProgGridListNameGiver.

    Inherits System.ComponentModel.TypeConverter
   
    Public Overloads Overrides Function GetStandardValues(ByVal context As System.ComponentModel.ITypeDescriptorContext) As StandardValuesCollection

        Dim d As Debt = TryCast(context.Instance, Debt)
        If Not d Is Nothing Then
            Dim c As Currency = d.IntCalcAmount.Curr
            Return New StandardValuesCollection(c.IntOptions)
        Else
            Return Nothing
        End If

    End Function

    Public Overloads Overrides Function GetStandardValuesExclusive(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return False ' other entries allowed.
    End Function
    Public Overloads Overrides Function GetStandardValuesSupported(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True 'No dropdown shown unless you do this.
    End Function

End Class
