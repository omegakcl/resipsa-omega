Friend MustInherit Class EnumStringConverter

    'There is an EnumConverter, but I wonder if it would be any simpler.

    'If standard values are exclusive then I want to use an enumeration for the property since _
    'will make it easy to use that property in code. But if they're not, e.g. with currency, 
    'then I can't use an enumeration. So I need to be able to handle both possibilities.

    'Note that a property which holds an enumerated value will by default be shown with a dropdown listing _
    'all the enumerations using their declared names. But that's not much help where I want the / or space symbols to appear.

    'There is an EnumConverter class you can use. It allows you to simply override the ConvertFrom and ConvertTo functions, 
    'but it needs the type of the enumerator as a constructor argument, so all inheritors of this then need a New Sub. 
    'It has the same problem with Nullable types as I have here. Doesn't seem worth bothering with.

    Inherits System.ComponentModel.TypeConverter
    Public ListItems As ArrayList = New ArrayList() 'If there's an enumeration it must match the items put in here.
    Protected ValuesExclusive As Boolean = True
    'For PropDescr's SetValue to work with nullable types an integer must be converted to the appropriate 
    'type of Enum. This function is how I do it.
    Friend MustOverride Function ConvertIntToEnum(ByVal x As Integer) As Object

    Public Overloads Overrides Function GetStandardValues(ByVal context As System.ComponentModel.ITypeDescriptorContext) As StandardValuesCollection

        Return New StandardValuesCollection(ListItems)

    End Function

    Public Overloads Overrides Function GetStandardValuesExclusive(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return ValuesExclusive 'no other entries allowed.
    End Function
    Public Overloads Overrides Function GetStandardValuesSupported(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True 'No dropdown shown unless you do this.
    End Function

    Public Overloads Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal sourceType As System.Type) As Boolean

        If (sourceType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, sourceType)

    End Function
    Public Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object
        'Converts from value given in grid to related propertytype.  
        Return ConvertIntToEnum(ListItems.IndexOf(value))

    End Function

    Public Overrides Function ConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, ByVal destinationType As System.Type) As Object
        'converts from property value to string to go in grid.
        If destinationType Is GetType(String) Then

            If VarType(value) = VariantType.Integer Then
                Return ListItems.Item(value)
            End If
        End If

        Return MyBase.ConvertTo(context, culture, value, destinationType)
    End Function

    'Public Overrides Function CanConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal destinationType As System.Type) As Boolean
    '    If destinationType Is GetType(Integer) Then
    '        Return MyBase.CanConvertTo(context, destinationType)
    '    End If
    'End Function
End Class

