Friend Class PercentConverter
    'Ensures there's a percent at the end of a decimal when it appears in the property grid.
    Inherits EndStringConverter
    Public Sub New()
        MyBase.New("%")
    End Sub
End Class

Friend Class PercentPAConverter
    'Ensures there's a percent p.a. at the end of a decimal when it appears in the property grid.
    Inherits EndStringConverter
    Public Sub New()
        MyBase.New("% p.a.")
    End Sub
End Class

Friend Class PercentPAPlusMinusConverter
    'Ensures there's a percent p.a. at the end of a decimal and allows an initial minus sign.
    Inherits EndStringConverter

    Public Sub New()
        MyBase.New("% p.a.", True)
    End Sub
End Class

Friend Class EndStringConverter
    Inherits System.ComponentModel.TypeConverter

    Private sEndString As String
    Private bAllowMinus As Boolean

    Public Sub New(ByVal EndString As String, Optional ByVal AllowMinus As Boolean = False)
        sEndString = EndString
        bAllowMinus = AllowMinus
    End Sub

    Public Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal sourceType As System.Type) As Boolean
        If sourceType Is GetType(String) Then
            Return True
        Else
            Return MyBase.CanConvertFrom(context, sourceType)
        End If
    End Function

    Public Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object

        If TypeOf value Is String Then
            Dim s As String = value
            If s = sEndString Or s = "" Then
                Return Nothing
            Else
                'Return any numerical value at the start of the string, forget the rest.
                Dim n As Long = Len(s) - 1

                Do Until IsNumeric(s)
                    s = s.Remove(n, 1)
                    n = n - 1
                    If n = -1 Then Return Nothing
                Loop

                Dim d As Decimal = CType(s, Decimal)
                If Not bAllowMinus Then d = Math.Abs(d)
                Return d

            End If

        Else
            Return MyBase.ConvertFrom(context, culture, value)
        End If

    End Function

    Public Overrides Function ConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, ByVal destinationType As System.Type) As Object
        If destinationType Is GetType(String) Then
            If value Is Nothing Then Return "" 'possible for nullable properties. Don't want the end string then.
            Return MyBase.ConvertTo(context, culture, value, destinationType) & sEndString
        Else
            Return MyBase.ConvertTo(context, culture, value, destinationType)
        End If
    End Function

End Class
