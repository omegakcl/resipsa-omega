Imports Microsoft.Office.Interop

<Serializable()> Friend Class TrancheCollection

    'Collection of EToECon connections, to be drawn together. 

    'I use this for ExistingCons, TransactionCons and SwapCons. The first two can contain both pass-throughs and fundings.

    Inherits List(Of Connection)

    Friend FromEntity As Entity 'need to store Entity rather than EntityControl so I can serialise this.
    Friend ToEntity As Entity
    Friend Parent As List(Of TrancheCollection) 'need to record this so I can remove this collection if it has nothing left.

    Friend LeftTabCon As Boolean   'For a collection of TransactionCons. Whether  this collection is to be represented by the left tab of the property grid. Basic idea is 
    'that this is the one drawn by the user, but he could draw several trancons between two entities not all in same direction. If he does that then I want them all to 
    'appear in the same tab (a) for purposes of selecting multiple objects and (b) as I think it will be visually easier to understand.

    Friend Function Orientation() As Line.Bearing
        'I could just store this property here instead of with each StandardCon. But CreditSuportCons use this property too and they're not in collections.
        'So I think just leave it as it is.
        If MyBase.Count > 0 Then Return Me.Item(0).Orientation
    End Function

    Friend Sub AddAlignMenuItem(ByVal ec As Entity, ByVal m As ContextMenuStrip)
        Dim AlignEntity As Entity = Nothing
        If ec Is Me.FromEntity Then AlignEntity = Me.ToEntity
        If ec Is Me.ToEntity And Not TypeOf MyBase.Item(0) Is TransactionCon Then AlignEntity = Me.FromEntity 'only need to add for one collection when a transaction collection.

        If Not AlignEntity Is Nothing Then m.Items.Add(AlignEntity.Name).Tag = Me 'can't just store AlignEntity cos need to know orientation of Me.
    End Sub

    Private tt As Type
    Friend Function BaseType() As Type
        'The type of StandardCon being stored here. Would think I ought to be able to make this class generic (Of T as StandardCon). 
        'But a variable declared as a generic related to a base class cannot hold a reference to a generic related to a class inheriting from that base class. So 
        'I can't have a collection holding them all.
        Return tt
    End Function

    Friend Sub DrawLines(ByVal gp As Graphics, ByVal ppp As PowerPoint.Slide)
        'go through all connections and draw them...
        For Each c As Connection In Me
            c.Draw(gp, Nothing, ppp)
        Next
    End Sub

    Public Sub New(ByVal F As Entity, ByVal T As Entity, ByVal cl As List(Of TrancheCollection))
        Me.FromEntity = F
        Me.ToEntity = T
        Me.Parent = cl
    End Sub

    'Bit tricky here trying to ensure that TransactionCon pairs always have the same index. If I simply overload the Insert sub then I can't easily call this for the other connection
    'without getting stuck in a loop. So have an extra boolean parameter. I should have no need to call Insert and Remove directly so can make these two procedures "Shadows" so that the 
    'basetype procedures can't be called inadvertently.
    Friend Shadows Sub Insert(ByVal i As Integer, ByVal b As Connection)
        Static r As Integer
        If Not MyBase.Contains(b) Then
            r = r + 1

            ClassInsert(i, b)

            Dim bt As TransactionCon = TryCast(b, TransactionCon)
            If Not bt Is Nothing Then
                Dim b2 As TransactionCon = bt.Consideration
                Dim c As TrancheCollection = b2.ParentCol
                If c Is Nothing Then c = b2.GetStanConCol(b.ToEntity, b.FromEntity)
                c.ClassInsert(i, b2)    'surpsisingly a private method of another object of the same class is accessible.
            End If
        End If
    End Sub

    Private Sub ClassInsert(ByVal i As Integer, ByVal b As Connection)
        If Not MyBase.Contains(b) Then MyBase.Insert(i, b)
        b.ParentCol = Me

        If tt Is Nothing Then tt = b.GetType

    End Sub

    Friend Shadows Sub Remove(ByVal value As Connection)

        ClassRemove(value)

    End Sub

    Private Sub ClassRemove(ByVal value As Connection)
        MyBase.Remove(value)
        value.ParentCol = Nothing

        If MyBase.Count = 0 And Not Parent Is Nothing Then Parent.Remove(Me)

    End Sub

    Friend Function PriorityIndex(ByVal b As Connection) As Integer
        'This is really complicated by the fact that I store connections in an order going outwards from the centre, but I'm presenting them to 
        'the user in the opposite order, i.e. the outermost has the lowest Priority figure, meaning the highest priority.
        Return Count - IndexOf(b)
    End Function

#Region "Sorting"
    Friend Shadows Sub Sort(Optional ByVal DesiredPos As Integer = -1, Optional ByVal b As Connection = Nothing)

        'For TransactionCons I will need to make a decision which one to sort and after sorting, sort the other one to match. Maybe do the latter part by having another IComparer class
        'which determines priority based on priority of Consideration? OR simply pre-empt what Compare is going to do and do the same for the Consideration?
        Dim ColToSort As TrancheCollection = Me
        Dim ConsidCol As TrancheCollection = Nothing
        If tt Is GetType(TransactionCon) Then
            ConsidCol = CType(Me.Item(0), TransactionCon).Consideration.ParentCol
            If DesiredPos <> -1 And ConsidCol.FundingCount > Me.FundingCount Then
                ColToSort = ConsidCol
                ConsidCol = Me
            End If

        End If

        ColToSort.ClassSort(DesiredPos, b)

        'Set the order of the consideration collection to match.
        If tt Is GetType(TransactionCon) Then
            For Each b2 As Connection In ColToSort
                ConsidCol.Add(CType(b2, TransactionCon).Consideration)
                ConsidCol.RemoveAt(0)
            Next
        End If

    End Sub

    Private Sub ClassSort(ByVal DesiredPos As Integer, ByVal b As Connection)
        MyBase.Sort(New ConSorter(DesiredPos, b, Me))
    End Sub

    Private Function FundingCount() As Integer
        'The number of fundings in the collection. Used so I can decide which one to sort.
        For Each b As Connection In Me
            If TypeOf b.Inst Is Funding Then FundingCount += 1
        Next
    End Function

    Private Class ConSorter
        Implements System.Collections.Generic.IComparer(Of Connection)

        Private bToPlace As Connection
        Private TargetPos As Integer = -1
        Private preSortList As New List(Of Connection)

        Friend Sub New(ByVal dPos As Integer, ByVal b As Connection, ByVal Parent As TrancheCollection)
            'I have a target position for one StandardCon if the user has set this through the property grid.
            bToPlace = b
            TargetPos = dPos
            For Each sc As Connection In Parent
                preSortList.Add(sc)
            Next
        End Sub

        Friend Sub New()
            'For looking at previous connections.
        End Sub

        Public Function Compare(ByVal x As Connection, ByVal y As Connection) As Integer Implements System.Collections.Generic.IComparer(Of Connection).Compare

            'Not used atm. Going to be a problem in sorting when order for one set of TransactionCons might differ from other set.

            'Return:
            ' Less than 0:  x will come before y in the list.
            '0: same priority
            'Greater than 0:  x will come after y.

            'So if I want x to appear on the outside I need it to be last in the collection so I need to return +1. If I want y to be on the outside I need to return -1.

            If x Is y Then Return 0 'seems that the comparer calls this even if you have the same objects. Without this line it generates an error.

            If TypeOf x Is SwapCon Then
                'don't sort

            ElseIf x.GetType Is GetType(Funding) And y.GetType Is GetType(Funding) Then
                'don't sort
            ElseIf x.GetType Is GetType(Funding) Then
                Return -1
            ElseIf y.GetType Is GetType(Funding) Then
                Return 1
            ElseIf TypeOf x.Inst Is Funding And TypeOf y.Inst Is Funding Then
                Dim xa As Funding.Rank = CType(x.Inst, Funding).IntrinsicRanking
                Dim ya As Funding.Rank = CType(y.Inst, Funding).IntrinsicRanking
                If xa = ya Then
                    If TargetPos >= 0 Then
                        If bToPlace Is x Then
                            Return bToPlace.ParentCol.PriorityIndex(y) - TargetPos
                        ElseIf bToPlace Is y Then
                            Return TargetPos - bToPlace.ParentCol.PriorityIndex(x)

                        End If

                    End If
                Else
                    Return ya - xa
                End If
            End If

            'If I return 0 in Compare, I'm saying that x and y are the same in priority. However they don't necessarily keep the same order as before. This is a device to ensure that they do.
            Return preSortList.IndexOf(x) - preSortList.IndexOf(y)

        End Function
    End Class
#End Region

End Class
