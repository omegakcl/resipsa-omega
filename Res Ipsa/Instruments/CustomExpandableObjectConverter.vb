Imports System.ComponentModel

Public Class FundingConverter
    Inherits InstrumentConverter
    Protected Overrides Function CategoryString(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As String
        Dim p As FundingCategoryInfoAttribute = pd.Attributes.Item(GetType(FundingCategoryInfoAttribute))

        If Not p Is Nothing Then
            Return p.CategoryText(converterobject, bHasIssueDetails, bHasMatDetails)
        Else
            Return MyBase.CategoryString(converterobject, pd)
        End If

    End Function

    Friend Overrides Function PropIsReadOnly(ByVal pd As System.ComponentModel.PropertyDescriptor, ByVal converterobject As Object, ByVal instance As Object) As Boolean
        'You can't override a property and give it a different scope, so I can't have a 
        'denomination class override Payment. So I think this is the only way to disable 
        'the currency item for denomination.

        'instance can be an array of instruments.
        If TypeOf instance Is Array Then
            For Each i As Instrument In CType(instance, Array)
                If ItemReadOnly(pd, converterobject, i) Then Return True
            Next
        Else
            Return ItemReadOnly(pd, converterobject, instance)
        End If

    End Function

    Private Function ItemReadOnly(ByVal pd As System.ComponentModel.PropertyDescriptor, ByVal converterobject As Object, ByVal inst As Instrument) As Boolean

        Dim f As Instrument.Securities = TryCast(inst, Instrument.Securities)
        If Not f Is Nothing Then
            If f.Denom Is converterobject And _
            pd.Attributes.Item(GetType(CurrencyAttribute)) IsNot Nothing Then Return True
        Else
            Return False
        End If
    End Function

End Class

Public Class PassThroughConverter
    Inherits InstrumentConverter
    Friend Overrides Function RemoveProperty(ByVal converterobject As Object, ByVal pd As System.ComponentModel.PropertyDescriptor) As Boolean

        Dim p As PassThrough = TryCast(converterobject, PassThrough)
        If Not p Is Nothing Then
            If pd.PropertyType Is GetType(UnderlyingBaseLine) And p.Underlyings IsNot Nothing Then Return True
            If pd.PropertyType Is GetType(UnderlyingsCol) And p.UnderlyingInst IsNot Nothing Then Return True
        End If

        Return MyBase.RemoveProperty(converterobject, pd)
    End Function
End Class

Public Class InstrumentConverter
    Inherits CustomExpandableObjectConverter

    'Stuff to ensure the numbering of categories is correct. 
    'Numbers are necessary so the order of categories is what I want.
    Protected bHasIssueDetails As Boolean
    Protected bHasMatDetails As Boolean
    Dim bHasIntDetails As Boolean

    Friend Overrides Sub DetermineCategories(ByVal pdc As System.ComponentModel.PropertyDescriptorCollection)
        For Each pd As PropertyDescriptor In pdc
            With pd.Attributes
                If .Item(GetType(IssueAttribute)) IsNot Nothing Then bHasIssueDetails = True
                If .Item(GetType(MaturityAttribute)) IsNot Nothing Then bHasMatDetails = True
                If .Item(GetType(InterestAttribute)) IsNot Nothing Then bHasIntDetails = True
            End With
        Next
    End Sub

    Friend Overrides Function RemoveProperty(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As Boolean

        'If it's a basic instrument type then I only want to show the related instruments. There are a few properties like Additional Terms that apply to all types but I don't want to show them here.

        If converterobject.GetType Is CType(converterobject, Instrument).Parent.GetBasicType.GetType And _
        Not TypeOf converterobject Is SwapLeg And Not TypeOf converterobject Is CashPayment And _
        Not pd.PropertyType.IsAssignableFrom(GetType(BaseLine)) Then Return True

        If InstrumentProperty(pd) And pd.GetValue(converterobject) Is Nothing Then Return True

        Return MyBase.RemoveProperty(converterobject, pd)

    End Function

    Friend Overrides Function GetCategoryAttribute(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As System.ComponentModel.CategoryAttribute

        'Propgrid will just say "Misc" if there is no category. I want "4. Miscellaneous" or something like that.
        Dim s As String = ""

        If InstrumentProperty(pd) Then
            s = "Related Instruments"
        Else
            s = CategoryString(converterobject, pd)
        End If

        Return New CategoryAttribute(s)

    End Function

    Protected Overridable Function CategoryString(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As String
        Return 1 - bHasMatDetails - bHasIntDetails - bHasIssueDetails & ". Miscellaneous"
    End Function

    Private Function InstrumentProperty(ByVal pd As PropertyDescriptor) As Boolean

        'Whether this property is an instrument type one.

        Dim g As EditorAttribute = pd.Attributes.Item(GetType(EditorAttribute))
        If Not g Is Nothing Then
            If g.EditorTypeName = GetType(CashDisplayer).AssemblyQualifiedName Then Return True 'bit of a hash, but can't see how to do this better.
        End If

        If pd.PropertyType.IsAssignableFrom(GetType(BaseLine)) Then Return True

    End Function

End Class

Public Class CashConverter
    Inherits InstrumentConverter

    Protected Overrides Function CategoryString(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As String
        Return "Cash Payment"
    End Function
    Friend Overrides Function RemoveProperty(ByVal converterobject As Object, ByVal pd As System.ComponentModel.PropertyDescriptor) As Boolean

        With pd.Attributes
            Dim at As OnwardAttribute = .Item(GetType(OnwardAttribute))
            If Not at Is Nothing Then If at.RelCash(converterobject) Is Nothing Then Return True

            'Hide the cash total item if not more than one Cash instrument selected.
            If .Item(GetType(CashTotalAttribute)) IsNot Nothing Then
                If Not frmMDI.ActiveDeal Is Nothing Then
                    Dim cashcount As Integer
                    For Each b As BaseLine In frmMDI.ActiveDeal.picCanvas.SelectedBLs

                        If TypeOf b.Inst Is CashPayment Then
                            cashcount += 1
                        Else
                            Continue For
                        End If

                    Next
                    If cashcount < 2 Then Return True
                End If
            End If

        End With

        Return MyBase.RemoveProperty(converterobject, pd)

    End Function

    Friend Overrides Function PropIsReadOnly(ByVal pd As System.ComponentModel.PropertyDescriptor, ByVal converterobject As Object, ByVal instance As Object) As Boolean

        'If payment is consideration for a funding then it shouldn't be editable cos can be set through the funding.

        Dim c As CashPayment = TryCast(instance, CashPayment)
        If Not c Is Nothing Then   'should be 
            Dim t As TransactionCon = TryCast(c.parent, TransactionCon)

            If Not t Is Nothing Then    'should be
                If TypeOf t.Inst Is Funding Then Return True
            End If

        End If
    End Function

End Class

Public Class DebtConverter
    Inherits FundingConverter

    Friend Overrides Function RemoveProperty(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As Boolean

        Dim d As Debt = converterobject
        With pd.Attributes
            If .Item(GetType(InterestAttribute)) IsNot Nothing Then

                If .Item(GetType(FixedRateAttribute)) IsNot Nothing Or .Item(GetType(FloatingRateAttribute)) IsNot Nothing Then
                    If Not CType(converterobject, Debt).IntOption.HasValue Then
                        Return True
                    Else
                        Select Case d.IntOption.Value
                            Case Debt.RateBasisType.Zero
                                Return True
                            Case Debt.RateBasisType.Fixed
                                If .Item(GetType(FixedRateAttribute)) Is Nothing Then Return True
                            Case Debt.RateBasisType.Floating
                                If .Item(GetType(FloatingRateAttribute)) Is Nothing Then Return True
                        End Select
                    End If
                End If
            End If
        End With
        Return MyBase.RemoveProperty(converterobject, pd)

    End Function

End Class

Friend Class PaymentConverter
    'Don't try to put this inside the Payment class - TypeConverters cannot be inside the type they convert.
    Inherits CustomExpandableObjectConverter

    Friend Overrides Function PropIsReadOnly(ByVal pd As System.ComponentModel.PropertyDescriptor, ByVal converterobject As Object, ByVal instance As Object) As Boolean

        'Prevent cash consideration for a funding from being edited because it should be done through the instrument. 
        Dim c As CashPayment = TryCast(instance, CashPayment)

        If Not c Is Nothing Then
            If TypeOf CType(c.parent, TransactionCon).Consideration.Inst Is Funding Then Return True
        End If

        Dim f As Instrument.Securities = TryCast(instance, Instrument.Securities)
        If Not f Is Nothing And TypeOf converterobject Is Cash And pd.Attributes.Item(GetType(CurrencyAttribute)) IsNot Nothing Then
            If f.Denom Is converterobject Then Return True
        End If

    End Function

    Public Overloads Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal sourceType As System.Type) As Boolean

        If (sourceType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, sourceType)

    End Function

    Public Overloads Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object

        'ConvertFrom means convert from something to the object.
        If (TypeOf value Is String) Then
            Try
                Dim s As String = CStr(value)

                s = s.Replace(" ", "")
                Dim ss As String
                Dim Start As Integer = -1   'The 0 based position of the first character in the amount.
                Dim Last As Integer = -1    'The 0 based position of the last character in the amount.

                'If the string contains one set of numbers then it's no problem.
                For n As Integer = 0 To s.Length - 1
                    ss = s.Substring(n, 1)
                    If Start = -1 And (IsNumeric(ss) Or ss = ".") Then Start = n
                    If Start <> -1 And Not IsNumeric(ss) And ss <> "." Then
                        Last = n - 1
                        Exit For
                    End If
                Next

                'Didn't find a non-numeric character after numeric characters had started, so last _
                'character of number must be the last one of s.
                If Start <> -1 And Last = -1 Then Last = s.Length - 1
                Dim f As New Cash.SuffixConverter
                Dim so As Cash = New Cash
                With so
                    If Start = -1 Then


                        If s.Length > 3 Then
                            Dim sEnd As String = s.Substring(s.Length - 1, 1)

                            If f.ListItems.Contains(sEnd) Then
                                .Suffix = f.ListItems.IndexOf(sEnd)
                                .Curr = FindCurrency(Left$(s, s.Length - 1))
                            Else
                                .Curr = FindCurrency(s)
                            End If
                        Else
                            .Curr = FindCurrency(s)
                        End If

                    Else
                        Debug.Assert(Last <> -1)
                        Dim sEnd As String = ""

                        'Note that I can make changes here and they will be updated in the parent grid item, 
                        'and I do this if the user types in a lower case suffix or the number begins with a 
                        'decimal point or ends with one.

                        If Last <> s.Length - 1 Then sEnd = (Right$(s, s.Length - 1 - Last))

                        .Curr = FindCurrency(Left$(s, Start))
                        .Amount = s.Substring(Start, Last - Start + 1)
                        sEnd = sEnd.ToUpper
                        If f.ListItems.Contains(sEnd) Then .Suffix = f.ListItems.IndexOf(sEnd)

                    End If
                End With
                Return so

            Catch
                'This triggers an error in debugging, but not in the release exe. If I don't do this, then 
                'the sub grid items cease to display. The text below appears in the details dropdown of the error message the user gets.
                Throw New NullReferenceException("Cannot convert '" & CStr(value) & "' to a monetary amount")

            End Try
        Else

            Return MyBase.ConvertFrom(context, culture, value)
        End If

    End Function

    Private Function FindCurrency(ByVal s As String) As Currency
        For Each c As Currency In CurrencySpace.Currencies.Values
            If c.Acronym = s Or c.Acronym = s.ToUpper Then Return c
        Next
    End Function
End Class
Public Class CustomExpandableObjectConverter
    Inherits ExpandableObjectConverter  'inherits from TypeConverter. GetProperties is a function of this, not TypeConverter.

    Friend Overridable Function RemoveProperty(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As Boolean
        Return False
    End Function

    Friend Overridable Function GetCategoryAttribute(ByVal converterobject As Object, ByVal pd As PropertyDescriptor) As CategoryAttribute
        Return Nothing
    End Function

    Friend Overridable Sub DetermineCategories(ByVal pdc As PropertyDescriptorCollection)

    End Sub

    Friend Overridable Function PropIsReadOnly(ByVal pd As PropertyDescriptor, ByVal converterobject As Object, ByVal instance As Object) As Boolean
        Return False
    End Function

    Public Overrides Function GetProperties(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal converterobject As Object, ByVal attributes() As System.Attribute) As System.ComponentModel.PropertyDescriptorCollection

        'Does three things - removes properties that I don't want to display, sets the category name for each property, 
        'and sets the order the attributes are to appear in. 

        'It seems that context.instance is the PropertyGrid object while converterobject is the object for which the expandableobjectconverter is an attribute, so a sub object, like a payment.

        Dim PDSortedList As New SortedList    'could use a SortedDictionary to make type bound,
        'but it doesn't have the GetByIndex method, and I don't think the need is great.

        DetermineCategories(MyBase.GetProperties(context, converterobject, attributes))

        For Each pd As PropertyDescriptor In MyBase.GetProperties(context, converterobject, attributes)
            'Certain inappropriate things for interest rate basis need removal. 
            'I suppose more logically this procedure would belong in a derived class used as the converter 
            'for Instrument/Debt, but I'm not that bothered.
            'An alternative way to hide properties is to have them as part of a class, and set the reference 
            'to an object of that class to Nothing. If the reference is Nothing then the +/- disappears. 
            'However this would mean that there was always a header 
            'row there (for the object reference), and also if the object reference is set to Nothing when 
            'the grid row is expanded, the properties remain displayed, but greyed out.

            Dim AttList As New List(Of Attribute)   'note there seems to be no need to add the existing attributes to this.

            'If I don't do the following most things seem to work out OK, strangely. But the ellipsis box for editors doesn't appear for one thing, so am still doing it.
            For Each at As Attribute In pd.Attributes
                AttList.Add(at)
            Next

            With pd.Attributes
                'Can do the following to prevent properties showing for secondary categories, but + mark is still shown, so am having a string property instead.
                'If TypeOf pd.GetValue(converterobject) Is Cash Then

                '    If frmMDI.prgOne.SelectedObjects.Length = 1 Then
                '        If frmMDI.prgOne.SelectedObject IsNot converterobject Then Return Nothing 
                '    End If
                'End If
                If RemoveProperty(converterobject, pd) Then Continue For

                Dim cat As CategoryAttribute = GetCategoryAttribute(converterobject, pd)
                If Not cat Is Nothing Then AttList.Add(cat)

                'In order to get around the problem that every value in the grid is a non-default, 
                'and therefore appears in bold I had the idea of adding an attribute which inherits from DefaultValueAttribute, 
                'and seeing what I could override in that. But none of the overridable methods ever seemed to get called.

                Dim c As OrderAttribute = .Item(GetType(OrderAttribute))

                'Unfortunately the SortedList can't contain more than one item with the same key. This will be a particular problem where
                'I don't specify any OrderAttribute, so h is 0. As long as I don't have more than 1000 properties the following
                'will be OK.

                Dim h As Single = 0   'if just declared as Single, value won't be reset to 0 in each loop.

                If Not c Is Nothing Then h = c.Position
                Do Until Not PDSortedList.ContainsKey(h)
                    h = h + 0.001
                Loop

                PDSortedList.Add(h, New CustomPropertyDescriptor(pd, AttList.ToArray, PropIsReadOnly(pd, converterobject, context.Instance)))
            End With
        Next

        'Convert SortedList to an array. Probably a standard way to do this,
        'but I don't know what. PSortedList.CopyArray copies DictionaryEntries (i.e. paired values) so no good.
        Dim PDArray(PDSortedList.Count - 1) As CustomPropertyDescriptor
        For n As Long = 0 To PDSortedList.Count - 1
            PDArray(n) = PDSortedList.GetByIndex(n)
        Next

        Dim PDC As New PropertyDescriptorCollection(PDArray)
        For Each p As CustomPropertyDescriptor In PDC
            p.ParentCol = PDC
        Next

        Return PDC

    End Function

    Public Overloads Overrides Function CanConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal destinationType As Type) As Boolean

        'This is never called and I don't know what it's for.
        'The following is what I'm supposed to put for the class this is the converter for.
        ' If (destinationType Is GetType(DatesSelection)) Then Return True

        Return MyBase.CanConvertTo(context, destinationType)

    End Function

    Public Overloads Overrides Function ConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, ByVal destinationType As System.Type) As Object

        'ConvertTo means convert the object to something.
        If destinationType Is GetType(System.String) And Not value Is Nothing Then value.ToString()

        Return MyBase.ConvertTo(context, culture, value, destinationType)

    End Function

    Public Overloads Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal sourceType As System.Type) As Boolean

        'Can you convert from the String to the object? Basic case is No. Override it otherwise.
        If (sourceType Is GetType(String)) Then
            Return False
        End If
        Return MyBase.CanConvertFrom(context, sourceType)

    End Function

End Class

Friend Class UnderlyingConverter
    Inherits CustomExpandableObjectConverter

End Class