'Various custom attributes.

<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Public Class PropIDAttribute
    'For when I want to display the class name in a toolstrip item or on a tabstrip.

    Inherits Attribute

    Public Sub New(ByVal ID As Integer)
        mID = ID
    End Sub

    Private mID As Integer
    Public ReadOnly Property ID() As Integer
        Get
            Return mID
        End Get
    End Property

End Class

<AttributeUsage(AttributeTargets.Class, AllowMultiple:=False)> Friend Class ClassDisplayNameAttribute
    'For when I want to display the class name in a toolstrip item or on a tabstrip.

    Inherits Attribute

    Public Sub New(Optional ByVal sMenuItemText As String = "", Optional ByVal sTabText As String = "", Optional ByVal SepBefore As Boolean = False)
        mMenuItemText = sMenuItemText
        mTabText = sTabText
        mSepBefore = SepBefore
    End Sub

    Private mMenuItemText As String
    Public ReadOnly Property MenuItemText() As String
        Get
            Return mMenuItemText
        End Get
    End Property

    Private mTabText As String
    Public ReadOnly Property TabText() As String
        Get
            If mTabText = "" Then Return mMenuItemText
            Return mTabText
        End Get
    End Property

    Private mSepBefore As Boolean       'whether to be preceded by a separator.
    Public ReadOnly Property SepBefore() As Boolean
        Get
            Return mSepBefore
        End Get
    End Property

End Class

<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Friend Class OrderAttribute
    'An attribute to specify the position of a property in relation to others in the same PropertyDescriptorCollection.
    Inherits Attribute

    Private mPosition As Single   'relative position to appear in propertygrid. OK for this to be negative.
    Public Sub New(ByVal Position As Single)
        mPosition = Position
    End Sub

    Public ReadOnly Property Position() As String
        Get
            Return mPosition
        End Get
    End Property

End Class

#Region "Category"

'Attributes to identify the category a property is to go under, and provide text for the category.
'Did have just one type and construct it with an enumeration identifying the category, but enumeration had to 
'be public, so I've split them out
<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Friend Class FundingCategoryInfoAttribute
    Inherits Attribute  'Why not simply inherit from CategoryAttribute instead of creating this attribute 
    'from information given here? Because I don't know the type of the object when this is created.
    Friend Overridable Function CategoryText(ByVal t As Funding, ByVal bHasIssueDetails As Boolean, ByVal bHasMatDetails As Boolean) As String
        'Don't make this MustOverride because then the class must be MustInherit and then it can't be 
        'created in Attributes.GetItem
    End Function
End Class

Friend Class IssueAttribute
    Inherits FundingCategoryInfoAttribute
    Friend Overrides Function CategoryText(ByVal t As Funding, ByVal bHasIssueDetails As Boolean, ByVal bHasMatDetails As Boolean) As String
        Return "1. " & t.IssueHeading
    End Function
End Class

Friend Class MaturityAttribute
    Inherits FundingCategoryInfoAttribute
    Friend Overrides Function CategoryText(ByVal t As Funding, ByVal bHasIssueDetails As Boolean, ByVal bHasMatDetails As Boolean) As String
        Dim n As Integer = 1
        If bHasIssueDetails Then n = 2
        Return n & ". " & t.MaturityHeading
    End Function
End Class

Friend Class InterestAttribute
    Inherits FundingCategoryInfoAttribute
    Friend Overrides Function CategoryText(ByVal t As Funding, ByVal bHasIssueDetails As Boolean, ByVal bHasMatDetails As Boolean) As String
        Dim n As Integer = 1
        If bHasIssueDetails Then n = n + 1
        If bHasMatDetails Then n = n + 1
        Return n & ". " & t.InterestHeading
    End Function

End Class

#End Region

#Region "ProviderCopier"

'To cope with idea that some properties (providers) will usually be the same as others (copiers). If they are not the same then the copier property will 
'be shown in bold. 

Public Class ProviderAttribute
    Inherits Attribute
    Enum ProviderEnum
        peIssueDate
        peNomAmount
        peMatDate
    End Enum

    Private pID As ProviderEnum
    Public Sub New(ByVal ID As ProviderEnum)
        MyBase.new()
        pID = ID
    End Sub
    Friend ReadOnly Property ID() As ProviderEnum
        Get
            Return pID
        End Get
    End Property
End Class

Public Class CopierAttribute
    Inherits Attribute

    Private pID As ProviderAttribute.ProviderEnum
    Public Sub New(ByVal ID As ProviderAttribute.ProviderEnum)
        MyBase.new()
        pID = ID
    End Sub
    Friend ReadOnly Property ID() As ProviderAttribute.ProviderEnum
        Get
            Return pID
        End Get
    End Property
End Class

#End Region

<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Friend Class FloatingRateAttribute
    'An attribute for floating rate properties.
    Inherits Attribute
End Class

<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Friend Class FixedRateAttribute
    'An attribute for floating rate properties.
    Inherits Attribute
End Class

<AttributeUsage(AttributeTargets.Property, AllowMultiple:=False)> Friend Class CurrencyAttribute
    Inherits Attribute
End Class

Friend Class CashRetentionAttribute
    Inherits Attribute
End Class

Friend Class CashTotalAttribute
    Inherits Attribute
End Class

<AttributeUsage(AttributeTargets.Class)> Friend Class DoesNotExistPostCompletionAttribute    'easier to  use an attribute to show this than an overridable property because 
    'it's easier to go from an object to its type than vice versa.
    Inherits Attribute
End Class