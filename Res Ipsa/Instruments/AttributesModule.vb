
Friend Module ModAttributes

    Friend InApp As Boolean = True     'whether in the application as opposed to a plug-in.
    'Another approach to this would be to create a deep clone of the object being sent.
    'Can't just make all the Set parts of Properties Friend because the PropertyGrid will then disable them.

    Friend Function GetToolStripItemAttribute(ByVal t As Type) As ClassDisplayNameAttribute
        'Why doesn't t just have an Attributes collection you can access?
        'Because you can use a type without having access to all its attributes 
        '(i.e. they may be Friend in a different assembly. If you could 
        'get all the attributes you would get access to objects you couldn't 
        'know the type of. So the get attribute function requires you to give a reference 
        'to the type of attribute you want.
        Return Attribute.GetCustomAttribute(t, GetType(ClassDisplayNameAttribute))

    End Function

    Friend Function ClassDisplayName(ByVal p As Type) As String

        'Something to display to the user without the ampersand
        Dim g As ClassDisplayNameAttribute = GetToolStripItemAttribute(p)
        If Not g Is Nothing Then Return g.MenuItemText.Replace("&", "") 'g should never be nothing but just in case.
        Return ""

    End Function

End Module

