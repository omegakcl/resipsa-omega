Imports System.ComponentModel

'Holds details of a selection of dates, periodically beginning on a particular date, 
'or a set of specific dates, or both.
<Serializable()> <TypeConverter(GetType(CustomExpandableObjectConverter))> Public Class DatesSelection

    Friend Parent As Debt

    Public Overrides Function ToString() As String
        Dim s As String = ""
        If pFrequency.HasValue Then
            Dim d As New PeriodConverter
            s = d.ListItems(pFrequency)
            If Not pCommencementDate = Nothing Then    'make it nullable
                s = s & " commencing " & pCommencementDate
            End If
        End If
        If Not pSpecificDates Is Nothing Then

            For Each ss As Date In pSpecificDates
                s = s & "; " & ss
            Next
        End If
        If s = "" Then s = "None"
        Return s

    End Function

    Private pFrequency As Nullable(Of Period)
    <System.ComponentModel.NotifyParentProperty(True), TypeConverter(GetType(PeriodConverter)), Order(1)> Public Property Frequency() As Nullable(Of Period)
        Get
            Return pFrequency
        End Get
        Set(ByVal value As Nullable(Of Period))
            pFrequency = value
            If Not value.HasValue Then pCommencementDate = Nothing
        End Set
    End Property

    Public Enum Period
        Weekly
        TwoWeekly
        Monthly
        TwoMonthly
        FourMonthly
        SixMonthly
        Annually
        TwoAnnually
    End Enum

    Friend Class PeriodConverter
        Inherits EnumStringConverter

        Public Sub New()
            'Will want to adjust this for the currency selected.
            With MyBase.ListItems
                .Add("Weekly")
                .Add("Every Two Weeks")
                .Add("Monthly")
                .Add("Every Two Months")
                .Add("Bi-annually")
                .Add("Annually")
                .Add("Every Two Years")
            End With
        End Sub

        Friend Overrides Function ConvertIntToEnum(ByVal x As Integer) As Object
            Return CType(x, Period)
        End Function
    End Class

    Private pCommencementDate As Date
    <System.ComponentModel.NotifyParentProperty(True), DisplayName("Commencing"), Order(2), Editor(GetType(PeriodicCommencementEditor), GetType(System.Drawing.Design.UITypeEditor))> Public Property CommencementDate() As Date
        Get
            Return pCommencementDate
        End Get
        Set(ByVal value As Date)
            pCommencementDate = value
        End Set
    End Property

    Private pSpecificDates As Date()
    <System.ComponentModel.NotifyParentProperty(True), DisplayName("Specific Dates"), Order(3), Editor(GetType(MultiSelectMonthViewEditor), GetType(System.Drawing.Design.UITypeEditor))> Public Property SpecificDates() As Date()
        'The grid will handle arrays, but by providing a fairly complicated looking form which isn't very user friendly.
        Get
            Return pSpecificDates
        End Get
        Set(ByVal value As Date())
            pSpecificDates = value
        End Set
    End Property

End Class