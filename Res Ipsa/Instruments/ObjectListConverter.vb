Public MustInherit Class ObjectListConverter

    'Where the user is to select from an array of objects implementing IProgGridListNameGiver.

    Inherits System.ComponentModel.TypeConverter
    Public ListItems As IPropGridListNameGiver() 'If there's an enumeration it must match the items put in here.
    Protected ValuesExclusive As Boolean = True

    Public Overloads Overrides Function GetStandardValues(ByVal context As System.ComponentModel.ITypeDescriptorContext) As StandardValuesCollection

        Return New StandardValuesCollection(ListItems)

    End Function

    Public Overloads Overrides Function GetStandardValuesExclusive(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return ValuesExclusive 'no other entries allowed.
    End Function
    Public Overloads Overrides Function GetStandardValuesSupported(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True 'No dropdown shown unless you do this.
    End Function

    Public Overloads Overrides Function CanConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal destinationType As Type) As Boolean
        If destinationType Is GetType(String) Then Return True
        Return MyBase.CanConvertFrom(context, destinationType)
    End Function

    Public Overloads Overrides Function ConvertTo(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, _
    ByVal destinationType As System.Type) As Object

        Dim ipng As IPropGridListNameGiver = TryCast(value, IPropGridListNameGiver)
        If destinationType Is GetType(String) And Not ipng Is Nothing Then Return ipng.PropGridName

        Return MyBase.ConvertTo(context, culture, value, destinationType)

    End Function

    Public Overloads Overrides Function CanConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal sourceType As System.Type) As Boolean

        If (sourceType Is GetType(String)) Then
            Return True
        End If
        Return MyBase.CanConvertFrom(context, sourceType)

    End Function

    Public Overloads Overrides Function ConvertFrom(ByVal context As System.ComponentModel.ITypeDescriptorContext, _
    ByVal culture As System.Globalization.CultureInfo, ByVal value As Object) As Object

        If VarType(value) = VariantType.String Then

            For Each b As IPropGridListNameGiver In Me.ListItems
                If b.PropGridName = value Then Return b
            Next

        End If

        Return MyBase.ConvertFrom(context, culture, value)

    End Function

End Class

