
'I customise the property grid control so as to:
'a) not always have things in alphabetical order. The control does have a PropertySort property - 
'no sort, categorised or alphabetical. No sort just gives an arbitrary order. Can do that with OrderAttribute.
'b) Show things bold when I want them, not just when they are not the default value (which is what 
'happens usually. If no default value is specified they are always in bold). Can do this through ShouldSerialize function of propertydescriptor.
'c) Have standard category headings. Do this through CategoryInfoAttribute inheritance.

'Nullable properties are very useful here. They allow a property not to be set initially so are useful 
'with properties which hold Enum values and numerical amounts which can be blank in the grid rather than 
'zero. Not needed for strings or dates - dates are blank if the property is nothing.

'Important principle here: If you override a property, the override will still have the attributes declared in 
'the base class, unless you redeclare them in the override. This is what I would want.

'Merging LOM into Res Ipsa has made things so much simpler. I have been able to get rid of several events and 
'interfaces. And things like the ToolStripItemAttribute no longer need to be exposed to the plug-ins. And also I can 
'search the whole thing in one go!

'TODO: Ratings, Max Min Floor and Cap, Descriptions, CP, Appropriate DCFs for Rates as per ISDA, ensure NIP not -ve,
'Denomination, improve messages on user entering something inappropriate,  relevant page, 
'specified duration, initial rate, perpetual, fixed amount.

#Region "Instrument Base Class"

<TypeConverter(GetType(InstrumentConverter))> <System.Serializable()> Public MustInherit Class Instrument
    Implements INameObject
    <Browsable(False)> Private pParent As BaseLine

    'I'd like a constructor passing the parent object, but I don't want to expose the BaseLine, so I need the constructor
    'to be Friend but then I can't figure out how to use the Activator object.

    Friend Property Parent() As BaseLine
        Get
            Return pParent
        End Get
        Set(ByVal value As BaseLine)
            pParent = value
            SetName()
        End Set
    End Property

    Private pName As String
    Friend bUserNameSet As Boolean
    <MergableProperty(False), Order(-90)> Public Overridable Property Name() As String Implements INameObject.Name
        'I think this is only needed for fundings. You'd give bonds a name but I don't think you'd give 
        'a subparticipation agreement, guarantee or collateral a name. And certainly not a cash payment.
        Get
            Name = pName
        End Get
        Set(ByVal value As String)
            If InApp Then
                pName = value   'need to do this even if set to "" since otherwise "Bonds 1" will change to "Bonds 2" cos name will still be there.
                If value = "" Then
                    SetName() 'give it a new name.
                Else
                    bUserNameSet = True
                End If

            End If

            RefreshCanvas()
        End Set
    End Property

    Friend Overridable Sub AddExplanation(ByVal THandler As ITopicsHandler)

    End Sub

    Friend Sub SetName()

        pName = Parent.Parent.GetInstrumentName(Me)

    End Sub

    Friend Overridable Function TypeIsEnabled(ByVal t As Type) As Boolean

        'Whether a type should be enabled for the given baseline. Did have this as a function which could be overriden for each type of instrument, 
        'but that required creating an instance of each type of instrument when filling the menu, which took up to a second. 
        'Can't have an overridable shared function. And even if I could the object that I would be dealing with would have to inherit from Type.
        'Classes can inherit from Type, but you get a huge list of Overrides. So do like this.

        'I think this is better at the Instrument level because EToECon can be a Passthrough or a Funding whereas with an Instrument you know 
        'what you have.

        'Use it (a) to determine if a given instrument in a menu should be enabled, and (b) to determine
        'if an existing instrument is a possibility. It may have been selected from a menu which was appropriate
        'to enable when shown, but which would not be enabled if shown now. In such a case I don't think I want 
        'to automatically delete it, because it could be annoying for the user, but I want to show it greyed out.
        'Do this through the drawing routines of the baseline. Note for SecurityArcs I have an extra check - whether 
        'security is possible at all. If security is drawn but it's not possible then I draw the whole line in grey.

        Return True

    End Function

    Public Overrides Function ToString() As String
        Return pName
    End Function

    Friend Shared CurrentJur As IJurisdiction   'need this so that I know what dll is adding a topic to the treeview.

    Public Enum YesNo
        No = False
        Yes = True
    End Enum

    Friend Shared Sub RefreshGrid()
        'Want to call this every time a property changes another. Can use RefreshProperties but that will only 
        'work when the first property is changed by the user through the grid, not by me in code.
        frmMDI.prgOne.Refresh()
    End Sub

    Friend Sub RefreshCanvas()
        'Call this if something that's displayed in picCanvas has been changed through the property grid.
        Parent.Parent.Refresh()
    End Sub

    Friend Function ExistsPostCompletion() As Boolean
        'Relevant for deciding if it's appropriate to give credit support or security for or over an Instrument, and also 
        'whether to show it in any repayment view.
        Return Attribute.GetCustomAttribute(Me.GetType, GetType(DoesNotExistPostCompletionAttribute)) Is Nothing
    End Function

    Friend Overridable Function Supportable() As Boolean
        'Whether credit support or security can be given for the Instrument.
        Return ExistsPostCompletion()
    End Function

    Friend Overridable Sub GetLaw()

        'Extensively overriden. Alternative would be to have this stuff in the exe, so not accessible 
        'by dll writers, but it is better structured in here - avoids lots of If TypeOf TheInstrument is ...
        'Call at end of overriding procedures.

        InApp = True
        'Cause the following to be reset in case the plug-in has changed them (not that it should of course 
        'but what if the Lebanon dll was to remove Israel from the collection?!).
        RefreshJurisdictions()
        RefreshCurrencies()

    End Sub

    Protected pGoverningLaw As IJurisdiction
    <DisplayName("Governing Law"), TypeConverter(GetType(JurConverter)), Order(900)> Public Overridable Property GoverningLaw() As IJurisdiction
        Get
            Return pGoverningLaw
        End Get
        Set(ByVal value As IJurisdiction)
            If InApp Then pGoverningLaw = value
        End Set
    End Property

    Friend Class JurConverter
        Inherits ObjectListConverter

        Public Sub New()

            ReDim MyBase.ListItems(JurisdictionSpace.Jurisdictions.Count - 1)
            Dim n As Long
            For Each v As IJurisdiction In JurisdictionSpace.Jurisdictions.Values   'CopyTo method doesn't work with interfaces.
                MyBase.ListItems(n) = v
                n = n + 1
            Next

        End Sub
    End Class

    Private pAddTerms As String
    <DisplayName("Additional Terms"), _
    Editor(GetType(TextBoxFormEditor), GetType(System.Drawing.Design.UITypeEditor)), _
    Order(800), Description("Any additional terms which cannot be represented elsewhere in the grid.")> _
    Public Overridable Property AddTerms() As String
        Get
            AddTerms = pAddTerms
        End Get
        Set(ByVal value As String)
            If InApp Then pAddTerms = value
        End Set
    End Property

    'The following  interface is really an inheritance relationship. But classes can't 
    'inherit more than one parent class. So having this allows me to test for TypeOf X is Securities.
    'I can just implement all of those in the same way for each type of securities, although it does require rather repetitive code.

    Public Interface Securities
        <Order(90), Editor(GetType(ISINChecker), _
      GetType(System.Drawing.Design.UITypeEditor)), Description("International Securities Identification Number")> Property ISIN() As String

        <DisplayName("Denomination"), Order(3), Issue()> Property Denom() As Cash    'If there is no instance of this then its properties won't show in the property grid.

        <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Property IssuePrice() As Decimal

        <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Property SellingCommission() As Decimal

        <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Property MUC() As Decimal

        <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> ReadOnly Property NetIssuePrice() As Decimal


        '4. Miscellaneous


        <Order(100), Editor(GetType(CUSIPChecker), _
        GetType(System.Drawing.Design.UITypeEditor))> Property CUSIP() As String

        <Editor(GetType(Funding.ListingCheckBoxEditor), GetType(System.Drawing.Design.UITypeEditor)), Order(550)> Property Listed() As String

        Sub SalesProcChanged()

    End Interface

End Class

#End Region

<Serializable()> Public MustInherit Class FromEInstrument
    'An Instrument which is drawn from an Entity. Having this class and the next one
    'parallels what I draw much better and saves me from needing to hide certain properties where they don't apply.
    Inherits Instrument

    Friend Overloads Property parent() As Line
        Get
            Return MyBase.Parent
        End Get
        Set(ByVal value As Line)
            MyBase.Parent = value
        End Set
    End Property

    Public Overridable Function FromEntity() As Entity

        Return parent.FromEntity

    End Function
End Class

<Serializable()> Public MustInherit Class EToEInstrument
    'An instrument for a Connection.
    Inherits FromEInstrument

    Friend Selected As Boolean

    Friend Overloads Property parent() As Connection
        Get
            Return MyBase.parent
        End Get

        Set(ByVal value As Connection)
            MyBase.parent = value
        End Set
    End Property

    Public Overridable Function ToEntity() As Entity
        Return parent.ToEntity
    End Function

    <Browsable(False)> Public Overridable ReadOnly Property Consideration() As Instrument
        'Would want this to be cash mostly, but will have to check that it is.
        Get
            Dim tc As TransactionCon = TryCast(parent, TransactionCon)
            If Not tc Is Nothing Then
                Return tc.Consideration.Inst
            End If
        End Get

    End Property

End Class

#Region "Fundings"

<Serializable(), DefaultProperty("Name"), TypeConverter(GetType(FundingConverter))> Public Class FundingBase

    Inherits EToEInstrument
    'Need this so that if no specific instrument is selected I can still show any related pass-throughs.
    'Don't just have Funding cos Cash inherits from this and I want that to appear together in the context menus.

    Friend Overrides Function TypeIsEnabled(ByVal t As System.Type) As Boolean
        If t Is GetType(Deposit) Then Return FromEntity.CanTakeDeposits
        Return MyBase.TypeIsEnabled(t)
    End Function

End Class

<Serializable(), DefaultProperty("Name"), TypeConverter(GetType(FundingConverter))> Public MustInherit Class Funding

    Inherits FundingBase
    Friend Enum Rank
        Debt
        SubDebt
        PrefShares
        OrdShares
    End Enum

    Friend Overridable ReadOnly Property IntrinsicRanking() As Rank
        Get
            Return Rank.Debt
        End Get
    End Property
    'the ranking which an instrument has based on its type
    <DisplayName("Priority"), Description("The priority of the instrument within the other instruments in its collection, 1 representing the highest priority"), Order(11), MergableProperty(False), TypeConverter(GetType(IntegerConverter))> Public Property NumberRanking() As Integer      'the ranking which an instrument can have based on the user's choice and dependent on its IntrinsicRanking.
        'Order this to come just after Subordination.
        Get

            Return parent.ParentCol.PriorityIndex(Me.parent)
        End Get
        Set(ByVal value As Integer)
            parent.ParentCol.Sort(value, Me.parent)
            parent.Parent.Refresh()
        End Set

    End Property

    Friend Overridable Function IssueHeading() As String
        Return "Issue"
    End Function
    Friend Overridable Function InterestHeading() As String
        Return "Coupon"
    End Function
    Friend Overridable Function MaturityHeading() As String
        Return "Maturity"
    End Function

    Friend Overrides Sub GetLaw()
        If Not Me.GetType Is GetType(Funding) Then  'only show law for derived instruments.

            'Handle all interfaces to do with the Borrower in a Funding arrangement.

            CurrentJur = Me.FromEntity.Incorp

            If Not CurrentJur Is Nothing Then

                With CType(CurrentJur, IFundingLaw)

                  
                    frmMDI.LawCat = frmMDI.LawCategory.cInstrumentDesc
                    AddExplanation(frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cAccounting
                    .BorrowerAccounting(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cInsolvency
                    .BorrowerInsolvency(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cStampDuty
                    .BorrowerStampDuty(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cWHT
                    .WHT(Me, frmMDI)

                    If TypeOf Me Is Shares Then
                        frmMDI.LawCat = frmMDI.LawCategory.cShareDisc
                        .ShareHoldingDisclosure(Me, frmMDI)
                    End If

                    If FromEntity.HasCapReq Then
                        frmMDI.LawCat = frmMDI.LawCategory.cRegCapQual
                        .FundingCapitalCriteria(Me, frmMDI)
                    End If

                    If TypeOf Me Is Shares Then
                        frmMDI.LawCat = frmMDI.LawCategory.cAccountConsol
                        .AccountsParentConsolidation(Me, frmMDI)
                        If ToEntity.HasCapReq Then
                            frmMDI.LawCat = frmMDI.LawCategory.cRegConsol
                            .RegulatoryParentConsolidation(Me, frmMDI)
                        End If
                    End If

                    frmMDI.LawCat = frmMDI.LawCategory.cCRD
                    .AdditionalBorrowerRelated(Me, frmMDI)

                End With
            End If

            CurrentJur = Me.ToEntity.Incorp

            If Not CurrentJur Is Nothing Then

                With CurrentJur

                    If TypeOf Me Is Securities Then

                        frmMDI.LawCat = frmMDI.LawCategory.cSalesRest

                        .InvestorSalesRestrictions(Me, frmMDI)

                    End If

                    If ToEntity.HasCapReq Then
                        frmMDI.LawCat = frmMDI.LawCategory.cRegCapTreat
                        .InvestorCapitalRequirement(Me, frmMDI)
                    End If

                    If TypeOf Me Is Shares Then
                        frmMDI.LawCat = frmMDI.LawCategory.cAccountConsol
                        .AccountsSubsidiaryConsolidation(Me, frmMDI)
                        If ToEntity.HasCapReq Then
                            frmMDI.LawCat = frmMDI.LawCategory.cRegConsol
                            .RegulatorySubsidiaryConsolidation(Me, frmMDI)
                        End If
                        .ShareHoldingRestrictions(Me, frmMDI)
                    End If

                    frmMDI.LawCat = frmMDI.LawCategory.cAdditional
                    .AdditionalInvestorRelated(Me, frmMDI)

                End With
            End If
        End If

    End Sub

#Region "Issue Details"
    Private pIssueDate As Date

    <IssueAttribute(), Order(0.5), Provider(ProviderAttribute.ProviderEnum.peIssueDate), DisplayName("Issuer")> Public Overridable Property Issuer() As OneLineEntity
        Get
            Return parent.FromEntity
        End Get
        Set(ByVal value As OneLineEntity)
            'do nothing?
        End Set

    End Property

    <IssueAttribute(), Editor(GetType(IssueDateEditor), _
    GetType(System.Drawing.Design.UITypeEditor)), Order(1), Provider(ProviderAttribute.ProviderEnum.peIssueDate), _
    DisplayName("Issue Date")> Public Overridable Property IssueDate() As Date
        Get
            Return pIssueDate
        End Get
        Set(ByVal value As Date)
            If InApp Then

                If TypeOf Me Is Debt Then
                    With CType(Me, Debt)
                        If .IntCommencementDate = pIssueDate Then
                            .IntCommencementDate = value
                            RefreshGrid()
                        End If
                    End With
                End If
                pIssueDate = value
            End If
        End Set
    End Property

    Friend WithEvents NomAmount As New Cash    'If there is no instance of this then its properties won't show in the property grid.
    <DisplayName("Nominal Amount"), Order(2), Provider(ProviderAttribute.ProviderEnum.peNomAmount), IssueAttribute()> _
    Public Overridable Property NominalAmount() As Cash
        Get
            Return NomAmount
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                If TypeOf Me Is Instrument.Securities Then CType(Me, Instrument.Securities).Denom.Curr = NomAmount.Curr
                If TypeOf Me Is Debt Then
                    With CType(Me, Debt)
                        If .IntCalcAmount = NomAmount Then
                            .IntCalcAmount = value
                        End If
                        If .RedemptionAmount = NomAmount Then
                            .RedemptionAmount = value
                        End If
                    End With
                End If
                NomAmount = value
                If TypeOf Me Is Instrument.Securities Then CType(Me, Instrument.Securities).SalesProcChanged()
            End If
        End Set
    End Property




#End Region

#Region "Miscellaneous"


    Friend Sub New()

        MyBase.New()

        IssueDate = Today   'will change if converting.

        NomAmount.Parent = Me
        'remember that this is called when I'm converting instrument types, so I don't want to set default values here.
    End Sub

    Friend Class ListingCheckBoxEditor
        Inherits CheckedListBoxEditor
        Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object
            MyBase.ListItems = New List(Of String)
            For Each j As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
                If Not j.StockExchanges Is Nothing Then
                    For Each s As String In j.StockExchanges
                        MyBase.ListItems.Add(s)
                    Next
                End If
            Next
            Return MyBase.EditValue(context, provider, value)
        End Function
    End Class

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class

#Region "Debt"

<Serializable(), TypeConverter(GetType(DebtConverter))> Public MustInherit Class Debt
    Inherits Funding

    Friend Sub New()

        MyBase.New()
        CallDates.Parent = Me
        PutDates.Parent = Me
    End Sub

    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            If Me.Subordinated Then
                Return Rank.SubDebt
            Else
                Return Rank.Debt
            End If
        End Get
    End Property

#Region "IssueDetails"
    <Provider(ProviderAttribute.ProviderEnum.peIssueDate), Issue()> Public Overrides Property IssueDate() As Date
        Get
            Return MyBase.IssueDate
        End Get
        Set(ByVal value As Date)
            If MyBase.IssueDate = Me.IntCommencementDate Or Me.IntCommencementDate = Nothing Then Me.IntCommencementDate = value 'keep the same if they are same.
            MyBase.IssueDate = value

            If MaturityDate < value Then MaturityDate = value

            RefreshGrid()
        End Set
    End Property

    Protected Overridable Sub NomAmount_Changed(ByVal OldValue As Cash) Handles NomAmount.Changed

        If OldValue = Me.RedemptionAmount Then
            Me.RedemptionAmount = NomAmount.GetCopy
            RefreshGrid()
        End If

        If OldValue = Me.IntCalcAmount Then
            Me.IntCalcAmount = NomAmount.GetCopy
            RefreshGrid()
        End If

    End Sub

    Public Overrides Property NominalAmount() As Cash
        Get
            Return MyBase.NominalAmount
        End Get
        Set(ByVal value As Cash)
            Dim f As Cash = NominalAmount.GetCopy
            MyBase.NominalAmount = value
            NomAmount_Changed(f)
        End Set
    End Property

#End Region

#Region "Maturity"
    Private pMaturityDate As Date
    <Maturity(), Editor(GetType(MatDateEditor), _
    GetType(System.Drawing.Design.UITypeEditor)), Order(1), Provider(ProviderAttribute.ProviderEnum.peMatDate), DisplayName("Maturity Date")> Public Overridable Property MaturityDate() As Date
        Get
            Return pMaturityDate
        End Get
        Set(ByVal value As Date)
            If InApp Then
                If pIntTermDate = pMaturityDate Or pIntTermDate = Nothing Then
                    pIntTermDate = value
                    RefreshGrid()
                End If

                pMaturityDate = value
            End If
        End Set
    End Property

    Private pRedemptionAmount As New Cash    'If there is no instance of an object then its properties won't show in the property grid.
    <DisplayName("Redemption Amount"), Order(2), Copier(ProviderAttribute.ProviderEnum.peNomAmount), Maturity()> _
    Public Overridable Property RedemptionAmount() As Cash
        Get
            Return pRedemptionAmount
        End Get
        Set(ByVal value As Cash)
            If InApp Then pRedemptionAmount = value
        End Set
    End Property

    Private pCallDates As New DatesSelection
    <Maturity(), Order(4), _
    DisplayName("Call Options")> Public Overridable Property CallDates() As DatesSelection
        Get
            Return pCallDates
        End Get
        Set(ByVal value As DatesSelection)
            If InApp Then pCallDates = value
        End Set
    End Property

    Private pPutDates As New DatesSelection
    <Maturity(), Order(3), _
    DisplayName("Put Options")> Public Overridable Property PutDates() As DatesSelection
        Get
            Return pPutDates
        End Get
        Set(ByVal value As DatesSelection)
            If InApp Then pPutDates = value
        End Set
    End Property
#End Region

#Region "Interest"

    Private pIntOption As Nullable(Of RateBasisType)
    <InterestAttribute(), Order(-100), _
    DisplayName("Interest Basis"), TypeConverter(GetType(RateBasisConverter))> Public Property IntOption() As Nullable(Of RateBasisType)
        Get
            Return pIntOption
        End Get
        Set(ByVal value As Nullable(Of RateBasisType))
            If InApp Then pIntOption = value
            RefreshGrid()
        End Set
    End Property

    Public Enum RateBasisType
        Zero
        Fixed
        Floating
    End Enum
    Friend Class RateBasisConverter
        Inherits EnumStringConverter

        Public Sub New()
            With MyBase.ListItems
                .Add("Zero Interest")
                .Add("Fixed Rate")
                .Add("Floating Rate")
            End With
        End Sub

        Friend Overrides Function ConvertIntToEnum(ByVal x As Integer) As Object
            Return CType(x, RateBasisType)
        End Function

    End Class

    Private pIntCommencementDate As Date
    <Copier(ProviderAttribute.ProviderEnum.peIssueDate), DisplayName("Interest Commencement Date"), Order(2), _
    FloatingRate(), FixedRate(), Interest(), Editor(GetType(MonthViewEditor), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Property IntCommencementDate() As Date
        Get
            Return pIntCommencementDate
        End Get
        Set(ByVal value As Date)
            pIntCommencementDate = value
        End Set
    End Property

    Private pIntTermDate As Date
    <DisplayName("Interest Termination Date"), Order(3), Copier(ProviderAttribute.ProviderEnum.peMatDate), _
    FloatingRate(), FixedRate(), Interest(), Editor(GetType(MonthViewEditor), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Property IntTermDate() As Date
        Get
            Return pIntTermDate
        End Get
        Set(ByVal value As Date)
            pIntTermDate = value
        End Set
    End Property

    Private pIntCalcAmount As New Cash
    <DisplayName("Interest Calculation Amount"), Order(4), Interest(), Copier(ProviderAttribute.ProviderEnum.peNomAmount), FloatingRate(), FixedRate()> Public Property IntCalcAmount() As Cash
        Get
            Return pIntCalcAmount
        End Get
        Set(ByVal value As Cash)
            pIntCalcAmount = value
        End Set
    End Property
    Private pFixedRate As Nullable(Of Decimal)
    <InterestAttribute(), FixedRate(), Order(6), _
    DisplayName("Fixed Rate"), TypeConverter(GetType(PercentPAConverter))> Public Property FixedRate() As Nullable(Of Decimal)
        Get
            Return pFixedRate
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            If InApp Then pFixedRate = value
        End Set
    End Property

    Private pMaxRate As Nullable(Of Decimal)
    <InterestAttribute(), FloatingRate(), Order(12), _
        DisplayName("Maximum Rate"), TypeConverter(GetType(PercentPAConverter))> Public Property MaxRate() As Nullable(Of Decimal)
        Get
            Return pMaxRate
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            If InApp Then pMaxRate = value
        End Set
    End Property
    Private pMinRate As Nullable(Of Decimal)
    <InterestAttribute(), FloatingRate(), Order(13), _
        DisplayName("Minimum Rate"), TypeConverter(GetType(PercentPAConverter))> Public Property MinRate() As Nullable(Of Decimal)
        Get
            Return pMinRate
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            If InApp Then pMinRate = value
        End Set
    End Property

    'Have only come across Cap and Floor rates in the context of swaps, but I suppose there's no reason why bonds/loan couldn't have them.
    Private pCapRate As Nullable(Of Decimal)
    <InterestAttribute(), FloatingRate(), Order(14), _
        DisplayName("Cap Rate"), TypeConverter(GetType(PercentPAConverter))> Public Property CapRate() As Nullable(Of Decimal)
        Get
            Return pCapRate
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            If InApp Then pCapRate = value
        End Set
    End Property
    Private pFloorRate As Nullable(Of Decimal)
    <InterestAttribute(), FloatingRate(), Order(15), _
        DisplayName("Floor Rate"), TypeConverter(GetType(PercentPAConverter))> Public Property FloorRate() As Nullable(Of Decimal)
        Get
            Return pFloorRate
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            If InApp Then pFloorRate = value
        End Set
    End Property

    Private pIntPayDates As DatesSelection
    <InterestAttribute(), Order(5), FloatingRate(), FixedRate(), DisplayName("Interest Period")> Public Property IntPayDates() As DatesSelection
        Get
            Return pIntPayDates
        End Get
        Set(ByVal value As DatesSelection)
            If InApp Then pIntPayDates = value
        End Set
    End Property

    Dim pDCF As Nullable(Of DayCountFraction)
    <InterestAttribute(), Order(11), FloatingRate(), FixedRate(), _
    DisplayName("Day Count Fraction"), TypeConverter(GetType(DCFConverter))> _
    Public Property DCF() As Nullable(Of DayCountFraction)
        Get
            Return pDCF
        End Get
        Set(ByVal value As Nullable(Of DayCountFraction))
            If InApp Then pDCF = value
        End Set
    End Property

    Public Enum DayCountFraction
        Thirty360
        ThirtyE360
        Actual360
        Actual365
        Actual365Fixed
        ActualActual
    End Enum

    Friend Class DCFConverter
        Inherits EnumStringConverter

        Public Sub New()
            'Will want to adjust this for the currency selected.
            With MyBase.ListItems
                .Add("30/360")
                .Add("30E/360")
                .Add("Actual/360")
                .Add("Actual/365")
                .Add("Actual/Actual")
            End With
        End Sub

        Friend Overrides Function ConvertIntToEnum(ByVal x As Integer) As Object
            Return CType(x, DayCountFraction)
        End Function

        Friend Function DCFToString(ByVal DCF As DayCountFraction) As String
            Return ListItems.Item(DCF)
        End Function
    End Class

    Dim pBDC As Nullable(Of BusinessDayConvention)
    <InterestAttribute(), Order(9), FloatingRate(), FixedRate(), _
    DisplayName("Business Day Convention"), DefaultValue(BusinessDayConvention.Following), TypeConverter(GetType(BDCConverter))> _
    Public Property BDC() As Nullable(Of BusinessDayConvention)
        Get
            Return pBDC
        End Get
        Set(ByVal value As Nullable(Of BusinessDayConvention))
            If InApp Then pBDC = value
        End Set
    End Property

    Public Enum BusinessDayConvention
        Following
        ModifiedFollowing
        Preceding
        FloatingRateConvention
    End Enum
    Friend Class BDCConverter
        Inherits EnumStringConverter

        Public Sub New()
            'Will want to adjust this for the currency selected.
            With MyBase.ListItems
                .Add("Following")
                .Add("Modified Following")
                .Add("Preceding")
                .Add("Floating Rate")
            End With
        End Sub

        Friend Overrides Function ConvertIntToEnum(ByVal x As Integer) As Object
            Return CType(x, BusinessDayConvention)
        End Function
    End Class

    Private pBDCents As String = ""  '="" cos I use Split on it.

    <InterestAttribute(), Order(10), FloatingRate(), FixedRate(), _
    DisplayName("Business Day Centres"), Editor(GetType(BDCCheckBoxEditor), GetType(System.Drawing.Design.UITypeEditor))> Public Property BDCents() As String
        Get
            Return pBDCents
        End Get
        Set(ByVal value As String)
            If InApp Then pBDCents = value
        End Set
    End Property

    Friend Class BDCCheckBoxEditor
        Inherits CheckedListBoxEditor
        Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object
            MyBase.ListItems = New List(Of String)
            For Each j As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
                If Not j.FinancialCentres Is Nothing Then
                    For Each s As String In j.FinancialCentres
                        MyBase.ListItems.Add(s)
                    Next
                End If
            Next
            Return MyBase.EditValue(context, provider, value)
        End Function
    End Class

    Dim pFloatRateOpt As String
    <InterestAttribute(), Order(7), FloatingRate(), _
    DisplayName("Floating Rate Option"), TypeConverter(GetType(RateOptionsStringConverter))> _
    Public Property FloatRateOpt() As String
        Get
            Return pFloatRateOpt
        End Get
        Set(ByVal value As String)
            If InApp Then pFloatRateOpt = value
        End Set
    End Property

    Private pMargin As Nullable(Of Decimal)
    <DisplayName("Margin"), FloatingRate(), Order(8), TypeConverter(GetType(PercentPAPlusMinusConverter)), _
    Interest()> Public Property Margin() As Nullable(Of Decimal)
        Get
            Return pMargin
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            pMargin = value
        End Set
    End Property
#End Region

#Region "Misc"
    '4. Miscellaneous

    Private pSubordinated As YesNo
    <DisplayName("Subordinated"), Order(10)> Public Overridable Property Subordinated() As YesNo
        Get
            Return pSubordinated
        End Get
        Set(ByVal value As YesNo)
            If InApp And value <> pSubordinated Then
                pSubordinated = value
                Me.parent.ParentCol.Sort()
                Me.parent.Parent.Refresh()
            End If

        End Set
    End Property

    Private pCrossDefault As YesNo
    <DisplayName("Cross Default"), Order(20)> Public Overridable Property CrossDefault() As YesNo
        Get
            Return pCrossDefault
        End Get
        Set(ByVal value As YesNo)
            If InApp Then pCrossDefault = value
        End Set
    End Property

    Private pNegativePledge As YesNo
    <DisplayName("Negative Pledge"), Order(30)> Public Overridable Property NegativePledge() As YesNo
        Get
            Return pNegativePledge
        End Get
        Set(ByVal value As YesNo)
            If InApp Then pNegativePledge = value
        End Set
    End Property

    Private pPayingAgent As String
    <DisplayName("Paying Agent"), Order(700)> Public Overridable Property PayingAgent() As String
        Get
            Return pPayingAgent
        End Get
        Set(ByVal value As String)
            If InApp Then pPayingAgent = value
        End Set
    End Property

    <DisplayName("Additional Terms")> Public Overrides Property AddTerms() As String
        Get
            Return MyBase.AddTerms
        End Get
        Set(ByVal value As String)
            If InApp Then MyBase.AddTerms = value
        End Set
    End Property
#End Region

End Class

#End Region

<ClassDisplayNameAttribute("&Bonds")> <Serializable()> Public Class Bonds
    Inherits Debt
    Implements Securities

    Private pISIN As String
    Public Property ISIN() As String Implements Instrument.Securities.ISIN
        Get
            Return pISIN
        End Get
        Set(ByVal value As String)
            If InApp Then pISIN = value
        End Set
    End Property

    Private Denom As New Cash    'If there is no instance of this then its properties won't show in the property grid.

    Public Property Denomination() As Cash Implements Instrument.Securities.Denom
        Get
            Return Denom
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                Denom = value
            End If
        End Set
    End Property

    Private pIssuePrice As Decimal = 100
    <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property IssuePrice() As Decimal Implements Instrument.Securities.IssuePrice
        Get
            Return pIssuePrice
        End Get
        Set(ByVal value As Decimal)
            pIssuePrice = value
            SalesProcChanged()
        End Set
    End Property

    Private pSellingCommission As Decimal = 0
    <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property SellingCommission() As Decimal Implements Instrument.Securities.SellingCommission
        Get
            Return pSellingCommission
        End Get
        Set(ByVal value As Decimal)
            pSellingCommission = value
            SalesProcChanged()
        End Set
    End Property

    Private pMUC As Decimal = 0
    <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property MUC() As Decimal Implements Instrument.Securities.MUC
        Get
            Return pMUC
        End Get
        Set(ByVal value As Decimal)
            pMUC = value
            SalesProcChanged()
        End Set
    End Property

    <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> Public Overridable ReadOnly Property NetIssuePrice() As Decimal Implements Instrument.Securities.NetIssuePrice
        Get
            Return pIssuePrice - pSellingCommission - pMUC
        End Get

    End Property

    Private Sub SalesProcChanged() Implements Instrument.Securities.SalesProcChanged

        If TypeOf Me.parent Is TransactionCon Then
            If TypeOf CType(Me.parent, TransactionCon).Consideration.Inst Is CashPayment Then
                With CType(CType(Me.parent, TransactionCon).Consideration.Inst, CashPayment).CashAmount

                    .Curr = Me.NominalAmount.Curr
                    .Amount = Me.NominalAmount.Amount * Me.NetIssuePrice / 100
                    .Suffix = Me.NominalAmount.Suffix
                End With

            End If
        End If
        RefreshGrid()
        Me.parent.Parent.Refresh()

    End Sub

    '4. Miscellaneous

    Private pCUSIP As String
    <Order(100), Editor(GetType(CUSIPChecker), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Overridable Property CUSIP() As String Implements Instrument.Securities.CUSIP
        Get
            Return pCUSIP
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCUSIP = value
                If ISIN = "" Then
                    Dim p As New CUSIPChecker
                    If p.ISGoodCODE(value) Then
                        Dim q As New ISINChecker
                        Dim s As String = value
                        If Me.parent.FromEntity.Incorp Is JurisdictionSpace.Jurisdictions.Item(PDJ.Canada) Then
                            s = "CA" & s
                        Else
                            s = "US" & s    'will just assume it's the US even if there's no indication, just by reason of it having a CUSIP code.
                        End If
                        ISIN = s & q.CheckDigit(s)
                    End If
                End If
            End If

        End Set
    End Property

    Private pListed As String
    Public Property Listed() As String Implements Instrument.Securities.Listed
        Get
            Return pListed
        End Get
        Set(ByVal value As String)
            If InApp Then pListed = value
        End Set
    End Property


End Class

<ClassDisplayNameAttribute("Bank &Deposit", "Deposit")> <Serializable()> Public Class Deposit
    Inherits Debt
End Class

<ClassDisplayNameAttribute("&Loan")> <Serializable()> Public Class Loan
    Inherits Debt

    Friend Overrides Function IssueHeading() As String
        Return "Drawdown"
    End Function
    Friend Overrides Function InterestHeading() As String
        Return "Interest"
    End Function
    Friend Overrides Function MaturityHeading() As String
        Return "Repayment"
    End Function

End Class
<ClassDisplayNameAttribute("&Commercial Paper", "CP")> <Serializable()> Public Class CP
    Inherits Debt
    Implements Securities
    Private pISIN As String
    Public Property ISIN() As String Implements Instrument.Securities.ISIN
        Get
            Return pISIN
        End Get
        Set(ByVal value As String)
            If InApp Then pISIN = value
        End Set
    End Property
    Friend Overrides Sub AddExplanation(ByVal THandler As ITopicsHandler)
        THandler.AddTopic("1-500-2569")
    End Sub
    Private Denom As New Cash    'If there is no instance of this then its properties won't show in the property grid.

    Public Property Denomination() As Cash Implements Instrument.Securities.Denom
        Get
            Return Denom
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                Denom = value
            End If
        End Set
    End Property

    Private pIssuePrice As Decimal = 100
    <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property IssuePrice() As Decimal Implements Instrument.Securities.IssuePrice
        Get
            Return pIssuePrice
        End Get
        Set(ByVal value As Decimal)
            pIssuePrice = value
            SalesProcChanged()
        End Set
    End Property

    Private pSellingCommission As Decimal = 0
    <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property SellingCommission() As Decimal Implements Instrument.Securities.SellingCommission
        Get
            Return pSellingCommission
        End Get
        Set(ByVal value As Decimal)
            pSellingCommission = value
            SalesProcChanged()
        End Set
    End Property

    Private pMUC As Decimal = 0
    <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property MUC() As Decimal Implements Instrument.Securities.MUC
        Get
            Return pMUC
        End Get
        Set(ByVal value As Decimal)
            pMUC = value
            SalesProcChanged()
        End Set
    End Property

    <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> Public Overridable ReadOnly Property NetIssuePrice() As Decimal Implements Instrument.Securities.NetIssuePrice
        Get
            Return pIssuePrice - pSellingCommission - pMUC
        End Get

    End Property

    Private Sub SalesProcChanged() Implements Instrument.Securities.SalesProcChanged

        If TypeOf Me.parent Is TransactionCon Then
            If TypeOf CType(Me.parent, TransactionCon).Consideration.Inst Is CashPayment Then
                With CType(CType(Me.parent, TransactionCon).Consideration.Inst, CashPayment).CashAmount

                    .Curr = Me.NominalAmount.Curr
                    .Amount = Me.NominalAmount.Amount * Me.NetIssuePrice / 100
                    .Suffix = Me.NominalAmount.Suffix
                End With

            End If
        End If
        RefreshGrid()
        Me.parent.Parent.Refresh()

    End Sub

    '4. Miscellaneous

    Private pCUSIP As String
    <Order(100), Editor(GetType(CUSIPChecker), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Overridable Property CUSIP() As String Implements Instrument.Securities.CUSIP
        Get
            Return pCUSIP
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCUSIP = value
                If ISIN = "" Then
                    Dim p As New CUSIPChecker
                    If p.ISGoodCODE(value) Then
                        Dim q As New ISINChecker
                        Dim s As String = value
                        If Me.parent.FromEntity.Incorp Is JurisdictionSpace.Jurisdictions.Item(PDJ.Canada) Then
                            s = "CA" & s
                        Else
                            s = "US" & s    'will just assume it's the US even if there's no indication, just by reason of it having a CUSIP code.
                        End If
                        ISIN = s & q.CheckDigit(s)
                    End If
                End If
            End If

        End Set
    End Property

    <Browsable(False)> Public Property Listed() As String Implements Instrument.Securities.Listed   'commercial paper never really listed in practice.
        Get
            Return False
        End Get
        Set(ByVal value As String)

        End Set
    End Property
End Class
<ClassDisplayNameAttribute("&Promissory Notes", "Prom Notes")> <Serializable()> Public Class PromNotes
    Inherits Debt
    'remember these are used primarily for trade finance, like a cheque. They can be onsold but I don't think would ever be listed or have eg an ISIN number so
    'not really securities. They are normally guaranteed (avalised) by a bank, but perhaps this should be added in in the normal way, with an arrow? Should I have Bill of Exchange as an instrument as well? The only thing is these are not debt obligations of the issuer, they
    'are instructions to a third party to make payment.

End Class

Public Interface Shares

End Interface

<ClassDisplayNameAttribute("&Preference Shares", , True)> <Serializable()> Public Class PrefShares
    Inherits Debt
    Implements Securities, Shares
    Private pISIN As String
    Public Property ISIN() As String Implements Instrument.Securities.ISIN
        Get
            Return pISIN
        End Get
        Set(ByVal value As String)
            If InApp Then pISIN = value
        End Set
    End Property

    Private Denom As New Cash    'If there is no instance of this then its properties won't show in the property grid.

    Public Property Denomination() As Cash Implements Instrument.Securities.Denom
        Get
            Return Denom
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                Denom = value
            End If
        End Set
    End Property

    Private pIssuePrice As Decimal = 100
    <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property IssuePrice() As Decimal Implements Instrument.Securities.IssuePrice
        Get
            Return pIssuePrice
        End Get
        Set(ByVal value As Decimal)
            pIssuePrice = value
            SalesProcChanged()
        End Set
    End Property

    Private pSellingCommission As Decimal = 0
    <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property SellingCommission() As Decimal Implements Instrument.Securities.SellingCommission
        Get
            Return pSellingCommission
        End Get
        Set(ByVal value As Decimal)
            pSellingCommission = value
            SalesProcChanged()
        End Set
    End Property

    Private pMUC As Decimal = 0
    <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property MUC() As Decimal Implements Instrument.Securities.MUC
        Get
            Return pMUC
        End Get
        Set(ByVal value As Decimal)
            pMUC = value
            SalesProcChanged()
        End Set
    End Property

    <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> Public Overridable ReadOnly Property NetIssuePrice() As Decimal Implements Instrument.Securities.NetIssuePrice
        Get
            Return pIssuePrice - pSellingCommission - pMUC
        End Get

    End Property

    Private Sub SalesProcChanged() Implements Instrument.Securities.SalesProcChanged

        If TypeOf Me.parent Is TransactionCon Then
            If TypeOf CType(Me.parent, TransactionCon).Consideration.Inst Is CashPayment Then
                With CType(CType(Me.parent, TransactionCon).Consideration.Inst, CashPayment).CashAmount

                    .Curr = Me.NominalAmount.Curr
                    .Amount = Me.NominalAmount.Amount * Me.NetIssuePrice / 100
                    .Suffix = Me.NominalAmount.Suffix
                End With

            End If
        End If
        RefreshGrid()
        Me.parent.Parent.Refresh()

    End Sub

    '4. Miscellaneous

    Private pCUSIP As String
    <Order(100), Editor(GetType(CUSIPChecker), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Overridable Property CUSIP() As String Implements Instrument.Securities.CUSIP
        Get
            Return pCUSIP
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCUSIP = value
                If ISIN = "" Then
                    Dim p As New CUSIPChecker
                    If p.ISGoodCODE(value) Then
                        Dim q As New ISINChecker
                        Dim s As String = value
                        If Me.parent.FromEntity.Incorp Is JurisdictionSpace.Jurisdictions.Item(PDJ.Canada) Then
                            s = "CA" & s
                        Else
                            s = "US" & s    'will just assume it's the US even if there's no indication, just by reason of it having a CUSIP code.
                        End If
                        ISIN = s & q.CheckDigit(s)
                    End If
                End If
            End If

        End Set
    End Property

    Private pListed As String
    Public Property Listed() As String Implements Instrument.Securities.Listed
        Get
            Return pListed
        End Get
        Set(ByVal value As String)
            If InApp Then pListed = value
        End Set
    End Property
    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.PrefShares
        End Get
    End Property

End Class
<ClassDisplayNameAttribute("&Ordinary Shares", , True)> <Serializable()> Public Class OrdShares
    Inherits Funding
    Implements Securities, Shares
    Private pISIN As String
    Public Property ISIN() As String Implements Instrument.Securities.ISIN
        Get
            Return pISIN
        End Get
        Set(ByVal value As String)
            If InApp Then pISIN = value
        End Set
    End Property

    Private Denom As New Cash    'If there is no instance of this then its properties won't show in the property grid.

    Public Property Denomination() As Cash Implements Instrument.Securities.Denom
        Get
            Return Denom
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                Denom = value
            End If
        End Set
    End Property

    Private pIssuePrice As Decimal = 100
    <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property IssuePrice() As Decimal Implements Instrument.Securities.IssuePrice
        Get
            Return pIssuePrice
        End Get
        Set(ByVal value As Decimal)
            pIssuePrice = value
            SalesProcChanged()
        End Set
    End Property

    Private pSellingCommission As Decimal = 0
    <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property SellingCommission() As Decimal Implements Instrument.Securities.SellingCommission
        Get
            Return pSellingCommission
        End Get
        Set(ByVal value As Decimal)
            pSellingCommission = value
            SalesProcChanged()
        End Set
    End Property

    Private pMUC As Decimal = 0
    <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property MUC() As Decimal Implements Instrument.Securities.MUC
        Get
            Return pMUC
        End Get
        Set(ByVal value As Decimal)
            pMUC = value
            SalesProcChanged()
        End Set
    End Property

    <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> Public Overridable ReadOnly Property NetIssuePrice() As Decimal Implements Instrument.Securities.NetIssuePrice
        Get
            Return pIssuePrice - pSellingCommission - pMUC
        End Get

    End Property

    Private Sub SalesProcChanged() Implements Instrument.Securities.SalesProcChanged

        If TypeOf Me.parent Is TransactionCon Then
            If TypeOf CType(Me.parent, TransactionCon).Consideration.Inst Is CashPayment Then
                With CType(CType(Me.parent, TransactionCon).Consideration.Inst, CashPayment).CashAmount

                    .Curr = Me.NominalAmount.Curr
                    .Amount = Me.NominalAmount.Amount * Me.NetIssuePrice / 100
                    .Suffix = Me.NominalAmount.Suffix
                End With

            End If
        End If
        RefreshGrid()
        Me.parent.Parent.Refresh()

    End Sub

    '4. Miscellaneous

    Private pCUSIP As String
    <Order(100), Editor(GetType(CUSIPChecker), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Overridable Property CUSIP() As String Implements Instrument.Securities.CUSIP
        Get
            Return pCUSIP
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCUSIP = value
                If ISIN = "" Then
                    Dim p As New CUSIPChecker
                    If p.ISGoodCODE(value) Then
                        Dim q As New ISINChecker
                        Dim s As String = value
                        If Me.parent.FromEntity.Incorp Is JurisdictionSpace.Jurisdictions.Item(PDJ.Canada) Then
                            s = "CA" & s
                        Else
                            s = "US" & s    'will just assume it's the US even if there's no indication, just by reason of it having a CUSIP code.
                        End If
                        ISIN = s & q.CheckDigit(s)
                    End If
                End If
            End If

        End Set
    End Property

    Private pListed As String
    Public Property Listed() As String Implements Instrument.Securities.Listed
        Get
            Return pListed
        End Get
        Set(ByVal value As String)
            If InApp Then pListed = value
        End Set
    End Property
    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.OrdShares
        End Get
    End Property

End Class

#Region "Cash"

<Serializable(), ClassDisplayNameAttribute("&Cash", , True), DoesNotExistPostCompletion(), TypeConverter(GetType(CashConverter))> Public Class CashPayment
    Inherits FundingBase    'so it appears in menu with fundings.
    Private WithEvents pCashAmount As New Cash

    'These could be initial exchanges of principal in swaps.

    <DisplayName("Onward Payments"), Onward(True), Editor(GetType(CashDisplayer), GetType(System.Drawing.Design.UITypeEditor))> Public ReadOnly Property NextCash() As List(Of CashPayment)

        'TODO Can't have this and PrevCash as Cash because then the CustomExpandableObjectConverter of Instrument allows this to be expandable as well and it can look a mess and v. confusing.
        Get
            Dim k As New List(Of CashPayment)
            Dim c As Currency = Me.pCashAmount.Curr
            If Not c Is Nothing Then
                Dim h As UnderlyingsCol = parent.Parent.SelectedBLs.GetEToEConsReEntity(Me.parent.ToEntity, False, True)  'but doesn't have to be selected!
                For Each i As UnderlyingBaseLine In h
                    Dim ch As CashPayment = TryCast(i.Underlying.Inst, CashPayment)
                    If Not ch Is Nothing Then
                        If ch.pCashAmount.Curr Is c Then k.Add(i.Underlying.Inst)
                    End If
                Next
            End If
        End Get

    End Property

    <DisplayName("Source Payments"), Onward(False), Editor(GetType(CashDisplayer), GetType(System.Drawing.Design.UITypeEditor))> Public ReadOnly Property PrevCash() As List(Of CashPayment)

        Get

        End Get

    End Property

    '****Parent could be an EToECon or a Swap. Will have issues.

    Friend Overrides Sub GetLaw()
        'Probably Currency needs to become an interface like Jurisdiction?
        '  LawCat = LawCategory.cDeposit
        '      s.Implemented = not TypeOf CObj is appjur
        ' CObj.DepositTaking(me)
    End Sub

    <DisplayName("Amount"), Order(1)> Public Property CashAmount() As Cash
        Get
            Return pCashAmount
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                pCashAmount = value
                RefreshCanvas()

            End If
        End Set
    End Property

    <DisplayName("Selected Total"), Order(100), CashTotal()> Public ReadOnly Property Total() As String

        'Return the total of all cash amounts selected. If some are of different currencies, show the amounts delimited with commas.
        'It would seem logical to make this property Shared, but then it won't appear in the PropertyGrid.
        'For display only when more than one cash object is selected - this is handled by the CustomExpandableObjectConverter
        Get

            If Not frmMDI.ActiveDeal Is Nothing Then
                'Sort all payments into a Dictionary of Lists, based on their currencies.
                Dim v As New Dictionary(Of Currency, List(Of Cash))

                Dim cblank As New Currency("")  'device to allow me to add payments where no currency is specified.

                Dim c As Currency
                For Each b As BaseLine In frmMDI.ActiveDeal.picCanvas.SelectedBLs
                    Dim ch As CashPayment = TryCast(b.Inst, CashPayment)
                    If Not ch Is Nothing Then   'if something isn't cash then this will return a value but the converter will hide it.
                        c = ch.CashAmount.Curr
                        If c Is Nothing Then c = cblank

                        If Not v.ContainsKey(c) Then
                            v.Add(c, New List(Of Cash))
                        End If

                        v.Item(c).Add(ch.CashAmount)
                    End If
                Next

                'Add them all up and list them.
                Dim s As String = ""
                For Each c2 As Currency In v.Keys
                    Dim t As New Cash
                    If Not c2 Is cblank Then t.Curr = c2 'the payments will have no currency so I want t to be the same.
                    For Each p As Cash In v.Item(c2)
                        t = t + p
                    Next
                    If s = "" Then
                        s = t.ToString
                    Else
                        s = s & ", " & t.ToString
                    End If
                Next
                Return s
            End If

        End Get
    End Property

    Private bRetentionByAmount As Boolean
    Private WithEvents pRetentionAmount As New Cash

    <DisplayName("Retention Amount"), Order(2), CashRetention()> Public Property RetentionAmount() As Cash
        Get
            Return pRetentionAmount
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                bRetentionByAmount = True
                pRetentionAmount = value
            End If
        End Set
    End Property

    Private pRetentionPercent As Decimal
    <DisplayName("Retention Percent"), Order(3), CashRetention(), TypeConverter(GetType(PercentConverter))> Public Property RetentionPercent() As Decimal
        Get
            Return pRetentionPercent
        End Get
        Set(ByVal value As Decimal)
            If InApp Then
                bRetentionByAmount = False
                pRetentionPercent = value
                If pCashAmount.Amount.HasValue Then pRetentionAmount.Amount = pCashAmount.Amount.Value * value
                RefreshGrid()
            End If
        End Set
    End Property

    'Override Additional Terms here so they don't appear.
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property AddTerms() As String

        Get
            Return MyBase.AddTerms
        End Get
        Set(ByVal value As String)
            If InApp Then MyBase.AddTerms = value
        End Set
    End Property

    'Don't want Governing Law either.
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property GoverningLaw() As IJurisdiction
        Get
            Return MyBase.GoverningLaw
        End Get
        Set(ByVal value As IJurisdiction)
            If InApp Then MyBase.GoverningLaw = value
        End Set
    End Property

    'Or Name
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property Name() As String
        Get
            Return MyBase.Name
        End Get
        Set(ByVal value As String)
            If InApp Then MyBase.Name = value
        End Set
    End Property

    Friend Function PropGridToString() As String
        'Display text for property grid items.
        If ToString() = "" Then
            Return parent.FromEntity.Name & " - " & parent.ToEntity.Name & " Payment"
        Else
            Return ToString()
        End If
    End Function

    Public Overrides Function ToString() As String
        ToString = pCashAmount.ToString

    End Function

    Public Sub New()    'must be Public if I want to use Activator.
        CashAmount.UpdateCanvasForChanges = True
        CashAmount.Parent = Me
    End Sub

    Private Sub pCashAmount_Changed(ByVal OldValue As Cash) Handles pCashAmount.Changed
        If bRetentionByAmount Then
            RetentionAmount = pRetentionAmount  'reset the percentage
        Else
            RetentionPercent = pRetentionPercent
        End If
        AdjustPrevAndNext()
        RefreshGrid()
        RefreshCanvas()
    End Sub

    Private Sub pRetentionAmount_Changed(ByVal OldValue As Cash) Handles pRetentionAmount.Changed
        bRetentionByAmount = True
        pRetentionPercent = pRetentionAmount.Amount.Value / pCashAmount.Amount.Value
        AdjustPrevAndNext()
        RefreshGrid()
    End Sub

    Private Sub AdjustPrevAndNext()
        'Needs more work. Difficulty is to avoid a loop.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

Friend Class CashDisplayer
    Inherits BaseLineDisplayer
    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        Dim ch As CashPayment = TryCast(context.Instance, CashPayment)

        If Not ch Is Nothing Then    'should be

            Dim at As OnwardAttribute = context.PropertyDescriptor.Attributes(GetType(OnwardAttribute))
            If Not at Is Nothing Then
                Dim RelCash As List(Of CashPayment) = at.RelCash(ch)
                If Not RelCash Is Nothing Then
                    '    Return MyBase.EditValue(context, provider, RelCash.parent) 'TODO - swaps?

                End If

            End If

        End If

    End Function

End Class

Public Class IEPAttribute
    Inherits Attribute 'need to add this to IEP amount.TODO
End Class

Public Class OnwardAttribute

    'Marker to show whether this is the onward payment or not.
    Inherits Attribute
    Private IsOnward As Boolean
    Public Sub New(ByVal bOnward As Boolean)
        IsOnward = bOnward
    End Sub
    Public Function RelCash(ByVal c As CashPayment) As List(Of CashPayment)

        If IsOnward Then
            RelCash = c.PrevCash
        Else
            RelCash = c.NextCash
        End If
    End Function

End Class

Public Class BaseLineAttribute
    Inherits Attribute
End Class

#End Region
#End Region

#Region "Receivables"
Public MustInherit Class Receivables
    Inherits Funding

End Class

<ClassDisplayNameAttribute("&Trade Receivables", , True)> <Serializable()> Public Class Trade
    Inherits Receivables

    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.Debt
        End Get
    End Property
End Class

<ClassDisplayNameAttribute("&Intellectual Property Receivables")> <Serializable()> Public Class IntProp
    Inherits Receivables

    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.Debt
        End Get
    End Property
End Class

<ClassDisplayNameAttribute("&Credit Card Receivables")> <Serializable()> Public Class CreditCard
    Inherits Receivables

    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.Debt
        End Get
    End Property
End Class

<ClassDisplayNameAttribute("&Mortgage Receivables")> <Serializable()> Public Class MortgageRecs
    Inherits Receivables

    Friend Overrides ReadOnly Property IntrinsicRanking() As Funding.Rank
        Get
            Return Rank.Debt
        End Get
    End Property
End Class

#End Region

#Region "Swaps"

<ClassDisplayNameAttribute(, "Swap"), Serializable()> Public Class SwapLeg   'need to specify no toolstripitemattribute so it won't appear in funding menu.

    Inherits Loan   'Hardly intuitive, but the loan has interest properties and masks all the securities properties like ISIN codes so I think this will work well.

    Private pIEAByPartyA As Boolean = True 'Whether the initial exchange amount of this leg is payable by PartyA

    Friend Overloads Property parent() As SwapCon
        Get
            Return MyBase.parent
        End Get
        Set(ByVal value As SwapCon)
            MyBase.parent = value
        End Set
    End Property

    Friend Overrides Function IssueHeading() As String
        Return "Initial Exchange"
    End Function
    Friend Overrides Function InterestHeading() As String
        Return "Periodic Payments"
    End Function
    Friend Overrides Function MaturityHeading() As String
        Return "Final Exchange"
    End Function

    Friend Overrides Sub GetLaw()
        CurrentJur = Me.FromEntity.Incorp

        With CurrentJur

            frmMDI.LawCat = frmMDI.LawCategory.cInsolvency
            .SwapInsolvency(Me, frmMDI)

            frmMDI.LawCat = frmMDI.LawCategory.cAccounting
            .SwapAccounting(Me, frmMDI)

            frmMDI.LawCat = frmMDI.LawCategory.cRegCapTreat
            .SwapCapReq(Me, frmMDI)

            frmMDI.LawCat = frmMDI.LawCategory.cStampDuty
            .SwapStampDuty(Me, frmMDI)

            frmMDI.LawCat = frmMDI.LawCategory.cWHT
            .SwapWHT(Me, frmMDI)

        End With

        MyBase.GetLaw()
    End Sub

    <Browsable(False)> Public Shadows ReadOnly Property Consideration() As SwapLeg
        'Overriding Instrument's Consideration wouldn't work, because this Property would have to be an Instrument as well.
        Get
            Return parent.Consideration.Inst
        End Get

    End Property

    <DisplayName("Party A"), RefreshProperties(RefreshProperties.All), _
        TypeConverter(GetType(PartyABConverter))> Public Property IEAByPartyA() As Boolean

        Get
            Return pIEAByPartyA
        End Get
        Set(ByVal value As Boolean)
            SetIEAByPartyA(value)
        End Set

    End Property

    <DisplayName("Party B"), RefreshProperties(RefreshProperties.All), _
    TypeConverter(GetType(PartyABConverter))> Public Property IEAByPartyB() As Boolean

        Get
            Return Not pIEAByPartyA
        End Get
        Set(ByVal value As Boolean)
            SetIEAByPartyA(Not value)
        End Set

    End Property

    Protected Overrides Sub NomAmount_Changed(ByVal OldValue As Cash)
        MyBase.NomAmount_Changed(OldValue)
        RefreshCanvas()
    End Sub

    Private Sub SetIEAByPartyA(ByVal value As Boolean)
        If InApp Then
            pIEAByPartyA = value
            If Not Consideration.IEAByPartyA = value Then
                Consideration.SetIEAByPartyA(value)
            End If
        End If
    End Sub

    Private pTradeDate As Date

    <DisplayName("Trade Date"), IssueAttribute()> Public Property TradeDate() As Date
        Get
            Return pTradeDate
        End Get
        Set(ByVal value As Date)
            If InApp Then
                pTradeDate = value
                If Not Consideration.TradeDate = pTradeDate Then Consideration.TradeDate = pTradeDate
            End If
        End Set

    End Property
    Public Overrides Function ToString() As String
        Return Me.NominalAmount.ToString    'will need to be different for repayment view.
    End Function

    <Browsable(EditorBrowsableState.Never)> Public Overrides Property NominalAmount() As Cash
        Get
            Return pNotionalAmount.CashAmount
        End Get
        Set(ByVal value As Cash)
            'Don't think I want to do anything.
        End Set
    End Property

    Private pNotionalAmount As New CashPayment
    <DisplayName("Notional Amount"), IEP()> Public Property NotionalAmount() As CashPayment
        'Need this to be Cash so it can be passed through. Loan doesn't include this. 
        Get
            Return pNotionalAmount
        End Get
        Set(ByVal value As CashPayment)
            If InApp Then pNotionalAmount = value
            RefreshCanvas()
        End Set
    End Property

    <DisplayName("Reference")> Public Overrides Property Name() As String
        Get
            Return MyBase.Name
        End Get
        Set(ByVal value As String)
            If InApp Then
                MyBase.Name = value
                If Not Consideration.Name = value Then Consideration.Name = value
            End If

        End Set
    End Property

    <DisplayName("Effective Date")> Public Overrides Property IssueDate() As Date
        Get
            Return MyBase.IssueDate
        End Get
        Set(ByVal value As Date)
            If InApp Then MyBase.IssueDate = value
        End Set
    End Property

    <DisplayName("Termination Date")> Public Overrides Property MaturityDate() As Date
        Get
            Return MyBase.MaturityDate
        End Get
        Set(ByVal value As Date)
            If InApp Then MyBase.MaturityDate = value
        End Set
    End Property

    <DisplayName("Final Exchange Amount")> Public Overrides Property RedemptionAmount() As Cash
        Get
            Return MyBase.RedemptionAmount
        End Get
        Set(ByVal value As Cash)
            If InApp Then MyBase.RedemptionAmount = value
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property Subordinated() As Instrument.YesNo
        Get
            Return MyBase.Subordinated
        End Get
        Set(ByVal value As Instrument.YesNo)
            MyBase.Subordinated = value
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property NegativePledge() As Instrument.YesNo
        Get
            Return MyBase.NegativePledge
        End Get
        Set(ByVal value As Instrument.YesNo)
        End Set
    End Property

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property CrossDefault() As Instrument.YesNo
        Get
            Return MyBase.CrossDefault
        End Get
        Set(ByVal value As Instrument.YesNo)
        End Set
    End Property

    Private pCalculationAgent As String
    <DisplayName("Calculation Agent")> Public Property CalculationAgent() As String
        Get
            Return pCalculationAgent
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCalculationAgent = value
                If Not Consideration.CalculationAgent = value Then Consideration.CalculationAgent = value
            End If

        End Set
    End Property

    <TypeConverter(GetType(SwapGovLawConverter))> Public Overrides Property GoverningLaw() As IJurisdiction
        Get
            Return pGoverningLaw
        End Get
        Set(ByVal value As IJurisdiction)
            If InApp Then
                pGoverningLaw = value
                If Not Consideration.GoverningLaw Is value Then Consideration.GoverningLaw = value 'watch out for infinite loops
            End If

        End Set
    End Property

    Friend Class SwapGovLawConverter
        Inherits ObjectListConverter

        Public Sub New()
            ReDim MyBase.ListItems(1)
            MyBase.ListItems(0) = JurisdictionSpace.Jurisdictions.Item(PDJ.UKEngWales)
            MyBase.ListItems(1) = JurisdictionSpace.Jurisdictions.Item(PDJ.USANewYork)
        End Sub
    End Class

    Friend Sub New()
        MyBase.NominalAmount.UpdateCanvasForChanges = True
    End Sub

End Class

#End Region

#Region "Credit Suport"

<Serializable()> Public Class CreditSupport
    Inherits FromEInstrument

    'See "Dual Property Structure" Word doc for explanation of need for this.
    Public Function SupportedInstrument() As FromEInstrument
        Return Parent.SupportedObligation.Inst
    End Function

    <DisplayName("Supported Obligation"), BaseLine()> Public ReadOnly Property SupportedObligationForPropGrid() As Connection

        Get
            Return Parent.SupportedObligation
        End Get

    End Property

    Private Overloads Property Parent() As CreditSupportCon

        Get
            Return MyBase.parent
        End Get
        Set(ByVal value As CreditSupportCon)
            MyBase.parent = value
        End Set

    End Property

    Friend Overrides Sub GetLaw()
        If Not Me.GetType Is GetType(CreditSupport) Then  'only show law for derived instruments.

            CurrentJur = Me.FromEntity.Incorp
            If Not CurrentJur Is Nothing Then
                With CurrentJur

                    frmMDI.LawCat = frmMDI.LawCategory.cInsolvency
                    .CredSupporterInsolvency(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cAccounting
                    .CredSupporterAccounting(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cRegCapTreat
                    .CredSupporterCapReq(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cStampDuty
                    .CredSupporterStampDuty(Me, frmMDI)

                    frmMDI.LawCat = frmMDI.LawCategory.cWHT
                    .CredSupporterWHT(Me, frmMDI)

                End With
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

<ClassDisplayNameAttribute("&Guarantee")> <Serializable()> Public Class Guarantee
    Inherits CreditSupport

End Class
<ClassDisplayNameAttribute("&Bank Guarantee")> <Serializable()> Public Class BankGuarantee
    Inherits CreditSupport

End Class
<ClassDisplayNameAttribute("&Keepwell Agreement")> <Serializable()> Public Class KeepWell
    Inherits CreditSupport

End Class

#End Region

#Region "Pass-throughs"

<Serializable(), TypeConverter(GetType(PassThroughConverter))> Public Class PassThrough
    Inherits EToEInstrument

    Friend Overrides Sub GetLaw()

        If Not Me.GetType Is GetType(PassThrough) Then  'only show law for derived instruments.

            Dim BaseFunding As New List(Of EToEInstrument)

            For Each e As UnderlyingBaseLine In parent.Underlyings
                If Not e.Underlying.Inst Is Nothing Then BaseFunding.Add(e.Underlying.Inst) 'TODO - isn't idea to go right to the foot of the chain?
            Next

            CurrentJur = Me.FromEntity.Incorp

            If Not BaseFunding Is Nothing Then
                If Not CurrentJur Is Nothing Then
                    With CurrentJur

                        frmMDI.LawCat = frmMDI.LawCategory.cInsolvency
                        .SellerInsolvency(Me, BaseFunding, frmMDI)

                        frmMDI.LawCat = frmMDI.LawCategory.cAccounting
                        .SellerAccountingOffset(Me, BaseFunding, frmMDI)

                        If FromEntity.HasCapReq Then
                            frmMDI.LawCat = frmMDI.LawCategory.cRegCapTreat
                            .SellerRegulatoryOffset(Me, BaseFunding, frmMDI)
                        End If

                        frmMDI.LawCat = frmMDI.LawCategory.cRecharacterisation
                        .Recharacterisation(Me, BaseFunding, frmMDI)

                    End With
                End If

                CurrentJur = Me.ToEntity.Incorp

                frmMDI.LawCat = frmMDI.LawCategory.cAccountConsol
                If Not CurrentJur Is Nothing Then
                    CurrentJur.InvestorAccounting(Me, BaseFunding, frmMDI)

                    For Each i As EToEInstrument In BaseFunding

                        CurrentJur = i.parent.FromEntity.Incorp

                        With CType(CurrentJur, IPassThroughLaw)

                            If TypeOf BaseFunding Is Shares Then
                                frmMDI.LawCat = frmMDI.LawCategory.cShareDisc
                                .ShareHoldingDisclosure(Me, BaseFunding, frmMDI)
                                If i.FromEntity Is ToEntity() Then
                                    frmMDI.LawCat = frmMDI.LawCategory.cOwnSharePurch
                                    .OwnSharePurchase(Me, BaseFunding, frmMDI)
                                End If
                                '   .StampDuty objX, BaseFunding, TopicHandler
                            End If

                        End With
                    Next
                End If
            End If
        End If

    End Sub

    Friend Overrides Function TypeIsEnabled(ByVal t As System.Type) As Boolean

        'I suppose it would be nice to contain these in the relevant instrument types. But then I have to keep
        'creating them, which takes some time. 

        If t Is GetType(FlawedAsset) Then
            Return FromEntity.Bank
        ElseIf t Is GetType(Trust) Then
            If Not Me.FromEntity.Incorp Is Nothing Then
                Select Case Me.FromEntity.Incorp.TrustRecognition
                    Case IJurisdiction.TrustRecognitionStatus.TrustsRecognised
                        Return True
                    Case IJurisdiction.TrustRecognitionStatus.TrustsNotRecognised
                        Return False
                    Case IJurisdiction.TrustRecognitionStatus.TrustsRecognisedByFiduciaryOnly
                        Return FromEntity.Fiduciary
                End Select
                Return True
            End If
        ElseIf t Is GetType(Sale) Or t Is GetType(Asstw) Or t Is GetType(Asstwo) Then

            Dim SomeSecs As Boolean
            For Each k As UnderlyingBaseLine In parent.Underlyings
                If TypeOf k.Underlying.Inst Is Instrument.Securities Then SomeSecs = True
            Next

            If t Is GetType(Sale) Then
                Return SomeSecs 'if there's a mix, I think showing Sale is the simplest.
            Else
                Return Not SomeSecs
            End If

        End If

        Return MyBase.TypeIsEnabled(t)

    End Function

    'Box that displays the collection of underlying instruments is unwieldy, so I'd like to only display that if needed, 
    'i.e. there's more than one underlying instrument.
    <DisplayName("Underlying Instruments")> Public Overridable ReadOnly Property Underlyings() As UnderlyingsCol

        Get
            If parent.Underlyings.Count > 1 Then Return parent.Underlyings
        End Get

    End Property

    <DisplayName("Underlying Instrument")> Public Overridable Property UnderlyingInst() As UnderlyingBaseLine

        Get
            If parent.Underlyings.Count = 1 Then Return parent.Underlyings.Item(0)
        End Get
        Set(ByVal value As UnderlyingBaseLine)
            '???????
        End Set

    End Property

    Public Sub New()
        'It creates a lot of problems if I try to set the underlyings here because then I need matching constructors for all inheritors and then
        'I get problems with Activator.CreateInstance.
    End Sub
End Class

<ClassDisplayNameAttribute("&Sale"), DoesNotExistPostCompletion()> <Serializable()> Public Class Sale
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Assignment with Notice"), DoesNotExistPostCompletion()> <Serializable()> Public Class Asstw
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("Assignment with&out Notice")> <Serializable()> Public Class Asstwo
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Credit Linked Notes", , True)> <System.Serializable()> Public Class CredLink
    Inherits PassThrough
    Implements Securities
    Private pISIN As String
    Public Property ISIN() As String Implements Instrument.Securities.ISIN
        Get
            Return pISIN
        End Get
        Set(ByVal value As String)
            If InApp Then pISIN = value
        End Set
    End Property

    Private Denom As New Cash    'If there is no instance of this then its properties won't show in the property grid.

    Public Property Denomination() As Cash Implements Instrument.Securities.Denom
        Get
            Return Denom
        End Get
        Set(ByVal value As Cash)
            If InApp Then
                Denom = value
            End If
        End Set
    End Property

    Private pIssuePrice As Decimal = 100
    <DisplayName("Issue Price"), Order(4), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property IssuePrice() As Decimal Implements Instrument.Securities.IssuePrice
        Get
            Return pIssuePrice
        End Get
        Set(ByVal value As Decimal)
            pIssuePrice = value
            SalesProcChanged()
        End Set
    End Property

    Private pSellingCommission As Decimal = 0
    <DisplayName("Selling Commission"), Order(5), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property SellingCommission() As Decimal Implements Instrument.Securities.SellingCommission
        Get
            Return pSellingCommission
        End Get
        Set(ByVal value As Decimal)
            pSellingCommission = value
            SalesProcChanged()
        End Set
    End Property

    Private pMUC As Decimal = 0
    <DisplayName("M && U Commission"), Order(6), IssueAttribute(), TypeConverter(GetType(PercentConverter))> Public Overridable Property MUC() As Decimal Implements Instrument.Securities.MUC
        Get
            Return pMUC
        End Get
        Set(ByVal value As Decimal)
            pMUC = value
            SalesProcChanged()
        End Set
    End Property

    <DisplayName("Net Issue Price"), IssueAttribute(), Order(7), TypeConverter(GetType(PercentConverter))> Public Overridable ReadOnly Property NetIssuePrice() As Decimal Implements Instrument.Securities.NetIssuePrice
        Get
            Return pIssuePrice - pSellingCommission - pMUC
        End Get

    End Property

    Private Sub SalesProcChanged() Implements Instrument.Securities.SalesProcChanged

        'If TypeOf Me.parent Is TransactionCon Then
        '    If TypeOf CType(Me.parent, TransactionCon).Consideration.Inst Is Cash Then
        '        With CType(CType(Me.parent, TransactionCon).Consideration.Inst, Cash).CashAmount

        '            .Curr = Me.NominalAmount.Curr
        '            .Amount = Me.NominalAmount.Amount * Me.NetIssuePrice / 100
        '            .Suffix = Me.NominalAmount.Suffix
        '        End With

        '    End If
        'End If
        'RefreshGrid()
        'Me.parent.Parent.Refresh()

    End Sub

    '4. Miscellaneous

    Private pCUSIP As String
    <Order(100), Editor(GetType(CUSIPChecker), _
    GetType(System.Drawing.Design.UITypeEditor))> Public Overridable Property CUSIP() As String Implements Instrument.Securities.CUSIP
        Get
            Return pCUSIP
        End Get
        Set(ByVal value As String)
            If InApp Then
                pCUSIP = value
                If ISIN = "" Then
                    Dim p As New CUSIPChecker
                    If p.ISGoodCODE(value) Then
                        Dim q As New ISINChecker
                        Dim s As String = value
                        If Me.parent.FromEntity.Incorp Is JurisdictionSpace.Jurisdictions.Item(PDJ.Canada) Then
                            s = "CA" & s
                        Else
                            s = "US" & s    'will just assume it's the US even if there's no indication, just by reason of it having a CUSIP code.
                        End If
                        ISIN = s & q.CheckDigit(s)
                    End If
                End If
            End If

        End Set
    End Property

    Private pListed As String
    Public Property Listed() As String Implements Instrument.Securities.Listed
        Get
            Return pListed
        End Get
        Set(ByVal value As String)
            If InApp Then pListed = value
        End Set
    End Property
End Class

<ClassDisplayNameAttribute("Sub&participation")> <Serializable()> Public Class Subpart
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("Total &Return Swap", "TRS")> <Serializable()> Public Class TRSPass
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Trust", , True)> <Serializable()> Public Class Trust
    Inherits PassThrough

    Friend Overrides Function Supportable() As Boolean
        'Can't give a guarantee or security for a trust obligation.
        Return False
    End Function

    Friend Overrides Sub AddExplanation(ByVal THandler As ITopicsHandler)
        THandler.AddTopic("4-107-4432")
    End Sub
End Class

<ClassDisplayNameAttribute("&Novation"), DoesNotExistPostCompletion()> <Serializable()> Public Class Novation
    Inherits PassThrough
    Friend Overrides Sub AddExplanation(ByVal THandler As ITopicsHandler)
        THandler.AddTopic("5-381-7510")
    End Sub
End Class

<ClassDisplayNameAttribute("Subro&gation")> <Serializable()> Public Class Subrogation
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Flawed Asset")> <Serializable()> Public Class FlawedAsset
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Depositary Receipts", "Dep Recs")> <Serializable()> Public Class DepRecs
    Inherits PassThrough

End Class

<ClassDisplayNameAttribute("&Cancellation", "Cancellation", True)> <Serializable(), DoesNotExistPostCompletion()> Public Class Cancellation
    Inherits PassThrough
    'if someone is passing an obligation back to the obligor, he might be selling it back, or very possibly, cancelling it.
    'e.g. debt for equity swap.
    'This can't be passed through in any meaningful sense itself. Need to think how I deal with that in code.

End Class

#End Region

#Region "Security and Quasi-security"

<Serializable()> Public Class SecurityBase

    Inherits Instrument

    Friend Overloads Property Parent() As SecurityArc
        Get
            Return MyBase.Parent
        End Get
        Set(ByVal value As SecurityArc)
            MyBase.Parent = value
        End Set
    End Property

    Friend Overrides Function TypeIsEnabled(ByVal t As System.Type) As Boolean

        If Not SecuredObligation() Is Nothing And TypeOf Me.Parent Is LineArc Then
            Dim sop As Connection = TryCast(SecuredObligation.parent, Connection)
            If t Is GetType(SetOff) And Not sop Is Nothing Then
                Return CType(Me.Parent, LineArc).Collateral.FromEntity Is sop.ToEntity
            End If
        End If

        Return True

    End Function

    Public Function SecuredObligation() As FromEInstrument

        If Not Parent.ToConnection Is Nothing Then
            Return Parent.ToConnection.Inst
        End If

    End Function

    ''The next two exist for the property grid. 
    '<DisplayName("Charged Assets"), BaseLine()> Public ReadOnly Property ChargedAssetsForPropGrid() As BaseLine

    '    Get
    '        Return Parent.FromConnection
    '    End Get

    'End Property

    <DisplayName("Secured Obligation"), BaseLine()> Public ReadOnly Property SecuredObligationForPropGrid() As BaseLine

        Get
            Return Parent.ToConnection
        End Get

    End Property

    Friend Overrides Sub GetLaw()

        If Not Me.GetType Is GetType(SecurityBase) Then  'only show law for derived instruments.

            If TypeOf Me.Parent Is LineArc Then

                Dim p As LineArc = Me.Parent

                CurrentJur = p.FromConnection.FromEntity.Incorp
                With CurrentJur
                    frmMDI.LawCat = frmMDI.LawCategory.cInsolvency
                    .CollateralInsolvency(Me, frmMDI)
                End With

                If Not p.Collateral() Is Nothing Then
                    CurrentJur = p.Collateral.FromEntity.Incorp
                    With CurrentJur
                        frmMDI.LawCat = frmMDI.LawCategory.cStampDuty
                        .CollateralAssetsStamp(Me, frmMDI)

                        If TypeOf p.Collateral() Is Shares Then
                            frmMDI.LawCat = frmMDI.LawCategory.cShareDisc
                            .CollateralShareDisc(Me, frmMDI)
                        End If

                        frmMDI.LawCat = frmMDI.LawCategory.cAdditional
                        .CollateralAdditional(Me, frmMDI)

                    End With
                End If

            End If
        End If

    End Sub

    Private pTradeDate As Date

    <DisplayName("Contract Date"), IssueAttribute()> Public Overridable Property TradeDate() As Date
        Get
            Return pTradeDate
        End Get
        Set(ByVal value As Date)
            If InApp Then pTradeDate = value
        End Set

    End Property

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub

    Public Overrides Function ToString() As String
        Return MyBase.ToString()

    End Function
End Class

<Serializable(), ClassDisplayNameAttribute("&Fixed Charge")> Public Class FixedCharge
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Fixed Charge"
    End Function

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property TradeDate() As Date
        Get
            Return Nothing
        End Get
        Set(ByVal value As Date)
        End Set
    End Property

    Friend Overrides Sub AddExplanation(ByVal THandler As ITopicsHandler)
        THandler.AddTopic("0-107-5768")
    End Sub

End Class

<Serializable(), ClassDisplayNameAttribute("&Floating Charge")> Public Class FloatCharge
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Floating Charge"
    End Function

    Friend Overrides Sub AddExplanation(ByVal THandler As ITopicsHandler)
        THandler.AddTopic("0-107-5773")
    End Sub

    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Overrides Property TradeDate() As Date
        Get
            Return Nothing
        End Get
        Set(ByVal value As Date)
        End Set
    End Property

End Class

<Serializable(), ClassDisplayNameAttribute("&Repo", , True)> Public Class Repo
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Repo"
    End Function

End Class

<Serializable(), ClassDisplayNameAttribute("Stock &Loan")> Public Class StockLoan
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Stock Loan"
    End Function

End Class

<Serializable(), ClassDisplayNameAttribute("&Buy Sell-Back Agreement", "Buy-Sell Back")> Public Class BuySellBack
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Buy-Sell Back"
    End Function

End Class

<Serializable(), ClassDisplayNameAttribute("&Right of Set-Off", , True)> Public Class SetOff
    Inherits SecurityBase
    Public Overrides Function ToString() As String
        Return "Set-Off"
    End Function

End Class

Public MustInherit Class CodeChecker
    Inherits System.Drawing.Design.UITypeEditor
    Public Overrides Function GetPaintValueSupported(ByVal context As System.ComponentModel.ITypeDescriptorContext) As Boolean
        Return True
        'tried only returning True if the code was invalid, so wouldn't need to show anything when the code was OK, but whether there's a box only gets updated when the Instrument gets selected rather than when the text changes so wasn't very satisfactory.
    End Function
    Public Overrides Sub PaintValue(ByVal e As System.Drawing.Design.PaintValueEventArgs)

        If e.Value = "" Then
            MyBase.PaintValue(e)    'box will still be there but will be blank.
        Else
            Dim f As New System.Drawing.Font("Arial", 9, FontStyle.Bold)
            If IsGoodCode(e.Value) Then
                e.Graphics.DrawString("OK", f, Brushes.Black, 1, 0)
            Else
                e.Graphics.DrawString("!", f, Brushes.Red, 8, 0)
            End If
        End If

    End Sub

    Friend MustOverride Function IsGoodCode(ByVal sCode As String) As Boolean

End Class

Public Class ISINChecker
    Inherits CodeChecker
    Friend Overrides Function ISGoodCODE(ByVal sCode As String) As Boolean

        'Jelle-Jeroen Lamkamp 28 Apr 2008 

        sCode = UCase(Trim(sCode))

        If Len(sCode) <> 12 Then Return False

        If Mid(sCode, 1, 1) < "A" Or Mid(sCode, 1, 1) > "Z" Then Return False
        If Mid(sCode, 2, 1) < "A" Or Mid(sCode, 2, 1) > "Z" Then Return False

        Dim t As Integer = CheckDigit(Mid(sCode, 1, 11))

        If t = CInt(Mid(sCode, 12, 1)) Then Return True

    End Function

    Friend Function CheckDigit(ByVal s As String) As Integer
        Dim i As Integer : Dim iTotalScore As Integer
        Dim sDigits As String
        sDigits = ""
        Dim ssub As String
        For i = 1 To 11
            ssub = Mid(s, i, 1)
            If ssub >= "0" And ssub <= "9" Then
                sDigits = sDigits & ssub
            ElseIf s >= "A" And s <= "Z" Then
                sDigits = sDigits & CStr(Asc(ssub) - 55)
            Else
                Return -1
            End If
        Next i

        sDigits = StrReverse(sDigits)

        iTotalScore = 0

        For i = 1 To Len(sDigits)
            iTotalScore = iTotalScore + CInt(Mid(sDigits, i, 1))
            If i Mod 2 = 1 Then
                iTotalScore = iTotalScore + CInt(Mid(sDigits, i, 1))
                If CInt(Mid(sDigits, i, 1)) > 4 Then
                    iTotalScore = iTotalScore - 9
                End If
            End If
        Next i

        Return (10 - (iTotalScore Mod 10)) Mod 10

    End Function
End Class

Public Class CUSIPChecker
    Inherits CodeChecker

    Friend Overrides Function ISGoodCODE(ByVal sCode As String) As Boolean

        'Code from Wikipedia.
        '     'CUSIP is a nine digit alphanumeric code.

        '     Input: an 8-character CUSIP
        'Output: the check digit for that CUSIP
        sCode = UCase(Trim(sCode))

        If sCode.Length = 9 Then

            Dim sum As Integer
            Dim i As Integer
            For i = 1 To 8
                Dim s As String = ""
                s = Mid(sCode, i, 1)

                Dim v As Integer = 0

                If s >= "0" And s <= "9" Then
                    v = s
                ElseIf s >= "A" And s <= "Z" Then
                    v = v & CStr(Asc(s) - 55)
                ElseIf s = "*" Then
                    v = 36
                ElseIf s = "@" Then
                    v = 37
                ElseIf s = "#" Then
                    v = 38
                End If

                If i Mod 2 = 0 Then
                    v = v * 2
                End If

                sum = sum + Int(v / 10) + v Mod 10
            Next

            If ((10 - (sum Mod 10)) Mod 10).ToString = Mid(sCode, 9, 1) Then Return True

            Return False
        End If

    End Function
End Class

#End Region
