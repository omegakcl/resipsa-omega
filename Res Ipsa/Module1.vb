Imports System.Management
Imports System.math
Imports System.Reflection

Module Module1

    Friend Function SysID() As String

        'Return the drive serial number. I think there may be some functionality in VB that automatically _
        'generates and hides keys.
        Dim manClass As New ManagementClass("Win32_Processor")
        Dim mObjCol As ManagementObjectCollection = manClass.GetInstances
        Dim m As ManagementObject
        SysID = ""
        For Each m In mObjCol
            If SysID = "" Then
                SysID = m.Properties("ProcessorId").Value
                Exit For
            End If
        Next

    End Function

    Friend Sub FillToolStripWithInheritors(ByVal tsiCol As ToolStripItemCollection, _
    ByVal BaseType As Type, Optional ByVal DisabledType As Type = Nothing)

        'Allows all classes in an assembly inheriting from BaseType and having the ControlAttribute to have an item 
        'added to a ToolStripItemCollection with the text specified in the ControlAttribute. The type _
        'of the class is stored in the tag of the item, so it can be accessed when the item is clicked.
        'The Address of the ClickHandler could be passed here, so that could be set up too.

        'Get rid of old stuff.
        Dim h As New List(Of ToolStripItem)
        For Each j As ToolStripItem In tsiCol
            If Not TypeOf j.Tag Is String Then h.Add(j) 'can't remove here cos will mess up the enumeration.
        Next

        For Each j2 As ToolStripItem In h
            tsiCol.Remove(j2)
        Next

        Dim qn As Integer = tsiCol.Count


        For Each t As Type In BaseType.Assembly.GetTypes
            If t.IsSubclassOf(BaseType) Then

                'This test checks for whether SelectedBL is an existing obligation and the type is one that doesn't exist post completion, if so omit it.
                If Not (TypeOf CurrentBL Is Connection And Not TypeOf CurrentBL Is TransactionCon And _
                Attribute.GetCustomAttribute(t, GetType(DoesNotExistPostCompletionAttribute)) IsNot Nothing) Then

                    Dim f As ClassDisplayNameAttribute = GetToolStripItemAttribute(t)

                    If f IsNot Nothing Then
                        If f.MenuItemText <> "" Then 'this is how I flag the fact that SwapLeg is not to appear in Funding menu.
                            If CurrentBL.TypeIsVisible(t) Then
                                If f.SepBefore And tsiCol.Count <> 0 Then tsiCol.Insert(tsiCol.Count - qn, New ToolStripSeparator)

                                Dim tt As New ToolStripMenuItem(f.MenuItemText)
                                With tt
                                    .Tag = t  'there is a constructor that accepts the address of the OnClick handler, but I'm happy to use the menu or toolstrip's itemclicked event instead.
                                    If t.IsSubclassOf(GetType(Instrument)) Then .Enabled = CurrentBL.Inst.TypeIsEnabled(t)
                                    If DisabledType Is t Then .Enabled = False 'for conversion of Instruments - want to disable existing type.
                                End With

                                tsiCol.Insert(tsiCol.Count - qn, tt)

                            End If
                        End If
                    End If
                End If

            End If
        Next

    End Sub

    Friend Function GetName(ByVal c As List(Of INameObject), ByVal NewItem As INameObject, ByVal valuetype As Type) As String

        'Returns a unique name for NewItem in the collection. Can't get at the GetType directly 
        'through an interface reference. Idea is to give objects a name  a unique 
        'number to identify them.

        Dim s2 As String = ClassDisplayName(valuetype)
        If Not s2 = "" Then
            Dim n As Integer
            Dim UnusedFound As Boolean
            Do
                n = n + 1
                UnusedFound = True
                For Each t As INameObject In c
                    If Not t Is NewItem Then    'not quite sure why it should be there, but sometimes it is so this will fix it.
                        If CType(t, Object).GetType Is CType(NewItem, Object).GetType Then
                            If InStr(t.Name, s2) = 1 Then
                                If t.Name = s2 & " " & n Then UnusedFound = False
                            End If
                        End If
                    End If
                Next
            Loop Until UnusedFound = True

            Return s2 & " " & n
        End If

    End Function

    Friend Function IsIDE() As Boolean
        IsIDE = System.Diagnostics.Debugger.IsAttached
    End Function

    Friend Function Hypot(ByVal X As Long, ByVal Y As Long) As Single
        Return Math.Sqrt(X ^ 2 + Y ^ 2)
    End Function

    Friend Function Hypot(ByVal A As Point, ByVal B As Point) As Single
        Return Hypot(A.X - B.X, A.Y - B.Y)
    End Function

    Private Sub HideExcessSepsonDropDownOpening(ByVal f As ToolStripItemCollection)

        'Hide any separators that follow another.
        'Not used. I can't get this to work because all items are considered to 
        'be invisible at this stage, so I don't know if two separators would appear 
        'next to each other or not.

        'With f
        '    For n As Integer = 1 To .Count - 1
        '        If TypeOf .Item(n) Is ToolStripSeparator And _
        '        TypeOf .Item(n - 1) Is ToolStripSeparator Then
        '            .Item(n).Visible = False
        '        End If
        '    Next

        'End With

    End Sub

    Friend Const FileSuffix As String = "trd"

    Friend Function DialogFilter() As String
        Return "Transaction Diagrams (*." & FileSuffix & ")|*." & FileSuffix 'make sure this matches the extension chosen in the Setup project.
    End Function

    Friend Function AreTheSame(ByVal a As Object, ByVal b As Object) As Boolean
        'Have had errors with using Is on things that are of different types, so this may be worthwhile.
        'Problem is that I really want to be comparing the reference that an object is declared with rather than what it contains, and I don't think I can do that.

        If a Is Nothing And b Is Nothing Then
            Return True
        ElseIf a Is Nothing Or b Is Nothing Then
            Return False
        elseIf a.GetType.IsAssignableFrom(b.GetType) Or b.GetType.IsAssignableFrom(a.GetType) Then
            Return a Is b
        Else
            If Not IsIDE() Then Stop
        End If
    End Function

    Friend Function GetDriveID() As String

        Dim fso As Object
        fso = CreateObject("Scripting.FileSystemObject")
        Return Hex(fso.GetDrive("C").SerialNumber())
    End Function

End Module
