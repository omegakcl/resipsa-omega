Imports System.Windows.Forms
Imports system.Math
Imports System.net
Imports System.IO

<Serializable> Friend Class frmMDI

    Implements ITopicsHandler

    Private Loaded As Boolean       'Whether the form has been loaded.

    Public Sub New()

        'Don't make this Friend or there'll be no entry point.

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        'Global error handling. This will prevent the user from getting the unhandled error message.
        'But sometimes I might still want to do On Error Resume Next e.g. in paint routine to prevent red cross appearing.
        ' If I run with Ctrl + F5 I will not be notified of any errors. If just run with F5 execution 
        'will stop as normal.
        AddHandler Application.ThreadException, AddressOf errorsub

    End Sub

    Private Sub frmMDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.SetPropGridPnlWidth(My.Settings.TermsPanelWidth)
        Me.SetTopicsPnlHeight(My.Settings.TreeViewPanelHeight)
        Me.SetBrowserPnlWidth(My.Settings.BrowserPanelWidth)

        mnuDevelopment.Visible = IsIDE()

        ShowRecentFiles()

        tbfDraw.Text = "Draw"
        tbfMain.Text = "Main Toolbar"

        'I don't use a toolstripcontainer because then MDI children don't show up. Have four seperate panels instead.
        'There's a bug with using settings directly to set checked status of menu items - 
        'the autocode generator puts in a line of code setting the checked property with the setting and another which is what it would 
        'put in anyway as a designer. And the two get confused. Even if I take the latter out I still can't 
        'get it to work, so just work around it all like this.
        mnuTermsTermsWindow.Checked = My.Settings.TermsWindowShowing
        mnuViewMainToolbar.Checked = My.Settings.MainToolbarShowing
        mnuViewDrawingToolbar.Checked = My.Settings.DrawToolbarShowing

        'Don't set at design time or context menu will appear under mnuBrowser first time it is used by right-clicking on the browser window.
        Me.mnuBrowser.DropDown = Me.cxmBrowser
        Me.mnuTerms.DropDown = Me.cxmTerms

        SetPRGObjects()  'ensure the property grid is showing in the tabcontrol.

        For Each t As ToolStripItem In Me.tstDraw.Items
            Dim c As ToolStripButton = TryCast(t, ToolStripButton)
            If Not c Is Nothing Then
                'Draw security buttons can be selected independently. 
                If Not (t Is Me.tsiDrawSecurityExisting Or t Is Me.tsiDrawSecurityNew) Then AddHandler c.CheckStateChanged, AddressOf DrawButtonCheckStateChanged
                AddHandler c.DoubleClick, AddressOf DrawButtonDoubleClick
            End If
        Next

        'Can't simply bind settings to properties here either, cos then there's a risk that 
        'the user will close the application while minimised. Minimised will then be the normalised 
        'position (if you see what I mean), so when user normalises he'll get minimised, and he'll never 
        'be able to normalise again. The following works OK, though it doesn't remember the normalised 
        'co-ordinates from session to session if the user doesn't close the app when normalised.
        'Set as normalised at design time or the form will appear maximised before normalising if the setting is normalised.
        WindowState = My.Settings.WState
        If WindowState = FormWindowState.Normal Then
            Size = My.Settings.Size
            'Bring it fully onto the screen if it's not.
            Dim R As Point
            R = My.Settings.Location
            With R
                .X = Max(.X, 0)
                .X = Min(My.Computer.Screen.WorkingArea.Width - Size.Width, .X)
                .Y = Max(.Y, 0)
                .Y = Min(My.Computer.Screen.WorkingArea.Height - Size.Height, .Y)
            End With
            Location = R
        End If

        'Want to do this after the form size had been set.
        For Each f As ToolStripMenuItem In mnuTermsTextSize.DropDown.Items
            If CInt(f.Tag) = My.Settings.TermsTextSize Then
                f.PerformClick()
            End If
        Next

        Loaded = True

        webBrowser1_CanGoBackChanged(Nothing, Nothing)
        webBrowser1_CanGoForwardChanged(Nothing, Nothing)

        Text = My.Application.Info.AssemblyName

        For Each T As Control In prgOne.Controls    'Controls property is hidden from Intellisense.
            'Also contains a "HotCommands" control. That contains two labels and you can 
            'do what you want with them - set their forecolours, set a withevents variable _
            'to refer to one etc. Good for help files.
            'Want to have a combo above this to show the tranches. Need to ownerdraw this 
            'to show lines between debt and equity etc. So may as well owner draw it to 
            'get lines part bold and part not, as with the property grid in visual studio.

            If TypeOf T Is ToolStrip Then

                PRGTStrip = T

                With PRGTStrip


                    .CanOverflow = True
                    .AllowMerge = True

                    .Items.RemoveAt(.Items.Count - 1) 'Remove the Property Pages button.

                    .Items.RemoveAt(.Items.Count - 1)   'Remove the separator

                    ToolStripManager.Merge(tstPropGrid, T)   'Merge the confirmation toolstrip into the PropertyGrid toolstrip.

                End With
            End If

        Next

        'check each parameter to get the file name (there is only one though)
        Dim bCommandLineFile As Boolean
        For Each param As String In My.Application.CommandLineArgs

            Try
                ' pass the file path if it exists
                If System.IO.File.Exists(param) Then
                    AddNewChild(True, param, False)
                    bCommandLineFile = True
                End If

            Catch
                'do nothing, just open the application with no file

            End Try
        Next param

        If Not bCommandLineFile Then AddNewChild(True, , True) 'load the initial blank file. Must come after filling the toolstrip.

    End Sub

    Private Sub errorsub(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
        MsgBox(e.Exception.Message & vbTab & e.Exception.ToString)
        'Debug.Print is of course no good here.
    End Sub

#Region "Resizing (Form and Panels)"

    'One day it would be nice to have dockable windows/ tear off panels like in the IDE, but I reckon it could take two weeks to do.
    'Something called a SandDock by divil would probably do it.

    Private Sub frmMDI_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        If WindowState <> FormWindowState.Minimized Then
            SetBrowserPnlWidth()
            SetTopicsPnlHeight()
            SetPropGridPnlWidth()
        End If

        If Loaded Then  'this event occurs before loading, so don't want to reset settings. Created Property no good here.
            'It also occurs on unloading, so I don't really need to worry about the fact that this doesn't 
            'catch moving the form.
            'Save window size settings. See Form_Load for explanation.
            If WindowState = FormWindowState.Normal Then
                My.Settings.Location = Me.Location
                My.Settings.Size = Me.Size
            End If

            If WindowState <> FormWindowState.Minimized Then My.Settings.WState = WindowState
        End If

    End Sub

    Private Function MDIClientWidth() As Long

        MDIClientWidth = ClientSize.Width   'this dimension includes left and right docked controls, so need to adjust it to get the real client area.
        For Each i As Control In Controls
            With i
                If .Parent Is Me And (.Dock = DockStyle.Left Or .Dock = DockStyle.Right) Then MDIClientWidth = MDIClientWidth - .Width
            End With
        Next

    End Function

    Private Function MDIClientHeight() As Long

        MDIClientHeight = ClientSize.Height
        For Each i As Control In Controls
            With i
                If .Parent Is Me And (.Dock = DockStyle.Top Or .Dock = DockStyle.Bottom) Then MDIClientHeight = MDIClientHeight - .Height
            End With
        Next

    End Function

    Private Sub PanelMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlWebBrowser.MouseMove, pnltreeView.MouseMove, pnlPropGrid.MouseMove

        'Could use Splitter controls, but they don't resize while mouse is being moved, only when it comes up. Can write code to do this, 
        'but then you still see the colour-inverted strip on top of the splitter which doesn't look great. And this is easy enough.

        Dim p As Panel = sender

        With p
            If e.Button = Windows.Forms.MouseButtons.Left Then

                If .Cursor <> Cursors.Default Then  'to do with problem mentioned below.
                    Select Case .Dock
                        Case DockStyle.Left
                            SetBrowserPnlWidth(e.X)
                        Case DockStyle.Top
                            SetTopicsPnlHeight(e.Y)
                        Case DockStyle.Right
                            SetPropGridPnlWidth(.Width - e.X)
                    End Select
                End If
            ElseIf e.Button = Windows.Forms.MouseButtons.None Then
                If p Is pnlPropGrid And e.X > tbsOne.Location.X Then
                    'Area to right of tabstrip and at top is p, that doesn't count. Also a bit of the left margin of tabstrip.
                    .Cursor = Cursors.Default
                Else
                    'VSplit and HSplit cursors are a bit small, sizing ones seem better.
                    .Cursor = IIf(.Dock = DockStyle.Left Or .Dock = DockStyle.Right, Cursors.SizeWE, Cursors.SizeNS)
                End If

            End If
        End With

    End Sub

    Private Const conKeepWithinLimits = -1000

    Private Sub SetBrowserPnlWidth(Optional ByRef w As Long = conKeepWithinLimits)

        If w = -1000 Then w = pnlWebBrowser.Width
        pnlWebBrowser.Width = Max(Min(w, MDIClientWidth() + pnlWebBrowser.Width - 40), 40)
        If Loaded Then My.Settings.BrowserPanelWidth = pnlWebBrowser.Width

    End Sub

    Private Sub SetTopicsPnlHeight(Optional ByVal w As Long = conKeepWithinLimits)

        If w = conKeepWithinLimits Then w = pnltreeView.Height
        pnltreeView.Height = Max(Min(w, MDIClientHeight() + pnltreeView.Height - 40), 40)
        If Loaded Then My.Settings.TreeViewPanelHeight = pnltreeView.Height

    End Sub

    Private Sub SetPropGridPnlWidth(Optional ByVal w As Long = conKeepWithinLimits)

        If w = conKeepWithinLimits Then w = pnlPropGrid.Width
        pnlPropGrid.Width = Max(Min(w, MDIClientWidth() + pnlPropGrid.Width - 40), 40)
        If Loaded Then My.Settings.TermsPanelWidth = pnlPropGrid.Width

    End Sub

#End Region

#Region "Toolbars - General"

    Private WithEvents tbfDraw As New frmToolbar
    Private WithEvents tbfMain As New frmToolbar

    Private Sub frmToolbar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles tbfMain.FormClosing, tbfDraw.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then    'I get the same value for e.CloseReason whether the user closes the form or I do it in code, so distinguish like this.
            e.Cancel = True 'don't really want it to be closed or will have to recreate it.
            With CType(sender, frmToolbar)
                .Location = New Point(-Width, -Height)    'move off the screen or it will appear for an instant in the wrong place when I next show it.
                .Hide()
            End With

            If sender Is tbfMain Then
                mnuViewMainToolbar.PerformClick()
            ElseIf sender Is tbfDraw Then
                mnuViewDrawingToolbar.PerformClick()
            End If
        End If
    End Sub

    Private Sub FormMove(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbfMain.Move, tbfDraw.Move

        With CType(sender, frmToolbar)
            If .Visible Then

                'Determine whether to redock the toolbar.
                Dim ThinDimension As Integer = Math.Min(tstDraw.Width, tstDraw.Height)
                Dim p As Point = PointToClient(MousePosition)

                Dim border As Integer = (Width - ClientSize.Width) / 2
                Dim ClientTop As Integer = Height - ClientSize.Height - border

                If Bounds.Contains(MousePosition) Then
                    Dim RelTSP As ToolStripPanel = Nothing

                    If p.Y > 0 And p.Y < ThinDimension Then
                        RelTSP = Me.tspTop
                    ElseIf p.Y > Height - border - ClientTop - ThinDimension Then
                        RelTSP = Me.tspBottom
                    ElseIf p.X < border + ThinDimension Then
                        RelTSP = Me.tspLeft
                    ElseIf p.X > Width - border - ThinDimension Then
                        RelTSP = Me.tspRight
                    End If

                    If Not RelTSP Is Nothing Then
                        Dim tst As ToolStrip
                        If sender Is tbfDraw Then
                            tst = Me.tstDraw
                        Else
                            tst = Me.tstMain
                        End If

                        tst.GripStyle = ToolStripGripStyle.Visible

                        If RelTSP Is Me.tspTop Or RelTSP Is Me.tspBottom Then
                            p.X = p.X - border
                        Else
                            p.Y = p.Y - ClientTop
                        End If
                        RelTSP.Join(tst, p)

                        Activate()

                        .Hide()

                    End If

                End If
            End If

            If sender Is tbfMain Then
                My.Settings.MainToolBarFormPosition = .Location
            Else
                My.Settings.DrawToolbarFormPosition = .Location
            End If
        End With

    End Sub

    Private Sub frmMDI_MdiChildActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MdiChildActivate

        'This happens even when the last form is closed, surprisingly. So I can disable all buttons in tstDraw here.
        'Don't disable the toolbar cos then it can't be moved.
        If Not Me.tsiSelect.Enabled Then Me.tsiSelect.Checked = True 'if I'm opening a new form this should make the default selection of the drag button.
        If Me.ActiveMdiChild Is Nothing Then
            For Each t As ToolStripItem In tstDraw.Items
                If TypeOf t Is ToolStripButton Then
                    With CType(t, ToolStripButton)
                        .Checked = False
                        .Enabled = False
                    End With
                End If
            Next
        End If

        SetDrawButtonEnablement()

        SetPRGObjects()

    End Sub

    Private Sub ToolStrip_EndDrag(ByVal sender As Object, ByVal e As System.EventArgs) Handles tstDraw.EndDrag, tstMain.EndDrag

        'Seems to be the one event that I can catch when a toolbar is dragged out of a toolstrippanel.
        Dim InPanel As Boolean
        For Each c As Control In Me.Controls
            If TypeOf c Is ToolStripPanel Then
                If c.Bounds.Contains(Me.PointToClient(MousePosition)) Then InPanel = True
            End If
        Next

        If Not InPanel Then ShowToolbarForm(sender, MousePosition, False)

    End Sub

    Private Sub ShowToolbarForm(ByVal tst As ToolStrip, ByVal p As Point, ByVal BringOnScreen As Boolean)

        If Not tst Is Nothing Then
            Dim tbf As frmToolbar
            If tst Is tstMain Then
                tbf = tbfMain
            Else
                tbf = tbfDraw
            End If

            tbf.Controls.Add(tst)   'n.b. this automatically removes tst from the toolstrip panel's Controls collection - no need for me to do this.

            tst.Location = New Point(0, 0)

            tst.GripStyle = ToolStripGripStyle.Hidden

            If BringOnScreen Then
                p.X = Min(Screen.GetWorkingArea(New Point(0, 0)).Width - tbf.Width, Max(p.X, 0))    'point argument is for if there's more than one screen. Assume just one screen.
                p.Y = Min(Screen.GetWorkingArea(New Point(0, 0)).Height - tbf.Height, Max(p.Y, 0))
            End If

            If Not tbf.Visible Then tbf.Show(Me)
            tbf.Location = p

        End If

    End Sub

    Private Sub mnuViewMainToolbar_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuViewMainToolbar.CheckStateChanged

        'Menu is set as unchecked at design time, so if I make it checked in New, this will trigger, setting the toolbar up.

        SetUpToolbar(sender, tstMain, My.Settings.MainToolbarPanel, My.Settings.MainToolBarFormPosition, My.Settings.MainToolBarPosition, My.Settings.MainToolbarRow)

        My.Settings.MainToolbarShowing = sender.Checked

    End Sub

    Private Sub mnuViewDrawToolbar_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuViewDrawingToolbar.CheckStateChanged

        'Menu is set as unchecked at design time, so if I make it checked in New, this will trigger, setting the toolbar up.

        SetUpToolbar(sender, tstDraw, My.Settings.DrawToolbarPanel, My.Settings.DrawToolbarFormPosition, My.Settings.DrawToolbarPosition, My.Settings.DrawToolbarRow)
        My.Settings.DrawToolbarShowing = sender.checked

    End Sub

    Private Sub SetUpToolbar(ByVal c As ToolStripMenuItem, ByVal t As ToolStrip, ByVal d As DockStyle, ByVal floc As Point, ByVal tloc As Integer, ByVal RowPos As Integer)

        If c.Checked Then
            t.Visible = True

            If d = DockStyle.None Then
                ShowToolbarForm(t, floc, True)
            Else
                Dim c2 As Control = Nothing
                For Each c2 In Me.Controls
                    If TypeOf c2 Is ToolStripPanel Then
                        If c2.Dock = d Then
                            CType(c2, ToolStripPanel).Join(t, RowPos)    'resets toolbar at origin.
                            Exit For
                        End If
                    End If
                Next

                If d = DockStyle.Top Or d = DockStyle.Bottom Then
                    t.Left = tloc
                Else
                    t.Top = tloc
                End If
            End If
        Else
            If TypeOf t.Parent Is frmToolbar Then
                CType(t.Parent, frmToolbar).Hide()
            Else
                t.Visible = False
            End If
        End If

    End Sub

    Private Sub Toolstrip_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tstDraw.MouseDoubleClick, tstMain.MouseDoubleClick
        'Double clicking ought to do something, so why not send send the toolstrip out to the middle?

        'I don't think a button for drawing security is necessary. If the mouse goes down on a connection that's unambiguous. I found it 
        'quite frustrating having to click on this button and back on the button for dragging an entity when I was playing around.

        Dim f As ToolStrip = sender
        Dim b As Boolean
        If f.Parent Is tspTop Or f.Parent Is tspBottom Then 'horizontally aligned.
            If e.X <= f.GripMargin.Horizontal + f.GripRectangle.Width Then b = True
        Else
            If e.Y <= f.GripMargin.Vertical + f.GripRectangle.Height Then b = True
        End If

        If b Then ShowToolbarForm(f, New Point(Me.Left + Me.Width / 2 - f.Width / 2, Me.Top + Me.Height / 2), False)

    End Sub

    Private Sub tst_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles tstMain.Move, tstDraw.Move
        Dim c As ToolStrip = sender
        If Loaded And Not c.Parent Is Nothing Then
            Dim pos As Integer
            If c.Parent Is Me.tspLeft Or c.Parent Is Me.tspRight Then
                pos = c.Top
            Else
                pos = c.Left
            End If
            Dim R As Integer
            Dim RR As Integer
            If TypeOf c.Parent Is ToolStripPanel Then
                Dim tsp As ToolStripPanel = c.Parent
                For R = 0 To tsp.Rows.Length - 1
                    Try
                        For n As Integer = 0 To tsp.Rows(R).Controls.Length - 1
                            If tsp.Rows(R).Controls(n) Is c Then RR = R
                        Next
                    Catch
                        Debug.Print(Err.Description) 'get an error when I try to access the Controls array here when a toolstrip is being dragged out. Doesn't seem to be any problem 
                        'if I run this with Ctrl + F5 so it continues on errors. "Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."
                    End Try
                Next
            End If
            If c Is tstMain Then
                My.Settings.MainToolBarPosition = pos
                My.Settings.MainToolbarRow = RR
            Else
                My.Settings.DrawToolbarPosition = pos
                My.Settings.DrawToolbarRow = RR
            End If

        End If
    End Sub

    Private Sub ToolStrip_ParentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tstMain.ParentChanged, tstDraw.ParentChanged

        With CType(sender, ToolStrip)

            If Not .Parent Is Nothing Then
                If .Parent.Dock = DockStyle.Left Or .Parent.Dock = DockStyle.Right Then
                    .TextDirection = ToolStripTextDirection.Vertical90
                    .LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow
                Else    'including no dock which will be the case for the frmToolbar
                    .TextDirection = ToolStripTextDirection.Horizontal
                    .LayoutStyle = ToolStripLayoutStyle.HorizontalStackWithOverflow
                End If

                For Each t As ToolStripItem In .Items
                    t.TextDirection = .TextDirection
                    .Refresh()
                Next

                If Loaded Then

                    If sender Is tstMain Then
                        My.Settings.MainToolbarPanel = .Parent.Dock
                    ElseIf sender Is tstDraw Then
                        My.Settings.DrawToolbarPanel = .Parent.Dock
                    End If
                End If
                'Irritating bug - if a toolstrip is at the bottom and the user leaves the cursor over one of its buttons, 
                'VB seems to get stuck alternating between the states of showing the tooltip and highlighting the button.
                'Could just turn off tooltips in that case. Can still happen for button at bottom of side-oriented 
                'toolbar, but too bad. Additional problem is that double clicking on a button tends not to work when this bug applies.
                'Set both toolbars at the top for their initial settings to make this situation less likely.
                .ShowItemToolTips = Not .Parent Is tspBottom
            End If

        End With

    End Sub

    Private Sub ToolStrip_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tstDraw.MouseEnter, tstMain.MouseEnter
        'Need to do this so that the toolbar registers button clicks when the toolbar form isn't activated.
        Dim t As ToolStrip = sender
        If TypeOf t.Parent Is frmToolbar Then t.Capture = True
    End Sub

    Private Sub ToolStrip_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tstDraw.MouseMove, tstMain.MouseMove
        Dim t As ToolStrip = sender
        If TypeOf t.Parent Is frmToolbar And Not t.Bounds.Contains(e.Location) Then t.Capture = False
        'mouseleave doesn't seem to trigger when the mouse is captured.
    End Sub

#End Region

#Region "Entity Addition"

    Private Sub tstDraw_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tstDraw.MouseMove

        'I want the user to be able to drag from the entity button in order to add an entity. Using DragDrop isn't great
        'because I can't detect when the user has pressed Esc to cancel the operation, and so don't know to reset the 
        'checked button in the toostrip. Easy enough to do with the mouse move events of the toolstrip. Note it's the 
        'toolstrip and not the button that gets these.

        If e.Button = Windows.Forms.MouseButtons.Left And EntityButtonDown Then Me.Capture = True

    End Sub

    Private Sub frmMDI_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove

        'Am using a prefab cursor when the dragdrop one would do. Tried doing a dragdrop without the cursor being down so 
        'I wouldn't need this but the drop happened as soon as the mouse moved over the canvas then.
        If EntityButtonDown Then
            Dim p As Point
            Dim fb As frmBase = Nothing
            GetFormAtMousePoint(fb, p)
            If fb Is Nothing Then
                Cursor = Cursors.No
            Else
                Cursor = crsAdd
            End If
        Else
            Cursor = Cursors.Default
        End If

    End Sub

    Private Sub frmMDI_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp

        'Placement of entity on canvas.
        If e.Button = Windows.Forms.MouseButtons.Left And EntityButtonDown Then

            Dim p2 As Point
            Dim fb As frmBase = Nothing
            GetFormAtMousePoint(fb, p2)
            If Not fb Is Nothing Then fb.picCanvas_MouseUp(sender, New MouseEventArgs(e.Button, e.Clicks, p2.X, p2.Y, e.Delta))

            Cursor = Cursors.Default

            EntityButtonDown = False

            Me.tsiSelect.Checked = True

        End If

    End Sub

    Private Sub GetFormAtMousePoint(ByRef fb As frmBase, ByRef p As Point)
        If tsiEntity.Checked Then
            'Don't want to have entities dropped through a floating toolbar.
            If tbfMain.Visible And tbfMain.Bounds.Contains(MousePosition) Then Exit Sub
            If tbfDraw.Visible And tbfDraw.Bounds.Contains(MousePosition) Then Exit Sub
            Dim p2 As Point = Point.Subtract(MousePosition, Me.Location)
            Dim c As Control = GetChildAtPoint(p2)
            If TypeOf c Is MdiClient Then   'this is just the client area of the mdi form.
                Dim j As frmBase = c.GetChildAtPoint(c.PointToClient(MousePosition))    'if forms are overlapping this will return the top one at this point.
                If j IsNot Nothing Then    'nothing if over unused area of mdiclient area.
                    If TypeOf j.GetChildAtPoint(j.PointToClient(MousePosition)) Is PartyCanvas Then
                        Dim p3 As Point = j.picCanvas.PointToClient(MousePosition)
                        If p2.X > 0 And p2.Y > 0 Then
                            p = p3
                            fb = j
                        End If
                    End If
                End If
            End If
        End If

    End Sub


    Private Sub tsiEntity_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tsiEntity.MouseUp
        'Necessary for where button is double clicked. Without this if the user clicks on the select button and then moves over the entity button it will be checked again.
        EntityButtonDown = False
    End Sub

#End Region

#Region "Files and Child Forms"

    Private Sub BlankTransaction1ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileNewBlank.Click, tsiNew.ButtonClick

        AddNewChild(True)

    End Sub

    Friend Function ActiveDeal() As frmBase

        'Will only ever put frmOne as a child so cast this so it's early bound.
        Return ActiveMdiChild

    End Function

    Friend Sub ShowRecentFiles()

        'Show the list of recent files. I don't think there's any need to make items invisible, 
        'except at design time, because I never remove any items from the recent files list.
        'Can't have arrays of controls nor of settings as far as I can see, so might as well _
        'write all the code out as below.
        With My.Settings
            If .RecentFile1 <> "" Then
                zspRecentFiles.Visible = True
                mnuRecentFile1.Text = .RecentFile1
                mnuRecentFile1.Visible = True
            End If
            If .RecentFile2 <> "" Then
                mnuRecentFile2.Text = .RecentFile2
                mnuRecentFile2.Visible = True
            End If
            If .RecentFile3 <> "" Then
                mnuRecentFile3.Text = .RecentFile3
                mnuRecentFile3.Visible = True
            End If
            If .RecentFile4 <> "" Then
                mnuRecentFile4.Text = .RecentFile4
                mnuRecentFile4.Visible = True
            End If
        End With

    End Sub

    Friend Function AddNewChild(ByVal NewTransaction As Boolean, Optional ByVal FileName As String = "", Optional ByVal InitialBlankForm As Boolean = False, Optional ByVal sParentText As String = "") As frmBase

        Application.UseWaitCursor = True 'Doesn't work.
        Application.DoEvents()

        If FileName <> "" Then
            If System.IO.File.Exists(FileName) Then
                For Each f As frmBase In MdiChildren
                    'Usual thing on opening a form is to close the initial blank form.
                    If f.InitialBlankForm And f.FileName = "" And Not f.picCanvas.Dirty Then f.Close()
                Next
            Else
                MsgBox("Unable to find " & FileName, MsgBoxStyle.Exclamation)
                Application.UseWaitCursor = False
                Exit Function
            End If

        End If

        'Create a new instance of the child form.
        Dim Abort As Boolean

        AddNewChild = New frmBase(FileName, Abort, sParentText)

        If Abort Then
            AddNewChild.Close()
            Exit Function
        End If

        'Make it a child of this MDI form before showing it.
        With AddNewChild

            .MdiParent = Me
            .InitialBlankForm = InitialBlankForm

            If FileName = "" And sParentText = "" Then
                .SetText()
            End If

            .WindowState = FormWindowState.Maximized  'Doesn't work if you set this at design time.

            .Show()
            .picCanvas.Visible = True   'If it's visible from the start then you get a button colour box showing 
            'for an instant where it's going to be.

            .Icon = My.Resources.File16

            'If you don't do the following, frmOne's icon will only appear in the system menu when _
            'frmMDI is resized.
            Dim r As ToolStripMenuItem
            Try
                r = mnsMain.Items(0)
                If TypeName(r) = "SystemMenuItem" Then
                    Dim t As Long
                    t = mnsMain.Height
                    r.Image = .Icon.ToBitmap
                    mnsMain.Height = t
                End If
            Catch ex As Exception
                Debug.Print("Failed to set system menu item.")
            End Try

            .Cursor = Cursors.Default
            .picCanvas.Cursor = Cursors.Default

        End With
        Application.UseWaitCursor = False

    End Function

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs) Handles mnuFileOpen.Click, tsiOpen.Click

        Dim OpenFileDialog As New OpenFileDialog

        With OpenFileDialog
            .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
            .Filter = DialogFilter()
            .ShowDialog(Me)

            Dim v As frmBase = GetChild(.FileName)

            If v Is Nothing Then
                AddNewChild(True, .FileName)
            Else
                If MsgBox("This file is already open. Do you wish to close it and revert to the saved version?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    v.picCanvas.Dirty = False     'make sure there's no prompt to save it.
                    v.Close()
                    AddNewChild(True, .FileName)
                Else
                    v.Activate()
                    Exit Sub
                End If
            End If

        End With

    End Sub

    Private Sub RecentFileMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRecentFile1.Click, mnuRecentFile2.Click, mnuRecentFile3.Click, mnuRecentFile4.Click

        'If already open, just activate it, otherwise open it.
        Dim m As ToolStripMenuItem = sender
        Dim v As frmBase = GetChild(m.Text)
        If v Is Nothing Then
            AddNewChild(True, m.Text)
        Else
            v.Activate() 'Already open
        End If

    End Sub

    Private Function GetChild(ByVal sFileName As String) As frmBase
        For Each v As frmBase In Me.MdiChildren
            If v.FileName = sFileName Then Return v
        Next
    End Function

#End Region

#Region "Drawing Toolbar"

    Friend EntityButtonDown As Boolean  'Record of the toolstripbutton that the mouse went down on, if any.

    Friend Sub SetDrawButtonEnablement()

        'Disable arrow selection buttons if there's no way what has been selected can be drawn. _
        'But I can do nothing about the user selecting an entity from which no pass-through can be drawn because there are no arrows to it which are selected.
        'If he does this and starts drawing then I think I can just draw in a funding. After all I'm drawing in a funding or cash on the side on 
        'which there are no selected arrows anyway - just do this twice here.

        'Had the idea of just having one button for both fundings and pass-throughs. If there is something that has 
        'been selected that can be passed through then add a pass-through. Otherwise it's a funding. But this is 
        'confusing. It's easy to have something selected and not really be conscious of this so just intend to 
        'draw a funding.

        'Also note it's no good insisting that a pass-through or swap be drawn from the ToEntity of the TreeViewBL. _
        'If there are two things being passed through (they must have different ToEntities)
        'then the user might reasonably want to draw from the selected BL which isn't the TreeViewBL _
        '- it should make no difference which way round he draws.
        'I think that if there's a choice of posible StanCons for drawing a passthrough or swap 
        'and one is the TreeViewBL I should use that. But if none of them is that then the drawing fails.

        If ActiveDeal() IsNot Nothing Then

            With ActiveDeal()
                Me.tsiDrawFundingExisting.Enabled = .picCanvas.EntityControls.Count > 1
                Me.tsiDrawFundingNew.Enabled = .picCanvas.EntityControls.Count > 1
                Me.tsiDrawSwapNew.Enabled = .picCanvas.EntityControls.Count > 1    'Normally a swap would be of something, but I suppose conceivably it might exist in isolation - someone want to borrow one currency and someone else want to borrow the other.
                Me.tsiDrawSwapExisting.Enabled = Me.tsiDrawSwapNew.Enabled

                Dim bCanPassThroughNew As Boolean
                For Each b As BaseLine In .picCanvas.SelectedBLs
                    If TypeOf b Is Connection Then bCanPassThroughNew = True
                Next
                Me.tsiDrawPassthroughNew.Enabled = bCanPassThroughNew             'if there's at least one EToEConnection selected.

                Dim bCanPassThroughExisting As Boolean
                For Each b As BaseLine In .picCanvas.SelectedBLs
                    If TypeOf b Is ExistingCon Then bCanPassThroughExisting = True
                Next
                Me.tsiDrawPassThroughExisting.Enabled = bCanPassThroughExisting        'if there's at least one existing EToEConnection selected.

                Me.tsiDrawCredSupNew.Enabled = .picCanvas.StanConCols.Count > 0
                Me.tsiDrawSecurityNew.Enabled = .picCanvas.StanConCols.Count > 1


                Dim ExistingCount As Integer
                For Each x As TrancheCollection In .picCanvas.StanConCols
                    For Each y As Connection In x
                        If TypeOf y Is ExistingCon Then ExistingCount += 1
                    Next
                Next
                Me.tsiDrawCredSupExisting.Enabled = ExistingCount > 0
                Me.tsiDrawSecurityExisting.Enabled = ExistingCount > 1

            End With

            'These are disabled when there's no child form, so need to do this.
            Me.tsiEntity.Enabled = True
            Me.tsiSelect.Enabled = True

        End If

        'Necessary where user has double clicked a button for which BLs must be selected and then deselects the BLs.
        For Each b As ToolStripItem In Me.tstDraw.Items
            Dim c As ToolStripButton = TryCast(b, ToolStripButton)
            If Not c Is Nothing Then
                If c.Checked And Not b.Enabled And Not b Is Me.tsiDrawSecurityExisting And Not b Is Me.tsiDrawSecurityNew Then
                    Me.tsiSelect.Checked = True
                End If

            End If
        Next

    End Sub

    Private Sub tsiEntityMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tsiEntity.MouseDown

        If e.Button = Windows.Forms.MouseButtons.Left Then EntityButtonDown = True

    End Sub

    Private Sub EntityButtonMouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsiEntity.MouseLeave

        'I think it's right that the drag event only begins when the cursor moves 
        'outside the button. Otherwise it just seems too soon. This does 
        'make things rather more complicated for me than just starting the DragDrop on 
        'the mousemove event, though.
        'For some controls the mousemove event continues to occur if the cursor moves off it while the left button is pressed.
        'This doesn't seem to be the case here though.

        If EntityButtonDown Then tsiEntity.Checked = True 'e has no Button parameter, so use EntityButtonDown to store fact that mouse went down.

    End Sub

    Private Sub DrawButtonCheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'Make sure all others are unchecked. Don't think there's a way to have the toolstrip buttons work like radio buttons i.e. only one can be selected.
        Dim c As ToolStripButton = sender
        If c.Checked Then
            For Each f As ToolStripItem In tstDraw.Items
                Dim f2 As ToolStripButton = TryCast(f, ToolStripButton)
                If Not f2 Is Nothing And Not f2 Is sender And Not f2 Is Me.tsiDrawSecurityExisting And Not f2 Is Me.tsiDrawSecurityNew Then
                    f2.Checked = False
                End If
            Next

            'Much easier to draw from an entity if it's selected, so why don't I just select them all for the user?
            Dim bSelectAll As Boolean = Not c Is Me.tsiEntity And Not c Is Me.tsiSelect

            If Not Me.ActiveDeal Is Nothing Then
                For Each ec As EntityControl In ActiveDeal.picCanvas.EntityControls
                    ec.Selected = bSelectAll
                Next
                Me.SetPRGObjects()
            End If
        End If

    End Sub

    'Seeking to copy PowerPoint here in allowing user to double click one of the buttons if he wants the option to remain selected after he has added the object, or 
    'simply select it by clicking on it if he only wants to do it once. Other option with entity button would be to add a control automatically like controls in 
    'the toolbox in the IDE, but I prefer this. 
    Friend bDrawButtonDoubleClicked As Boolean  'whether the selected draw toolbar button was double clicked to select it and so should remain selected after the operation has been carried out.
    Private Sub DrawButtonDoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        'Note that each toolbutton has to have its DoubleClickEnabled property set to True for this event to occur.
        CType(sender, ToolStripButton).Checked = True  'if button is checked without having been double clicked and user then double clicks, it will cease to be checked. But I want it to enter the double click state (see PowerPoint) so do this.
        bDrawButtonDoubleClicked = True
    End Sub

    Friend Sub tstDraw_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles tstDraw.ItemClicked

        Dim h As ToolStripButton = TryCast(e.ClickedItem, ToolStripButton)
        If Not h Is Nothing Then
            h.Checked = True   'If CheckOnClick is True buttons will be deselected if clicked when selected. Don't want that.
            bDrawButtonDoubleClicked = False 'this event happens before the doubleclick event so will be made True there if that happens.
        End If

    End Sub

    'Allow doubleclick for these buttons. It doesn't do anything different to a single click, but if user is used to 
    'it for the other buttons, he'll be expecting it here. If I don't allow it, a double click will be treated as two 
    'clicks, so checked status will be turned back off.
    'Always keep one checked.
    Private Sub tsiDrawSecurityExisting_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsiDrawSecurityExisting.CheckStateChanged
        Me.tsiDrawSecurityNew.Checked = Not Me.tsiDrawSecurityExisting.Checked
    End Sub

    Private Sub tsiDrawSecurityNew_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsiDrawSecurityNew.CheckStateChanged
        Me.tsiDrawSecurityExisting.Checked = Not Me.tsiDrawSecurityNew.Checked
    End Sub

#End Region

#Region "Property Grid"

    Private WithEvents PRGTStrip As ToolStrip   'the toolstrip in the propertygrid.

    Friend Sub SetPRGObjects()

        'Set the selected Instruments or Entities for the property grid. Related code is in bcol, Name property of Instrument and Total property for Cash.

        'This is called by the baseline collection if I add or remove a baseline to the collection of baselines, or if the user switches to consideration.
        'Also if an Instrument of a selected baseline changes.

        'I need to be setting the Instruments there and I need every baseline to have an instrument so that related baselines can be shown 
        'through the property grid.

        'BTW if you set the selected object(s) to a reference and change what the reference is to, the property grid's selected object does not change. So can't try to do like that.

        Dim bNothingToShow As Boolean
        Dim SelEnts As New List(Of Entity)
        If Me.ActiveDeal Is Nothing Then
            bNothingToShow = True
        Else
            bNothingToShow = Me.ActiveDeal.picCanvas.SelectedBLs.Count = 0 'will be 0 if there's nothing there.
            If bNothingToShow Then
                For Each ec As EntityControl In Me.ActiveDeal.picCanvas.EntityControls
                    If ec.Selected Then SelEnts.Add(ec.LEntity)
                    bNothingToShow = False
                Next
            End If
        End If

        If bNothingToShow Then
            SetTabText(tbpRight, Nothing)
            SetTabText(tbpLeft, Nothing)
            prgOne.SelectedObjects = Nothing
            'It is possible to remove both tab pages, but it looks a bit of a big bare space if you do.
            If tbsOne.TabPages.Contains(tbpRight) Then tbsOne.TabPages.Remove(tbpRight)
            Me.TabControl1_SelectedIndexChanged(Nothing, Nothing)   'ensure the property grid is still showing.

            ShowPRGWithEnabledInstrument(True)
        Else

            If SelEnts.Count > 0 Then
                If tbsOne.TabPages.Contains(tbpRight) Then tbsOne.TabPages.Remove(tbpRight)
                Me.prgOne.SelectedObjects = SelEnts.ToArray

            Else

                If Not tbsOne.TabPages.Contains(tbpLeft) Then tbsOne.TabPages.Add(tbpLeft)

                Dim tbsToDisplay As TabPage = tbpLeft
                Me.prgOne.SelectedObjects = Me.ActiveDeal.picCanvas.SelectedBLs.InstArray

                If Me.ActiveDeal.picCanvas.SelectedBLs.LeftTab Or Me.ActiveDeal.picCanvas.SelectedBLs.RightTab Then    'need to change this so that only get two tabs if all BLs are in the same collection of TransactionCons.

                    Dim ConsidBLs As New SelectedCol
                    For i As Integer = 0 To Me.ActiveDeal.picCanvas.SelectedBLs.Count - 1
                        ConsidBLs.add(CType(Me.ActiveDeal.picCanvas.SelectedBLs(i), TransactionCon).Consideration, False)
                    Next

                    Dim LeftBLs As List(Of BaseLine)
                    Dim RightBLs As List(Of BaseLine)

                    If Me.ActiveDeal.picCanvas.SelectedBLs.LeftTab Then
                        LeftBLs = Me.ActiveDeal.picCanvas.SelectedBLs
                        RightBLs = ConsidBLs
                    Else
                        LeftBLs = ConsidBLs
                        RightBLs = Me.ActiveDeal.picCanvas.SelectedBLs
                        tbsToDisplay = tbpRight
                    End If

                    'Calling Show and Hide methods of tbpIn has no effect.
                    If Not tbsOne.TabPages.Contains(tbpRight) Then tbsOne.TabPages.Insert(1, tbpRight)

                    SetTabText(tbpLeft, LeftBLs)
                    SetTabText(tbpRight, RightBLs)

                Else    'just one tab to display.
                    SetTabText(tbpLeft, Me.ActiveDeal.picCanvas.SelectedBLs)
                    If tbsOne.TabPages.Contains(tbpRight) Then tbsOne.TabPages.Remove(tbpRight)
                End If


                If tbsOne.SelectedTab Is tbsToDisplay Then
                    TabControl1_SelectedIndexChanged(Nothing, Nothing)
                Else
                    tbsOne.SelectedTab = tbsToDisplay     'will trigger changetab event.
                End If
            End If
            SetSplitter()

            'Visual indication that the selected instrument is not appropriate.
            'If prgOne.SelectedObject Is Nothing Then
            '    ShowPRGWithEnabledInstrument(True)
            'Else
            '    If DisplayedBL.TypeIsEnabled(prgOne.SelectedObject.GetType) Then
            '        ShowPRGWithEnabledInstrument(True)
            '    Else
            '        ShowPRGWithEnabledInstrument(False)
            '    End If
            'End If

        End If

    End Sub

    Private conBadInstrumentColour As Color = Color.Red

    Private Sub ShowPRGWithEnabledInstrument(ByVal PRGenabled As Boolean)
        'Initially tried greying out the whole propertygrid, but lines got lost.
        '   Me.prgOne.ForeColor = IIf(PRGenabled, Color.Black, conBadInstrumentColour)
    End Sub

    Private Sub SetTabText(ByVal tbp As TabPage, ByVal BLs As List(Of BaseLine))

        'Set up the association between the tabpage and a BaseLine.
        tbp.Text = ""

        If Not BLs Is Nothing Then
            If BLs.Count > 0 Then
                Dim b As Boolean
                Dim t As Type = Nothing     'if they're all the same type then use the typename for the text on the tab. Otherwise just put "Mixed"
                For n As Integer = 0 To BLs.Count - 1
                    If BLs(n).Inst IsNot Nothing Then
                        If t Is Nothing Then
                            t = BLs(n).Inst.GetType
                            b = True
                        ElseIf BLs(n).Inst.GetType IsNot t Then
                            t = Nothing
                            Exit For
                        End If
                    End If
                Next

                Dim s As String = ""
                If b Then   'otherwise no object selected so better just leave the text blank.
                    If t Is Nothing Then
                        s = "Mixed"
                    Else
                        Dim f As ClassDisplayNameAttribute = GetToolStripItemAttribute(t)
                        If f IsNot Nothing Then s = f.TabText.Replace("&", "")
                    End If
                End If
                tbp.Text = s

            End If
        End If

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbsOne.SelectedIndexChanged

        'Call this when the selected tab hasn't changed too, so I can set things correctly.
        With tbsOne
            If Not .SelectedTab Is Nothing Then

                If .TabCount = 2 Then
                    If (.SelectedTab Is .TabPages(0) And Me.ActiveDeal.picCanvas.SelectedBLs.RightTab) Or (.SelectedTab Is .TabPages(1) And Me.ActiveDeal.picCanvas.SelectedBLs.LeftTab) Then
                        Me.ActiveDeal.picCanvas.SelectedBLs.SwitchToConsideration()
                    End If
                End If
                prgOne.Parent = .SelectedTab
                'If I want I can put the propgrid in a split panel with two panels and have a richtextbox in the top panel and display extra info in that richtextbox. But
                'I think it's better to display everything in the propgrid. Maybe I want an extra category for related instruments.
            End If
        End With

    End Sub

    Private Sub mnuTermsTermsWindow_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTermsTermsWindow.CheckStateChanged

        pnlPropGrid.Visible = mnuTermsTermsWindow.Checked
        If pnlPropGrid.Visible Then SetPropGridPnlWidth()

    End Sub

    Private Sub mnuTermsTermsWindow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTermsTermsWindow.Click
        My.Settings.TermsWindowShowing = mnuTermsTermsWindow.Checked
    End Sub

    Private Sub tsiCopyPG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsiCopyPG.Click

        Dim strCopy As New String("")

        With prgOne

            If Not .SelectedGridItem Is Nothing Then

                For Each gi2 As GridItem In Me.GridItemsCol

                    GridItemToString(strCopy, gi2)

                    If gi2.GridItemType = GridItemType.Category Then
                        'Category grid items have their own GridItems collection, so need to iterate again.
                        For Each h2 As GridItem In gi2.GridItems
                            GridItemToString(strCopy, h2)
                        Next
                        strCopy = strCopy & vbCrLf
                    End If

                Next

                Try
                    Clipboard.SetText(strCopy, TextDataFormat.Text)
                Catch
                    MsgBox("Failed to copy to Clipboard")   'This happens sometimes, don't know why.
                End Try

            Else
                'only ever seems to be the case if the SelectedObject is Nothing, so not a problem.
            End If
        End With

    End Sub

    Private Sub tsiConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsiConfirm.Click

        Dim sc As SwapCon = TryCast(CurrentBL, SwapCon)
        If Not sc Is Nothing Then sc.GenerateConfirm()

    End Sub

    Private Sub GridItemToString(ByRef v As String, ByVal gi As GridItem)

        'Adds name and value of grid item to v. If gi is the parent of a property extender, i.e.
        'it drops open, it will have child grid items. But I don't want those, so don't do an iteration for them.
        'Two things would be quite nice here: to have headings in bold and to have everything appear in a table 
        'if copied to Word. I don't think the first is possible since if you copy between Excel and Word any 
        'bold formatting is lost. But if you copy data from Excel into Word it appears in a table, so that must be
        'possible.

        With gi
            v = v & .Label & vbTab
            If Not .Value Is Nothing Then v = v & .Value.ToString
            v = v & vbCrLf
        End With

    End Sub

    Private Sub prgOne_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        'Would quite like to prevent cursor from changing to sizeEW when it is over white space
        'at bottom of prgOne (where the instrument has few terms for instance). Can get a WithEvents reference to the PropertyGridView control, 
        'and reset the cursor, but can't find out the position of the bottom of the line.
    End Sub

    Private Sub prgOne_PropertySortChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles prgOne.PropertySortChanged
        'I don't want things within categories and under expanders sorted alphabetically.
        If prgOne.PropertySort = PropertySort.CategorizedAlphabetical Then prgOne.PropertySort = PropertySort.Categorized
    End Sub

    Friend Sub SetSplitter()

        'PropGrid by default sets its splitter in the middle. Not very good where the values are quite narrow 
        'and the fields quite wide. So adjust as follows. SelectedObjectChanged event is no good - there is no 
        'SelectedGridItem at this stage. Also the propgrid will reset to 50% on double clicking the middle.
        'I could make g below WithEvents and reset it, but two resizings will then be visible, so haven't bothered.

        If Not prgOne.SelectedObject Is Nothing Then
            If Not GridItemsCol() Is Nothing Then
                Dim a As Integer
                Dim b As Integer    'width of longest value. Not currently used.
                GetLongest(GridItemsCol, a, b)

                Dim SplitPos As Integer
                'Idea is to set it to show the full width of the longest label, not including category labels.
                'Try not to make any labels too long.
                SplitPos = Math.Min(a + 20, prgOne.Width)

                Try
                    For Each g As Control In prgOne.Controls
                        If TypeName(g) = "PropertyGridView" Then
                            g.GetType.InvokeMember("MoveSplitterTo", Reflection.BindingFlags.InvokeMethod Or _
                                   Reflection.BindingFlags.NonPublic Or Reflection.BindingFlags.Instance, Nothing, g, New Object() {SplitPos})   '20 is determined by trial and error, not very good.
                            Exit For
                        End If
                    Next
                Catch ex As Exception
                    Debug.Print(ex.ToString)
                End Try
            End If
        End If
    End Sub

    Friend Function GridItemsCol() As GridItemCollection

        'Get the collection of GridItems in prgone. Will need for selecting Cash griditem in the grid.
        Dim f As Windows.Forms.GridItem = prgOne.SelectedGridItem
        If Not f Is Nothing Then
            Do Until f.Parent Is Nothing
                f = f.Parent
            Loop
            Return f.GridItems
        End If

    End Function

    Private Sub GetLongest(ByVal col As Windows.Forms.GridItemCollection, ByRef MaxLabel As Integer, ByRef MaxValue As Integer)

        For Each i As Windows.Forms.GridItem In col
            If Not i.GridItemType = GridItemType.Category Then
                MaxLabel = Math.Max(MaxLabel, TextRenderer.MeasureText(i.Label, Me.prgOne.Font).Width)
                If Not i.Value Is Nothing Then

                    Dim s As String
                    If i.Value.GetType Is GetType(Date) Then
                        s = i.Value 'i.value.ToString will include the time as well.
                    Else
                        s = i.Value.ToString
                    End If
                    MaxValue = Math.Max(MaxValue, TextRenderer.MeasureText(s, prgOne.Font).Width)

                End If
            End If
            If i.Expanded Then GetLongest(i.GridItems, MaxLabel, MaxValue)
        Next

    End Sub

    Private Sub prgOne_PropertyValueChanged(ByVal s As Object, ByVal e As System.Windows.Forms.PropertyValueChangedEventArgs) Handles prgOne.PropertyValueChanged
        If Not ActiveDeal() Is Nothing Then ActiveDeal.picCanvas.Dirty = True
    End Sub

    Private Sub prgOne_SelectedGridItemChanged1(ByVal sender As Object, ByVal e As System.Windows.Forms.SelectedGridItemChangedEventArgs) Handles prgOne.SelectedGridItemChanged

        'Will be useful to show help for specific items maybe things like business day convention.

        If Not e.NewSelection Is Nothing And Not e.NewSelection.PropertyDescriptor Is Nothing Then
            If TypeOf Me.prgOne.SelectedObject Is Entity Then
                FillTreeView()
                LawCat = LawCategory.cPropertyGrid
                CType(Me.prgOne.SelectedObject, Entity).Incorp.HandlePropID(e.NewSelection.PropertyDescriptor.Attributes.Item(GetType(PropIDAttribute)), Me)

            End If

        End If
    End Sub

    Private Sub mnuTermsTextSize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTermsTextSizeLarge.Click, mnuTermsTextSizeMedium.Click, mnuTermsTextSizeSmall.Click

        Dim h As Integer = CInt(DirectCast(sender, ToolStripMenuItem).Tag)

        prgOne.Font = New Font(prgOne.Font.FontFamily, h)

        My.Settings.TermsTextSize = h

        For Each f As ToolStripMenuItem In mnuTermsTextSize.DropDown.Items
            f.Checked = (f Is sender)
        Next

    End Sub

    Private Sub prgOne_SelectedObjectsChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles prgOne.SelectedObjectsChanged
        For Each b As Object In Me.prgOne.SelectedObjects
            Debug.Assert(TypeOf b Is Instrument Or TypeOf b Is Entity)
        Next
    End Sub

#End Region

#Region "Treeview"

    Friend Enum LawCategory
        cAdditional = 0
        cInsolvency = 1
        cStampDuty = 2
        cSalesRest = 3
        cWHT = 4
        cRegCapTreat = 5
        cRegCapQual = 6
        cAccounting = 7
        cRegConsol = 8
        cAccountConsol = 9
        cShareDisc = 10
        cOwnSharePurch = 11
        cFinAssist = 12
        cCRD = 13
        cDeposit = 14
        cRecharacterisation = 15
        cPropertyGrid = 16
        cInstrumentDesc = 17
    End Enum

    Private Enum TVImage
        tviHelp = 0
        tviHelpGrey = 1
        tviHelpOpen = 2
        tviTopic = 3
        tviTopicPriority = 4
        tviTopicUnavailable = 5
    End Enum

    Private CurrentTitleNode As TreeNode 'Holds a reference to the law category currently under consideration - necessary because I want to get topics under headings in the treeview.
    Friend WriteOnly Property LawCat() As LawCategory

        Set(ByVal value As LawCategory)
            CurrentTitleNode = Nothing

            'Check for whether it's already there.
            For Each n As TreeNode In tvOne.Nodes
                If n.Tag = value Then CurrentTitleNode = n
            Next

            If CurrentTitleNode Is Nothing Then
                CurrentTitleNode = tvOne.Nodes.Add("", LawCatTitle(value), TVImage.tviHelpOpen, TVImage.tviHelpOpen)
                CurrentTitleNode.Tag = value
            End If

            If TypeOf Instrument.CurrentJur Is AppJur Then
                'FillTreeview didn't create an object, so do not _
                'expect any calls to Add. So add a blue item for the jurisdiction, _
                'provided it's set, and a heading in any case.
                If value <> LawCategory.cAdditional Then
                    CurrentTitleNode.Nodes.Add("", Instrument.CurrentJur.AdjectivalName & " law", TVImage.tviTopicUnavailable, TVImage.tviTopicUnavailable)
                End If
            End If
        End Set
    End Property

    Private Sub pnltreeView_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnltreeView.MouseLeave
        'Important to do this this way because then it's always reset when it goes over the treeview. 
        'If I just reset it when the cursor went over the treeview then it wouldn't be reset when a popup menu 
        'is showing and the cursor goes over the treeview.
        pnltreeView.Cursor = Cursors.Default
    End Sub

    Private Function LawCatTitle(ByVal X As LawCategory) As String
        Select Case X

            Case LawCategory.cAccountConsol
                Return "Accounting Consolidation"
            Case LawCategory.cAccounting
                Return "Accounting Treatment"
            Case LawCategory.cAdditional
                Return "Additional Matters"
            Case LawCategory.cCRD
                Return "CRD"
            Case LawCategory.cDeposit
                Return "Deposit-taking Restrictions"
            Case LawCategory.cFinAssist
                Return "Financial Assistance"
            Case LawCategory.cInsolvency
                Return "Insolvency"
            Case LawCategory.cOwnSharePurch
                Return "Purchase of Own Shares"
            Case LawCategory.cRecharacterisation
                Return "Recharacterisation"
            Case LawCategory.cRegCapQual
                Return "Regulatory Capital Qualification"
            Case LawCategory.cRegCapTreat
                Return "Regulatory Capital Treatment"
            Case LawCategory.cRegConsol
                Return "Regulatory Consolidation"
            Case LawCategory.cSalesRest
                Return "Sales Restrictions"
            Case LawCategory.cShareDisc
                Return "Shareholding Disclosure"
            Case LawCategory.cStampDuty
                Return "Stamp Duty"
            Case LawCategory.cWHT
                Return "With-holding Taxes"
            Case LawCategory.cPropertyGrid
                Return "Regulatory"
            Case LawCategory.cInstrumentDesc
                Return "Instrument Explanation"
        End Select
    End Function

    'Awkward that the nodes work on the basis of an image for when selected and when not, and 
    'I want an image for when expanded and when collapsed. If there are no topics under a title,
    'then show the open image at all times.
    Private Sub TreeView1_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvOne.AfterCollapse
        If e.Node.Nodes.Count > 0 Then
            e.Node.SelectedImageIndex = TVImage.tviHelp
            e.Node.ImageIndex = TVImage.tviHelp
        End If
    End Sub

    Private Sub TreeView1_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvOne.AfterExpand
        e.Node.SelectedImageIndex = TVImage.tviHelpOpen
        e.Node.ImageIndex = TVImage.tviHelpOpen
    End Sub

    Private Sub TreeView1_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvOne.NodeMouseClick

        With e.Node
            If .Parent Is Nothing Then    'header
                .SelectedImageIndex = IIf(.IsExpanded Or .Nodes.Count = 0, 2, 0)
                .ImageIndex = .SelectedImageIndex
            Else
                If .Tag <> "" Then wbOne.Navigate(JurisdictionSpace.Jurisdictions(PDJ.UKEngWales).RootFolder & .Tag)
            End If
        End With

    End Sub

    Friend bws As ArrayList     'Collection of backgroundworkers.
    Friend Sub AddTopic(ByRef TopicID As String, Optional ByRef Title As String = "", Optional ByRef Priority As Boolean = False) Implements ITopicsHandler.AddTopic

        If CurrentTitleNode Is Nothing Then
            Debug.Print("Missing title!")
        Else

            'Add the topic.
            Dim n As TreeNode = CurrentTitleNode.Nodes.Add(TopicID, TopicID, IIf(Priority, TVImage.tviTopicPriority, TVImage.tviTopic), IIf(Priority, TVImage.tviTopicPriority, TVImage.tviTopic))

            If TopicID <> "" Then

                n.Tag = TopicID

                Dim sPath As String '= JurisdictionSpace.Jurisdictions(PDJ.UKEngWales).RootFolder & TopicID & ".htm"
                sPath = "http://finance.practicallaw.com/" & TopicID

                Dim t As New tvtopic
                t.RelNode = n
                t.Page = sPath

                'Get the topic title. Much better to do this in the background cos it takes a short while to do.
                Dim bw As New BackgroundWorker

                With bw
                    .WorkerSupportsCancellation = True
                    AddHandler .DoWork, AddressOf BackgroundWorker1_DoWork
                    AddHandler .RunWorkerCompleted, AddressOf BackgroundWorker1_RunWorkerCompleted

                    .RunWorkerAsync(t)
                End With

                bws.Add(bw)

            End If
        End If

    End Sub

    Private Class tvtopic
        Public RelNode As TreeNode
        Public Page As String
        Public Title As String
    End Class

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        Dim t As tvtopic = e.Argument
        Try

            Dim req As Net.HttpWebRequest = DirectCast(Net.HttpWebRequest.Create(t.Page), Net.HttpWebRequest)

            Dim resp As HttpWebResponse = CType(req.GetResponse, HttpWebResponse)

            'Start reading data from the response stream.
            Dim sReader As New IO.StreamReader(resp.GetResponseStream)

            Dim html As String = sReader.ReadToEnd

            sReader.Close()
            resp.Close()

            'Not supposed to access user interface elements here, so send the info to the RunWorkerCompleted event to do that.
            t.Title = System.Text.RegularExpressions.Regex.Split(html, "(<h1>)|(</h1>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase)(2)

        Catch

            'If Internet is down will get a System.Web.NetException.

            t.Title = "! Unable to access page."

        End Try

        e.Result = t

    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted

        Dim t As tvtopic = e.Result
        t.RelNode.Text = t.Title
        Try
            bws.Remove(sender)
        Catch ex As Exception
            'Couldn't really care.
        End Try

    End Sub

#End Region

#Region "Browser"

    Friend Sub FillTreeView()

        'Needs to be called both on setting the TreeViewBL and setting its Instrument.

        Cursor = Cursors.WaitCursor  'No need to set cursors for underlying forms as long as 
        'they have the Default Cursor. The UseWaitCursor property seems to be useless.

        tvOne.Nodes.Clear()

        'Not sure that this is necessary but it seems considerate to release resources.
        If Not bws Is Nothing Then
            For Each b As BackgroundWorker In bws
                b.CancelAsync()
            Next
        End If
        bws = New ArrayList

        If Not CurrentBL Is Nothing Then
            If Not CurrentBL.Inst Is Nothing Then
                '    Dim f As New ProcessInterfaces
                '   f.LawObjSuccess = Not TypeOf JurisdictionSpace.Jurisdictions(0) Is AppJur 'still want to go through interface
                'procedures if the jurisdiction is an AppJur to highlight issues even if the answer can't be given.
                'Need to think about when I've got an AppJur
                InApp = False
                Try

                    CurrentBL.Inst.GetLaw()

                Catch
                End Try

                InApp = True
            End If

        End If

        CurrentTitleNode = Nothing

        tvOne.ExpandAll()

        Cursor = Cursors.Default

    End Sub

    Private Sub pnlPropGrid_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlPropGrid.MouseLeave
        pnlPropGrid.Cursor = Cursors.Default
    End Sub

    Private Sub BackToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserBack.Click, tsiBack.Click
        wbOne.GoBack()
    End Sub

    Private Sub mniForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserForward.Click, tsiForward.Click
        wbOne.GoForward()
    End Sub
    'Disables the Back button at the beginning of the navigation history.
    Private Sub webBrowser1_CanGoBackChanged(ByVal sender As Object, ByVal e As EventArgs) Handles wbOne.CanGoBackChanged

        mnuBrowserBack.Enabled = wbOne.CanGoBack
        tsiBack.Enabled = wbOne.CanGoBack

    End Sub

    'Disables the Back button at the beginning of the navigation history.
    Private Sub webBrowser1_CanGoForwardChanged(ByVal sender As Object, ByVal e As EventArgs) Handles wbOne.CanGoForwardChanged

        mnuBrowserForward.Enabled = wbOne.CanGoForward
        tsiForward.Enabled = wbOne.CanGoForward

    End Sub

    Private Sub mnuBrowserProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserProperties.Click
        wbOne.ShowPropertiesDialog()
    End Sub

    Private Sub mnuBrowserPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserPrint.Click
        wbOne.ShowPrintDialog()
    End Sub

    Private Sub mnuBrowserSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserSelectAll.Click
        Const OLECMDID_SELECTALL = 17
        wbOne.ActiveXInstance.execWB(OLECMDID_SELECTALL, 2)

    End Sub

    Private Sub mnuBrowserClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserClear.Click
        wbOne.Navigate("")
    End Sub

    Private Sub mnuBrowserCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserCopy.Click
        Const OLECMDID_COPY = 12
        wbOne.ActiveXInstance.execWB(OLECMDID_COPY, 2)

    End Sub

    Private Sub mnuBrowserTextSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBrowserTextSizeLargest.Click, mnuBrowserTextSizeLarger.Click, mnuBrowserTextSizeMedium.Click, mnuBrowserTextSizeSmaller.Click, mnuBrowserTextSizeSmallest.Click

        Const OLECMDID_ZOOM = 19

        For Each f As ToolStripMenuItem In mnuBrowserTextSize.DropDown.Items
            f.Checked = (f Is sender)
        Next

        Dim h As Object = wbOne.ActiveXInstance

        'Next line doesn't seem to work if I remove the last argument and put it in as a variable.
        If Not h Is Nothing Then h.execWB(OLECMDID_ZOOM, 2, CInt(DirectCast(sender, ToolStripMenuItem).Tag))
        My.Settings.BrowserTextSize = CInt(CInt(DirectCast(sender, ToolStripMenuItem).Tag))

        'So that other Internet Explorer Windows don't open at this size.
        h = wbHidden.ActiveXInstance
        If Not h Is Nothing Then
            Try 'sometimes get Trying to revoke a drop target that has not been registered (Exception from HRESULT: 0x80040100 (DRAGDROP_E_NOTREGISTERED))
                wbHidden.ActiveXInstance.execWB(OLECMDID_ZOOM, 2, 2)
            Catch
            End Try
        End If
        'Strange behaviour - code after this seems not to be executed. I originally had the code for checking 
        'the appropriate menu item here, but it never got executed.

    End Sub

    Private Sub wbOne_Navigated(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatedEventArgs) Handles wbOne.Navigated
        'So wbHidden has an ActiveXInstance
        wbHidden.Navigate(e.Url)

        Static SettingSet As Boolean

        If Not SettingSet Then
            'Set the stored Browser text size. If I try doing this on loading or activation, 
            'I get an error, so do it here, once.
            For Each f As ToolStripMenuItem In mnuBrowserTextSize.DropDown.Items
                If CInt(f.Tag) = My.Settings.BrowserTextSize Then
                    f.PerformClick()
                End If
            Next
            SettingSet = True
        End If

    End Sub

#End Region

#Region "Other Menu and ToolStrip Items"

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuFileExit.Click

        Global.System.Windows.Forms.Application.Exit()

    End Sub

    Private Sub mnuResetSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuResetSettings.Click
        My.Settings.Reset() 'so that I can check that things will work the first time the app is run.
        Debug.Print(My.Settings.MainToolbarPanel)
        End     'Otherwise settings will be set again.
    End Sub

    Private Sub StopInFrmMDIToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StopInFrmMDIToolStripMenuItem.Click
        'Because I have the application framework enabled I can't evaluate things in the Immediate Panel when
        'I interrupt running, unless I actually stop within the relevant form. The application framework handles the settings
        'and the splashscreen.
        Stop
    End Sub

    Private Sub StopInFrmOneToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles StopInFrmOneToolStripMenuItem.Click
        ActiveDeal.Stophere()
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuWindowCascade.Click
        LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuWindowTileVertical.Click
        LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuWindowTileHorizontal.Click
        LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles mnuWindowArrangeIcons.Click
        LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolsOptions.Click
        frmOptions.ShowDialog()
        frmOptions = Nothing
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHelpAbout.Click
        frmAbout.ShowDialog()
    End Sub

    Private Sub tsiNew_DropDownClosed(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsiNew.DropDownClosed
        mnuFileNewBlank.Visible = True
        zspTemplates.Visible = True
    End Sub

    Private Sub ToolStripSplitButton1_DropDownOpening(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsiNew.DropDownOpening

        mnuFileNewBlank.Visible = False
        zspTemplates.Visible = False

    End Sub

    Private Sub mnuToolsReviewDynamicLinkedLibraries_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolsReviewDynamicLinkedLibraries.Click
        frmDLLReview.ShowDialog()
    End Sub

#End Region

#Region "Cursors"

    'Create them just once or drawing will be badly slowed down.
    Friend crsAdd As Cursor = GetCursor(My.Resources.AddCursor)
    Private crsTransaction As Cursor = GetCursor(My.Resources.AddTrans)
    Private crsCredSupNew As Cursor = GetCursor(My.Resources.AddNewCredSupport)
    Private crsCredSupExisting As Cursor = GetCursor(My.Resources.AddExistingCredSupport)
    Private crsExisting As Cursor = GetCursor(My.Resources.AddExisting)
    Private crsExistingPassThrough As Cursor = GetCursor(My.Resources.AddExistingPassThrough)
    Private crsNewPassThrough As Cursor = GetCursor(My.Resources.AddNewPassThrough)
    Friend crsSec As Cursor = GetCursor(My.Resources.AddSecurity)
    Private crsSwapNew As Cursor = GetCursor(My.Resources.AddNewSwap)
    Private crsSwapExisting As Cursor = GetCursor(My.Resources.AddExistingSwap)

    Friend crsXHair As Cursor = GetCursor(My.Resources.CrossHair1)
    Friend crsText As Cursor = GetCursor(My.Resources.TextCursor)

    Private Function GetCursor(ByVal ByteArray As Byte()) As Cursor
        'Had big problems with the Dotfuscator when I used the type, string overload of the cursor constructor.
        Return New Cursor(New System.IO.MemoryStream(ByteArray))
    End Function

    Friend Function EntityCursor() As Cursor

        'Return the cursor to be shown when mouse goes over an entity control or starts drawing a connection. 
        'Can't have this in types of connection because I need it even before they've been created.

        If tsiDrawCredSupNew.Checked Then Return crsCredSupNew
        If tsiDrawCredSupExisting.Checked Then Return crsCredSupExisting

        If tsiDrawFundingExisting.Checked Then Return crsExisting
        If tsiDrawFundingNew.Checked Then Return crsTransaction

        If tsiDrawPassThroughExisting.Checked Then Return crsExistingPassThrough
        If tsiDrawPassthroughNew.Checked Then Return crsNewPassThrough

        If tsiDrawSwapNew.Checked Then Return crsSwapNew
        If tsiDrawSwapExisting.Checked Then Return crsSwapExisting

        If tsiEntity.Checked Then Return crsAdd 'not allowing an entity to be added on top of another.

        Return Cursors.SizeAll

    End Function

#End Region

    Private Sub mnuHelpResIpsa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuHelpResIpsa.Click
        '   Help.ShowHelp(Me, "C:\Res Ipsa\Help.hlp")   'think I need to convert it to an html help file.

    End Sub

    Private Sub tvOne_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvOne.AfterSelect

    End Sub
End Class