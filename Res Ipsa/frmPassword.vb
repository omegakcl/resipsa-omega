Friend Class frmPassword

    Private InitOpen As String
    Private InitModify As String

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If Not ConfirmPassword(txtPOpen, InitOpen) Then Exit Sub
        If Not ConfirmPassword(txtPModify, InitModify) Then Exit Sub

        Me.DialogResult = Windows.Forms.DialogResult.OK

        Close()

    End Sub

    Private Function ConfirmPassword(ByVal V As TextBox, ByVal CheckText As String) As Boolean

        ConfirmPassword = True
        If V.Text <> "" And CheckText <> V.Text Then
            Dim f As New frmConfirmPassword(V.Text, V Is txtPOpen)
            f.ShowDialog()
            If f.DialogResult = Windows.Forms.DialogResult.Cancel Or f.DialogResult = Windows.Forms.DialogResult.Abort Then ConfirmPassword = False

            If f.DialogResult = Windows.Forms.DialogResult.Cancel Then V.Text = ""

            f.Dispose()

        End If

    End Function

    Public Sub New(ByVal POpen As String, ByVal PModify As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        Me.txtPOpen.Text = POpen
        Me.txtPModify.Text = PModify
        InitOpen = POpen
        InitModify = PModify

    End Sub

End Class
