
Imports System.Runtime.Serialization
Imports System.Collections
Imports System.Reflection

'There is a problem with Serialization. Basically if you try to serialize an object to which another object has a WithEvents reference, VB will try to 
'serialize that latter object too. If it's not marked as Serializable, you'll get an error. If you mark it as serializable and it inherits 
'from a control, you get an error because controls are not serializable. The purpose of this class is to avoid serializing any events.
'Tried removing this provision Nov 15 now that I have vs 2013 and problem seems to be subsisting - got a message that TableLayoutPanel is not serializable.
'But something has changed because I can't load files any more.

<Serializable()> Public MustInherit Class SerializableObject
    Implements ISerializable, IDeserializationCallback

    '*******************************************************************************
    ' New:  Default constructor
    ' 1) Blank Object
    ' 2) Deserialization Constructor
    '*******************************************************************************
    Public Sub New()
        MyBase.New()
    End Sub

    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)

        ' Instantiate base class
        MyBase.New()

        ' Record the Info object between the constructor and the deserialization callback
        ' because if we set the values of fields that are declared with
        ' an initializer, the initializer will come along and overwrite the value
        ' after this constructor has been called
        mInfo = info

    End Sub

#Region "Serialization Implementation"

    ' Variable to store the serialization info between New() and OnDeserialization()
    <NonSerialized()> Private mInfo As SerializationInfo

    Protected Overridable Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext) Implements ISerializable.GetObjectData

        ' Use the shared method to populate the SerializationInfo object
        '  SerializableObject.SerializeObject(Me, info)

        ' Local Variables
        Dim aMembersToSerialize As MemberInfo()

        ' Error handler
        Try

            ' Get a list of all fields in this object and derived objects
            ' that are to be serialized
            aMembersToSerialize = SerializableObject.GetSerializableMembers(Me.GetType)

            '  Loop around all fields and save their values
            For Each objMember As MemberInfo In aMembersToSerialize

                ' It is valid to serialize if it is in this array
                ' Derived Fields with the same name as base fields
                ' will automaticall be handled, so we don't need to
                ' worry about duplicates

                ' Determine if it is a field or property
                If objMember.MemberType = MemberTypes.Field Then

                    Dim objField As FieldInfo = DirectCast(objMember, FieldInfo)
                    info.AddValue(objField.Name, objField.GetValue(Me))

                ElseIf objMember.MemberType = MemberTypes.Property Then

                    Dim objProperty As PropertyInfo = DirectCast(objMember, PropertyInfo)
                    info.AddValue(objProperty.Name, objProperty.GetValue(Me, Nothing))

                End If

            Next

        Catch ex As Exception

            ' Trace any errors
            Trace.WriteLine("Error during serialization: " & ex.Message)
            Throw New SerializationException("Error during Serialization. ", ex)

        End Try

    End Sub

    Protected Overridable Sub OnDeserialization(ByVal sender As Object) Implements System.Runtime.Serialization.IDeserializationCallback.OnDeserialization

        ' Call the shared method to deserialize the object

        ' The object must be fully created for this method to be called
        ' i.e. Don't call this from a constructor

        ' Local Variables
        Dim aFieldsToDeserialize As MemberInfo()
        Dim aValues As Object()

        ' Error handler
        Try

            ' Get a list of all fields in this object and derived objects
            aFieldsToDeserialize = SerializableObject.GetSerializableMembers(Me.GetType)

            ' Loop around all fields and get their values from the info object
            aValues = DirectCast(Array.CreateInstance(GetType(Object), aFieldsToDeserialize.Length), Object())

            For nCount As Integer = 0 To aFieldsToDeserialize.Length - 1
                Dim C As Type = GetMemberType(aFieldsToDeserialize(nCount))

                If Not C Is Nothing Then aValues(nCount) = mInfo.GetValue(aFieldsToDeserialize(nCount).Name, C)

            Next

            ' Now use formatter services to populate this object's fields
            FormatterServices.PopulateObjectMembers(Me, aFieldsToDeserialize, aValues)

        Catch ex As Exception

            ' Trace errors
            Trace.WriteLine("Error during deserialization: " & ex.Message)
            Throw New SerializationException("Error during deserialization", ex)

        End Try

        ' Kill the recorded info object
        mInfo = Nothing

    End Sub

#End Region

#Region "Shared Methods"

    '*******************************************************************************
    ' GetSerializableMembers: Returns an array of all fields in the type and base types
    '                         That are to be serialized.
    '                         It excludes delegates and event delegates
    '*******************************************************************************
    <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, Assertion:=True, SerializationFormatter:=True)> _
    Private Shared Function GetSerializableMembers(ByVal PersistableType As System.Type) As MemberInfo()

        ' Local Variables

        Dim aSerializableMembers As New System.Collections.ArrayList

        ' Filter non-serializable fields and event delegates
        For Each objMember As MemberInfo In FormatterServices.GetSerializableMembers(PersistableType)

            Dim c As Type = GetMemberType(objMember)
            ' We're only interested in fields and properties

            If Not c Is Nothing Then

                ' If it has a NonSerialized Attribute or if it is an event delegate, skip it
                If (Not GetType(System.Delegate).IsAssignableFrom(c)) Then

                    ' Add the field as it is valid for serialization
                    aSerializableMembers.Add(objMember)

                End If

            End If

        Next objMember

        ' Return the array of persistable fields - need to convert to an array first
        Return DirectCast(aSerializableMembers.ToArray(GetType(MemberInfo)), MemberInfo())

    End Function

    Shared Function GetMemberType(ByVal objMember As MemberInfo) As Type

        If objMember.MemberType = MemberTypes.Field Then

            Return DirectCast(objMember, FieldInfo).FieldType

        ElseIf objMember.MemberType = MemberTypes.Property Then

            Return DirectCast(objMember, PropertyInfo).PropertyType

        End If
    End Function

#End Region

End Class

<Serializable()> Friend Class SerialDataClass
    'THIS CLASS IS ESSENTIAL. DON'T BE TEMPTED TO SERIALIZE THINGS SEPARATELY OR THEY WON'T RELATE TO EACH OTHER WHEN THEY'RE UNPACKED.
    'Took it out of frmBase just so I could see it in the class diagram.

    Friend SerialSecurityCollection As List(Of SecurityArc)
    Friend SerialCredSupCollection As List(Of CreditSupportCon)
    Friend SerialStanConCols As List(Of TrancheCollection)

    Friend eCol As New List(Of Entity)
End Class
