Friend Class frmOptions

    Dim TempFlat As Boolean
    'Good to have a PartyCanvas as the picture control here. However if I just disable it the 
    'constituent controls will also get the disabled look. So I have to draw the control in.
    Dim P1 As EntityControl
    Dim P2 As EntityControl

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        With picSample
            'These won't be shown, because I just want copies of them to display to the 
            'user and I use DrawToBitmap to achieve that. But it seems they do actually 
            'need to be visible or the textbox doesn't get shown.
            P1 = .AddEC(New Point(20, .Height + 50), False)
            P1.convertrtbtotxtb()
            P2 = .AddEC(New Point(.Width + 50, .Height + 50), False)
            P2.convertrtbtotxtb()   'keeps lines horizontal even if P2 isn't visible.
            MouseDownControl = P1
            Dim j As New TransactionCon(picSample, False, P1)
            Dim e2 As New PartyCanvas.CursorEventArgs(New MouseEventArgs(Windows.Forms.MouseButtons.Left, 0, 0, 0, 0))
            e2.OverControl = P2
            j.SetTarget(P1, e2)
            j.MakeReal()
            .BackColor = My.Settings.CanvasBackColour   'designer sets it back to design time value without this.
            .Refresh()
        End With

        If My.Settings.ControlStyleFlat Then
            optFlat.Checked = True
        Else
            opt3D.Checked = True
        End If

        JurisdictionSpace.SetItems(Me.JCombo1.Items)

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        With My.Settings
            .CanvasBackColour = Me.picSample.BackColor
            .ControlForeColour = P1.BackColor
            .ControlStyleFlat = TempFlat
        End With

        For Each f As frmBase In frmMDI.MdiChildren
            With f.picCanvas
                .BackColor = Me.picSample.BackColor
                .SetControlColour(Me.picSample.EntityControls(0).BackColor)
                .SetControlStyle(optFlat.Checked)
            End With
        Next
        Close()

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdBackColour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBackColour.Click
        With ColorDialog1
            .Color = picSample.BackColor
            .ShowDialog()
            picSample.BackColor = .Color
        End With
    End Sub

    Private Sub cmdPartyColour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPartyColour.Click
        With ColorDialog1
            .Color = picSample.EntityControls(0).BackColor
            .ShowDialog()
            picSample.SetControlColour(.Color)
        End With
    End Sub

    Private Sub optFlat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optFlat.CheckedChanged
        TempFlat = True
        picSample.SetControlStyle(True)
    End Sub

    Private Sub opt3D_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt3D.CheckedChanged
        TempFlat = False
        picSample.SetControlStyle(False)
    End Sub

    Private Sub cmdReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
        opt3D.Checked = True
        picSample.BackColor = Color.Teal
        picSample.SetControlColour(System.Drawing.SystemColors.Control)
    End Sub
End Class