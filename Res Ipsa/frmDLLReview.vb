Imports System.Reflection

Friend Class frmDLLReview

    Dim WithEvents s As New JurisdictionFinder

    Private Sub frmDLLReview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListView1.Items.Clear()
        Label1.Text = "Source directory: " & SourceDir()
        s.GetJurs()
        Icon = frmMDI.Icon
        OK_Button.Focus()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Close()
    End Sub

    Private Sub s_GotJur(ByVal JJ As IJurisdiction, ByVal objDLL As System.Reflection.Assembly, ByVal sPath As String) Handles s.GotJur

        If Not JJ Is Nothing Then
            With ListView1.Items.Add(My.Computer.FileSystem.GetName(sPath))
                .ImageIndex = 0

                .SubItems.Add(JJ.Name)
                Dim aca() As AssemblyCompanyAttribute = objDLL.GetCustomAttributes(GetType(AssemblyCompanyAttribute), True)
                If Not aca Is Nothing Then .SubItems.Add(aca(0).Company)
                .SubItems.Add(FileSystem.FileDateTime(sPath).ToString)

            End With
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Process.Start("explorer.exe", SourceDir)
        Catch
        End Try

    End Sub

    Private Function SourceDir() As String
        Return My.Application.Info.DirectoryPath
    End Function
End Class
