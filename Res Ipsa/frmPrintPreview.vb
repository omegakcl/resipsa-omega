Public Class frmPrintPreview

    'Better to have a form with a printpreview control than use a printpreview dialog because then I 
    'don't have the superfluous toolbuttons that the dialog has (twopages etc.).
    Private MyPic As PartyCanvas
    WithEvents ppcOne As PrintPreviewControl
 
    Private Sub Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbClose.Click
        Me.Close()
    End Sub

    Private Sub tsbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPrint.Click
        ppcOne.Document.Print()
    End Sub

    Friend Sub New(ByVal b As frmBase)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        MyPic = b.picCanvas

        tsbShowTitle.Checked = b.picCanvas.ShowTitleWhenPrinting
        LoadPrintPreviewControl()
        AddHandler tsbShowTitle.CheckStateChanged, AddressOf ToolStripButton1_CheckStateChanged
        b.SetPrintButtonToolTip(Me.tsbPrint)

    End Sub

    Private Sub LoadPrintPreviewControl()

        'Only way to refresh the Document.
        If Not ppcOne Is Nothing Then Controls.Remove(ppcOne)

        ppcOne = New PrintPreviewControl
        ppcOne.Dock = DockStyle.Fill

        Controls.Add(ppcOne)
        ppcOne.Document = MyPic.PrintDocument1
        ToolStrip1.SendToBack()      'Otherwise ppcOne will dock before the toolstrip and go underneath it.

    End Sub

    Private Sub ToolStripButton1_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        MyPic.ShowTitleWhenPrinting = tsbShowTitle.Checked
        LoadPrintPreviewControl()
    End Sub

    Private Sub frmPrintPreview_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If Not ppcOne Is Nothing Then Debug.Print(ppcOne.Bounds.ToString)
    End Sub
End Class