<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Friend Class frmMDI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMDI))
        Me.mnuHelpResIpsa = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuHelpMechanisms = New System.Windows.Forms.ToolStripMenuItem
        Me.zspIUH = New System.Windows.Forms.ToolStripSeparator
        Me.mnuHelpAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuToolsOptions = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileExit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewTransaction = New System.Windows.Forms.ToolStripMenuItem
        Me.cxmTemplates = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuFileNewBlank = New System.Windows.Forms.ToolStripMenuItem
        Me.zspTemplates = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFileNewUnderwrittenBondIssue = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewSecondaryTrade = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewRepo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewBuySellBack = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewRepackaging = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewSecuritisation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewAssetbackedCP = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewShareBuyback = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileNewWholeBusinessSecuritisation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFileOpen = New System.Windows.Forms.ToolStripMenuItem
        Me.zspRecentFiles = New System.Windows.Forms.ToolStripSeparator
        Me.mnuRecentFile1 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecentFile2 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecentFile3 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRecentFile4 = New System.Windows.Forms.ToolStripMenuItem
        Me.zspWEE = New System.Windows.Forms.ToolStripSeparator
        Me.tsiNew = New System.Windows.Forms.ToolStripSplitButton
        Me.mnsMain = New System.Windows.Forms.MenuStrip
        Me.mnuView = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewMainToolbar = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewDrawingToolbar = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTools = New System.Windows.Forms.ToolStripMenuItem
        Me.zspOIJ = New System.Windows.Forms.ToolStripSeparator
        Me.mnuToolsReviewDynamicLinkedLibraries = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowser = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTerms = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWindow = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWindowCascade = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWindowTileVertical = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWindowTileHorizontal = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWindowArrangeIcons = New System.Windows.Forms.ToolStripMenuItem
        Me.zspIJ = New System.Windows.Forms.ToolStripSeparator
        Me.mnuDevelopment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuResetSettings = New System.Windows.Forms.ToolStripMenuItem
        Me.StopInFrmMDIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StopInFrmOneToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cxmTerms = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuTermsTermsWindow = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTermsTextSize = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTermsTextSizeLarge = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTermsTextSizeMedium = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTermsTextSizeSmall = New System.Windows.Forms.ToolStripMenuItem
        Me.cxmBrowser = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuBrowserBack = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserForward = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserClear = New System.Windows.Forms.ToolStripMenuItem
        Me.zspOKJ = New System.Windows.Forms.ToolStripSeparator
        Me.mnuBrowserTextSize = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserTextSizeLargest = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserTextSizeLarger = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserTextSizeMedium = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserTextSizeSmaller = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserTextSizeSmallest = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserSelectAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserCopy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.zsp17 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuBrowserProperties = New System.Windows.Forms.ToolStripMenuItem
        Me.zsp234 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuBrowserContents = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrowserIndexes = New System.Windows.Forms.ToolStripMenuItem
        Me.zsp123 = New System.Windows.Forms.ToolStripSeparator
        Me.tstMain = New System.Windows.Forms.ToolStrip
        Me.tsiOpen = New System.Windows.Forms.ToolStripButton
        Me.zstW = New System.Windows.Forms.ToolStripSeparator
        Me.tsiBack = New System.Windows.Forms.ToolStripButton
        Me.tsiForward = New System.Windows.Forms.ToolStripButton
        Me.tsbContents = New System.Windows.Forms.ToolStripButton
        Me.tsbIndex = New System.Windows.Forms.ToolStripButton
        Me.pnlWebBrowser = New System.Windows.Forms.Panel
        Me.pnlWBBorderProvider = New System.Windows.Forms.Panel
        Me.wbOne = New System.Windows.Forms.WebBrowser
        Me.pnltreeView = New System.Windows.Forms.Panel
        Me.tvOne = New System.Windows.Forms.TreeView
        Me.ilsTreeview = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlPropGrid = New System.Windows.Forms.Panel
        Me.tbsOne = New System.Windows.Forms.TabControl
        Me.tbpLeft = New System.Windows.Forms.TabPage
        Me.tbpRight = New System.Windows.Forms.TabPage
        Me.prgOne = New System.Windows.Forms.PropertyGrid
        Me.tspTop = New System.Windows.Forms.ToolStripPanel
        Me.tspLeft = New System.Windows.Forms.ToolStripPanel
        Me.tspBottom = New System.Windows.Forms.ToolStripPanel
        Me.tspRight = New System.Windows.Forms.ToolStripPanel
        Me.tstPropGrid = New System.Windows.Forms.ToolStrip
        Me.tsiCopyPG = New System.Windows.Forms.ToolStripButton
        Me.tsiConfirm = New System.Windows.Forms.ToolStripButton
        Me.wbHidden = New System.Windows.Forms.WebBrowser
        Me.tstDraw = New System.Windows.Forms.ToolStrip
        Me.tsiEntity = New System.Windows.Forms.ToolStripButton
        Me.zspOne = New System.Windows.Forms.ToolStripSeparator
        Me.tsiSelect = New System.Windows.Forms.ToolStripButton
        Me.zspTwo = New System.Windows.Forms.ToolStripSeparator
        Me.tsiDrawFundingExisting = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawFundingNew = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawPassThroughExisting = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawPassthroughNew = New System.Windows.Forms.ToolStripButton
        Me.zspThree = New System.Windows.Forms.ToolStripSeparator
        Me.tsiDrawSwapExisting = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawSwapNew = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsiDrawCredSupExisting = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawCredSupNew = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsiDrawSecurityExisting = New System.Windows.Forms.ToolStripButton
        Me.tsiDrawSecurityNew = New System.Windows.Forms.ToolStripButton
        Me.tsiNonRepSec = New System.Windows.Forms.ToolStripButton
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.cxmTemplates.SuspendLayout()
        Me.mnsMain.SuspendLayout()
        Me.cxmTerms.SuspendLayout()
        Me.cxmBrowser.SuspendLayout()
        Me.tstMain.SuspendLayout()
        Me.pnlWebBrowser.SuspendLayout()
        Me.pnlWBBorderProvider.SuspendLayout()
        Me.pnltreeView.SuspendLayout()
        Me.pnlPropGrid.SuspendLayout()
        Me.tbsOne.SuspendLayout()
        Me.tbpRight.SuspendLayout()
        Me.tstPropGrid.SuspendLayout()
        Me.tstDraw.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuHelpResIpsa
        '
        Me.mnuHelpResIpsa.Image = Global.LegalObjectsModel.My.Resources.Resources.Help
        Me.mnuHelpResIpsa.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuHelpResIpsa.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuHelpResIpsa.Name = "mnuHelpResIpsa"
        Me.mnuHelpResIpsa.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1), System.Windows.Forms.Keys)
        Me.mnuHelpResIpsa.Size = New System.Drawing.Size(231, 22)
        Me.mnuHelpResIpsa.Text = "&Res Ipsa Help"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelpResIpsa, Me.mnuHelpMechanisms, Me.zspIUH, Me.mnuHelpAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "&Help"
        '
        'mnuHelpMechanisms
        '
        Me.mnuHelpMechanisms.ImageTransparentColor = System.Drawing.Color.Black
        Me.mnuHelpMechanisms.Name = "mnuHelpMechanisms"
        Me.mnuHelpMechanisms.Size = New System.Drawing.Size(231, 22)
        Me.mnuHelpMechanisms.Text = "&Mechanisms and General Law"
        '
        'zspIUH
        '
        Me.zspIUH.Name = "zspIUH"
        Me.zspIUH.Size = New System.Drawing.Size(228, 6)
        '
        'mnuHelpAbout
        '
        Me.mnuHelpAbout.Name = "mnuHelpAbout"
        Me.mnuHelpAbout.Size = New System.Drawing.Size(231, 22)
        Me.mnuHelpAbout.Text = "&About Res Ipsa"
        '
        'mnuToolsOptions
        '
        Me.mnuToolsOptions.Name = "mnuToolsOptions"
        Me.mnuToolsOptions.Size = New System.Drawing.Size(221, 22)
        Me.mnuToolsOptions.Text = "&Options"
        '
        'mnuFileExit
        '
        Me.mnuFileExit.MergeIndex = 1006
        Me.mnuFileExit.Name = "mnuFileExit"
        Me.mnuFileExit.Size = New System.Drawing.Size(163, 22)
        Me.mnuFileExit.Text = "E&xit"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewTransaction, Me.mnuFileOpen, Me.zspRecentFiles, Me.mnuRecentFile1, Me.mnuRecentFile2, Me.mnuRecentFile3, Me.mnuRecentFile4, Me.zspWEE, Me.mnuFileExit})
        Me.mnuFile.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.mnuFile.MergeIndex = 4
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuFileNewTransaction
        '
        Me.mnuFileNewTransaction.DropDown = Me.cxmTemplates
        Me.mnuFileNewTransaction.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileNewTransaction.Name = "mnuFileNewTransaction"
        Me.mnuFileNewTransaction.Size = New System.Drawing.Size(163, 22)
        Me.mnuFileNewTransaction.Text = "New &Transaction"
        '
        'cxmTemplates
        '
        Me.cxmTemplates.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewBlank, Me.zspTemplates, Me.mnuFileNewUnderwrittenBondIssue, Me.mnuFileNewSecondaryTrade, Me.mnuFileNewRepo, Me.mnuFileNewBuySellBack, Me.mnuFileNewRepackaging, Me.mnuFileNewSecuritisation, Me.mnuFileNewAssetbackedCP, Me.mnuFileNewShareBuyback, Me.mnuFileNewWholeBusinessSecuritisation})
        Me.cxmTemplates.Name = "cxmTemplates"
        Me.cxmTemplates.OwnerItem = Me.tsiNew
        Me.cxmTemplates.Size = New System.Drawing.Size(231, 230)
        '
        'mnuFileNewBlank
        '
        Me.mnuFileNewBlank.Name = "mnuFileNewBlank"
        Me.mnuFileNewBlank.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuFileNewBlank.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewBlank.Text = "Blank &Transaction1"
        '
        'zspTemplates
        '
        Me.zspTemplates.Name = "zspTemplates"
        Me.zspTemplates.Size = New System.Drawing.Size(227, 6)
        '
        'mnuFileNewUnderwrittenBondIssue
        '
        Me.mnuFileNewUnderwrittenBondIssue.Name = "mnuFileNewUnderwrittenBondIssue"
        Me.mnuFileNewUnderwrittenBondIssue.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewUnderwrittenBondIssue.Text = "Underwritten Bond Issue"
        '
        'mnuFileNewSecondaryTrade
        '
        Me.mnuFileNewSecondaryTrade.Name = "mnuFileNewSecondaryTrade"
        Me.mnuFileNewSecondaryTrade.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewSecondaryTrade.Text = "Secondary Trade"
        '
        'mnuFileNewRepo
        '
        Me.mnuFileNewRepo.Name = "mnuFileNewRepo"
        Me.mnuFileNewRepo.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewRepo.Text = "Repo"
        '
        'mnuFileNewBuySellBack
        '
        Me.mnuFileNewBuySellBack.Name = "mnuFileNewBuySellBack"
        Me.mnuFileNewBuySellBack.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewBuySellBack.Text = "Buy/Sell Back"
        '
        'mnuFileNewRepackaging
        '
        Me.mnuFileNewRepackaging.Name = "mnuFileNewRepackaging"
        Me.mnuFileNewRepackaging.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewRepackaging.Text = "Repackaging"
        '
        'mnuFileNewSecuritisation
        '
        Me.mnuFileNewSecuritisation.Name = "mnuFileNewSecuritisation"
        Me.mnuFileNewSecuritisation.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewSecuritisation.Text = "Securitisation"
        '
        'mnuFileNewAssetbackedCP
        '
        Me.mnuFileNewAssetbackedCP.Name = "mnuFileNewAssetbackedCP"
        Me.mnuFileNewAssetbackedCP.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewAssetbackedCP.Text = "Asset-backed CP Issue"
        '
        'mnuFileNewShareBuyback
        '
        Me.mnuFileNewShareBuyback.Name = "mnuFileNewShareBuyback"
        Me.mnuFileNewShareBuyback.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewShareBuyback.Text = "Share Buy-back"
        '
        'mnuFileNewWholeBusinessSecuritisation
        '
        Me.mnuFileNewWholeBusinessSecuritisation.Name = "mnuFileNewWholeBusinessSecuritisation"
        Me.mnuFileNewWholeBusinessSecuritisation.Size = New System.Drawing.Size(230, 22)
        Me.mnuFileNewWholeBusinessSecuritisation.Text = "Whole Business Securitisation"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Image = Global.LegalObjectsModel.My.Resources.Resources.Open
        Me.mnuFileOpen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuFileOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuFileOpen.Name = "mnuFileOpen"
        Me.mnuFileOpen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuFileOpen.Size = New System.Drawing.Size(163, 22)
        Me.mnuFileOpen.Text = "&Open"
        '
        'zspRecentFiles
        '
        Me.zspRecentFiles.MergeIndex = 1000
        Me.zspRecentFiles.Name = "zspRecentFiles"
        Me.zspRecentFiles.Size = New System.Drawing.Size(160, 6)
        Me.zspRecentFiles.Visible = False
        '
        'mnuRecentFile1
        '
        Me.mnuRecentFile1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuRecentFile1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuRecentFile1.MergeIndex = 1001
        Me.mnuRecentFile1.Name = "mnuRecentFile1"
        Me.mnuRecentFile1.Size = New System.Drawing.Size(163, 22)
        Me.mnuRecentFile1.Text = "RecentFile1"
        Me.mnuRecentFile1.Visible = False
        '
        'mnuRecentFile2
        '
        Me.mnuRecentFile2.MergeIndex = 1002
        Me.mnuRecentFile2.Name = "mnuRecentFile2"
        Me.mnuRecentFile2.Size = New System.Drawing.Size(163, 22)
        Me.mnuRecentFile2.Text = "RecentFile2"
        Me.mnuRecentFile2.Visible = False
        '
        'mnuRecentFile3
        '
        Me.mnuRecentFile3.MergeIndex = 1003
        Me.mnuRecentFile3.Name = "mnuRecentFile3"
        Me.mnuRecentFile3.Size = New System.Drawing.Size(163, 22)
        Me.mnuRecentFile3.Text = "RecentFile3"
        Me.mnuRecentFile3.Visible = False
        '
        'mnuRecentFile4
        '
        Me.mnuRecentFile4.MergeIndex = 1004
        Me.mnuRecentFile4.Name = "mnuRecentFile4"
        Me.mnuRecentFile4.Size = New System.Drawing.Size(163, 22)
        Me.mnuRecentFile4.Text = "RecentFile4"
        Me.mnuRecentFile4.Visible = False
        '
        'zspWEE
        '
        Me.zspWEE.MergeIndex = 1005
        Me.zspWEE.Name = "zspWEE"
        Me.zspWEE.Size = New System.Drawing.Size(160, 6)
        '
        'tsiNew
        '
        Me.tsiNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiNew.DropDown = Me.cxmTemplates
        Me.tsiNew.Image = Global.LegalObjectsModel.My.Resources.Resources._New
        Me.tsiNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiNew.Name = "tsiNew"
        Me.tsiNew.Size = New System.Drawing.Size(32, 22)
        Me.tsiNew.Text = "ToolStripSplitButton1"
        Me.tsiNew.ToolTipText = "New"
        '
        'mnsMain
        '
        Me.mnsMain.AllowItemReorder = True
        Me.mnsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuView, Me.mnuTools, Me.mnuBrowser, Me.mnuTerms, Me.mnuWindow, Me.mnuHelp, Me.mnuDevelopment})
        Me.mnsMain.Location = New System.Drawing.Point(0, 0)
        Me.mnsMain.MdiWindowListItem = Me.mnuWindow
        Me.mnsMain.Name = "mnsMain"
        Me.mnsMain.Size = New System.Drawing.Size(815, 24)
        Me.mnsMain.TabIndex = 5
        Me.mnsMain.TabStop = True
        Me.mnsMain.Text = "MenuStrip"
        '
        'mnuView
        '
        Me.mnuView.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewMainToolbar, Me.mnuViewDrawingToolbar})
        Me.mnuView.Name = "mnuView"
        Me.mnuView.Size = New System.Drawing.Size(44, 20)
        Me.mnuView.Text = "&View"
        '
        'mnuViewMainToolbar
        '
        Me.mnuViewMainToolbar.CheckOnClick = True
        Me.mnuViewMainToolbar.Name = "mnuViewMainToolbar"
        Me.mnuViewMainToolbar.Size = New System.Drawing.Size(162, 22)
        Me.mnuViewMainToolbar.Text = "Main &Toolbar"
        '
        'mnuViewDrawingToolbar
        '
        Me.mnuViewDrawingToolbar.CheckOnClick = True
        Me.mnuViewDrawingToolbar.Name = "mnuViewDrawingToolbar"
        Me.mnuViewDrawingToolbar.Size = New System.Drawing.Size(162, 22)
        Me.mnuViewDrawingToolbar.Text = "&Drawing Toolbar"
        '
        'mnuTools
        '
        Me.mnuTools.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsOptions, Me.zspOIJ, Me.mnuToolsReviewDynamicLinkedLibraries})
        Me.mnuTools.Name = "mnuTools"
        Me.mnuTools.Size = New System.Drawing.Size(48, 20)
        Me.mnuTools.Text = "&Tools"
        '
        'zspOIJ
        '
        Me.zspOIJ.Name = "zspOIJ"
        Me.zspOIJ.Size = New System.Drawing.Size(218, 6)
        '
        'mnuToolsReviewDynamicLinkedLibraries
        '
        Me.mnuToolsReviewDynamicLinkedLibraries.Name = "mnuToolsReviewDynamicLinkedLibraries"
        Me.mnuToolsReviewDynamicLinkedLibraries.Size = New System.Drawing.Size(221, 22)
        Me.mnuToolsReviewDynamicLinkedLibraries.Text = "&Review Legal Extension Files"
        '
        'mnuBrowser
        '
        Me.mnuBrowser.Name = "mnuBrowser"
        Me.mnuBrowser.Size = New System.Drawing.Size(61, 20)
        Me.mnuBrowser.Text = "&Browser"
        '
        'mnuTerms
        '
        Me.mnuTerms.Name = "mnuTerms"
        Me.mnuTerms.Size = New System.Drawing.Size(52, 20)
        Me.mnuTerms.Text = "Ter&ms"
        '
        'mnuWindow
        '
        Me.mnuWindow.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWindowCascade, Me.mnuWindowTileVertical, Me.mnuWindowTileHorizontal, Me.mnuWindowArrangeIcons, Me.zspIJ})
        Me.mnuWindow.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuWindow.MergeIndex = 6
        Me.mnuWindow.Name = "mnuWindow"
        Me.mnuWindow.Size = New System.Drawing.Size(63, 20)
        Me.mnuWindow.Text = "&Window"
        '
        'mnuWindowCascade
        '
        Me.mnuWindowCascade.Image = Global.LegalObjectsModel.My.Resources.Resources.Cascade
        Me.mnuWindowCascade.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuWindowCascade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuWindowCascade.Name = "mnuWindowCascade"
        Me.mnuWindowCascade.Size = New System.Drawing.Size(151, 22)
        Me.mnuWindowCascade.Text = "&Cascade"
        '
        'mnuWindowTileVertical
        '
        Me.mnuWindowTileVertical.Image = Global.LegalObjectsModel.My.Resources.Resources.TileH
        Me.mnuWindowTileVertical.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuWindowTileVertical.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuWindowTileVertical.Name = "mnuWindowTileVertical"
        Me.mnuWindowTileVertical.Size = New System.Drawing.Size(151, 22)
        Me.mnuWindowTileVertical.Text = "Tile &Vertical"
        '
        'mnuWindowTileHorizontal
        '
        Me.mnuWindowTileHorizontal.Image = Global.LegalObjectsModel.My.Resources.Resources.TileV
        Me.mnuWindowTileHorizontal.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuWindowTileHorizontal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuWindowTileHorizontal.Name = "mnuWindowTileHorizontal"
        Me.mnuWindowTileHorizontal.Size = New System.Drawing.Size(151, 22)
        Me.mnuWindowTileHorizontal.Text = "Tile &Horizontal"
        '
        'mnuWindowArrangeIcons
        '
        Me.mnuWindowArrangeIcons.Name = "mnuWindowArrangeIcons"
        Me.mnuWindowArrangeIcons.Size = New System.Drawing.Size(151, 22)
        Me.mnuWindowArrangeIcons.Text = "&Arrange Icons"
        '
        'zspIJ
        '
        Me.zspIJ.Name = "zspIJ"
        Me.zspIJ.Size = New System.Drawing.Size(148, 6)
        '
        'mnuDevelopment
        '
        Me.mnuDevelopment.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuResetSettings, Me.StopInFrmMDIToolStripMenuItem, Me.StopInFrmOneToolStripMenuItem})
        Me.mnuDevelopment.Name = "mnuDevelopment"
        Me.mnuDevelopment.Size = New System.Drawing.Size(130, 20)
        Me.mnuDevelopment.Text = "[Development Tools]"
        '
        'mnuResetSettings
        '
        Me.mnuResetSettings.Name = "mnuResetSettings"
        Me.mnuResetSettings.Size = New System.Drawing.Size(202, 22)
        Me.mnuResetSettings.Text = "Reset Settings and Close"
        '
        'StopInFrmMDIToolStripMenuItem
        '
        Me.StopInFrmMDIToolStripMenuItem.Name = "StopInFrmMDIToolStripMenuItem"
        Me.StopInFrmMDIToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.StopInFrmMDIToolStripMenuItem.Text = "Stop in frmMDI"
        '
        'StopInFrmOneToolStripMenuItem
        '
        Me.StopInFrmOneToolStripMenuItem.Name = "StopInFrmOneToolStripMenuItem"
        Me.StopInFrmOneToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.StopInFrmOneToolStripMenuItem.Text = "Stop in frmOne"
        '
        'cxmTerms
        '
        Me.cxmTerms.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTermsTermsWindow, Me.mnuTermsTextSize})
        Me.cxmTerms.Name = "cxmTerms"
        Me.cxmTerms.Size = New System.Drawing.Size(155, 48)
        '
        'mnuTermsTermsWindow
        '
        Me.mnuTermsTermsWindow.Checked = True
        Me.mnuTermsTermsWindow.CheckOnClick = True
        Me.mnuTermsTermsWindow.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuTermsTermsWindow.Name = "mnuTermsTermsWindow"
        Me.mnuTermsTermsWindow.Size = New System.Drawing.Size(154, 22)
        Me.mnuTermsTermsWindow.Text = "Terms &Window"
        '
        'mnuTermsTextSize
        '
        Me.mnuTermsTextSize.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTermsTextSizeLarge, Me.mnuTermsTextSizeMedium, Me.mnuTermsTextSizeSmall})
        Me.mnuTermsTextSize.Name = "mnuTermsTextSize"
        Me.mnuTermsTextSize.Size = New System.Drawing.Size(154, 22)
        Me.mnuTermsTextSize.Text = "Text Size"
        '
        'mnuTermsTextSizeLarge
        '
        Me.mnuTermsTextSizeLarge.Name = "mnuTermsTextSizeLarge"
        Me.mnuTermsTextSizeLarge.Size = New System.Drawing.Size(119, 22)
        Me.mnuTermsTextSizeLarge.Tag = "12"
        Me.mnuTermsTextSizeLarge.Text = "&Large"
        '
        'mnuTermsTextSizeMedium
        '
        Me.mnuTermsTextSizeMedium.Name = "mnuTermsTextSizeMedium"
        Me.mnuTermsTextSizeMedium.Size = New System.Drawing.Size(119, 22)
        Me.mnuTermsTextSizeMedium.Tag = "10"
        Me.mnuTermsTextSizeMedium.Text = "&Medium"
        '
        'mnuTermsTextSizeSmall
        '
        Me.mnuTermsTextSizeSmall.Name = "mnuTermsTextSizeSmall"
        Me.mnuTermsTextSizeSmall.Size = New System.Drawing.Size(119, 22)
        Me.mnuTermsTextSizeSmall.Tag = "8"
        Me.mnuTermsTextSizeSmall.Text = "&Small"
        '
        'cxmBrowser
        '
        Me.cxmBrowser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBrowserBack, Me.mnuBrowserForward, Me.mnuBrowserClear, Me.zspOKJ, Me.mnuBrowserTextSize, Me.mnuBrowserSelectAll, Me.mnuBrowserCopy, Me.mnuBrowserPrint, Me.zsp17, Me.mnuBrowserProperties, Me.zsp234, Me.mnuBrowserContents, Me.mnuBrowserIndexes})
        Me.cxmBrowser.Name = "cxmBrowser"
        Me.cxmBrowser.Size = New System.Drawing.Size(128, 242)
        '
        'mnuBrowserBack
        '
        Me.mnuBrowserBack.Image = CType(resources.GetObject("mnuBrowserBack.Image"), System.Drawing.Image)
        Me.mnuBrowserBack.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuBrowserBack.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuBrowserBack.Name = "mnuBrowserBack"
        Me.mnuBrowserBack.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserBack.Text = "&Back"
        '
        'mnuBrowserForward
        '
        Me.mnuBrowserForward.Image = CType(resources.GetObject("mnuBrowserForward.Image"), System.Drawing.Image)
        Me.mnuBrowserForward.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuBrowserForward.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuBrowserForward.Name = "mnuBrowserForward"
        Me.mnuBrowserForward.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserForward.Text = "&Forward"
        '
        'mnuBrowserClear
        '
        Me.mnuBrowserClear.Name = "mnuBrowserClear"
        Me.mnuBrowserClear.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserClear.Text = "&Clear"
        '
        'zspOKJ
        '
        Me.zspOKJ.Name = "zspOKJ"
        Me.zspOKJ.Size = New System.Drawing.Size(124, 6)
        '
        'mnuBrowserTextSize
        '
        Me.mnuBrowserTextSize.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBrowserTextSizeLargest, Me.mnuBrowserTextSizeLarger, Me.mnuBrowserTextSizeMedium, Me.mnuBrowserTextSizeSmaller, Me.mnuBrowserTextSizeSmallest})
        Me.mnuBrowserTextSize.Name = "mnuBrowserTextSize"
        Me.mnuBrowserTextSize.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserTextSize.Text = "&Text Size"
        '
        'mnuBrowserTextSizeLargest
        '
        Me.mnuBrowserTextSizeLargest.Name = "mnuBrowserTextSizeLargest"
        Me.mnuBrowserTextSizeLargest.Size = New System.Drawing.Size(119, 22)
        Me.mnuBrowserTextSizeLargest.Tag = "4"
        Me.mnuBrowserTextSizeLargest.Text = "Lar&gest"
        '
        'mnuBrowserTextSizeLarger
        '
        Me.mnuBrowserTextSizeLarger.Name = "mnuBrowserTextSizeLarger"
        Me.mnuBrowserTextSizeLarger.Size = New System.Drawing.Size(119, 22)
        Me.mnuBrowserTextSizeLarger.Tag = "3"
        Me.mnuBrowserTextSizeLarger.Text = "&Larger"
        '
        'mnuBrowserTextSizeMedium
        '
        Me.mnuBrowserTextSizeMedium.Name = "mnuBrowserTextSizeMedium"
        Me.mnuBrowserTextSizeMedium.Size = New System.Drawing.Size(119, 22)
        Me.mnuBrowserTextSizeMedium.Tag = "2"
        Me.mnuBrowserTextSizeMedium.Text = "&Medium"
        '
        'mnuBrowserTextSizeSmaller
        '
        Me.mnuBrowserTextSizeSmaller.Name = "mnuBrowserTextSizeSmaller"
        Me.mnuBrowserTextSizeSmaller.Size = New System.Drawing.Size(119, 22)
        Me.mnuBrowserTextSizeSmaller.Tag = "1"
        Me.mnuBrowserTextSizeSmaller.Text = "&Smaller"
        '
        'mnuBrowserTextSizeSmallest
        '
        Me.mnuBrowserTextSizeSmallest.Name = "mnuBrowserTextSizeSmallest"
        Me.mnuBrowserTextSizeSmallest.Size = New System.Drawing.Size(119, 22)
        Me.mnuBrowserTextSizeSmallest.Tag = "0"
        Me.mnuBrowserTextSizeSmallest.Text = "Sm&allest"
        '
        'mnuBrowserSelectAll
        '
        Me.mnuBrowserSelectAll.Name = "mnuBrowserSelectAll"
        Me.mnuBrowserSelectAll.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserSelectAll.Text = "&Select All"
        '
        'mnuBrowserCopy
        '
        Me.mnuBrowserCopy.Image = Global.LegalObjectsModel.My.Resources.Resources.Copy
        Me.mnuBrowserCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuBrowserCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuBrowserCopy.Name = "mnuBrowserCopy"
        Me.mnuBrowserCopy.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserCopy.Text = "&Copy"
        '
        'mnuBrowserPrint
        '
        Me.mnuBrowserPrint.Image = Global.LegalObjectsModel.My.Resources.Resources.Print
        Me.mnuBrowserPrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuBrowserPrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuBrowserPrint.Name = "mnuBrowserPrint"
        Me.mnuBrowserPrint.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserPrint.Text = "&Print"
        '
        'zsp17
        '
        Me.zsp17.Name = "zsp17"
        Me.zsp17.Size = New System.Drawing.Size(124, 6)
        '
        'mnuBrowserProperties
        '
        Me.mnuBrowserProperties.Name = "mnuBrowserProperties"
        Me.mnuBrowserProperties.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserProperties.Text = "P&roperties"
        '
        'zsp234
        '
        Me.zsp234.Name = "zsp234"
        Me.zsp234.Size = New System.Drawing.Size(124, 6)
        '
        'mnuBrowserContents
        '
        Me.mnuBrowserContents.Name = "mnuBrowserContents"
        Me.mnuBrowserContents.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserContents.Text = "Contents"
        '
        'mnuBrowserIndexes
        '
        Me.mnuBrowserIndexes.Name = "mnuBrowserIndexes"
        Me.mnuBrowserIndexes.Size = New System.Drawing.Size(127, 22)
        Me.mnuBrowserIndexes.Text = "Indexes"
        '
        'zsp123
        '
        Me.zsp123.Name = "zsp123"
        Me.zsp123.Size = New System.Drawing.Size(6, 25)
        '
        'tstMain
        '
        Me.tstMain.AllowItemReorder = True
        Me.tstMain.Dock = System.Windows.Forms.DockStyle.None
        Me.tstMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsiNew, Me.tsiOpen, Me.zstW, Me.tsiBack, Me.tsiForward, Me.tsbContents, Me.tsbIndex})
        Me.tstMain.Location = New System.Drawing.Point(181, 280)
        Me.tstMain.Name = "tstMain"
        Me.tstMain.Size = New System.Drawing.Size(165, 25)
        Me.tstMain.TabIndex = 9
        Me.tstMain.TabStop = True
        Me.tstMain.Text = "ToolStrip"
        Me.tstMain.Visible = False
        '
        'tsiOpen
        '
        Me.tsiOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiOpen.Image = Global.LegalObjectsModel.My.Resources.Resources.Open
        Me.tsiOpen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsiOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiOpen.Name = "tsiOpen"
        Me.tsiOpen.Size = New System.Drawing.Size(23, 22)
        Me.tsiOpen.Text = "Open"
        '
        'zstW
        '
        Me.zstW.Name = "zstW"
        Me.zstW.Size = New System.Drawing.Size(6, 25)
        '
        'tsiBack
        '
        Me.tsiBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiBack.Image = CType(resources.GetObject("tsiBack.Image"), System.Drawing.Image)
        Me.tsiBack.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiBack.Name = "tsiBack"
        Me.tsiBack.Size = New System.Drawing.Size(23, 22)
        Me.tsiBack.Text = "Back"
        '
        'tsiForward
        '
        Me.tsiForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiForward.Image = CType(resources.GetObject("tsiForward.Image"), System.Drawing.Image)
        Me.tsiForward.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiForward.Name = "tsiForward"
        Me.tsiForward.Size = New System.Drawing.Size(23, 22)
        Me.tsiForward.Text = "Forward"
        '
        'tsbContents
        '
        Me.tsbContents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbContents.Image = Global.LegalObjectsModel.My.Resources.Resources.Contents
        Me.tsbContents.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbContents.Name = "tsbContents"
        Me.tsbContents.Size = New System.Drawing.Size(23, 22)
        Me.tsbContents.Text = "Contents"
        '
        'tsbIndex
        '
        Me.tsbIndex.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbIndex.Image = Global.LegalObjectsModel.My.Resources.Resources.Index
        Me.tsbIndex.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbIndex.Name = "tsbIndex"
        Me.tsbIndex.Size = New System.Drawing.Size(23, 22)
        Me.tsbIndex.Text = "Index"
        '
        'pnlWebBrowser
        '
        Me.pnlWebBrowser.Controls.Add(Me.pnlWBBorderProvider)
        Me.pnlWebBrowser.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlWebBrowser.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlWebBrowser.Location = New System.Drawing.Point(0, 24)
        Me.pnlWebBrowser.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlWebBrowser.Name = "pnlWebBrowser"
        Me.pnlWebBrowser.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.pnlWebBrowser.Size = New System.Drawing.Size(117, 506)
        Me.pnlWebBrowser.TabIndex = 18
        '
        'pnlWBBorderProvider
        '
        Me.pnlWBBorderProvider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlWBBorderProvider.Controls.Add(Me.wbOne)
        Me.pnlWBBorderProvider.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlWBBorderProvider.Location = New System.Drawing.Point(0, 0)
        Me.pnlWBBorderProvider.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlWBBorderProvider.Name = "pnlWBBorderProvider"
        Me.pnlWBBorderProvider.Size = New System.Drawing.Size(113, 506)
        Me.pnlWBBorderProvider.TabIndex = 1
        '
        'wbOne
        '
        Me.wbOne.ContextMenuStrip = Me.cxmBrowser
        Me.wbOne.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbOne.IsWebBrowserContextMenuEnabled = False
        Me.wbOne.Location = New System.Drawing.Point(0, 0)
        Me.wbOne.Margin = New System.Windows.Forms.Padding(2, 3, 30, 3)
        Me.wbOne.Name = "wbOne"
        Me.wbOne.Size = New System.Drawing.Size(109, 502)
        Me.wbOne.TabIndex = 1
        Me.wbOne.TabStop = False
        '
        'pnltreeView
        '
        Me.pnltreeView.Controls.Add(Me.tvOne)
        Me.pnltreeView.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnltreeView.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnltreeView.Location = New System.Drawing.Point(117, 24)
        Me.pnltreeView.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnltreeView.Name = "pnltreeView"
        Me.pnltreeView.Padding = New System.Windows.Forms.Padding(0, 0, 0, 4)
        Me.pnltreeView.Size = New System.Drawing.Size(698, 217)
        Me.pnltreeView.TabIndex = 19
        '
        'tvOne
        '
        Me.tvOne.AllowDrop = True
        Me.tvOne.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvOne.ImageIndex = 0
        Me.tvOne.ImageList = Me.ilsTreeview
        Me.tvOne.Location = New System.Drawing.Point(0, 0)
        Me.tvOne.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tvOne.Name = "tvOne"
        Me.tvOne.SelectedImageIndex = 0
        Me.tvOne.Size = New System.Drawing.Size(698, 213)
        Me.tvOne.TabIndex = 0
        Me.tvOne.TabStop = False
        '
        'ilsTreeview
        '
        Me.ilsTreeview.ImageStream = CType(resources.GetObject("ilsTreeview.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilsTreeview.TransparentColor = System.Drawing.Color.Fuchsia
        Me.ilsTreeview.Images.SetKeyName(0, "Help.bmp")
        Me.ilsTreeview.Images.SetKeyName(1, "HelpGrey.bmp")
        Me.ilsTreeview.Images.SetKeyName(2, "HelpOpen.bmp")
        Me.ilsTreeview.Images.SetKeyName(3, "Topic.bmp")
        Me.ilsTreeview.Images.SetKeyName(4, "TopicPriorityHigh.bmp")
        Me.ilsTreeview.Images.SetKeyName(5, "TopicUnavailable.bmp")
        '
        'pnlPropGrid
        '
        Me.pnlPropGrid.Controls.Add(Me.tbsOne)
        Me.pnlPropGrid.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlPropGrid.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlPropGrid.Location = New System.Drawing.Point(515, 241)
        Me.pnlPropGrid.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.pnlPropGrid.Name = "pnlPropGrid"
        Me.pnlPropGrid.Padding = New System.Windows.Forms.Padding(4, 0, 0, 0)
        Me.pnlPropGrid.Size = New System.Drawing.Size(300, 289)
        Me.pnlPropGrid.TabIndex = 20
        '
        'tbsOne
        '
        Me.tbsOne.Controls.Add(Me.tbpLeft)
        Me.tbsOne.Controls.Add(Me.tbpRight)
        Me.tbsOne.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbsOne.Location = New System.Drawing.Point(4, 0)
        Me.tbsOne.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tbsOne.Name = "tbsOne"
        Me.tbsOne.SelectedIndex = 0
        Me.tbsOne.Size = New System.Drawing.Size(296, 289)
        Me.tbsOne.TabIndex = 0
        '
        'tbpLeft
        '
        Me.tbpLeft.BackColor = System.Drawing.SystemColors.Control
        Me.tbpLeft.Location = New System.Drawing.Point(4, 22)
        Me.tbpLeft.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tbpLeft.Name = "tbpLeft"
        Me.tbpLeft.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tbpLeft.Size = New System.Drawing.Size(288, 263)
        Me.tbpLeft.TabIndex = 1
        '
        'tbpRight
        '
        Me.tbpRight.Controls.Add(Me.prgOne)
        Me.tbpRight.Location = New System.Drawing.Point(4, 22)
        Me.tbpRight.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tbpRight.Name = "tbpRight"
        Me.tbpRight.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.tbpRight.Size = New System.Drawing.Size(288, 263)
        Me.tbpRight.TabIndex = 0
        '
        'prgOne
        '
        Me.prgOne.BackColor = System.Drawing.SystemColors.Control
        Me.prgOne.ContextMenuStrip = Me.cxmTerms
        Me.prgOne.Dock = System.Windows.Forms.DockStyle.Fill
        Me.prgOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prgOne.Location = New System.Drawing.Point(2, 3)
        Me.prgOne.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.prgOne.Name = "prgOne"
        Me.prgOne.PropertySort = System.Windows.Forms.PropertySort.Categorized
        Me.prgOne.Size = New System.Drawing.Size(284, 257)
        Me.prgOne.TabIndex = 57
        '
        'tspTop
        '
        Me.tspTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.tspTop.Location = New System.Drawing.Point(0, 24)
        Me.tspTop.Name = "tspTop"
        Me.tspTop.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.tspTop.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.tspTop.Size = New System.Drawing.Size(815, 0)
        '
        'tspLeft
        '
        Me.tspLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.tspLeft.Location = New System.Drawing.Point(0, 24)
        Me.tspLeft.Name = "tspLeft"
        Me.tspLeft.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tspLeft.RowMargin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.tspLeft.Size = New System.Drawing.Size(0, 506)
        '
        'tspBottom
        '
        Me.tspBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tspBottom.Location = New System.Drawing.Point(0, 530)
        Me.tspBottom.Name = "tspBottom"
        Me.tspBottom.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.tspBottom.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.tspBottom.Size = New System.Drawing.Size(815, 0)
        '
        'tspRight
        '
        Me.tspRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.tspRight.Location = New System.Drawing.Point(815, 24)
        Me.tspRight.Name = "tspRight"
        Me.tspRight.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tspRight.RowMargin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.tspRight.Size = New System.Drawing.Size(0, 506)
        '
        'tstPropGrid
        '
        Me.tstPropGrid.Dock = System.Windows.Forms.DockStyle.None
        Me.tstPropGrid.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsiCopyPG, Me.tsiConfirm})
        Me.tstPropGrid.Location = New System.Drawing.Point(209, 442)
        Me.tstPropGrid.Name = "tstPropGrid"
        Me.tstPropGrid.Size = New System.Drawing.Size(58, 25)
        Me.tstPropGrid.TabIndex = 26
        Me.tstPropGrid.TabStop = True
        Me.tstPropGrid.Text = "ToolStrip1"
        Me.tstPropGrid.Visible = False
        '
        'tsiCopyPG
        '
        Me.tsiCopyPG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiCopyPG.Image = Global.LegalObjectsModel.My.Resources.Resources.Copy
        Me.tsiCopyPG.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiCopyPG.Name = "tsiCopyPG"
        Me.tsiCopyPG.Size = New System.Drawing.Size(23, 22)
        Me.tsiCopyPG.Text = "ToolStripButton2"
        Me.tsiCopyPG.ToolTipText = "Copy Grid Data"
        '
        'tsiConfirm
        '
        Me.tsiConfirm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiConfirm.Image = Global.LegalObjectsModel.My.Resources.Resources.Confirm
        Me.tsiConfirm.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiConfirm.Name = "tsiConfirm"
        Me.tsiConfirm.Size = New System.Drawing.Size(23, 22)
        Me.tsiConfirm.Text = "ToolStripButton1"
        Me.tsiConfirm.ToolTipText = "Generate Confirmation"
        '
        'wbHidden
        '
        Me.wbHidden.Location = New System.Drawing.Point(396, 164)
        Me.wbHidden.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.wbHidden.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbHidden.Name = "wbHidden"
        Me.wbHidden.Size = New System.Drawing.Size(131, 141)
        Me.wbHidden.TabIndex = 32
        Me.wbHidden.TabStop = False
        Me.wbHidden.Visible = False
        '
        'tstDraw
        '
        Me.tstDraw.Dock = System.Windows.Forms.DockStyle.None
        Me.tstDraw.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsiEntity, Me.zspOne, Me.tsiSelect, Me.zspTwo, Me.tsiDrawFundingExisting, Me.tsiDrawFundingNew, Me.zsp123, Me.tsiDrawPassThroughExisting, Me.tsiDrawPassthroughNew, Me.zspThree, Me.tsiDrawSwapExisting, Me.tsiDrawSwapNew, Me.ToolStripSeparator1, Me.tsiDrawCredSupExisting, Me.tsiDrawCredSupNew, Me.ToolStripSeparator2, Me.tsiDrawSecurityExisting, Me.tsiDrawSecurityNew, Me.tsiNonRepSec})
        Me.tstDraw.Location = New System.Drawing.Point(119, 381)
        Me.tstDraw.Name = "tstDraw"
        Me.tstDraw.Size = New System.Drawing.Size(365, 25)
        Me.tstDraw.TabIndex = 50
        Me.tstDraw.TabStop = True
        Me.tstDraw.Visible = False
        '
        'tsiEntity
        '
        Me.tsiEntity.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsiEntity.DoubleClickEnabled = True
        Me.tsiEntity.Image = CType(resources.GetObject("tsiEntity.Image"), System.Drawing.Image)
        Me.tsiEntity.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiEntity.Name = "tsiEntity"
        Me.tsiEntity.Size = New System.Drawing.Size(41, 22)
        Me.tsiEntity.Text = "Entity"
        Me.tsiEntity.ToolTipText = "Add Entity"
        '
        'zspOne
        '
        Me.zspOne.Name = "zspOne"
        Me.zspOne.Size = New System.Drawing.Size(6, 25)
        '
        'tsiSelect
        '
        Me.tsiSelect.Checked = True
        Me.tsiSelect.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tsiSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiSelect.Image = Global.LegalObjectsModel.My.Resources.Resources.Pointer
        Me.tsiSelect.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiSelect.Name = "tsiSelect"
        Me.tsiSelect.Size = New System.Drawing.Size(23, 22)
        Me.tsiSelect.ToolTipText = "Select"
        '
        'zspTwo
        '
        Me.zspTwo.Name = "zspTwo"
        Me.zspTwo.Size = New System.Drawing.Size(6, 25)
        '
        'tsiDrawFundingExisting
        '
        Me.tsiDrawFundingExisting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawFundingExisting.DoubleClickEnabled = True
        Me.tsiDrawFundingExisting.Enabled = False
        Me.tsiDrawFundingExisting.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawArrow
        Me.tsiDrawFundingExisting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawFundingExisting.Name = "tsiDrawFundingExisting"
        Me.tsiDrawFundingExisting.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawFundingExisting.Text = "D"
        Me.tsiDrawFundingExisting.ToolTipText = "Draw Existing Funding"
        '
        'tsiDrawFundingNew
        '
        Me.tsiDrawFundingNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawFundingNew.DoubleClickEnabled = True
        Me.tsiDrawFundingNew.Enabled = False
        Me.tsiDrawFundingNew.Image = Global.LegalObjectsModel.My.Resources.Resources.Draw2Arrows
        Me.tsiDrawFundingNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawFundingNew.Name = "tsiDrawFundingNew"
        Me.tsiDrawFundingNew.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawFundingNew.Text = "ToolStripButton1"
        Me.tsiDrawFundingNew.ToolTipText = "Draw New Funding"
        '
        'tsiDrawPassThroughExisting
        '
        Me.tsiDrawPassThroughExisting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawPassThroughExisting.Enabled = False
        Me.tsiDrawPassThroughExisting.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawPassThruExisting
        Me.tsiDrawPassThroughExisting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawPassThroughExisting.Name = "tsiDrawPassThroughExisting"
        Me.tsiDrawPassThroughExisting.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawPassThroughExisting.Text = "ToolStripButton2"
        Me.tsiDrawPassThroughExisting.ToolTipText = "Draw Existing Pass-through"
        '
        'tsiDrawPassthroughNew
        '
        Me.tsiDrawPassthroughNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawPassthroughNew.DoubleClickEnabled = True
        Me.tsiDrawPassthroughNew.Enabled = False
        Me.tsiDrawPassthroughNew.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawPassThruNew
        Me.tsiDrawPassthroughNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawPassthroughNew.Name = "tsiDrawPassthroughNew"
        Me.tsiDrawPassthroughNew.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawPassthroughNew.Text = "ToolStripButton1"
        Me.tsiDrawPassthroughNew.ToolTipText = "Draw New Pass-through"
        '
        'zspThree
        '
        Me.zspThree.Name = "zspThree"
        Me.zspThree.Size = New System.Drawing.Size(6, 25)
        '
        'tsiDrawSwapExisting
        '
        Me.tsiDrawSwapExisting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawSwapExisting.DoubleClickEnabled = True
        Me.tsiDrawSwapExisting.Enabled = False
        Me.tsiDrawSwapExisting.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawExistingSwap
        Me.tsiDrawSwapExisting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawSwapExisting.Name = "tsiDrawSwapExisting"
        Me.tsiDrawSwapExisting.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawSwapExisting.Text = "ToolStripButton1"
        Me.tsiDrawSwapExisting.ToolTipText = "Draw Existing Swap"
        '
        'tsiDrawSwapNew
        '
        Me.tsiDrawSwapNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawSwapNew.DoubleClickEnabled = True
        Me.tsiDrawSwapNew.Enabled = False
        Me.tsiDrawSwapNew.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawNewSwap
        Me.tsiDrawSwapNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawSwapNew.Name = "tsiDrawSwapNew"
        Me.tsiDrawSwapNew.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawSwapNew.Text = "ToolStripButton1"
        Me.tsiDrawSwapNew.ToolTipText = "Draw New Swap"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'tsiDrawCredSupExisting
        '
        Me.tsiDrawCredSupExisting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawCredSupExisting.DoubleClickEnabled = True
        Me.tsiDrawCredSupExisting.Enabled = False
        Me.tsiDrawCredSupExisting.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawExistingCredSupArrow
        Me.tsiDrawCredSupExisting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawCredSupExisting.Name = "tsiDrawCredSupExisting"
        Me.tsiDrawCredSupExisting.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawCredSupExisting.Text = "ToolStripButton2"
        Me.tsiDrawCredSupExisting.ToolTipText = "Draw Existing Credit Support"
        '
        'tsiDrawCredSupNew
        '
        Me.tsiDrawCredSupNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawCredSupNew.DoubleClickEnabled = True
        Me.tsiDrawCredSupNew.Enabled = False
        Me.tsiDrawCredSupNew.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawNewCredSupArrow
        Me.tsiDrawCredSupNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawCredSupNew.Name = "tsiDrawCredSupNew"
        Me.tsiDrawCredSupNew.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawCredSupNew.Text = "ToolStripButton1"
        Me.tsiDrawCredSupNew.ToolTipText = "Draw New Credit Support"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'tsiDrawSecurityExisting
        '
        Me.tsiDrawSecurityExisting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawSecurityExisting.DoubleClickEnabled = True
        Me.tsiDrawSecurityExisting.Enabled = False
        Me.tsiDrawSecurityExisting.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawSecurityExisting
        Me.tsiDrawSecurityExisting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawSecurityExisting.Name = "tsiDrawSecurityExisting"
        Me.tsiDrawSecurityExisting.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawSecurityExisting.Text = "Draw Existing Security"
        '
        'tsiDrawSecurityNew
        '
        Me.tsiDrawSecurityNew.Checked = True
        Me.tsiDrawSecurityNew.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tsiDrawSecurityNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiDrawSecurityNew.DoubleClickEnabled = True
        Me.tsiDrawSecurityNew.Enabled = False
        Me.tsiDrawSecurityNew.Image = Global.LegalObjectsModel.My.Resources.Resources.DrawSecurityNew
        Me.tsiDrawSecurityNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiDrawSecurityNew.Name = "tsiDrawSecurityNew"
        Me.tsiDrawSecurityNew.Size = New System.Drawing.Size(23, 22)
        Me.tsiDrawSecurityNew.Text = "Draw New Security"
        '
        'tsiNonRepSec
        '
        Me.tsiNonRepSec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsiNonRepSec.Image = Global.LegalObjectsModel.My.Resources.Resources.CNonRep
        Me.tsiNonRepSec.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsiNonRepSec.Name = "tsiNonRepSec"
        Me.tsiNonRepSec.Size = New System.Drawing.Size(23, 22)
        Me.tsiNonRepSec.Text = "ToolStripButton1"
        Me.tsiNonRepSec.ToolTipText = "Draw Security over Non-represented assets"
        '
        'BackgroundWorker1
        '
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'frmMDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(815, 530)
        Me.Controls.Add(Me.pnlPropGrid)
        Me.Controls.Add(Me.pnltreeView)
        Me.Controls.Add(Me.wbHidden)
        Me.Controls.Add(Me.pnlWebBrowser)
        Me.Controls.Add(Me.tstPropGrid)
        Me.Controls.Add(Me.tspRight)
        Me.Controls.Add(Me.tspBottom)
        Me.Controls.Add(Me.tspLeft)
        Me.Controls.Add(Me.tspTop)
        Me.Controls.Add(Me.tstDraw)
        Me.Controls.Add(Me.mnsMain)
        Me.Controls.Add(Me.tstMain)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnsMain
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Name = "frmMDI"
        Me.Text = "MDIParent"
        Me.cxmTemplates.ResumeLayout(False)
        Me.mnsMain.ResumeLayout(False)
        Me.mnsMain.PerformLayout()
        Me.cxmTerms.ResumeLayout(False)
        Me.cxmBrowser.ResumeLayout(False)
        Me.tstMain.ResumeLayout(False)
        Me.tstMain.PerformLayout()
        Me.pnlWebBrowser.ResumeLayout(False)
        Me.pnlWBBorderProvider.ResumeLayout(False)
        Me.pnltreeView.ResumeLayout(False)
        Me.pnlPropGrid.ResumeLayout(False)
        Me.tbsOne.ResumeLayout(False)
        Me.tbpRight.ResumeLayout(False)
        Me.tstPropGrid.ResumeLayout(False)
        Me.tstPropGrid.PerformLayout()
        Me.tstDraw.ResumeLayout(False)
        Me.tstDraw.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mnuHelpResIpsa As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspIUH As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsOptions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnsMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuTools As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlWebBrowser As System.Windows.Forms.Panel
    Friend WithEvents pnltreeView As System.Windows.Forms.Panel
    Friend WithEvents tvOne As System.Windows.Forms.TreeView
    Friend WithEvents pnlPropGrid As System.Windows.Forms.Panel
    Friend WithEvents tbsOne As System.Windows.Forms.TabControl
    Friend WithEvents tbpRight As System.Windows.Forms.TabPage
    Friend WithEvents tbpLeft As System.Windows.Forms.TabPage
    Friend WithEvents mnuHelpMechanisms As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTransaction As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecentFile1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewBlank As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspTemplates As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileNewUnderwrittenBondIssue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewSecondaryTrade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRepo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewBuySellBack As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRepackaging As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewSecuritisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewAssetbackedCP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewShareBuyback As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewWholeBusinessSecuritisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspOIJ As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuToolsReviewDynamicLinkedLibraries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTerms As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowCascade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowTileVertical As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowTileHorizontal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowArrangeIcons As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspIJ As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tstMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tsiOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents zstW As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiBack As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiForward As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbContents As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbIndex As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiNew As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents tspTop As System.Windows.Forms.ToolStripPanel
    Friend WithEvents tspLeft As System.Windows.Forms.ToolStripPanel
    Friend WithEvents tspBottom As System.Windows.Forms.ToolStripPanel
    Friend WithEvents tspRight As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ilsTreeview As System.Windows.Forms.ImageList
    Friend WithEvents mnuView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewMainToolbar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewDrawingToolbar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tstPropGrid As System.Windows.Forms.ToolStrip
    Friend WithEvents wbHidden As System.Windows.Forms.WebBrowser
    Friend WithEvents cxmBrowser As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuBrowserBack As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserForward As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserClear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspOKJ As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBrowserTextSize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserTextSizeLargest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserTextSizeLarger As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserTextSizeMedium As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserTextSizeSmaller As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserTextSizeSmallest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zsp123 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBrowserPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zsp17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBrowserProperties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zsp234 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBrowserContents As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBrowserIndexes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cxmTemplates As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tstDraw As System.Windows.Forms.ToolStrip
    Friend WithEvents zspRecentFiles As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents zspWEE As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuRecentFile2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecentFile3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecentFile4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDevelopment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuResetSettings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlWBBorderProvider As System.Windows.Forms.Panel
    Friend WithEvents wbOne As System.Windows.Forms.WebBrowser
    Friend WithEvents StopInFrmMDIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StopInFrmOneToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsiConfirm As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiCopyPG As System.Windows.Forms.ToolStripButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents cxmTerms As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuTermsTermsWindow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTermsTextSize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTermsTextSizeLarge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTermsTextSizeMedium As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTermsTextSizeSmall As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsiSelect As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiDrawFundingExisting As System.Windows.Forms.ToolStripButton
    Friend WithEvents zspOne As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiDrawFundingNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiDrawCredSupNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiEntity As System.Windows.Forms.ToolStripButton
    Friend WithEvents zspTwo As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiDrawSwapNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents zspThree As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents prgOne As System.Windows.Forms.PropertyGrid
    Friend WithEvents tsiDrawPassThroughExisting As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiDrawPassthroughNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiDrawSwapExisting As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiDrawCredSupExisting As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsiDrawSecurityExisting As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiDrawSecurityNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsiNonRepSec As System.Windows.Forms.ToolStripButton
    'Property Grid sort options:
    '1. CategorizedAlphabetical - categorised and sorted alphabetically under categories. 
    'Not sorted alphabetically for extendertypeconverters.
    '2. Categorised - categorised with an arbitrary sort under categories.
    '3. Alphabetical - categories not shown, everything alpabetical. Arbitrary sort for type extenders.
    '4. NoSort - arbitrarily ordered list of everything. Not equivalent to result of any button in toolbar.
    'Must be some logic to the ordering, but I can't see what it is -it's certainly not the order they appear 
    'in the code in.

End Class
