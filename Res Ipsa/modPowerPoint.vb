Option Explicit On
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.PowerPoint
Imports Microsoft.Office

Module modPowerPoint
    'Various things for making PowerPoint slides.

    'Because I have DoEvents I need to be careful the user can't change anything while the slide is being _
    'created. Need to check this.

    'n.b. It's because I use PowerPoint that I need a reference in the project to Microsoft office. _
    'A lot of the constants that PowerPoint uses (prefixed "mso") are Office constants, not PowerPoint ones.

    Friend PPEsc As Boolean             'For when user presses Esc key.
    Friend Const conEscError = 416
    Friend PPApp As PowerPoint.Application 'can make WithEvents if I want.
    'Tried to use WindowActivate event to prevent user from deminimising it while slide created, but doesn't work.

    Friend Sub PPFillText(ByVal TF As TextFrame, ByVal Text As String, ByVal PPA As PpParagraphAlignment)

        With TF.TextRange
            .Text = Text 'must come before all the rest.
            .ParagraphFormat.Alignment = PPA
            .LanguageID = Core.MsoLanguageID.msoLanguageIDNoProofing   'don't want to see red underlining.
            .Font.Name = "Arial"
            .Font.Size = 12
        End With
        TF.VerticalAnchor = Core.MsoVerticalAnchor.msoAnchorMiddle

    End Sub

    Friend Sub DoPPEvents(ByVal PPSlide As Slide)

        'Called to enable user to cancel slide creation by pressing "Esc".
        If Not PPSlide Is Nothing Then
            '  My.Application.DoEvents()
            If Not PPApp Is Nothing Then PPApp.WindowState = PpWindowState.ppWindowMinimized

            If PPEsc Then   'delete slide, and its parent presentation if it's the only slide in that, and the application if the presentation is the only presentation in that.

                Dim PPPres As PowerPoint.Presentation = PPSlide.Parent

                If Not PPSlide Is Nothing Then PPSlide.Delete()
                If Not PPPres Is Nothing Then

                    If PPPres.Slides.Count = 0 Then
                        PPPres.Close()
                        PPPres = Nothing
                    End If

                    If Not PPApp Is Nothing Then
                        If PPApp.Presentations.Count = 0 Then
                            PPApp.Quit()
                            PPApp = Nothing 'not enough to get rid of it entirely, but can't figure out how to do so.
                        End If
                    End If

                End If

                Err.Raise(conEscError)

            End If
        End If

    End Sub

    Friend Function GroupPP(ByVal ppp As PowerPoint.Slide, ByVal PPList As List(Of String)) As PowerPoint.Shape

        'Group an array of shapes in a powerpoint slide.
        With PPList
            If .Count > 1 Then
                Return ppp.Shapes.Range(.ToArray).Group()
            ElseIf .Count = 1 Then
                Return ppp.Shapes.Item(.Item(0))
            End If
        End With

    End Function
End Module