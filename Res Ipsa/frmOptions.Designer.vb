<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Friend Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdOK = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBackColour = New System.Windows.Forms.Button
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.cmdPartyColour = New System.Windows.Forms.Button
        Me.optFlat = New System.Windows.Forms.RadioButton
        Me.opt3D = New System.Windows.Forms.RadioButton
        Me.cmdReset = New System.Windows.Forms.Button
        Me.picSample = New LegalObjectsModel.PartyCanvas
        Me.JCombo1 = New LegalObjectsModel.JCombo
        CType(Me.picSample, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.cmdOK.Location = New System.Drawing.Point(278, 275)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "OK"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Default Jurisdiction:"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(278, 241)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdBackColour
        '
        Me.cmdBackColour.Location = New System.Drawing.Point(15, 235)
        Me.cmdBackColour.Name = "cmdBackColour"
        Me.cmdBackColour.Size = New System.Drawing.Size(101, 23)
        Me.cmdBackColour.TabIndex = 5
        Me.cmdBackColour.Text = "Set Back Colour"
        Me.cmdBackColour.UseVisualStyleBackColor = True
        '
        'cmdPartyColour
        '
        Me.cmdPartyColour.Location = New System.Drawing.Point(15, 273)
        Me.cmdPartyColour.Name = "cmdPartyColour"
        Me.cmdPartyColour.Size = New System.Drawing.Size(101, 23)
        Me.cmdPartyColour.TabIndex = 6
        Me.cmdPartyColour.Text = "Set Control Colour"
        Me.cmdPartyColour.UseVisualStyleBackColor = True
        '
        'optFlat
        '
        Me.optFlat.AutoSize = True
        Me.optFlat.Location = New System.Drawing.Point(147, 241)
        Me.optFlat.Name = "optFlat"
        Me.optFlat.Size = New System.Drawing.Size(42, 17)
        Me.optFlat.TabIndex = 7
        Me.optFlat.TabStop = True
        Me.optFlat.Text = "Flat"
        Me.optFlat.UseVisualStyleBackColor = True
        '
        'opt3D
        '
        Me.opt3D.AutoSize = True
        Me.opt3D.Location = New System.Drawing.Point(217, 241)
        Me.opt3D.Name = "opt3D"
        Me.opt3D.Size = New System.Drawing.Size(39, 17)
        Me.opt3D.TabIndex = 8
        Me.opt3D.TabStop = True
        Me.opt3D.Text = "3D"
        Me.opt3D.UseVisualStyleBackColor = True
        '
        'cmdReset
        '
        Me.cmdReset.Location = New System.Drawing.Point(147, 270)
        Me.cmdReset.Name = "cmdReset"
        Me.cmdReset.Size = New System.Drawing.Size(93, 28)
        Me.cmdReset.TabIndex = 9
        Me.cmdReset.Text = "Reset Defaults"
        Me.cmdReset.UseVisualStyleBackColor = True
        '
        'picSample
        '
        Me.picSample.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.picSample.BackColor = System.Drawing.Color.Teal
        Me.picSample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picSample.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.picSample.Location = New System.Drawing.Point(36, 12)
        Me.picSample.Name = "picSample"
        Me.picSample.Size = New System.Drawing.Size(293, 155)
        Me.picSample.TabIndex = 4
        Me.picSample.TabStop = False
        '
        'JCombo1
        '
        Me.JCombo1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.JCombo1.FormattingEnabled = True
        Me.JCombo1.Location = New System.Drawing.Point(122, 189)
        Me.JCombo1.Name = "JCombo1"
        Me.JCombo1.Size = New System.Drawing.Size(121, 21)
        Me.JCombo1.TabIndex = 1
        '
        'frmOptions
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 312)
        Me.Controls.Add(Me.cmdReset)
        Me.Controls.Add(Me.opt3D)
        Me.Controls.Add(Me.optFlat)
        Me.Controls.Add(Me.cmdPartyColour)
        Me.Controls.Add(Me.cmdBackColour)
        Me.Controls.Add(Me.picSample)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.JCombo1)
        Me.Controls.Add(Me.cmdOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOptions"
        Me.ShowInTaskbar = False
        Me.Text = "Options"
        CType(Me.picSample, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents JCombo1 As LegalObjectsModel.JCombo
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBackColour As System.Windows.Forms.Button
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents picSample As PartyCanvas
    Friend WithEvents cmdPartyColour As System.Windows.Forms.Button
    Friend WithEvents optFlat As System.Windows.Forms.RadioButton
    Friend WithEvents opt3D As System.Windows.Forms.RadioButton
    Friend WithEvents cmdReset As System.Windows.Forms.Button
End Class
