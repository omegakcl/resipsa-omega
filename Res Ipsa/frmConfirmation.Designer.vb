Partial Class frmConfirmation
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New(ByVal pComments As String, Optional ByVal pLeg1 As SwapLeg = Nothing, Optional ByVal pLeg2 As SwapLeg = Nothing, Optional ByVal pSecObj As SecurityBase = Nothing)
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        Comments = pComments
        Leg1 = pLeg1
        Leg2 = pLeg2
        SecObj = pSecObj
    End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			Static fTerminateCalled As Boolean
			If Not fTerminateCalled Then
                fTerminateCalled = True
            End If
            If Not components Is Nothing Then
                components.Dispose()
            End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents prog1 As System.Windows.Forms.ProgressBar
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents lblProgress As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough(), System.Diagnostics.DebuggerNonUserCode()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.prog1 = New System.Windows.Forms.ProgressBar
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblProgress = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.optAB_1 = New System.Windows.Forms.RadioButton
        Me.optAB_0 = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'prog1
        '
        Me.prog1.Location = New System.Drawing.Point(16, 136)
        Me.prog1.Maximum = 10
        Me.prog1.Name = "prog1"
        Me.prog1.Size = New System.Drawing.Size(100, 17)
        Me.prog1.TabIndex = 3
        Me.prog1.Visible = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(160, 136)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 23)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(160, 104)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'lblProgress
        '
        Me.lblProgress.BackColor = System.Drawing.SystemColors.Control
        Me.lblProgress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProgress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProgress.Location = New System.Drawing.Point(16, 120)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProgress.Size = New System.Drawing.Size(97, 17)
        Me.lblProgress.TabIndex = 4
        Me.lblProgress.Text = "Processing:"
        Me.lblProgress.Visible = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(129, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Address confirmation:"
        '
        'optAB_1
        '
        Me.optAB_1.BackColor = System.Drawing.SystemColors.Control
        Me.optAB_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAB_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAB_1.Location = New System.Drawing.Point(16, 64)
        Me.optAB_1.Name = "optAB_1"
        Me.optAB_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAB_1.Size = New System.Drawing.Size(167, 19)
        Me.optAB_1.TabIndex = 9
        Me.optAB_1.Text = "from Party B to Party A."
        Me.optAB_1.UseVisualStyleBackColor = False
        '
        'optAB_0
        '
        Me.optAB_0.BackColor = System.Drawing.SystemColors.Control
        Me.optAB_0.Checked = True
        Me.optAB_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAB_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAB_0.Location = New System.Drawing.Point(16, 40)
        Me.optAB_0.Name = "optAB_0"
        Me.optAB_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAB_0.Size = New System.Drawing.Size(167, 18)
        Me.optAB_0.TabIndex = 10
        Me.optAB_0.TabStop = True
        Me.optAB_0.Text = "from Party A to Party B."
        Me.optAB_0.UseVisualStyleBackColor = False
        '
        'frmConfirmation
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(250, 170)
        Me.Controls.Add(Me.optAB_0)
        Me.Controls.Add(Me.optAB_1)
        Me.Controls.Add(Me.prog1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.Label1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(2, 18)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfirmation"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Swap Confirmation"
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents optAB_1 As System.Windows.Forms.RadioButton
    Public WithEvents optAB_0 As System.Windows.Forms.RadioButton
#End Region
End Class