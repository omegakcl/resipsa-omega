Imports System.Runtime.Serialization.Formatters.Binary
Imports System.IO
Imports System.Math
Imports System.Security
Imports System.Security.Cryptography
Imports System.Runtime.InteropServices
Imports System.Text
Imports Microsoft.Office.Interop
Imports Microsoft.Office

<Serializable> Public Class frmBase
    Inherits Form

#Region "New"

    Shared Number As Integer
    Friend Sub SetNewFileText()

        Number += 1

        Text = "Status Diagram " & Number

    End Sub

    Public Sub New(Optional ByVal sFileName As String = "", Optional ByRef Abort As Boolean = False, Optional ByVal sParentText As String = "")
        Me.TabStop = False
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        picCanvas.PrintDocument1.DefaultPageSettings.Landscape = True    'default

        SetPrintButtonToolTip(tsbPrint)

        'Add any initialization after the InitializeComponent() call.
        If sFileName <> "" Then

            FileName(sParentText) = sFileName

            Dim FileStream As Stream = New FileStream(sFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)

            Dim encryptedFromStream(FileStream.Length) As Byte

            FileStream.Read(encryptedFromStream, 0, CInt(encryptedFromStream.Length))

            Try
here:
                Dim DES As DESCryptoServiceProvider = New DESCryptoServiceProvider()
                Dim gch As GCHandle
                InitialiseDES(DES, gch)

                Dim memoryStreamDeserialized As MemoryStream

                Dim decryptedData As Byte() = DES.CreateDecryptor().TransformFinalBlock(encryptedFromStream, 0, encryptedFromStream.Length - 1)
                memoryStreamDeserialized = New MemoryStream(decryptedData)

                'See ms-help://MS.VSCC.v80/MS.MSDN.v80/MS.KB.v10.en/enu_kbvbnetkb/vbnetkb/301070.htm for explanation of following. Have checked, and 8 is the length required - one per byte.
                ZeroMemory(gch.AddrOfPinnedObject, 8)
                gch.Free()

                With New BinaryFormatter

                    PasswordModify = CType(.Deserialize(memoryStreamDeserialized), String)

                    If PasswordModify <> "" Then
                        With New frmEnterModifyPassword(Me)
                            .ShowDialog()
                            If .DialogResult = Windows.Forms.DialogResult.Ignore Then
                                OpenedAsReadOnly = True
                                SetText()   'setting FullPath above will suffice if not OpenedAsReadOnly.
                            ElseIf .DialogResult = Windows.Forms.DialogResult.Cancel Then
                                Abort = True
                                Exit Sub
                            End If
                        End With
                    End If

                    Dim obj As Object = .Deserialize(memoryStreamDeserialized)
                    Debug.Print(TypeName(obj))
                    Dim sdc As SerialDataClass = CType(.Deserialize(memoryStreamDeserialized), SerialDataClass)

                    For Each f1 As Entity In sdc.eCol

                        With picCanvas.AddEC(f1.ControlPoint, True, f1)

                            Dim bprevmodified As Boolean = f1.bNameModified

                            .NameChanged()  'update the name in the richtextbox.

                            'Serialization will create new Jurisdiction instances. These won't be the same 
                            'instances as in the Jurisdictions list. Make them the same so that the correct entry in jcbJur is selected.
                            f1.Incorp = GetJColJurn(f1.Incorp)

                            .Visible = True
                            f1.bNameModified = bprevmodified
                            .rtbName.Modified = bprevmodified
                        End With

                    Next

                    picCanvas.StanConCols = sdc.SerialStanConCols
                    picCanvas.CredSupCollection = sdc.SerialCredSupCollection
                    picCanvas.SecurityCollection = sdc.SerialSecurityCollection

                    picCanvas.UnpackSerialData()

                    picCanvas.PrintArea = CType(.Deserialize(memoryStreamDeserialized), Rectangle)

                End With

                memoryStreamDeserialized.Flush()
                memoryStreamDeserialized.Close()

            Catch
                If TypeOf Err.GetException Is CryptographicException Then
                    'Use the password as the encryption key.

                    If PasswordOpen <> "" Then
                        If MsgBox("Incorrect Password", MsgBoxStyle.OkCancel Or MsgBoxStyle.Exclamation) = MsgBoxResult.Cancel Then
                            Abort = True
                            Exit Try
                        End If

                    End If

                    Application.UseWaitCursor = False
                    PasswordOpen = InputBox("Enter the password for this file.")
                    Application.UseWaitCursor = True

                    If PasswordOpen = "" Then   'will get this if the user presses cancel.
                        Abort = True    'If I close the form in the New event, the form will continue being created.
                        Exit Try
                    End If

                    GoTo here
                Else

                    'I think if the data is corrupted you will just get a cryptographicexception each time so I can't distinguish this from the 
                    'wrong password. But I ought to have some code in case there's the possibility of some other type of error.
                    MsgBox("Unable to open this file. It may be corrupted.", MsgBoxStyle.Exclamation)
                    Abort = True
                    Exit Try

                End If
            Finally
                AddAsRecentFile()

                ReformatForm()


                Me.picCanvas.Dirty = False
                frmMDI.tsiSelect.Checked = True
            End Try

            FileStream.Close()

            Application.UseWaitCursor = False
        Else
            PasswordOpen = ""
            PasswordModify = ""
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        frmMDI.tstDraw.Enabled = True

        'Dunno how to get notification when this changes.
        Dim f As New System.Drawing.Printing.PrinterSettings
        tsbPrint.ToolTipText = "Print" & IIf(f.PrinterName <> "", " (" & f.PrinterName & ")", "")
        picCanvas.AllowDrop = True    'Not impressed that this property is hidden.
        mnuEdit.DropDown = picCanvas.cmsEdit
        mnuFilePrintArea.DropDown = picCanvas.cmsPrintArea

        picCanvas.SetPrintingArea()
        picCanvas.SetPrintArea(False)

    End Sub
#End Region

#Region "Canvas Mouse Down and Up"

    Private Sub picCanvas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picCanvas.MouseDown

        Activate() 'see comment in ScrollSet.
        If e.Button = Windows.Forms.MouseButtons.Left Then  'For one thing I don't want to deselect the DisplayedBL if the user is just right-clicking to show the format menu.
            MouseDownControl = picCanvas

            If Control.ModifierKeys <> Keys.Control Then
                For Each u As EntityControl In picCanvas.EntityControls()
                    u.Selected = False
                Next
                picCanvas.SelectedBLs.Clear()
            End If
            'clear the selections and start afresh. If user is pressing the Ctrl key then add this 
            'to existing selection. This is the normal way to make multiple non-adjacent selections in eg Excel, Outlook, Explorer. But note in PowerPoint Shift is used. There
            'Ctrl is used to make a copy of a shape and add it elsewhere.

            If Not MouseOverBL Is Nothing Then

                If picCanvas.SelectedBLs.Contains(MouseOverBL) Then   'Ctrl must have been down cos otherwise I'd have cleared SelectedBLs.
                    '  if user Ctrl + clicks on a baseline which is selected then he wants to deselect it. 
                    'Can't really do the same for rubberbanding baselines because could have a range selected, some already selected and some not - gets confusing.
                    picCanvas.SelectedBLs.Remove(MouseOverBL, True)
                    CurrentBL = picCanvas.SelectedBLs.Last
                Else
                    'Simply add the chosen baseline to SelectedBLs and make it the frmMDI.DisplayedBL
                    picCanvas.SelectedBLs.add(MouseOverBL, True)
                    CurrentBL = MouseOverBL
                End If
            End If

            If e.Button = Windows.Forms.MouseButtons.Left And picCanvas.Cursor = Cursors.Default Then    'second test to enable print area resizing
                picCanvas.CanvasDownPoint = e.Location
            End If

            'Deselect all parties.

            'I need to redetermine the MouseOverBL here because if a context menu is showing and 
            'the user then dismisses that by clicking on the canvas
            picCanvas.picCanvas_MouseMove(sender, e)

        Else
            Me.frmOne_KeyDown(Me, New KeyEventArgs(Keys.Escape))    'right mouse button coming up is like pressing the Esc key
        End If

    End Sub

    Friend Sub picCanvas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picCanvas.MouseUp

        picCanvas.Image = Nothing   'remove what was being drawn.

        If e.Button = Windows.Forms.MouseButtons.Left Then

            MouseDownControl = Nothing

            'Add an entity if one is selected.

            If frmMDI.tsiEntity.Checked Then
                picCanvas.AddEC(e.Location, True)
                frmMDI.EntityButtonDown = False
            End If

            If Not frmMDI.bDrawButtonDoubleClicked Then frmMDI.tsiSelect.Checked = True 'reset the default button in the toolbar.

            'Select appropriate items. I think it's OK to select items of different types together - Entities are selected for dragging
            'and baselines for getting info. There's no conflict between the two. If the user just wants to select entities then 
            'he can do this with the Ctrl key.
            If BLDrawing Is Nothing Then

                For Each c As EntityControl In picCanvas.EntityControls()
                    With c
                        If Not .Selected Or Control.ModifierKeys <> Keys.Control Then
                            .Selected = picCanvas.RubberBand(e.Location).Contains((.Left + .Right) / 2, (.Top + .Bottom) / 2)
                        End If
                    End With
                Next

                frmMDI.SetPRGObjects()

                'look for a baseline whose midpoint is in the rubberband.
                'Can't be bothered to check the text - the midpoint will be close to it anyway.
                Dim BLToSelect As BaseLine = Nothing
                picCanvas.QueryRB(BLToSelect, e.Location)   'SelectedBLs will have been emptied when the mouse went down unless the 
                'CTRL key was pressed. In either case it's OK to just add any contained BLs. This will cause the QueryBandingInclusion event
                'of picCanvas to be raised. That needs to be an event of picCanvas cos I don't have a reference to the form from the BaseLine.

                If CurrentBL Is Nothing Then CurrentBL = BLToSelect 'arbitrary selection if more than one.

                picCanvas.CanvasDownPoint = Nothing
            End If
        End If

    End Sub
#End Region

#Region "Files"
    Friend InitialBlankForm As Boolean      'Whether this is the initial blank form loaded on starting the application.

    Friend OpenedAsReadOnly As Boolean      'Whether this file has been opened read-only, because the Modify password hasn't been entered.

    Friend bClosing As Boolean

    Private Sub frmOne_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If Me.picCanvas.Dirty And (picCanvas.EntityControls.Count > 0 Or Me.PasswordOpen <> "" Or Me.PasswordModify <> "") Then
            Select Case MsgBox("Do you want to save " & IIf(FileName = "", Text, "the changes to " & FileTitle()) & "?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Exclamation)
                Case MsgBoxResult.Yes
                    mnuFileSave_Click(Nothing, Nothing)
                    If Me.picCanvas.Dirty Then e.Cancel = True 'If Dirty then saving must have been cancelled. Cancel the unload too.
                Case MsgBoxResult.No
                    'Do nothing
                Case MsgBoxResult.Cancel
                    e.Cancel = True
            End Select
        End If
        If Not e.Cancel Then
            ToolStripManager.RevertMerge(frmMDI.tstMain, Me.tstMain)
            bClosing = True
        End If

    End Sub

    Public Function FileTitle() As String

        'Returns the name of the file. Easier than parsing it myself. Would call this "FileName" but that's confusing because the open and close file dialog boxes use that for thw whole path.
        Return System.IO.Path.GetFileNameWithoutExtension(FileName)

    End Function

    Protected Function GetJColJurn(ByVal j As IJurisdiction) As IJurisdiction
        If TypeOf j Is AppJur Then
            For Each J2 As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
                If J2.PredefinedJur = j.PredefinedJur Then Return J2
            Next
        ElseIf Not j Is Nothing Then
            For Each K As Object In JurisdictionSpace.Jurisdictions.Values
                Dim O2 As Object = j
                If K.GetType Is O2.GetType Then Return K
            Next
        End If
    End Function

    Private Sub Save(Optional ByVal CopyFilePath As String = "")

        frmMDI.UseWaitCursor = True
        UseWaitCursor = True

        'Am either saving this file or creating a copy of it.
        Dim s As String = ""
        If CopyFilePath = "" Then
            s = FileName
        Else
            s = CopyFilePath
        End If

        Dim MyMemoryStream As New MemoryStream
        'Code is from http://forums.msdn.microsoft.com/en-US/netfxremoting/thread/68c200c2-4aa4-48dc-95be-6fe077fd10f4/

        With New BinaryFormatter
            If CopyFilePath = "" Then
                .Serialize(MyMemoryStream, PasswordModify)
            Else
                .Serialize(MyMemoryStream, "")
            End If

            Dim sdc As New SerialDataClass

            sdc.SerialStanConCols = picCanvas.StanConCols
            sdc.SerialCredSupCollection = picCanvas.CredSupCollection
            sdc.SerialSecurityCollection = picCanvas.SecurityCollection

            For Each c As EntityControl In picCanvas.EntityControls
                'ControlPointInfo should already be stored for serialization through each control's move event.
                sdc.eCol.Add(c.LEntity)
            Next

            .Serialize(MyMemoryStream, sdc)
            .Serialize(MyMemoryStream, picCanvas.PrintArea)
        End With

        Dim DES As New DESCryptoServiceProvider()
        Dim gch As GCHandle
        InitialiseDES(DES, gch)

        Dim encryptedbytes As Byte() = DES.CreateEncryptor.TransformFinalBlock(MyMemoryStream.ToArray(), 0, MyMemoryStream.Length)

        MyMemoryStream.Close()

        ZeroMemory(gch.AddrOfPinnedObject, 8)
        gch.Free()

        With File.Open(s, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)

            .Write(encryptedbytes, 0, encryptedbytes.Length)
            .Flush()
            .Close()

        End With

        If CopyFilePath = "" Then picCanvas.Dirty = False
        frmMDI.UseWaitCursor = False
        UseWaitCursor = False

    End Sub

    Private Sub AddAsRecentFile()

        'Add this file to the top position in list of recent files, and move all existing ones down the list.
        'If the value that would be entered for recent file 2, 3 or 4 is the same as the FullPath (entered as 
        'RecentFile1, then stop the process, as otherwise it would appear twice.
        If Me.FileName.EndsWith(FileSuffix) Then
            With My.Settings
                If FileName <> .RecentFile1 Then
                    Dim r As String
                    Dim s As String
                    r = .RecentFile1
                    .RecentFile1 = FileName
                    If r <> "" Then
                        s = .RecentFile2
                        If r <> FileName Then
                            .RecentFile2 = r
                            r = s
                            If r <> "" Then
                                s = .RecentFile3
                                If r <> FileName Then
                                    .RecentFile3 = r
                                    If r <> "" And s <> FileName Then .RecentFile4 = s
                                End If
                            End If
                        End If
                    End If
                    frmMDI.ShowRecentFiles()    'update the menus.
                End If
            End With
        End If

    End Sub

    Private Sub mnuFileSaveAs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFileSaveAs.Click

        Dim SaveFileDialog As New SaveFileDialog    'can see no reason to create dialogue boxes at design time.

        'Dialog handles overwriting of files by itself.
        With SaveFileDialog

            If FileName = "" Then
                .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
            Else
                'Show the location of current file.
                .InitialDirectory = System.IO.Path.GetDirectoryName(FileName)
            End If

            .Filter = DialogFilter()
            .ShowDialog(Me)

            If .FileName <> "" Then 'will be this result if user cancels, even if a filename is entered.

                If OpenedAsReadOnly And FileName = .FileName Then
                    'Can't allow the user to circumvent read-only restriction by saving the file as itself.
                    MsgBox("Cannot replace " & .FileName & ". This is the current file which has been opened as read-only.", MsgBoxStyle.Exclamation)
                Else

                    OpenedAsReadOnly = False    'needs to be before next line as that will call SetText
                    FileName = .FileName
                    Save()
                    AddAsRecentFile()

                End If
            End If
        End With

    End Sub

    Private Sub mnuFileSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFileSave.Click, tsbSave.Click

        If FileName = "" Or OpenedAsReadOnly Then
            mnuFileSaveAs_Click(sender, e)  'Treat as a "Save As" request.
        Else
            Save()
        End If

    End Sub

    Private mFileName As String     'The full file path of the file.
    Public Property FileName(Optional ByVal sParentText As String = "") As String   'Don't call this "Path" because Path is a namespace (well a class with only shared methods).
        'Call it "FileName" to conform with the open and save dialog boxes.

        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
            If sParentText = "" Then
                SetText()
            Else
                'I can't have more than one "Post Completion" at the end. Just do on the basis of the string. I don't think
                'I want to record a value because the user might change the name and I'd have to keep a record of the original file also.
                Dim s As String = "Post Completion"
                Dim sText As String = sParentText
                sText = sParentText & " - " & s
                If sParentText.EndsWith(s) Then
                    sText = sParentText & "(2)"
                ElseIf sParentText.EndsWith(")") Then
                    'if there's a number at the end before a ")" increase the number by one.
                    Dim spos As Integer = sParentText.IndexOf(s & "(") + s.Length
                    If spos > 0 Then
                        Dim sposno As String = sParentText.Substring(spos + 1, sParentText.Length - spos - 2)   'possible number - user might have something else there.

                        If IsNumeric(sposno) Then
                            sText = sParentText.Substring(0, spos + 1) & CInt(sposno) + 1 & ")"
                        End If
                    End If
                End If
                Text = sText
            End If
        End Set

    End Property

#End Region

#Region "Printing"

    Friend ShowTitleWhenPrinting As Boolean = True 'whether to show title at bottom of print page.

    Private Sub mnuFilePrintPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFilePrintPreview.Click, tsbPrintPreview.Click

        Dim PreviewDialog As New frmPrintPreview(Me)

        PreviewDialog.ShowDialog(Me)

    End Sub

    Private Sub tsbPrintArea_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsbPrintArea.CheckStateChanged
        picCanvas.mnuFilePrintAreaShowHide.Checked = tsbPrintArea.Checked
        picCanvas.Refresh()
    End Sub

    Private Sub PrintDialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFilePrint.Click
        ' The PrintDialog allows the user to select the printer that they want to print 
        ' to, as well as other printing options.   
        PrintDialog1.Document = picCanvas.PrintDocument1
        If PrintDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            picCanvas.PrintDocument1.Print()
            SetPrintButtonToolTip(tsbPrint)
        End If
    End Sub

    Private Sub btnPageSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFilePageSetup.Click

        With PageSetupDialog1
            .Document = picCanvas.PrintDocument1
            .PageSettings = picCanvas.PrintDocument1.DefaultPageSettings
        End With

        If PageSetupDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            With picCanvas.PrintDocument1.DefaultPageSettings
                If CInt(.PrintableArea.Left) > .Margins.Left Or CInt(.PrintableArea.Top) > .Margins.Top Or .PaperSize.Height - CInt(.PrintableArea.Bottom) > .Margins.Bottom Or .PaperSize.Width - CInt(.PrintableArea.Right) > .Margins.Right Then
                    Select Case MsgBox("One or more of the margins which you have set is outside the printable area of the page. Do you wish to reset them to default values?", MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNoCancel)
                        Case MsgBoxResult.Yes
                            picCanvas.SetMarginsToPrintArea()

                        Case MsgBoxResult.Cancel
                            Exit Sub    '************
                    End Select
                End If
            End With
            picCanvas.PrintDocument1.DefaultPageSettings = PageSetupDialog1.PageSettings
            SetPrintButtonToolTip(tsbPrint)
        End If

    End Sub

    Friend Sub SetPrintButtonToolTip(ByVal btn As ToolStripItem)
        btn.ToolTipText = "Print (" & Me.picCanvas.PrintDocument1.PrinterSettings.PrinterName & ")"
    End Sub
    Private Sub tsbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPrint.Click
        picCanvas.PrintDocument1.Print()
    End Sub

#End Region

#Region "PowerPoint"

    Friend PPPres As PowerPoint.Presentation

    Private Sub mnuFileSendToPowerPoint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileSendToPowerPoint.Click, tsbPowerPoint.Click

        'Code doesn't differ much from the VB6 code. Did have a form that user could go through accessible from 
        'menu item where he could set certain things such as the title of the slide and whether to use colours.
        'But all these things can be changed at least as easily from within PowerPoint. So I don't think I 
        'want to bother with this anymore.

        PPEsc = False
        Dim PPTried As Boolean
        Dim ppp As PowerPoint.Slide = Nothing

StartHere:

        Try
            If PPApp Is Nothing Then PPApp = New PowerPoint.Application

            Dim OldState As PowerPoint.PpWindowState = PPApp.WindowState

            'Can't do anything to the slide if the application is invisible, _
            'but don't want whole process _
            'to be seen cos doesn't look very professional, so _
            'minimise.However(don)'t _
            'expressly make it visible cos that then activates _
            'it so the whole process is shown. _
            ''If user maximizes the application then that will work, and I can't _
            'prevent this through the PPApp_Activate Event. All I can do is to _
            'minimize the application in DoEvents each time.
            PPApp.WindowState = PowerPoint.PpWindowState.ppWindowMinimized 'nb this won't work if a dialog box is showing in PowerPoint.

            'If user closes a presentation it won't become nothing so have to check for it like this.
            Dim bExists As Boolean
            For Each pres As PowerPoint.Presentation In PPApp.Presentations
                If pres Is PPPres Then bExists = True
            Next
            If Not bExists Then PPPres = PPApp.Presentations.Add

            ppp = PPPres.Slides.Add(PPPres.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutTitleOnly)
            DoPPEvents(ppp)

            'Give it a title.
            Dim TitleShape As PowerPoint.Shape = Nothing

            If ppp.Shapes.Count > 0 Then    'should be, but just in case.
                TitleShape = ppp.Shapes.Item(1)
                With TitleShape
                    .TextFrame.AutoSize = PowerPoint.PpAutoSize.ppAutoSizeShapeToFitText
                    With .TextFrame.TextRange

                        .Text = Text
                        With .Font
                            .Size = 16
                            .Bold = True
                        End With
                        .ParagraphFormat.Alignment = PowerPoint.PpParagraphAlignment.ppAlignCenter
                    End With
                    .Visible = Core.MsoTriState.msoCTrue
                    .Top = PPPres.PageSetup.SlideHeight - 2 * .Height
                End With
            End If
            'Using Screen dimensions to scale the slide is OK I think _
            'as it ensures that the sizes of the entities will always be the same.
            'If the user wants to move the whole diagram in PP then that is very easy, and _
            'if he wants to resize it then that is pretty easy too, as he just needs to _
            'resize the group and changes its font size.

            'Look at how many Rs can be fit in both horizontally and vertically, and take the one that _
            'will fit the fewest in. For the vertical fit, take the height of the caption out, _
            'provided SlideHeight is over 80, so there's room to draw above it.

            Dim R As Rectangle = picCanvas.GetUsedArea(True)

            With PPPres.PageSetup
                picCanvas.ScaleFactor = Max(R.Width / (.SlideWidth * 0.8), _
                R.Height / (.SlideHeight * 0.8 + 40 * (.SlideHeight > 80)))
                picCanvas.ScaleXZero = (.SlideWidth - (R.Right + R.Left) / picCanvas.ScaleFactor) / 2     'R.Right + R.Left because need difference, but then subtract R.Left/Deal.ScaleFactor
                picCanvas.ScaleYZero = (.SlideHeight - (R.Bottom + R.Top) / picCanvas.ScaleFactor) / 2
            End With

            DoPPEvents(ppp)

            picCanvas.Draw(, , ppp)

            For Each P As EntityControl In picCanvas.EntityControls()
                GroupPP(ppp, P.PPShapes)
            Next

            Dim pplist As New List(Of String)
            For Each s As PowerPoint.Shape In ppp.Shapes
                If Not s Is TitleShape Then pplist.Add(s.Name)
            Next
            GroupPP(ppp, pplist)

            DoPPEvents(ppp)

            With PPApp
                .Visible = True 'otherwise only see an empty application
                .WindowState = OldState 'return to original state.
                .Activate()
            End With

            PPPres.Windows.Item(1).Activate()
            ppp.Select()

        Catch
            '462 will happen if user closes PP with Ctrl + Alt + Del. _
            '  PPApp *not* set to Nothing then so need to force it. _
            '  Will be problems if user closes with Ctrl + Alt + Del _
            '  and then recreates a slide before getting the _
            '  "This application is not responding" dialog box.
            If Err.Number = conEscError Then
                MsgBox("Slide creation cancelled.", MsgBoxStyle.Exclamation, My.Application.Info.Title)
            ElseIf Err.Number = -2147467259 And Not PPTried Then
                'User closed powerpoint.

                PPApp = Nothing
                PPPres = Nothing
                PPTried = True
                GoTo StartHere
            ElseIf Err.Number = 429 Then  '"ActiveX component can't create object"

                frmMDI.UseWaitCursor = False
                MsgBox("Unable to access Microsoft PowerPoint.", vbExclamation, My.Application.Info.Title)
                'User probably doesn't have PowerPoint.

            End If

        End Try

        frmMDI.UseWaitCursor = False
        picCanvas.ScaleFactor = 1
        picCanvas.ScaleXZero = 0
        picCanvas.ScaleYZero = 0

        For Each P As EntityControl In picCanvas.EntityControls()
            P.PPWidth = P.Width
        Next

        picCanvas.Refresh()

    End Sub

#End Region

#Region "Scrolling"

    Private Declare Function SetScrollInfo Lib "user32" (ByVal hwnd As Integer, ByVal n As SCROLLBAR_FLAG, ByRef lpcScrollInfo As SCROLLINFO, ByVal bool As Boolean) As Integer

    Friend Enum SCROLLBAR_FLAG
        SB_HORZ = 0
        SB_VERT = 1
    End Enum

    Private HasScroll(SCROLLBAR_FLAG.SB_VERT) As Boolean    'Flag as to whether the scrollbars are there.

    Private Enum SCROLLBAR_MASK
        SIF_RANGE = &H1S
        SIF_PAGE = &H2S
        SIF_POS = &H4S
        SIF_DISABLENOSCROLL = &H8S
        SIF_TRACKPOS = &H10S
    End Enum

    Private Structure SCROLLINFO
        Dim cbSize As Integer
        Dim fMask As SCROLLBAR_MASK
        Dim nMin As Integer
        Dim nMax As Integer
        Dim nPage As Integer
        Dim nPos As Integer
        Dim nTrackPos As Integer
    End Structure

    Structure Rect
        Dim Left As Integer
        Dim Right As Integer
        Dim Top As Integer
        Dim Bottom As Integer
    End Structure

    Private Sub frmOne_ClientSizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ClientSizeChanged

        'This occurs when toolbars are set or removed and when normal resizing takes place.
        SetCanvasSize()
        ScrollSet()

    End Sub

    Private Sub ReformatForm()

        'Basic things that make sure it's all looking OK.
        SetCanvasSize()
        ScrollSet()
        picCanvas.Invalidate()

    End Sub

    Friend Sub SetCanvasSize()

        'Set the size of piccanvas to be big enough to contain all 
        'party controls and to fill the client area of the form.

        'I think it's best not to dock the canvas even if there is no need for scrollbars. Otherwise it reverts to its
        'design time size when I remove the docking and you get a flicker.

        Dim BB As Rectangle = ClientRectangle

        If picCanvas.EntityControls.Count > 0 Then
            Dim D As Rectangle = picCanvas.GetUsedArea(False)
            D.Offset(picCanvas.Location) 'convert to ClientRectangle co-ordinates.
            BB = Rectangle.Union(D, BB)
        End If

        If picCanvas.Bounds <> BB Then  'don't do the adjustment if none is needed or will occur while mouse is down on partycontrol but not being dragged.
            'If picCanvas's left isn't the same as D's left, then will be adjusting the controls' lefts, so need to move _
            'them to keep them in the same relative positions.
            'Without this the effect is not too bad, but parties that aren't being dragged will appear to follow those that are.
            For Each F As EntityControl In picCanvas.EntityControls()
                With F
                    .Visible = False    'avoids flickering if control being dragged off left or top.
                    .Location += (New Size(picCanvas.Left - BB.Left, picCanvas.Top - BB.Top))   'don't try to use the Offset method of Point here - it has no effect on a control's location.
                End With
            Next

            picCanvas.PrintArea.Offset(picCanvas.Left - BB.Left, picCanvas.Top - BB.Top)

            'Picturebox has an autosize property, but I'm not sure it would be that useful since 
            'like autoscrolling it doesn't take account of controls off the left and top.
            'Also it needs to be at least as big as frmOne, not just its Controls.
            picCanvas.Bounds = BB

            For Each F As EntityControl In picCanvas.EntityControls()
                F.Visible = True
            Next

            picCanvas.Refresh()
        End If

    End Sub

    Private Sub ScrollSet()

        If Created Then 'scrollbars appear for an instant without this.

            'Set appropriate scrollbars.

            Dim Dirn As SCROLLBAR_FLAG
            For Dirn = SCROLLBAR_FLAG.SB_HORZ To SCROLLBAR_FLAG.SB_VERT

                Dim SBI As SCROLLINFO
                'Set up or hide horizontal and vertical scrollbars as necessary.
                With SBI
                    .cbSize = Len(SBI)
                    .fMask = SCROLLBAR_MASK.SIF_RANGE Or SCROLLBAR_MASK.SIF_POS Or SCROLLBAR_MASK.SIF_PAGE
                    .nMax = IIf(Dirn = SCROLLBAR_FLAG.SB_HORZ, picCanvas.Width, picCanvas.Height)

                    .nPage = IIf(Dirn = SCROLLBAR_FLAG.SB_HORZ, ClientRectangle.Width, ClientRectangle.Height)

                    If .nPage <> .nMax Then

                        'Haven't figured out why you need the +1 in the following line, 
                        'but if you don't have it the scrollbars will get always bounce back to one pixel before the right or bottom of the 
                        'relevant scrollbar.
                        .nPage = .nPage + 1
                        .nPos = -IIf(Dirn = SCROLLBAR_FLAG.SB_HORZ, picCanvas.Left, picCanvas.Top)
                    Else
                        .nMax = 0
                        .nPage = 0
                        .nPos = 0
                    End If

                    'This next line has the undesired effect of preventing the form from being activated 
                    'when the user clicks on it in a normalised state. Only clicking on the title bar 
                    'causes it to be activated. Hmm.
                    'Had a go at using the form's HorizontalScroll and VerticalScroll properties, but very flickery, and couldn't 
                    'see how to set the page size.
                    SetScrollInfo(Handle.ToInt32, Dirn, SBI, True)

                    If HasScroll(Dirn) <> (.nPage <> .nMax) Then
                        HasScroll(Dirn) = (.nPage <> .nMax)
                        picCanvas.Invalidate() 'otherwise bits of scrollbar seem to get left lying around.
                        For Each F As EntityControl In picCanvas.EntityControls()
                            F.Refresh()
                        Next
                    End If

                End With
            Next
        End If

    End Sub

    Private Sub Form1_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles Me.Scroll

        'Note that this event will move picCanvas by itself, but wrongly. So I need to correct it.

        Dim Dirn As SCROLLBAR_FLAG
        Dirn = IIf(e.ScrollOrientation = ScrollOrientation.HorizontalScroll, SCROLLBAR_FLAG.SB_HORZ, SCROLLBAR_FLAG.SB_VERT)

        'If this form is normalised within the MDI area, and the user clicks on the scrollbar while another MDI child is activated, 
        'this form won't get activated, although the scrollbar will work. So activate it.
        Activate()

        'e.NewValue is not very reliable. For instance if the scrollbar is at the minimum and the user clicks the arrow at the end of 
        'the scrollbar, the OldValue is 0 and the NewValue 1. That's correct for the first click, but the values remain the same thereafter so the
        'scrollbar gets stuck.

        Dim Delta As Integer = e.NewValue + IIf(Dirn = SCROLLBAR_FLAG.SB_HORZ, picCanvas.Left, picCanvas.Top)

        Select Case e.Type
            Case ScrollEventType.SmallDecrement
                Delta = -5
            Case ScrollEventType.SmallIncrement
                Delta = 5
            Case ScrollEventType.LargeDecrement
                Delta = -10
            Case ScrollEventType.LargeIncrement
                Delta = 10
        End Select

        If HasScroll(Dirn) Then 'stupid event continues to fire even after the scrollbar has been removed, so have to check for this.
            With picCanvas
                If Dirn = SCROLLBAR_FLAG.SB_HORZ Then
                    .Left = Math.Max(ClientRectangle.Width - .Width, Math.Min(.Left - Delta, 0))
                Else
                    .Top = Math.Max(ClientRectangle.Height - .Height, Math.Min(.Top - Delta, 0))
                End If
            End With
            SetCanvasSize()
            ScrollSet() 'must do this or no change will be shown.
        End If

    End Sub

    Private Sub Form1_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel

        'Favour the vertical scrollbar over the horizontal one if both are there.
        'BUG - this event doesn't seem to occur once a topic has been displayed in the web browser.
        'Also it doesn't occur for left to right scrolling.

        Dim se As ScrollEventType
        If e.Delta < 0 Then
            se = ScrollEventType.SmallIncrement
        Else
            se = ScrollEventType.SmallDecrement
        End If

        If HasScroll(SCROLLBAR_FLAG.SB_VERT) Then
            Form1_Scroll(sender, New ScrollEventArgs(se, 0, ScrollOrientation.VerticalScroll))
        ElseIf HasScroll(SCROLLBAR_FLAG.SB_HORZ) Then
            Form1_Scroll(sender, New ScrollEventArgs(se, 0, ScrollOrientation.HorizontalScroll))
        End If

    End Sub

    Private Sub picCanvas_Reformat() Handles picCanvas.Reformat
        ReformatForm()
    End Sub

#End Region

#Region "Password Protection"

    Friend PasswordOpen As String = ""
    Friend PasswordModify As String = ""
    Private Sub mnuFilePasswordProtection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFilePasswordProtection.Click

        Dim f As New frmPassword(PasswordOpen, PasswordModify)
        f.ShowDialog()

        If f.DialogResult = Windows.Forms.DialogResult.OK Then
            If Me.PasswordOpen <> f.txtPOpen.Text Or Me.PasswordModify <> f.txtPModify.Text Then Me.picCanvas.Dirty = True '
            Me.PasswordOpen = f.txtPOpen.Text
            Me.PasswordModify = f.txtPModify.Text
        End If
        f.Dispose()

    End Sub

    Friend key(7) As Byte
    Private Sub InitialiseDES(ByVal DES As DESCryptoServiceProvider, ByRef gch As GCHandle)

        Dim hashMD5 As New MD5CryptoServiceProvider

        'Use the open password as the key. I could just serialize it as part of the file but I'd need a universal password for that then, and 
        'if someone found that he would probably be able to open all files. For the modify password it's not so important.
        'Note that this means that even I would be unable to open someone else's password protected file.
        Array.Copy(hashMD5.ComputeHash(Encoding.UTF8.GetBytes(PasswordOpen)), key, 8)

        DES.Key = key
        DES.IV = ASCIIEncoding.ASCII.GetBytes("DefaultV")

        gch = GCHandle.Alloc(key, GCHandleType.Pinned)

    End Sub

    ' Call this function to remove the key from memory after it is used for security.
    <DllImport("kernel32.dll")> _
    Shared Sub ZeroMemory(ByVal addr As IntPtr, ByVal size As Integer)
    End Sub

#End Region

#Region "Post-Completion Diagrams"
    Private Sub mnuGeneratePost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileGeneratePost.Click, tsbGeneratePost.Click
        'Create a new frmBase with all Instruments which don't exist post-completion stripped out, and only show those entities which 
        'continue to have an involvement.
        'Maybe I want to make things dynamic so that a change in one form leads to the appropriate change in the other, 
        'though then I need to think about how to keep track of which file relates to which.
        'Do I clone everything or use the same objects across files?
        'A. I think the easy approach will be fine. i.e. use the same objects. When I close a file and reopen it the serialisation will
        'create new objects. But that's OK - there's no need to ask the user if he wants to update the post-completion diagram because 
        'he can very easily just create a new one if he wants. But while both transactions remain open it's nice if the post-completion diagram 
        'updates as he's making changes.

        'Save this in a temporary file. Reload it as a new one. Delete the temporary file. Delete all Instruments that don't exist post-completion.
        'Anything that was passed-through should be moved to a different baseline collection, with the appropriate toentity.TransactionCons should be converted to 
        'ExistingCons. Cash should be deleted. Possibly I could make the Instruments the same instances between the pre and the post files, so it's all dynamic. But there's no way
        'the connections can be the same because they're not drawn to the same entities.Entities could perhaps also be the same. If I do make it dynamic, maybe changes to terms of Instruments and 
        'name/jur of Entity get reflected, but deleting or adding or moving entity controls is not reflected - have to make a new form if that's what you want to do.

        Dim tempFileName As String = ""

        Try
            ' Return the path and name of a newly created Temporary
            ' file. The GetTempFileName() method creates
            ' a 0-byte file and returns the name of the created file.
            tempFileName = System.IO.Path.GetTempFileName()

            ' Create a FileInfo object to manipulate properties of 
            ' the created temporary file
            Dim myFileInfo As New FileInfo(tempFileName)
            ' Use the FileInfo object to set the Attribute property of this
            ' file to Temporary. Although this is not completely necessary, 
            ' the .NET Framework is able to optimize the use of Temporary
            ' files by keeping them cached in memory.
            ' The Attribute given to a file created with
            ' the GetTempFileName() method is Archive, which prevents the 
            ' .NET Framework from optimizing its use.
            myFileInfo.Attributes = FileAttributes.Temporary

        Catch exc As Exception
            ' Warn the user if there is a problem.
            MsgBox("Unable to create a temporary file - process cannot be carried out.")
        End Try

        Me.Save(tempFileName)

        With frmMDI.AddNewChild(True, tempFileName, , Text)
            .picCanvas.ConvertToPostCompletion()
            .FileName(Text) = ""    'not saved.
            .picCanvas.Dirty = True
        End With

        ' Attempt to delete the file.
        Try
            File.Delete(tempFileName)

        Catch exc As Exception

        End Try

    End Sub

#End Region

#Region "Misc"
    Private Sub tsbAlign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsbAlign.Click
        picCanvas.mniFormatAlign_Click(sender, e)
    End Sub

    Private Sub tsbCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbCopy.Click, picCanvas.EditCopyClicked

        'Nice to give the user the chance to copy the image as a picture.

        With New PictureBox
            'I did originally want to do this through the print preview dialog so the user could see what he 
            'was copying and adjust the size of it. But I can't get the size of the printdocument which is shown there
            'so it didn't really work. So the user just have to have it the size it appears in picCanvas. At least this is
            'more intuitive.
            .Width = picCanvas.PrintArea.Width + 1
            .Height = picCanvas.PrintArea.Height + 1

            .Image = New Bitmap(.Width, .Height)

            Dim MyGraphics As Graphics = Graphics.FromImage(.Image)

            With MyGraphics
                .TranslateTransform(-picCanvas.PrintArea.Left, -picCanvas.PrintArea.Top)
                .Clear(Color.White) 'otherwise the background is blue for some reason.
            End With

            picCanvas.Draw(MyGraphics, , , True)
            Clipboard.SetDataObject(.Image)

        End With

    End Sub

    Private Sub frmOne_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        'Basically, call all mouseup handlers without a MouseEventArgs object. They should check for this 
        'being nothing and not do any of the actions that depend on accepting the mouseup in this case - just the resetting things.
        If e.KeyCode = Keys.Escape Then

            PPEsc = True
            If BLDrawing Is Nothing Then
                picCanvas.Image = Nothing
                picCanvas.CanvasDownPoint = Nothing
                picCanvas.Cursor = Cursors.Default
                If Not frmMDI.tsiSelect.Checked Then frmMDI.tsiSelect.PerformClick()
                frmMDI.Capture = False
                frmMDI.EntityButtonDown = False
            Else
                BLDrawing.UserDrawingDone(True)
            End If
            MouseDownControl = Nothing
            frmMDI.Cursor = Cursors.Default
            'Print area resizing won't be cancelled. Cursor is still over it so cursor should reflect this and so more movement should cause resizing.
            'Same with moving parties, unless I really want to bother.
        End If

    End Sub

    Private Sub Form1_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        'Make sure the appropriate child toolstrip items are always showing. In the Beta version of Vis Studio this _
        'happened automatically, but there were problems with it merging into the PropertyGrid's toolbar, 
        'so having to do it in code seems better.
        ToolStripManager.Merge(tstMain, frmMDI.tstMain)

        'Do the same for tstDraw?
        'Could do, but that means each frmOne must have its own tstDraw. When the user changes between 
        'frmOnes, this will flicker. frmMDI then still needs a toolstrip for the tstDraw to merge into, 
        'and it doesn't go invisible if it has no buttons, so have to figure that out.

        'For menus I can do everything through the Properties window. But it's not very intuitive.
        'Essential that the child form menu strip is invisible, and the top level menu items 
        'have a MergeAction of MatchOnly (if matching with an existing one, otherwise, Insert).

        'See also frmMDI MDIChildActivated event.

    End Sub

    Private Sub Form1_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate

        'Remove the toolstrip for this form so new form's one can be inserted.
        'One day started having problems here with "cannot access a disposed object object: icon" when I closed the form. So don't do this when closing the form.

        If Not bClosing Then ToolStripManager.RevertMerge(frmMDI.tstMain, tstMain)

        CurrentBL = Nothing        'Confusing if terms of an instrument in a form that's not showing are still displayed.

    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFileClose.Click
        Close()
    End Sub

    Friend Sub SetText()

        If FileTitle() = "" Then
            SetNewFileText()
        Else
            Text = FileTitle() & IIf(Me.OpenedAsReadOnly, " (Read-only)", "")
        End If

    End Sub

    Friend Sub Stophere()
        Stop
    End Sub

    Private Sub picCanvas_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles picCanvas.ControlAdded
        If picCanvas.EntityControls.Count > 1 Then tsbPrintArea.Enabled = True
    End Sub

    Private Sub picCanvas_ControlRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles picCanvas.ControlRemoved

        If picCanvas.EntityControls.Count < 2 Then
            'Can't show printarea if no parties - it would just be artbitrary. And with just one it's silly too.
            picCanvas.mnuFilePrintAreaShowHide.Enabled = False
            tsbPrintArea.Enabled = False
            picCanvas.mnuFilePrintAreaShowHide.Checked = False
            tsbPrintArea.Checked = False
            picCanvas.Refresh() 'hide printarea if necessary.
        End If

    End Sub

    Private Sub frmOne_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.TextChanged
        If Not picCanvas.PrintDocument1 Is Nothing Then picCanvas.PrintDocument1.DocumentName = Me.Text
    End Sub

#End Region

End Class

