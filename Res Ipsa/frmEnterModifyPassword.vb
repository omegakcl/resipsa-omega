Imports System.Windows.Forms

Friend Class frmEnterModifyPassword

    'Can't use the InputBox function here because I need to give the user the option to open the file as readonly.

    Private FP As frmBase

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

            If Me.TextBox1.Text = FP.PasswordModify Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Close()
            Else
                MsgBox("Incorrect Password", MsgBoxStyle.Exclamation)
            End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Public Sub New(ByVal f As frmBase)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        FP = f
        Label1.Text = "Enter password to modify this file:"
        Application.UseWaitCursor = False

    End Sub

    Private Sub cmdReadOnly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReadOnly.Click
        Me.DialogResult = Windows.Forms.DialogResult.Ignore
        Me.Close()
    End Sub

    Private Sub frmEnterPassword_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.UseWaitCursor = True
    End Sub

    Private Sub frmEnterModifyPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
