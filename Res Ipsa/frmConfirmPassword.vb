Friend Class frmConfirmPassword

    Private Expected As String

    Public Sub New(ByVal TextToExpect As String, ByVal OpenPassword As Boolean)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Expected = TextToExpect
        Label1.Text = "Confirm password to " & IIf(OpenPassword, "open", "modify") & ":"

    End Sub

    Private Sub cmdOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        'If confirmation matches, close this form and frmPassword. If not, just close this form.

        If Me.TextBox1.Text <> Expected Then

            MsgBox("The confirmation does not match the password.", MsgBoxStyle.Exclamation)
            Me.DialogResult = Windows.Forms.DialogResult.Abort
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
        Close()

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        'Previous entry of password should be removed. Event is handled by setting the button's DialogResult.
    End Sub

    Private Sub frmConfirmPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class