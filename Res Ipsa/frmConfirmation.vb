Option Strict Off
Option Explicit On

Imports Microsoft.Office
Imports Microsoft.Office.Interop

Friend Class frmConfirmation
    Inherits System.Windows.Forms.Form

    Private WordAppConfirms As Word.Application

    Public Leg1 As SwapLeg 'Outline for Swap.
    Public Leg2 As SwapLeg 'InLine for Swap.
    Public SecObj As SecurityBase 'Security Object for Repo or Buyback.
    Public FileName As String 'File path, so can be set up to save in the same folder.
    Public Comments As String 'Comments for inclusion in the document's properties.

    Private EscConfirm As Boolean 'To return whether Esc pressed so confirm creation should be cancelled.
    Private MakingConfirm As Boolean 'Flags whether a confirm is being created, for use when DoEvents is called.
    Private Const conEscError As Integer = 514 'When user presses Esc to exit a process

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click

        'If user presses Esc then this event occurs. It has two different roles - _
        'to cancel the swap generation, and to unload the form.

        If MakingConfirm Then
            MakingConfirm = False 'making a confirm so want to cancel that and then unload.
        Else
            Close()
        End If

    End Sub

    Private Class ConfirmWrapper

        Friend Sub New(ByVal d As Word.Documents, ByVal PropText As String, ByVal Comments As String)
            Confirm = d.Add

            Confirm.Content.Paste()
            My.Computer.Clipboard.Clear()

            'Fill out comments and name details for both types of confirmation.
            'UPGRADE_WARNING: Couldn't resolve default property of object WordAppConfirms.Documents.Add.BuiltInDocumentProperties(). Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            '  Confirm.BuiltInDocumentProperties(Word.WdBuiltInProperty.wdPropertyTitle) = PropText '"Repo Confirmation Ref: ABC"
            'UPGRADE_WARNING: Couldn't resolve default property of object WordAppConfirms.Documents.Add.BuiltInDocumentProperties(). Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ' Confirm.BuiltInDocumentProperties(Word.WdBuiltInProperty.wdPropertyComments) = Comments

        End Sub
        'Have this wrapper class because I keep doing the same operations on the bookmarks, and easiest if I have an 
        'object I do this on all the time. Can't inherit Word.Document.
        Private Confirm As Word.Document
        Friend Sub FillBookmark(ByVal BookmarkName As String, ByVal InsertText As String)
            Try
                Confirm.Bookmarks.Item(BookmarkName).Range.Text = InsertText
            Catch
            End Try
        End Sub
        Friend Sub CompleteMacro(ByRef B As String, ByRef TheText As String)

            'Fills in a macro, i.e. the text that says "Insert Here". If there's nothing to insert because the _
            'user hasn't specified the value, then I don't want to lose the macro, so I need to check for _
            'Text being nothing.
            Try
                If TheText <> "" Then Confirm.Bookmarks.Item(B).Range.Text = TheText
            Catch
                Debug.Print(B)
            End Try

        End Sub

        Friend Sub DeleteBookmark(ByVal B As String)
            Try
                Confirm.Bookmarks.Item(B).Range.Delete()
            Catch ex As Exception

            End Try
        End Sub

        Friend Sub Table2Delete()
            Confirm.Tables.Item(2).Delete()
        End Sub

        Friend Sub RenumberFinalRows()

            'Need to renumber final rows - a bit tedious because of leading square brackets.
            Dim n As Integer
            Dim R As Word.Range
            Dim a As Boolean
            Dim PropText As String

            'Have added reference to Table 0 here from VB6 - not sure if it's correct.
            For n = 23 To Confirm.Tables.Item(0).Rows.Count Step 2
                R = Confirm.Tables.Item(0).Cell(n, 1).Range
                a = (R.Text.Substring(1, 1) = "[")
                R.SetRange(R.Start, R.Start + 2 - CShort(a))
                R.Delete()
                PropText = CStr(n / 2 + 1 / 2)
                If a Then PropText = "[" & PropText
                R.InsertBefore(PropText)
            Next

        End Sub

        Friend Sub Finalise()

            'Because deleting a bookmark that covers rows only deletes the rows' content, I still _
            'have to delete the actual rows. So look for any consecutive empty ones.
            'All cells including empty ones seem to end with a line feed and a Chr(7) so look for _
            'lengths less than two.
            Dim n As Integer
            With Confirm
                With .Tables.Item(1)
                    n = 2
                    Do
                        If Len(.Cell(n, 1).Range.Text) <= 2 And Len(.Cell(n - 1, 1).Range.Text) <= 2 Then
                            .Rows.Item(n).Delete()
                        Else
                            n += 1
                        End If
                    Loop Until n > .Rows.Count
                End With

                'Remove all bookmarks.
                Do
                    .Bookmarks.Item(1).Delete()     'getting stuck in this loop somehow.
                Loop Until .Bookmarks.Count = 0

                'Make sure all fields are toggled off. For some reason they're all on in the templates, not sure _
                'why so I'll leave them and turn them off here.
                For n = 1 To .Fields.Count
                    .Fields.Item(n).ShowCodes = False
                Next

                .UndoClear() 'don't want user to see my methodology.

                .Activate()
            End With

        End Sub
    End Class

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click

        On Error GoTo ErrorHandler

        'Don't disable the form, just ignore cmdOK and reset optAB if user presses _
        'while confirm is being generated.

        If Not AcceptButton Is cmdOK Then Exit Sub '**** pressed while making confirm.

        MakingConfirm = True

        AcceptButton = Nothing

        Capture = True
        Me.UseWaitCursor = True
        EscConfirm = False

        Dim n As Short
        Dim PropText As String = ""

        lblProgress.Visible = True
        prog1.Visible = True
        prog1.Value = 0 'user may have pressed Esc on a previous attempt.
        Refresh()

        DoWordEvents(1, False)

StartHere:

        'WordAppConfirms will *not* be Nothing if the user closes it, so rely On Error handling.
        If WordAppConfirms Is Nothing Then WordAppConfirms = New Word.Application 'creating a new application.

        Dim Sender As Entity
        Dim Recipient As Entity

        Const TempPath As String = "C:\Template.tmp"
        Dim R As Word.Range
        Dim a As Boolean
        Dim PartyA As Entity
        Dim PartyB As Entity
        Dim PAIntTerms As SwapLeg
        Dim PBIntTerms As SwapLeg
        Dim MaxMin As Boolean
        Dim NomDiffer As Boolean
        Dim DT As String
        Dim RowText As String
        Dim ABLet As String
        Dim SameBasis As Boolean
        Dim IntFunding As SwapLeg
        Dim MarginText As String
        Dim s As String
        Dim CurrCollection As New List(Of Currency)
        Dim P As Cash
        Dim q As Cash
        Dim m As Object
        Dim OldCurr As Boolean
        Dim w As Integer
        Dim v As Integer
        Dim AddTerms As String = ""
        Dim Folder As String = "" 'whether both fixed or both floating. 'holds a userDates' DatesText 'Whether different nominal amounts apply. '           "           "           "           "        PartyB 'Terms which holds the Periodic Payments to be made by PartyA 'Party which is PartyB 'Party which is PartyA
        Dim DC As New Debt.DCFConverter

        Dim ABCD As EToEInstrument = SecObj.SecuredObligation

        With WordAppConfirms

            'Did have an issue here where the page size of the new document was really small, and _
            'had to overcome it by opening document first, storing page and paper size before pasting, and then _
            'restoring it. But seems to have gone now.

            'Copies template form's OLE document to the clipboard, ready for pasting to a new document.
            'There are some problems here with cancelling - sometimes get the message on copying the OLE object _
            'that the Object has not been set if the user runs this after previously cancelling.
            'I think this is resolved by calling DoVerb and Close - using those means that the _
            'templates must not appear in their forms as icons or the template will be briefly visible on copying.
            'But then won't work at all on laptop - "The add method is unavailable because the _
            'document is being edited in another application."! So have gone back to original situation - _
            'probably Esc won't work very well.


            'It's mighty hard to get a document to open with the bookmarks. It was OK on the PC, but on the _
            'laptop they seem not to be copied over if I do OLE1.Object.Range.Copy. Can save the document and then open a new one with _
            'that as a template, but then get "Command Failed" when I try to save it again, because the template _
            'is in use, so can't be altered. If I try to _
            'delete it I get "Permission Denied." Could have a new temporary file each time, but not very good _
            'if I have no way of deleting them. So instead, open the OLE document, save it somewhere, copy its content, _
            'open a new document, and save the content there.

            'Must use bookmarks, not some standard text ("BK1/2/3") cos what if user was to have an entry that _
            'included the standard text and I looked for the standard text after making some insertion?

            Dim b As Byte() = IIf(TypeOf Leg1 Is SwapLeg, My.Resources.Swap_Confirm, My.Resources.Repo_Confirm)

            Dim TempFile As System.IO.FileStream

            If Not IO.File.Exists(TempPath) Then
                TempFile = IO.File.Create(TempPath)
                TempFile.Write(b, 0, b.Length)
                TempFile.Close()
            End If

            With .Documents.Open(TempPath) 'Doesn't seem to be possible to just open the OLE doc into word.
                .Content.Copy()
                .Close()
            End With

            DoWordEvents(1, False) 'make sure it's closed.

            IO.File.Delete(TempPath)

            DoWordEvents(2, False)
            If Leg1.Name <> "" Then PropText = " Ref: " & Leg1.Name

            With New ConfirmWrapper(.Documents, Text & PropText, Comments)

                DoWordEvents(3, True)
                'Date can't be inserted in _
                'the template as it would either always be updated when the document _
                'was looked at, or not updated when the new document was created.

                .FillBookmark("Date", Ordinal(Today))

                'Put the code into the relevant template form as a macro?
                'Does have appeal from the point of view of neatness. But would mean that there _
                'remained an extra ~50kb form loaded while all the Word stuff was being done.
                'Word stuff seems to be quite onerous, so probably better not to.
                If Leg1 Is Nothing Then

                    With ABCD
                        If optAB_0.Checked = True Then
                            Sender = .FromEntity
                            Recipient = .ToEntity
                        Else
                            Sender = .ToEntity
                            Recipient = .FromEntity
                        End If
                    End With

                    .FillBookmark("Recipient", Recipient.Name)
                    .FillBookmark("Sender", Sender.Name)
                    '   .Bookmarks.Item("Subject").Range.Text = IIf(TypeOf SecObj Is Repo, "Repo", "Buy/Sell Back") & " Transaction (Reference Number: " & SecObj.Reference & ")"
                    .FillBookmark("Sender2", Sender.Name)

                    DoWordEvents(5, True)

                    .FillBookmark("ConDate2", Ordinal((SecObj.TradeDate))) 'Contract Date
                    'Would like to use Terms.Title function for next insertion, but not very easy cos _
                    'it depends on controls rather than a Terms and Entity object.

                    PropText = CType(SecObj.Parent, LineArc).Collateral.FromEntity.Name
                    If PropText <> "" Then PropText = PropText & " "
                    '   PropText = PropText & SecObj.Collateral.Fundings.Current.FixedRate & " " 'BUG floating rate?!
                    'If SecObj.Collateral.LinkedCon.Fundings.Current.Perpetual Then
                    '    PropText = PropText & "Perpetual " & SecObj.Collateral.LinkedCon.Fundings.Current.Name
                    'Else
                    '    PropText = PropText & SecObj.Collateral.LinkedCon.Fundings.Current.Name
                    '    If Not SecObj.Collateral.LinkedCon.RType = LOM.Mechanism.OrdShares Then PropText = PropText & " due " & Year(CDate(SecObj.Collateral.LinkedCon.Fundings.Current.MatDate))
                    'End If
                    .CompleteMacro("PurchSec2", PropText) 'Purchased Securities

                    DoWordEvents(6, True)

                    PropText = ""
                    '       If SecObj.Collateral.LinkedCon.Fundings.Current.CUSIP <> "" Then PropText = "CUSIP No." & " " & SecObj.Collateral.LinkedCon.Fundings.Current.Name
                    'If SecObj.Collateral.LinkedCon.Fundings.Current.ISIN <> "" Then
                    '    If PropText <> "" Then PropText = PropText & ";"
                    '    PropText = PropText & "ISIN No." & " " & SecObj.Collateral.LinkedCon.Fundings.Current.ISIN
                    'End If
                    'CompleteMacro(.Bookmarks.Item("ISINCUSIP2"), PropText) 'CUSIP/ISIN
                    .CompleteMacro("Buyer2", ABCD.ToEntity.Name) 'Buyer
                    .CompleteMacro("Seller2", SecObj.SecuredObligation.FromEntity.Name) 'Seller
                    '         CompleteMacro(.Bookmarks.Item("PurchDate2"), Ordinal((SecObj.PurchaseDate))) 'Purchase Date

                    DoWordEvents(7, True)

                    '  PropText = CStr(SecObj.SecuredObln.FundingPayment.dblAmount)
                    'CompleteMacro(.Bookmarks.Item("PurchPrice2"), PropText) 'Purchase Price
                    'If Len(PropText) > 2 And Not IsNumeric(VB.Left(PropText, 3)) Then
                    '    CompleteMacro(.Bookmarks.Item("ConCur2"), VB.Left(PropText, 3)) 'Contractual Currency
                    'End If
                    'CompleteMacro(.Bookmarks.Item("RepDate2"), Ordinal((SecObj.RepurchaseDate))) 'Repurchase Date
                    'CompleteMacro(.Bookmarks.Item("PricRat2"), CStr(SecObj.PricingRate)) 'Pricing Rate
                    'CompleteMacro(.Bookmarks.Item("SellBack2"), CStr(SecObj.SecuredObln.LinkedFunding.Repayment.dblAmount)) 'Sell Back Price

                    If SecObj.AddTerms = "" Then
                        .DeleteBookmark("AddTerms")
                    Else
                        .CompleteMacro("AddTerms2", (SecObj.AddTerms))
                    End If

                    DoWordEvents(8, True)

                    If TypeOf SecObj Is Repo Then 'Repo
                        .DeleteBookmark("SellBackPrice")
                        'Remove Repurchase Date or terminable on demand respectively.
                        '.Bookmarks.Item(IIf(SecObj.TerminableOnDemand, "RepurchaseDate", "OnDemand")).Range.Delete()
                    Else 'BuyBack
                        .DeleteBookmark("OnTerm")
                        .RenumberFinalRows()

                    End If

                    DoWordEvents(9, True)

                Else

                    If Leg1.IEAByPartyA Then
                        PartyA = Leg1.FromEntity
                        PartyB = Leg1.ToEntity
                        PAIntTerms = Leg2
                        PBIntTerms = Leg1
                    Else
                        PartyA = Leg1.ToEntity
                        PartyB = Leg1.FromEntity
                        PAIntTerms = Leg1
                        PBIntTerms = Leg2
                    End If


                    If optAB_0.Checked = True Then
                        Sender = PartyA
                        Recipient = PartyB
                    Else
                        Sender = PartyB
                        Recipient = PartyA
                    End If

                    '   .Confirm.Fields.Item(1).Code.InsertAfter(Sender.LEntity.Name & "]")
                    .FillBookmark("Recipient", Recipient.Name)
                    .FillBookmark("Subject", Leg1.Name)
                    .FillBookmark("PartyA", PartyA.Name)
                    .FillBookmark("PartyB", PartyB.Name)
                    .FillBookmark("SenderEnd", Sender.Name)
                    .FillBookmark("RecipientEnd", Recipient.Name)

                    DoWordEvents(5, True)

                    If Leg1.IntCalcAmount Is Nothing Then
                        .DeleteBookmark("SameNomAmount")
                    Else
                        If Leg1.IntCalcAmount.ToString = Leg2.IntCalcAmount.ToString Then
                            .CompleteMacro("NotAmount2", Leg1.IntCalcAmount.ToString)
                        Else
                            NomDiffer = True
                            .DeleteBookmark("SameNomAmount")
                        End If
                    End If

                    .CompleteMacro("TradeDate2", Ordinal((Leg1.TradeDate)))

                    'Interest Commencement and End Dates.
                    For n = 1 To 2
                        'If n = 2 And Leg1.Perpetual Then
                        '    RowText = "The Swap Transaction is perpetual, and shall " & "only terminate on such date as Party A and Party B may agree."
                        'Else
                        If Leg1.IntCommencementDate = Leg2.IntCommencementDate Then
                            RowText = Ordinal((Leg1.IntCommencementDate))
                        Else
                            RowText = "For Party A, " & Ordinal((Leg1.IntCommencementDate)) & "; for Party B, " & Ordinal((Leg2.IntCommencementDate)) 'BUG Leg1 and Fndg2 aren't right.
                        End If
                        '  End If
                        .CompleteMacro("TEDate2" & n, RowText)
                    Next

                    DoWordEvents(6, True)

                    'Fixed and Floating details. The template contains Fixed and Floating _
                    'rate entries for both Party A and Party B, so I never need to add Rows, _
                    'only delete them.

                    If Leg1.IntOption.Value = Leg2.IntOption.Value Then SameBasis = True

                    'Just have one set of details in the template and duplicate it as necessary?
                    'Could be done, and might be simpler in some ways. Only thing is that the bookmarks don't _
                    'get copied if you do that, so would need to go through original bit, _
                    'duplicating them all. Doesn't seem worth the trouble.

                    For n = 0 To 1

                        ABLet = IIf(n = 0, "A", "B")
                        IntFunding = IIf(n = 0, PAIntTerms.Name, PBIntTerms.Name)

                        If IntFunding.IntOption.Value = Debt.RateBasisType.Fixed Then

                            'Remove floating rate stuff
                            .DeleteBookmark("Float" & ABLet)

                            If SameBasis Then .FillBookmark("SameBasisFixed" & ABLet, "(" & n + 1 & ")")

                            If NomDiffer Then
                                .CompleteMacro("FixedRPC2" & ABLet, IntFunding.IntCalcAmount.ToString)
                            Else
                                .DeleteBookmark("FixedRPC" & ABLet)
                            End If

                            DT = IntFunding.IntPayDates.ToString
                            '    CompleteMacro(.Bookmarks.Item("FixedPD2" & ABLet), DT & BDCText(Leg1.FixedAmount))

                            'If IntFunding.FixedAmount.strText(False) <> "" Then
                            '    CompleteMacro(.Bookmarks.Item("FixedRate" & ABLet), "Fixed Amount:")
                            '    CompleteMacro(.Bookmarks.Item("FixedRate2" & ABLet), IntFunding.FixedAmount.strText(False))
                            'Else
                            If Val(IntFunding.FixedRate) <> 0 Then
                                If IntFunding.DCF.HasValue Then .FillBookmark("FixedRate2" & ABLet, "; " & DC.DCFToString(IntFunding.DCF))
                            Else
                                If Not IntFunding.DCF.HasValue Then
                                    .FillBookmark("FixedRate2" & ABLet, IntFunding.FixedRate.Value & "%p.a.; ")
                                Else
                                    .CompleteMacro("FixedRate2" & ABLet, IntFunding.FixedRate.Value & "%p.a.; " & DC.DCFToString(IntFunding.DCF))
                                End If
                                'End If

                            End If

                        Else

                            'Floating

                            'Remove fixed rate stuff.
                            .DeleteBookmark("Fixed" & IIf(ABLet = "A", "B", "A"))

                            If SameBasis Then .FillBookmark("SameBasisFloating" & ABLet, "(" & n + 1 & ")")

                            If NomDiffer Then
                                .CompleteMacro("FloatRPC2" & ABLet, IntFunding.IntCalcAmount.ToString)
                            Else
                                .DeleteBookmark("FloatRPC" & ABLet)
                            End If

                            If IntFunding.MaxRate.HasValue Then
                                .CompleteMacro("CapRate2" & ABLet, IntFunding.MaxRate.Value & "% p.a.")
                            Else
                                .DeleteBookmark("CapRate" & ABLet)
                            End If

                            If IntFunding.MinRate.HasValue Then
                                .CompleteMacro("FloorRate2" & ABLet, IntFunding.MinRate.Value & "% p.a.")
                            Else
                                .DeleteBookmark("FloorRate" & ABLet)
                            End If

                            'Need to retain the "Insert" in the confirm if _
                            'no dates are set, just add bus day convention.
                            DT = IntFunding.IntPayDates.ToString
                            '     CompleteMacro(.Bookmarks.Item("FloatPD2" & ABLet), DT & BDCText(Leg1.FixedAmount))

                            'If IntFunding.InitRate <> "" Then
                            '    CompleteMacro(.Bookmarks.Item("InitRate2" & ABLet), IntFunding.InitRate & "% p.a.")
                            'Else
                            .DeleteBookmark("InitRate" & ABLet)
                            '   End If

                            'CompleteMacro(.Bookmarks.Item("FRO2" & ABLet), (IntFunding.RelPage))

                            ' CompleteMacro(.Bookmarks.Item("DesMat" & ABLet), (IntFunding.SpecDur))

                            If Val(CStr(IntFunding.Margin)) = 0 Then
                                MarginText = "None"
                            Else

                                s = CStr(IntFunding.Margin)
                                Select Case Mid$(s, 1, 1)

                                    Case "-"
                                        MarginText = "Minus " & Mid$(s, 2)
                                    Case "+"
                                        MarginText = "Plus " & Mid$(s, 2)
                                    Case Else
                                        MarginText = "Plus " & s
                                End Select
                                s = s & "% p.a."
                            End If

                            .CompleteMacro("Spread2" & ABLet, MarginText)

                            If IntFunding.MaxRate.HasValue Then
                                .CompleteMacro("MaxRate2" & ABLet, IntFunding.MaxRate.Value & "% p.a.")
                                MaxMin = True
                            Else
                                .DeleteBookmark("MaxRate" & ABLet)
                            End If

                            If IntFunding.MinRate.HasValue Then
                                .CompleteMacro("MinRate2" & ABLet, IntFunding.MinRate.Value & "% p.a.")
                                MaxMin = True
                            Else
                                .DeleteBookmark("MinRate" & ABLet)
                            End If

                            .CompleteMacro("DCF2" & ABLet, DC.DCFToString(IntFunding.DCF.Value))

                        End If

                        DoWordEvents(7, True)

                    Next

                    'Initial Exchange of Principal.
                    If Not Leg1.NominalAmount Is Nothing Then   '? NominalAmount
                        .CompleteMacro("IED2", Ordinal((PAIntTerms.IssueDate)) & BDCText(PAIntTerms.NominalAmount))
                        .CompleteMacro("IEA2A", PBIntTerms.NominalAmount.ToString)
                        .CompleteMacro("IEA2B", PAIntTerms.NominalAmount.ToString)
                    Else
                        .DeleteBookmark("InitExchange")
                    End If

                    'Interim(Exchange)
                    'If PAIntTerms.InterimPayments Then
                    '    'easier to add a page break here than delete a page break if no interim exchange.
                    '    .Tables.Item(2).Range.InsertBreak(Type:=Word.WdBreakType.wdPageBreak)
                    'Else
                    .DeleteBookmark("InterimExchange")
                    .Table2Delete()
                    '  End If

                    'Final Exchange of Principal.
                    If Not Leg1.RedemptionAmount Is Nothing Then
                        '  CompleteMacro(.Bookmarks.Item("FED2"), IIf(Leg1.Perpetual, "Such date as Party A and Party B may agree.", Ordinal(Leg1.MaturityDate) & BDCText(Leg1.RedemptionAmount)))
                        .CompleteMacro("FEA2A", PAIntTerms.RedemptionAmount.ToString)
                        .CompleteMacro("FEA2B", PBIntTerms.RedemptionAmount.ToString)
                    Else
                        .DeleteBookmark("FinalExchange")
                    End If

                    DoWordEvents(8, True)

                    'Business day centres. A bit tedious cos need an entry for each currency _
                    'and there could be as many as six.
                    P = Leg1.NominalAmount
                    If P.Curr.ToString <> "" Then
                        CurrCollection.Add(P.Curr)
                        .CompleteMacro("BDCur0", "Business Days for " & P.Curr.ToString & ":")
                    End If
                    '  CompleteMacro(.Bookmarks.Item("BDCur02"), (Leg1.BDCent))

                    q = Leg2.NominalAmount
                    If P.Curr IsNot q.Curr Then
                        If q.Curr IsNot Nothing Then
                            CurrCollection.Add(q.Curr)
                            .CompleteMacro("BDCur1", "Business Days for " & q.Curr.ToString & ":")
                        End If
                        '       CompleteMacro(.Bookmarks.Item("BDCur12"), (Leg2.BDCent))
                    Else
                        .DeleteBookmark("BD1")
                    End If

                    'User has no facility to enter Business _
                    'Day Centres for currencies other than those of periodic payments in the form _
                    'so will have to do it on the confirmation.

                    For w = 1 To 2
                        For v = 1 To 2
                            OldCurr = False

                            If v = 1 Then
                                q = (IIf(w = 1, Leg1.Name, Leg2.Name)).IntCalcAmount
                            Else
                                q = (IIf(w = 1, Leg1.Name, Leg2.Name)).Repayment
                            End If

                            For Each sc As Currency In CurrCollection
                                If sc.Acronym = q.Curr.Acronym Or q.Curr.Acronym = "" Then
                                    .DeleteBookmark("BD" & v)
                                    OldCurr = True
                                    Exit For
                                End If
                            Next sc
                            If Not OldCurr Then
                                CurrCollection.Add(q.Curr)
                                .CompleteMacro("BDCur" & v, "Business Days for " & q.Curr.Acronym & ":")
                            End If
                        Next
                    Next

                    'Always delete the general business day convention stuff, for now at least.
                    .DeleteBookmark("BusDayCon")

                    'Calculation agent.
                    .CompleteMacro("CalcAg2", (Leg1.CalculationAgent))

                    DoWordEvents(9, True)

                    'If no early termination options then delete all that stuff.
                    'BUG - putdates or calldates? Have guessed which Dates object is appropriate here.
                    If Leg1.PutDates.ToString = "" And Leg1.CallDates.ToString = "" Then
                        .DeleteBookmark("EarlyTerm")
                    Else
                        'No way do I want to allow completion of all details through a form, _
                        'but I do have some information so ought to insert that.
                        If Leg1.PutDates.ToString = "" Xor Leg1.CallDates.ToString = "" Then
                            'Dates just specified for one Party.
                            .CompleteMacro("EarlyTermDates2", Leg1.PutDates.ToString)
                            .CompleteMacro("OptionSeller2", "") 'get "Party A/B"
                            .CompleteMacro("OptionBuyer2", "") 'get the other Party.
                        Else 'option dates specified for each.
                            .CompleteMacro("EarlyTermDates2", IIf(Leg1.CallDates.ToString = Leg1.PutDates.ToString, Leg1.CallDates.ToString, "For Party A, " & Leg1.PutDates.ToString & " ; for Party B, " & Leg1.CallDates.ToString & "."))
                            .DeleteBookmark("SellerBuyer")
                        End If
                    End If

                    ''Paste any additional terms directly in.

                    If MaxMin Then AddTerms = "Where a Maximum or Minimum Rate is specified above, " & "references to 'Floating Rate +/- Spread' in Section 6.1(a) and Section 6.3(c) " & "to 'Floating Rate' in Section 6.3(f) of the ISDA 200 Definitions shall all be " & "subject to such Maximum or Minimum Rate."

                    If Leg1.AddTerms <> "" Then
                        If AddTerms <> "" Then AddTerms = AddTerms & vbCrLf
                        AddTerms = AddTerms & Leg1.AddTerms
                    End If

                    If AddTerms <> "" Then
                        .CompleteMacro("AddTerms2", AddTerms)
                    Else
                        .DeleteBookmark("AddTerms")
                    End If

                End If

                .Finalise()



            End With

            .WindowState = Word.WdWindowState.wdWindowStateMaximize

            DoWordEvents(10, True)

            .Visible = True
            .Activate()

            'Sets the default folder to the one holding the Transaction, if any.
            If FileName <> "" Then
                'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                '     Folder = LRootFunctions_definst.RStrip(FileName, Len(Dir(FileName)) + 1)
                'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                If Dir(Folder, FileAttribute.Directory) <> "" Then .ChangeFileOpenDirectory(Folder)
            End If

        End With

        'Successful completion.

        Capture = False

        'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        Me.Close()

        Exit Sub '**********

ErrorHandler:

        '462 will happen if user closes WordAppConfirms with Ctrl + Alt + Del, or _
        'by closing all docs. _
        'WordAppConfirms not set to Nothing then so need to force it.
        'It might be better to check for the type of Err.Exception rather than the number here if I could figure out the type.

        If Err.Number = 462 Then 'The remote server machine does not exist or is unavailable
            Resume Below 'clear the error so Resume Next will work.
Below:
            On Error Resume Next

            On Error GoTo ErrorHandler
            GoTo StartHere 'have cleared error so can't use Resume
        ElseIf Err.Number = 429 Then  'Unable to start ActiveX component.
            Resume Below4
Below4:
            Select Case MsgBox("Unable to start Microsoft Word.", MsgBoxStyle.RetryCancel + MsgBoxStyle.Exclamation, My.Application.Info.Title)

                Case MsgBoxResult.Cancel
                    'Reset things.
                    Resume Below2
Below2:
                    On Error Resume Next
                    Me.AcceptButton = cmdOK
                    lblProgress.Visible = False
                    prog1.Visible = False
                    Capture = False
                    UseWaitCursor = False
                    MakingConfirm = False

                Case MsgBoxResult.Retry
                    Resume

            End Select
        ElseIf Err.Number = conEscError Then
            Resume Below3
Below3:
            On Error Resume Next
            MsgBox("Operation cancelled.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, My.Application.Info.Title)
            Me.Close()
        Else
            Resume Next
        End If

    End Sub

    Private Function DocText(ByRef Amortising As Boolean, ByRef P As Cash, Optional ByRef PartyA As Boolean = False) As String

        'Determines amount for insertion into _
        'swap confirmation. BUG - not called by swap proc now.

        Dim PS As String
        Dim Column As String
        If Amortising Then
            If PartyA Then
                PS = "B" 'because it's the other party's initial exchange amount which determines interest.
                Column = "C"
            Else
                PS = "A"
                Column = "E"
            End If

            DocText = "For the initial Calculation Period, an amount equal to the Party " & PS & " Initial Exchange Amount, and thereafter, the " & "amount set out in Column " & Column & " of the attached Table 1, " & "against the Interim Exchange Date falling at the beginning of the Calculation Period."
        Else
            DocText = P.ToString
        End If
    End Function


    Private Sub frmConfirmation_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        If Leg1 Is Nothing Then
            optAB_0.Text = "from Seller to Buyer."
            optAB_1.Text = "from Buyer to Seller."
        End If

    End Sub

    Private Sub frmConfirmation_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        'If confirm is being generated then the user will get the "Operation cancelled." _
        'message, but I don't do anything to the Cancel argument so the form will still unload.

        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then cmdCancel_Click(cmdCancel, New System.EventArgs())

        eventArgs.Cancel = Cancel
    End Sub

    Private Sub DoWordEvents(ByRef ProgVal As Single, ByRef DocCreated As Boolean)

        On Error GoTo ErrorHandler

        With prog1
            .Value = Math.Max(Math.Max(Math.Min(ProgVal, .Maximum), .Minimum), .Value)
        End With

        System.Windows.Forms.Application.DoEvents()

        'MakingConfirm is set to True while making a confirm. This sub is _
        'only called while making a confirm. User pressing Esc or Cancel sets MakingConfirm to false, so _
        'I know to cancel the operation.
        'Big problems here if the user presses cancel before the doc has been created - WordAppConfirms _
        'is not destroyed in spite of what I say below. If the user then tries to create a confirmation, _
        'the template form's OLE1's object Is Nothing, and get the message to this effect. So am preventing _
        'cancellation in the early stages.
        If Not MakingConfirm And DocCreated Then
            If Not DocCreated Or (DocCreated And WordAppConfirms.Documents.Count = 1) Then
                WordAppConfirms.Quit(Word.WdSaveOptions.wdDoNotSaveChanges)
            End If
            Err.Raise(conEscError) 'first available free number.
        End If

        Exit Sub '******

ErrorHandler:

        If Err.Number = conEscError Then 'forced error cos user pressed Esc
            Err.Raise(conEscError) 'go back to calling routine.
            Exit Sub '*******
        Else
            Resume Next
        End If

    End Sub

    Private Function BDCText(ByRef P As Cash) As String

        'Dim b As Debt.BDCConverter
        'BDCText = IIf(P.BDC = LOM.BDC.BDCNone, ", without adjustment.", ", subject to adjustment in accordance with the " & LOM.BDCText((P.BDC)) & " Business Day Convention.")

        Exit Function '******


    End Function


    Private Sub frmConfirmation_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed


        'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default


    End Sub

    Private Function Ordinal(ByVal d As Date) As String
        Return d.ToShortDateString  'i think idea here was to return "6th June 2007" or so , have to check 
    End Function
End Class