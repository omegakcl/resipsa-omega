Imports System.Math
Imports Microsoft.Office.Interop
Imports Microsoft.Office

<Serializable()> Friend MustInherit Class SecurityArc

    'The drawn representation of the SecurityBase object.

    Inherits BaseLine

    Friend ToConnection As Line

    Friend Overrides Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument

        Return New SecurityBase()

    End Function

    Friend Overrides Sub OtherBaseLineDeleted(ByVal BL As BaseLine)
        If BL Is Me.ToConnection Then Me.Delete()
    End Sub

    Friend Overrides Sub SetTarget(ByVal sender As Control, ByVal e As PartyCanvas.CursorEventArgs)

        If TypeOf sender Is PartyCanvas Or TypeOf sender Is EntityControl Then    'should be but just to avoid any error.
            TargetSet = False
            ToConnection = Nothing
            If TypeOf e.OverBL Is Line Then
                Dim h As Line = e.OverBL
                TargetSet = ValidObligation(h)
                If TargetSet Then ToConnection = h

            End If

            MyBase.SetTarget(sender, e)
        End If

    End Sub

    Friend Overridable Function ValidObligation(ByVal ProposedSupportedLine As Line) As Boolean

        If Existing And Not ProposedSupportedLine.Existing Then Return False

        If ProposedSupportedLine.Inst Is Nothing Then
            Return True
        ElseIf Not ProposedSupportedLine.Inst.Supportable Then
            Return False
        End If

        Return True

    End Function

    Protected Overrides Function drawingcursor() As Cursor
        Return frmMDI.crsSec
    End Function

    Friend Overrides Sub MakeReal()
        MyBase.MakeReal()
        If Not Parent.SecurityCollection.Contains(Me) Then Parent.SecurityCollection.Add(Me) 'will contain Me if deserializing.
    End Sub

    Friend MustOverride Function GetPoints(ByVal Points As List(Of Point), ByRef ArrowHeadPoint As Point, ByRef VertTextAdj As Integer) As Boolean

    Friend Overrides Sub Draw(ByVal gpcs As System.Drawing.Graphics, ByVal MousePos As Nullable(Of Point), Optional ByVal ppp As PowerPoint.Slide = Nothing)

        MyBase.Draw(gpcs, MousePos)

        'Inevitably rather complicated to draw a curved line avoiding controls, but I'm up to it!

        'Approach is to determine the positions of various points.
        Dim Points As New List(Of Point)

        Dim ArrowHeadSource As Point

        'End point is also easy.
        Dim ArrowHeadPoint As Point
        If ToConnection Is Nothing Then
            ArrowHeadPoint = MousePos   'must be drawing collateral.
        Else
            'CSng is important to make sure the right LinePoint overload is called.
            ArrowHeadPoint = ToConnection.LinePoint(CSng(IIf(UserDrawn, ToConnection.DrawToPos, Me.DrawToPos)))
        End If

        Dim VertTextAdj As Integer
        If GetPoints(Points, ArrowHeadPoint, VertTextAdj) Then

            'Add the end point.
            Points.Add(ArrowHeadPoint)

            Dim ppgroup As New List(Of String)  'list of names of shapes added to ppp, so I can group them, among other things.

            'Draw the points, cutting them short if they end at or begin at a rightangle.

            For n As Integer = 1 To Points.Count - 1
                Dim StartStraight As Point = Points(n - 1)
                Dim EndStraight As Point = Points(n)

                'if not the end
                If n <> Points.Count - 1 Then EndStraight = Point.Subtract(EndStraight, New Size(Radius() * Sign(EndStraight.X - StartStraight.X), Radius() * Math.Sign(EndStraight.Y - StartStraight.Y)))
                'if not the beginning
                If n <> 1 Then StartStraight = Point.Add(StartStraight, New Size(Radius() * Sign(EndStraight.X - StartStraight.X), Radius() * Math.Sign(EndStraight.Y - StartStraight.Y)))

                If Hypot(Points(n - 1), EndStraight) > Radius() Or Hypot(Points(n), StartStraight) > Radius() Or Points.Count = 2 Then  'if just two points it will just be a straight line and I want to draw no matter how short.
                    Path.AddLine(StartStraight, EndStraight)
                    If Not ppp Is Nothing Then ppgroup.Add(ppp.Shapes.AddLine(StartStraight.X, StartStraight.Y, EndStraight.X, EndStraight.Y).Name)
                    ArrowHeadSource = StartStraight
                End If

                'Draw a 90 degree arc between the lines.
                If n <> Points.Count - 1 Then

                    Dim ATL As Point  'Arc top left
                    Dim DL As Integer = Min(Radius, Max(0, Max(Points(n).X - Points(n + 1).X, Points(n).X - Points(n - 1).X)))
                    Dim DT As Integer = Min(Radius, Max(0, Max(Points(n).Y - Points(n + 1).Y, Points(n).Y - Points(n - 1).Y)))
                    Dim DW As Integer = Min(Radius, Max(Abs(Points(n).X - Points(n + 1).X), Abs(Points(n).X - Points(n - 1).X)))
                    Dim DH As Integer = Min(Radius, Max(Abs(Points(n).Y - Points(n + 1).Y), Abs(Points(n).Y - Points(n - 1).Y)))

                    ATL = Point.Subtract(Points(n), New Size(2 * DL, 2 * DT))

                    Dim StartAngle As Integer   'Start angle for drawing arc.
                    Dim Clockwise As Boolean    'Whether the arc is to be drawn clockwise. Must draw from the right end or
                    'path will jump to the end and double back to the start, which will show.

                    'Find a point to use as the source of the arrowhead. Idea is that this should be on the 
                    'line from Points(n-1) to Points(n), one third of DH/DW from Points(n). Works OK.
                    'Angles begin from positive x-axis, going round clockwise.

                    If Points(n).X = Points(n - 1).X Then
                        ArrowHeadSource = New Point(Points(n).X, Points(n).Y - Math.Sign(Points(n).Y - Points(n - 1).Y) * DH / 3)
                        If Points(n + 1).X > Points(n).X Then
                            StartAngle = 180
                            Clockwise = Points(n + 1).Y < Points(n - 1).Y

                        Else
                            StartAngle = 0
                            Clockwise = Points(n + 1).Y > Points(n - 1).Y
                        End If
                    Else
                        ArrowHeadSource = New Point(Points(n).X - Math.Sign(Points(n).X - Points(n - 1).X) * DW / 3, Points(n).Y)
                        If Points(n + 1).Y > Points(n).Y Then
                            StartAngle = 270
                            Clockwise = Points(n + 1).X > Points(n - 1).X

                        Else
                            StartAngle = 90
                            Clockwise = Points(n + 1).X < Points(n - 1).X
                        End If
                    End If

                    Dim ppStartAngle As Integer = StartAngle

                    'Must go through the following even for powerpoint or there will be the wrong number of points in the 
                    'path and the text will be placed in the wrong position.
                    For j As Integer = 1 To 2
                        'If width/height is zero you'll get an error. OK if 1 when 0 calculated.
                        Path.AddArc(ATL.X, ATL.Y, 2 * Math.Max(DW, 1), 2 * Math.Max(DH, 1), StartAngle, IIf(Clockwise, 45, -45))
                        If j = 1 Then RedPoints.Add(Path.PathPoints(Path.PathPoints.Length - 1)) 'simplest way to get hold of the point at the centre of the arc.
                        StartAngle = StartAngle + IIf(Clockwise, 45, -45)
                    Next

                    If Not ppp Is Nothing Then

                        With ppp.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeArc, 0, 0, Math.Max(DW, 1), Math.Max(DH, 1))
                            Dim adj1 As Long
                            Dim adj2 As Long

                            Select Case IIf(Clockwise, ppStartAngle, ppStartAngle - 90)
                                'Can't really understand the logic behind the appropriate adjustments - these were figured out largely by trial and error.
                                Case 0
                                    adj1 = 0
                                    adj2 = -90
                                Case 90
                                    adj1 = -90
                                    adj2 = 180
                                Case 180
                                    adj1 = 180
                                    adj2 = 90
                                Case -90, 270
                                    adj1 = 90
                                    adj2 = 0

                            End Select

                            .Adjustments(1) = adj1
                            .Adjustments(2) = adj2

                            'Problem is that adjustments move it, so can only position at the end.
                            .Left = ATL.X + DL
                            .Top = ATL.Y + DT
                            ppgroup.Add(.Name)
                        End With

                    End If

                End If
            Next

            If ppp Is Nothing Then

                RedPoints.Add(Path.PathPoints(0))
                RedPoints.Add(Path.PathPoints(Path.PathPoints.Length - 1))
                gpcs.DrawPath(LinePen, Path)
            Else
                MyBase.SetArrowStyle(ppp.Shapes.Item(ppgroup.Item(ppgroup.Count - 1)).Line)
            End If

            Dim PPpTextName As String = ""  'Powerpoint textbox name - for grouping
            TextRect = Nothing
            If Inst IsNot Nothing Then

                Dim Dirn As Bearing

                Select Case Points.Count

                    Case 2  'Straight line.

                        If Math.Abs(Points(0).X - Points(1).X) > Math.Abs(Points(0).Y - Points(1).Y) Then
                            Dirn = Bearing.North    'maybe I should differentiate and have south too in some circs, but not that easy to do.
                        Else
                            Dirn = Bearing.East
                        End If
                        MyBase.DrawTextByLine(gpcs, ppp, Dirn, Points(0), Points(1), False, PPpTextName)

                    Case 3  'Right Angle

                        Const RectDim = 200

                        Dim R As Rectangle
                        Dim ss As New System.Drawing.StringFormat

                        'Find a square that extends away from the corner. Think of as there being four possibilities 
                        'as to which corner of the square the corner of the Path lies on.
                        If Points(0).X < Points(1).X Or Points(2).X < Points(1).X Then

                            If Points(0).Y > Points(1).Y Or Points(2).Y > Points(1).Y Then
                                'Path corner on bottom left of square corner
                                R = New Rectangle(MidPoint.X, MidPoint.Y - RectDim, RectDim, RectDim)
                                ss.LineAlignment = StringAlignment.Far
                            Else
                                'Top left
                                R = New Rectangle(MidPoint.X, MidPoint.Y, RectDim, RectDim)
                                ss.LineAlignment = StringAlignment.Near
                            End If
                            ss.Alignment = StringAlignment.Near

                        Else
                            If Points(0).Y > Points(1).Y Or Points(2).Y > Points(1).Y Then
                                'Bottom right 
                                R = New Rectangle(MidPoint.X - RectDim, MidPoint.Y - RectDim, RectDim, RectDim)
                                ss.LineAlignment = StringAlignment.Far
                            Else
                                'Top Right
                                R = New Rectangle(MidPoint.X - RectDim, MidPoint.Y, RectDim, RectDim)
                                ss.LineAlignment = StringAlignment.Near
                            End If
                            ss.Alignment = StringAlignment.Far
                        End If
                        Dim g As Drawing2D.GraphicsPath = Path

                        DrawText(gpcs, ppp, R, ss, Nothing, Nothing, , , PPpTextName)

                    Case 4  'Two Right angles

                        If Points(1).X = Points(2).X Then
                            If Points(0).X < Points(1).X Then
                                Dirn = Bearing.East
                            Else
                                Dirn = Bearing.West
                            End If
                        Else
                            If Points(0).Y < Points(1).Y Then
                                Dirn = Bearing.South
                            Else
                                Dirn = Bearing.North
                            End If
                        End If

                        'Prevent overlap if there are two arcs going round the left or right of a control.
                        Dim hj As SizeF
                        If ppp IsNot Nothing Then
                            hj = New SizeF(100, 20)
                        Else
                            hj = gpcs.MeasureString(Inst.ToString, New Font("Arial", 10), 2000)
                        End If

                        VertTextAdj = VertTextAdj * (hj.Height + 2)
                        Dim P1 As New Point(Points(1).X, Points(1).Y + VertTextAdj)
                        Dim P2 As New Point(Points(2).X, Points(2).Y + VertTextAdj)

                        MyBase.DrawTextByLine(gpcs, ppp, Dirn, P1, P2, False, PPpTextName)

                End Select
            End If

            If gpcs IsNot Nothing Then
                DrawArrowHead(gpcs, LinePen.Color, ArrowHeadSource, Path.GetLastPoint, Nothing, False)
            ElseIf ppp IsNot Nothing Then
                If PPpTextName <> "" Then ppgroup.Add(PPpTextName)
                If ppgroup.Count > 0 Then
                    GroupPP(ppp, ppgroup).Line.ForeColor.RGB = RGB(0, 0, 255)
                End If
            End If

            DrawingDone()

        End If

    End Sub

    Friend Overrides Function LinePen() As System.Drawing.Pen
        If Not Me.ToConnection Is Nothing Then
            If Not Me.ValidObligation(Me.ToConnection) Then Return New Pen(conBadSupportColour)
        End If
        Dim p As New Pen(Color.Blue)
        SetLineWidthAndStyle(p)

        Return p
    End Function

    Friend Overrides Function TextColour() As System.Drawing.Color
        If Not Inst.TypeIsEnabled(Inst.GetType) Then Return BaseLine.conBadSupportColour
        Return Color.Blue
    End Function

    Friend Overrides Sub Delete()
        MyBase.Delete()
        Parent.SecurityCollection.Remove(Me)
        Me.ToConnection = Nothing
        Me.Parent = Nothing
    End Sub

    Friend Function UserDrawn() As Boolean
        Return Not Parent.SecurityCollection.Contains(Me)
    End Function

    Sub New(ByVal c As PartyCanvas, ByVal bExisting As Boolean, ByVal bUserDraw As Boolean)
        MyBase.new(c, bUserDraw)
        mExisting = bExisting
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Friend Function Radius() As Single
        Return 20 / MyBase.Parent.ScaleFactor
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
