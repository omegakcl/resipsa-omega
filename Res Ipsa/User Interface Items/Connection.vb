Imports Microsoft.Office.Interop
Imports Microsoft.Office

<Serializable(), DisplayName("Underlying Instruments")> Public MustInherit Class Connection 'name is set for collection displayer, might call it "EntityToEntityCon" for my purposes. Display name seems to have no effect.
    'Basic connectin between two Entities.
    Inherits Line

    Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal FEC As EntityControl)

        MyBase.New(c, bUserDraw, FEC)
        tempFromEC = FEC.LEntity

    End Sub

    Friend Overridable Sub SetPrevious()

    End Sub

    Friend Underlyings As UnderlyingsCol  'underlying assets - to do with repayments and rights transferred (e.g. by sale). 
    'Am calling it this to distinguish from next/prev principal payment.
    'Need to consider whether this should be a collection or an array. With an array you get 
    'the possibility to expand a node to see them all in the property grid.
    'Also if there's just one item it would be nice to simply show that. Neither a collection nor an array show things any simpler in this case.

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Public Overrides Function ToString() As String

        'Necessary for displaying in propertygrid.
        'Remember the Inst.ToString appears on picCanvas, this just appears on the property grid.
        Dim s As String = ""
        If Not Inst Is Nothing Then s = Inst.ToString
        If s = "" Then     'For the basic types.
            Return FromEntity.ToString & " - " & ToEntity.ToString  'need to keep this short for the propertygrid.
        Else
            Return Inst.ToString
        End If

    End Function

    <CLSCompliant(False)> Protected Overrides Sub DoPPLine(ByVal ppp As PowerPoint.Slide)

        With ppp.Shapes

            Dim pc As PowerPoint.Shape
            If OffsetInteger() = 0 Then

                If TypeOf Me.Inst Is PassThrough Then
                    pc = .AddLine(Path.PathPoints(0).X, Path.PathPoints(0).Y, Path.PathPoints(1).X, Path.PathPoints(1).Y)
                    'Don't use a connector cos will be setting the line style, which is unavailable for connectors.
                Else
                    'Only if it's not a tranche and not a pass-through can I connect it.'Co-ordinates are usually irrelevant cos will be connected (but not always!)
                    pc = .AddConnector(Core.MsoConnectorType.msoConnectorStraight, Path.PathPoints(0).X, Path.PathPoints(0).Y, Path.PathPoints(1).X, Path.PathPoints(1).Y)
                    With pc.ConnectorFormat
                        .BeginConnect(CreatePPConnection(ppp, pc), 1)
                        .EndConnect(CreatePPConnection(ppp, pc, ToEntity.Parent), 1)
                    End With
                End If

            Else

                With .BuildFreeform(Core.MsoEditingType.msoEditingAuto, Path.PathPoints(0).X, Path.PathPoints(0).Y)
                    For i As Integer = 1 To Path.PointCount - 1
                        .AddNodes(Core.MsoSegmentType.msoSegmentLine, Core.MsoEditingType.msoEditingAuto, Path.PathPoints(i).X, Path.PathPoints(i).Y)
                    Next
                    pc = .ConvertToShape
                End With

            End If

            SetArrowStyle(pc.Line, OffsetInteger() <> 0)

        End With

    End Sub

    Friend Overrides Sub OtherBaseLineDeleted(ByVal BL As BaseLine)
        ' If BL Is Me.Underlyings Then Me.Delete()
    End Sub

    Friend Overrides Sub SetLineWidthAndStyle(Optional ByRef pn As System.Drawing.Pen = Nothing, Optional ByVal pc As PowerPoint.LineFormat = Nothing)
        'Show pass-throughs with a double line.
        If TypeOf Me.Inst Is PassThrough Then
            If pn IsNot Nothing Then
                pn.Width = 3
                pn.CompoundArray = New Single() {0, 1 / 3, 2 / 3, 1}
            End If
            If pc IsNot Nothing Then
                pc.Weight = 3.0#
                pc.Style = Core.MsoLineStyle.msoLineThinThin     'property is unavailable for connectors, that's why I don't use one here.
            End If
        End If
        MyBase.SetLineWidthAndStyle(pn, pc)
    End Sub

    Friend Overrides Sub ClearUpAfterNonCompletedDrawing()
        'Want to set these while drawing, so need to unset them if drawing not completed. Need to do this before draw buttons are set in MDI Form.
        'If Not Underlyings Is Nothing Then
        '    Underlyings = Nothing   'Will need to reset the PassThroughs reference of the Underlyings too.
        'End If
        'If Not PassThroughs Is Nothing Then
        '    PassThroughs = Nothing
        'End If

    End Sub

    Friend ParentCol As TrancheCollection

    Private tempFromEC As Entity    'a temporary store of the From Entity until this is added to its parent collection.
    Friend Overrides ReadOnly Property FromEntity() As Entity
        Get
            If ParentCol Is Nothing Then
                Return tempFromEC
            Else
                Return ParentCol.FromEntity
            End If
        End Get

    End Property

    Friend Overrides Sub MakeReal()

        MyBase.MakeReal()
        With Me.Parent.StanConCols
            If Not .Contains(Me.ParentCol) Then
                .Add(Me.ParentCol)
                Me.ParentCol.Sort()
            End If
        End With

    End Sub

    Protected Overridable Function TrancheBranchPoint() As Single
        Return 1
    End Function

    Protected Overrides Sub SetTrancheOffset(ByVal gpcs As Graphics)

        'Divert the arrow outwards to reflect that it's a tranche. Best to do perpendicularly so that text can fit better.
        Dim n As Integer
        If OffsetInteger() > 0 Then

            Dim t As Integer = TrancheWidth(gpcs)

            Dim NewPath As New System.Drawing.Drawing2D.GraphicsPath    'can't just use the existing Path cos LinePoint will refer to that.

            Dim L As Integer = Hypot(Point.Truncate(MainStartPoint), Point.Truncate(MainEndPoint))

            Dim c As Size = New Size((MainEndPoint.Y - MainStartPoint.Y) * t / L, -(MainEndPoint.X - MainStartPoint.X) * t / L)

            Dim pBranchOut As PointF = Me.LinePoint(CSng((LinePointType.TrancheBase / LinePointType.FullLength * TrancheBranchPoint())))  'point where the tranche offset branches out.

            For n = 0 To OffsetInteger() - 2
                pBranchOut = pBranchOut + c
            Next

            Dim pFirstCorner As PointF = pBranchOut + c     'the corner where the tranche offset turns to be parallel to the main line.
            If FirstInCollection() Then NewPath.AddLine(Path.PathPoints(0), pBranchOut)
            NewPath.AddLine(pBranchOut, pFirstCorner)
            RedPoints.Add(pFirstCorner)   'I think just p1 and p3 will do as redpoints. Otherwise it looks like the main line has been selected too.

            Dim pBranchBack As PointF = Me.LinePoint(CSng(1 - (LinePointType.TrancheBase / LinePointType.FullLength * TrancheBranchPoint())))     'the point where the tranche offset reconnects.

            For n = 0 To OffsetInteger() - 2
                pBranchBack = pBranchBack + c
            Next

            Dim pSecondCorner As PointF = pBranchBack + c       'the point where the tranche offset turns back towards the main line.
            AddLineToPath(NewPath, pSecondCorner)
            RedPoints.Add(pSecondCorner)

            AddLineToPath(NewPath, pBranchBack)
            If FirstInCollection() Then AddLineToPath(NewPath, Path.PathPoints(Path.PathPoints.Length - 1))

            'Reset these now so that credit support and collateral and text can be positioned on the basis of the limb.
            Path = NewPath
            MainStartPoint = pFirstCorner
            MainEndPoint = pSecondCorner
        End If

    End Sub

    Friend Overrides Function OffsetInteger() As Integer
        If ParentCol Is Nothing Then
            Return 0
        ElseIf ParentCol.Contains(Me) Then 'can't determine this by whether MousePos is Nothing in calling (Draw) sub because this 
            'is set to Nothing when the Target is Set.
            Return ParentCol.IndexOf(Me)
        ElseIf Me.TargetSet Then
            Return ParentCol.Count  'not yet created, need to show as an additional one.
        End If
    End Function

    Protected Friend Overrides Function FirstInCollection() As Boolean
        'Not the same as OffsetInteger = 0 because I may need an offset to cater for ExistingCons between the same Entities.
        If ParentCol IsNot Nothing Then
            If ParentCol.Count > 0 Then
                If ParentCol.IndexOf(Me) > 0 Then Return False
            End If
        End If
        Return True
    End Function

    Friend Overrides Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument

        'I like calling this from BaseLine.New because all Baselines need an Instrument. But, since the instrument type depends on whether there 
        'are any underlyings, and since the call to the base type constructor has to be the first line in the inheriting class's constructor, 
        'I have to set the underlying instruments here.

        If Not FEC Is Nothing And (frmMDI.tsiDrawPassThroughExisting.Checked Or frmMDI.tsiDrawPassthroughNew.Checked) Then
            'set the underlying instruments.
            Underlyings = Me.Parent.SelectedBLs.GetEToEConsReEntity(FEC.LEntity, True, frmMDI.tsiDrawPassthroughNew.Checked)
        End If

        If Underlyings Is Nothing Then
            Return New FundingBase
        Else
            If Underlyings.Count = 0 Then
                Return New FundingBase
            Else
                Return New PassThrough
            End If
        End If

    End Function

    Friend Overrides Function GetBasicType() As Type

        'This sub is similar to GetNewInst but is for determining the root type for menus. 
        If TypeOf Inst Is PassThrough Then
            Return GetType(PassThrough)
        Else
            Return GetType(FundingBase)
        End If

    End Function

    Protected Overrides Function TextStartPoint() As System.Drawing.PointF

        'Purpose of the following is to reduce the area available for text to be drawn in when there are tranches so that there's no overlap.
        If Orientation = Bearing.North Or Orientation = Bearing.South Then

            If ParentCol.Count > 1 Then
                Dim ra As Single = ParentCol.IndexOf(Me) / ParentCol.Count
                ra = 0
                If ParentCol.IndexOf(Me) = 0 Then ra = (LinePointType.TrancheBase + ra * (LinePointType.TrancheEnd - LinePointType.TrancheBase)) / LinePointType.FullLength
                Return LinePoint(ra)
            End If
        Else
            If ParentCol.Count > 1 And ParentCol.IndexOf(Me) = 0 Then Return LinePoint(LinePointType.TrancheBase)
        End If
        Return MyBase.TextStartPoint

    End Function

    Protected Overrides Function TextEndPoint() As System.Drawing.PointF

        If Orientation = Bearing.North Or Orientation = Bearing.South Then

            If ParentCol.Count > 1 Then
                Dim rb As Single = (ParentCol.IndexOf(Me) + 1) / ParentCol.Count
                rb = 1
                If ParentCol.IndexOf(Me) = 0 Then rb = (LinePointType.TrancheBase + rb * (LinePointType.TrancheEnd - LinePointType.TrancheBase)) / LinePointType.FullLength
                Return LinePoint(rb)

            End If
        Else
            If ParentCol.Count > 1 And ParentCol.IndexOf(Me) = 0 Then Return LinePoint(LinePointType.TrancheEnd)

        End If
        Return MyBase.TextEndPoint

    End Function

    Protected Overrides Sub EntityDeleted(ByVal ent As EntityControl)
        MyBase.EntityDeleted(ent)
        If AreTheSame(ent, Me.ToEntity) Then Me.Delete()
    End Sub

    Friend Overrides ReadOnly Property ToEntity() As Entity
        Get
            If Not ParentCol Is Nothing Then Return ParentCol.ToEntity
        End Get
    End Property

    Protected Overrides Sub SetDrawToCoords(ByRef R As Rectangle, ByRef MinW As Long)
        If Me.ToEntity.Parent Is Nothing Then Exit Sub
        R = Me.ToEntity.Parent.ActiveBounds

        MinW = Math.Min(MinW, R.Width)

    End Sub
    Friend Overrides Function BeneficiaryIfSecured() As Entity
        Return ToEntity
    End Function
    Friend Overrides Function CanPassThroughExisting() As Boolean
        Return True
    End Function
    Friend Overrides Sub Delete()
        MyBase.Delete()
        Me.ParentCol.Remove(Me)

    End Sub

    Friend Overrides Sub SetTarget(ByVal sender As Control, ByVal e As PartyCanvas.CursorEventArgs)

        Dim T As EntityControl = Nothing
        If Not ToEntity Is Nothing Then T = ToEntity.Parent

        If Not AreTheSame(e.OverControl, T) Then

            Dim bOK As Boolean

            If Not AreTheSame(e.OverControl, FromEntity.Parent) And e.OverControl IsNot Nothing Then

                bOK = SetToEntityRelatedProperties(e.OverControl)

            End If

            If bOK Then
                PutIntoParentCol(e.OverControl.LEntity) 'overriden by TransactionCon to do the same for the Consideration.
            Else
                UnSetTarget()
            End If

        End If

        If Not TypeOf sender Is PictureBox Then MyBase.SetTarget(sender, e)

    End Sub

    'Set the Connection details based on the entity it's drawn to, and return whether it's valid.
    Protected MustOverride Function SetToEntityRelatedProperties(ByVal OverControl As EntityControl) As Boolean

    Friend Function GetStanConCol(ByVal FE As Entity, ByVal TE As Entity) As TrancheCollection

        Dim c As TrancheCollection = Parent.GetStanConColl(FE, TE, Me.GetType)
        If c Is Nothing Then c = New TrancheCollection(FE, TE, Me.Parent.StanConCols)
        Return c

    End Function

    Friend Overridable Sub PutIntoParentCol(ByVal TE As Entity)

        GetStanConCol(Me.FromEntity, TE).Insert(0, Me)
        TargetSet = True

    End Sub

    Friend Overridable Sub UnSetTarget()

        'Removal of connection from parentcol when user draws away from target entity.
        If Not Me.ParentCol Is Nothing Then Me.ParentCol.Remove(Me)

        Me.ParentCol = Nothing

        TargetSet = False

    End Sub

    Friend Overrides Function TypeIsVisible(ByVal t As System.Type) As Boolean
        'Cancellation should only be shown if at least one instrument is being passed back to its obligor, otherwise it makes no sense.
        If t Is GetType(Cancellation) Then
            If Underlyings IsNot Nothing Then
                For Each j As UnderlyingBaseLine In Underlyings
                    If AreTheSame(j.Underlying.FromEntity, ToEntity) Then Return True
                Next
                Return False
            End If
        End If
        Return MyBase.TypeIsVisible(t)
    End Function

#Region "Post Completion Diagram"
    <NonSerialized()> Private tempNewToEntity As Entity

    Friend Sub CompletionInitialisation() Handles Parent.PostCompletionInitialisation
        Me.tempNewToEntity = Me.ParentCol.ToEntity
    End Sub

    Friend Sub SetPostCompletionData() Handles Parent.Complete

        'order that connections are dealt with doesn't matter. What does matter is that test for transferring a connection is the same as for deleting it in partycanvas.ConvertToPostCompletion
        'If Not PassThroughs Is Nothing Then
        '    'If Not Passthroughs.Inst.ExistsPostCompletion Then
        '    '    Me.tempNewToEntity = Passthroughs.tempNewToEntity
        '    'End If
        'End If

    End Sub

    Friend Sub ClearUpAfterCompletion() Handles Parent.PostCompletionClearUp

        If Not tempNewToEntity Is Me.ToEntity Then
            Me.ParentCol.Remove(Me)
            Me.PutIntoParentCol(tempNewToEntity)
            Me.MakeReal()     'puts parent collection into next level collection up
        End If

        tempNewToEntity = Nothing

    End Sub

#End Region


End Class

