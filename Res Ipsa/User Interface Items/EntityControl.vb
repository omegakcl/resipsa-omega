Imports System.Reflection
Imports Microsoft.Office
Imports Microsoft.Office.Interop

<Serializable> Public Class EntityControl

    'The control which represents an entity in picCanvas. One idea would be to use a form for this
    'instead of a usercontrol, and have frmBase an MDI form as well as frmMDI. Might then even be able to make the 
    'entity representations party transparent, and get a bit 3D. Need to get hold of the MDIClient object if I want to do this.
    'Getting hold of the MDIClient can be done through the form's Controls collection. Unfortunately MDI children are 
    'always shown as fully opaque, whatever you set the Opacity to, so this idea won't work.

    'Should I allow the user to have dual representations of entities? Easy enough to keep them in sync. Harder bit would be to allow user to transfer
    'connections over once they had been created. Need a further menu I suppose, don't really want to do it by dragging. But I just don't think it's necessary.
    'The representation that I now have of multiple Connections between two Entities is good enough.

    Inherits TableLayoutPanel   'seems to take a bit longer to load the control now that I do this instead of having a usercontrol with 
    'a docked tablelayoutpanel, but the code is so much simpler, this will have to do. Basically it saves me having to worry about which 
    'control is going to raise events.

    Private mSelected As Boolean
    Dim ControlDownPoint As Nullable(Of Point)   'Point in Control where the mouse goes down, for dragging.

    Event Reformat()

    Shared WithEvents tltAutoText As New ToolTip    'I can only ever have one showing, so why not make it shared?

    'Auto-generated code.
    Private components As System.ComponentModel.IContainer
    Friend WithEvents rtbName As System.Windows.Forms.RichTextBox
    Friend WithEvents txtBranch As System.Windows.Forms.TextBox
    Friend WithEvents jcbJur As JCombo
    Friend WithEvents cmsEntityControl As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniEntityDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspOne As System.Windows.Forms.ToolStripSeparator

    'Following reflect the number of units out to draw an arc from the control.
    Friend ArcsLeft As Integer
    Friend ArcsRight As Integer
    Friend ArcsTop As Integer
    Friend ArcsBottom As Integer

    Friend WithEvents cmsTextBox As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniUndo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspTwo As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mniCut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniPaste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniShowBranch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tscBranchJur As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents zspFour As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mniBranchJur As System.Windows.Forms.ToolStripMenuItem  'margin to avoid controls by.

    Private conBranchBrackets = "{Branch}"
    Friend WithEvents mniEntityAlign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsAlign As System.Windows.Forms.ContextMenuStrip

    Private txtSample As TextBox     'Can't use DrawToBitmap on richtextbox so have an ordinary textbox for frmOptions.

    Event DrawingFromControl(ByVal sender As EntityControl)

    Friend bAligned As Boolean
    Friend WithEvents mniEntityStop As System.Windows.Forms.ToolStripMenuItem

    Friend AlignSpotsOccupied() As Boolean

    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.rtbName = New System.Windows.Forms.RichTextBox
        Me.cmsTextBox = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mniBranchJur = New System.Windows.Forms.ToolStripMenuItem
        Me.tscBranchJur = New System.Windows.Forms.ToolStripComboBox
        Me.zspFour = New System.Windows.Forms.ToolStripSeparator
        Me.mniUndo = New System.Windows.Forms.ToolStripMenuItem
        Me.zspTwo = New System.Windows.Forms.ToolStripSeparator
        Me.mniCut = New System.Windows.Forms.ToolStripMenuItem
        Me.mniCopy = New System.Windows.Forms.ToolStripMenuItem
        Me.mniPaste = New System.Windows.Forms.ToolStripMenuItem
        Me.mniDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.txtBranch = New System.Windows.Forms.TextBox
        Me.cmsEntityControl = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mniEntityDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mniEntityAlign = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsAlign = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.zspOne = New System.Windows.Forms.ToolStripSeparator
        Me.mniShowBranch = New System.Windows.Forms.ToolStripMenuItem
        Me.mniEntityStop = New System.Windows.Forms.ToolStripMenuItem
        Me.jcbJur = New LegalObjectsModel.JCombo
        Me.cmsTextBox.SuspendLayout()
        Me.cmsEntityControl.SuspendLayout()
        Me.SuspendLayout()
        '
        'rtbName
        '
        Me.rtbName.AcceptsTab = True
        Me.SetColumnSpan(Me.rtbName, 2)
        Me.rtbName.ContextMenuStrip = Me.cmsTextBox
        Me.rtbName.Dock = System.Windows.Forms.DockStyle.Top
        Me.rtbName.Location = New System.Drawing.Point(3, 3)
        Me.rtbName.Multiline = False
        Me.rtbName.Name = "rtbName"
        Me.rtbName.ShortcutsEnabled = False
        Me.rtbName.Size = New System.Drawing.Size(194, 19)
        Me.rtbName.TabIndex = 1
        Me.rtbName.Text = ""
        '
        'cmsTextBox
        '
        Me.cmsTextBox.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mniBranchJur, Me.tscBranchJur, Me.zspFour, Me.mniUndo, Me.zspTwo, Me.mniCut, Me.mniCopy, Me.mniPaste, Me.mniDelete})
        Me.cmsTextBox.Name = "cmsRichTextBox"
        Me.cmsTextBox.ShowImageMargin = False
        Me.cmsTextBox.Size = New System.Drawing.Size(157, 173)
        '
        'mniBranchJur
        '
        Me.mniBranchJur.Enabled = False
        Me.mniBranchJur.Name = "mniBranchJur"
        Me.mniBranchJur.Size = New System.Drawing.Size(156, 22)
        Me.mniBranchJur.Text = "Branch Jurisdiction:"
        '
        'tscBranchJur
        '
        Me.tscBranchJur.Name = "tscBranchJur"
        Me.tscBranchJur.Size = New System.Drawing.Size(121, 21)
        Me.tscBranchJur.ToolTipText = "Branch Jurisdiction"
        '
        'zspFour
        '
        Me.zspFour.Name = "zspFour"
        Me.zspFour.Size = New System.Drawing.Size(153, 6)
        '
        'mniUndo
        '
        Me.mniUndo.Name = "mniUndo"
        Me.mniUndo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.mniUndo.Size = New System.Drawing.Size(156, 22)
        Me.mniUndo.Text = "Undo"
        '
        'zspTwo
        '
        Me.zspTwo.Name = "zspTwo"
        Me.zspTwo.Size = New System.Drawing.Size(153, 6)
        '
        'mniCut
        '
        Me.mniCut.Name = "mniCut"
        Me.mniCut.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.mniCut.Size = New System.Drawing.Size(156, 22)
        Me.mniCut.Text = "Cut"
        '
        'mniCopy
        '
        Me.mniCopy.Name = "mniCopy"
        Me.mniCopy.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.mniCopy.Size = New System.Drawing.Size(156, 22)
        Me.mniCopy.Text = "Copy"
        '
        'mniPaste
        '
        Me.mniPaste.Name = "mniPaste"
        Me.mniPaste.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.mniPaste.Size = New System.Drawing.Size(156, 22)
        Me.mniPaste.Text = "Paste"
        '
        'mniDelete
        '
        Me.mniDelete.Name = "mniDelete"
        Me.mniDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.mniDelete.Size = New System.Drawing.Size(156, 22)
        Me.mniDelete.Text = "Delete"
        '
        'txtBranch
        '
        Me.txtBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtBranch.ContextMenuStrip = Me.cmsTextBox
        Me.txtBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBranch.Location = New System.Drawing.Point(0, 28)
        Me.txtBranch.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.txtBranch.Name = "txtBranch"
        Me.txtBranch.Size = New System.Drawing.Size(197, 20)
        Me.txtBranch.TabIndex = 2
        Me.txtBranch.Text = "{Branch}"
        Me.txtBranch.Visible = False
        '
        'cmsEntityControl
        '
        Me.cmsEntityControl.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mniEntityDelete, Me.mniEntityAlign, Me.zspOne, Me.mniShowBranch, Me.mniEntityStop})
        Me.cmsEntityControl.Name = "cmsBaseControl"
        Me.cmsEntityControl.Size = New System.Drawing.Size(207, 148)
        '
        'mniEntityDelete
        '
        Me.mniEntityDelete.Image = Global.LegalObjectsModel.My.Resources.Resources.Delete
        Me.mniEntityDelete.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mniEntityDelete.Name = "mniEntityDelete"
        Me.mniEntityDelete.Size = New System.Drawing.Size(206, 22)
        Me.mniEntityDelete.Text = "Delete"
        '
        'mniEntityAlign
        '
        Me.mniEntityAlign.DropDown = Me.cmsAlign
        Me.mniEntityAlign.Image = Global.LegalObjectsModel.My.Resources.Resources.Align
        Me.mniEntityAlign.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mniEntityAlign.Name = "mniEntityAlign"
        Me.mniEntityAlign.Size = New System.Drawing.Size(206, 22)
        Me.mniEntityAlign.Text = "Align with..."
        '
        'cmsAlign
        '
        Me.cmsAlign.Name = "cmsAlign"
        Me.cmsAlign.OwnerItem = Me.mniEntityAlign
        Me.cmsAlign.Size = New System.Drawing.Size(61, 4)
        '
        'zspOne
        '
        Me.zspOne.Name = "zspOne"
        Me.zspOne.Size = New System.Drawing.Size(203, 6)
        '
        'mniShowBranch
        '
        Me.mniShowBranch.CheckOnClick = True
        Me.mniShowBranch.Name = "mniShowBranch"
        Me.mniShowBranch.Size = New System.Drawing.Size(206, 22)
        Me.mniShowBranch.Text = "Show Branch"
        '
        'mniEntityStop
        '
        Me.mniEntityStop.Name = "mniEntityStop"
        Me.mniEntityStop.Size = New System.Drawing.Size(206, 22)
        Me.mniEntityStop.Text = "Stop"
        '
        'jcbJur
        '
        Me.jcbJur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.jcbJur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SetColumnSpan(Me.jcbJur, 2)
        Me.jcbJur.Dock = System.Windows.Forms.DockStyle.Fill
        Me.jcbJur.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.jcbJur.ForeColor = System.Drawing.SystemColors.WindowText
        Me.jcbJur.FormattingEnabled = True
        Me.jcbJur.Location = New System.Drawing.Point(3, 51)
        Me.jcbJur.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.jcbJur.Name = "jcbJur"
        Me.jcbJur.Size = New System.Drawing.Size(194, 21)
        Me.jcbJur.TabIndex = 3
        '
        'EntityControl
        '
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ColumnCount = 2
        Me.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.ContextMenuStrip = Me.cmsEntityControl
        Me.Controls.Add(Me.rtbName, 0, 0)
        Me.Controls.Add(Me.txtBranch, 1, 0)
        Me.Controls.Add(Me.jcbJur, 0, 1)
        Me.MinimumSize = New System.Drawing.Size(110, 0)
        Me.RowCount = 2
        Me.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Size = New System.Drawing.Size(123, 75)
        Me.cmsTextBox.ResumeLayout(False)
        Me.cmsEntityControl.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LEntity As Entity

    Public Overrides Function ToString() As String
        Return Me.LEntity.ToString
    End Function

#Region "Create and Destroy"
    Friend Sub New(ByVal E As Entity)

        InitializeComponent()

        mniEntityStop.Visible = IsIDE()

        BackColor = My.Settings.ControlForeColour

        rtbName.AllowDrop = True
        txtBranch.AllowDrop = True
        For Each JJ As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
            If Not JJ.FinancialCentres Is Nothing Then
                For Each s As String In JJ.FinancialCentres
                    txtBranch.AutoCompleteCustomSource.Add(s)
                Next
            End If
        Next
        LEntity = E
        E.Parent = Me

        SetStylex(My.Settings.ControlStyleFlat)
        ForeColor = My.Settings.ControlForeColour

        JurisdictionSpace.SetItems(jcbJur.Items)
    End Sub

    Friend Sub SetStylex(ByVal Flat As Boolean)

        Dim b As Boolean = txtSample Is Nothing
        If b Then
            txtSample = New TextBox
            Me.Controls.Add(txtSample)
        End If

        If Flat Then
            rtbName.BorderStyle = Windows.Forms.BorderStyle.None
            txtBranch.BorderStyle = Windows.Forms.BorderStyle.None
            jcbJur.FlatStyle = FlatStyle.Flat
        Else
            rtbName.BorderStyle = Windows.Forms.BorderStyle.Fixed3D
            txtBranch.BorderStyle = Windows.Forms.BorderStyle.Fixed3D
            jcbJur.FlatStyle = FlatStyle.Standard
        End If

        txtSample.BorderStyle = txtBranch.BorderStyle   'I need to do this so I can get the right height for the richtextbox. If I use txtBranch's height
        'and it's not visible then it's height won't change until it does become visible, so won't work for setting rtb's height.

        Me.RowStyles.Clear()

        rtbName.Height = txtSample.Height   'textbox's height will be set automatically, rtb's won't. 
        Me.RowStyles.Add(New Windows.Forms.RowStyle(SizeType.Absolute, txtSample.Height + txtSample.Margin.Top + txtSample.Margin.Bottom))

        If b Then
            Me.Controls.Remove(txtSample)
            txtSample = Nothing
        End If

        Refresh()

    End Sub

    'Form overrides dispose to clean up the component list. Don't understand this.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
#End Region

#Region "Richtextbox"

    Public Sub NameChanged()

        'Keeps the control in check with the name of the entity. 
        'First check is needed to prevent a name change where this event is occurring 
        'because the user has changed the text in rtbName. Without it the caret 
        'would go to the start of the textbox.
        With rtbName
            If .Text <> LEntity.Name Then
                .Text = LEntity.Name
                If Not rtbNameSet Then  'for initial change (i.e. setting in code, set the back grey)
                    .SelectAll()
                    .SelectionBackColor = System.Drawing.SystemColors.ControlLight
                    .DeselectAll()
                End If
            End If
        End With

    End Sub

    Private Sub rtbName_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rtbName.KeyDown

        'Determines whether to accept any AutoText on the basis of KeyCode.
        If tltAutoText.GetToolTip(rtbName) <> "" And (e.KeyCode = Keys.Tab Or e.KeyCode = Keys.F3 Or e.KeyCode = Keys.Return _
        Or e.KeyCode = Keys.Escape) Then

            If e.KeyCode <> Keys.Escape Then  'accept it.
                With rtbName
                    .SelectionStart = InStrRev(.Text, Space(1))
                    .SelectionLength = Len(.Text) - .SelectionStart
                    .SelectedText = tltAutoText.GetToolTip(rtbName)
                End With
            End If
            e.SuppressKeyPress = True
        End If
        If e.KeyCode <> Keys.Tab Then RemoveToolTip()

    End Sub

    Private Sub rtbName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbName.Leave

        'If user presses Tab to accept autotext and all other controls have TabStop false then rtb's
        'LostFocus event occurs but this doesn't, so I need this event.
        RemoveToolTip()

    End Sub

    Private Sub rtbName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles rtbName.KeyUp

        'Decide whether to show the autotext tooltip.
        'Word shows the tip on KeyDown but I haven't done that. Would need to determine what the text would be after the key has been entered.

        'Shows a tooltip with autotext suggestions for "Limited" etc. Should stay there until: _
        ' a) user clicks elsewhere; _
        ' b) user accepts it by pressing Tab, F3 or Enter; _
        ' c) user types as far as the end of the word; or _
        ' d) user types text that is incompatible with the word.

        With CType(sender, RichTextBox)

            Dim n As Long
            Dim t As String
            t = .Text

            If .SelectionStart = Len(t) Then 'only work if the user is typing at the end.

                Dim EndWord As String   'text to compare.

                If sender Is rtbName Then
                    'Last part of text following last space.
                    Dim ss() As String = t.Split(" ")
                    EndWord = ss(UBound(ss))
                Else
                    'Whole of Text
                    EndWord = t
                End If

                If Len(EndWord) > 1 Then

                    Dim JJ As IJurisdiction
                    Dim CC As New List(Of String)

                    If sender Is rtbName Then
                        JJ = LEntity.Incorp

                        If Not JJ Is Nothing Then CC = JJ.EntityTypes

                    End If

                    If Not CC Is Nothing Then
                        'Look at the last part of rtbParty.Text and see if there is the start of an EntityType, _
                        'immediately following a space.
                        Dim Suggestion As String = ""

                        For n = 0 To CC.Count - 1
                            If InStr(UCase$(CC(n)), UCase$(EndWord)) = 1 Then
                                If Suggestion = "" Then
                                    Suggestion = CC(n)
                                    Exit For
                                Else
                                    If (InStr(CC(n), EndWord) = 1 And Len(CC(n)) = Len(Suggestion)) Or Len(CC(n)) < Len(Suggestion) Then
                                        Suggestion = CC(n)     'go for a shorter one or one that has the same case in favour of any other.
                                        Exit For
                                    End If
                                End If
                            End If
                        Next

                        'If we've found a suffix and it's not the whole word, then show the autotext suggestion.
                        If EndWord = Suggestion Then
                            RemoveToolTip()
                        Else
                            If Suggestion <> "" Then
                                Dim P As New Point(rtbName.GetPositionFromCharIndex(rtbName.Text.Length - 1).X + 10, -rtbName.Height + 5)
                                'Tried setting rtbName.AcceptsTab to True here, but focus still went to jcbJur.
                                tltAutoText.Show(Suggestion, rtbName, P)
                            End If
                        End If
                    End If
                End If
            End If
        End With

    End Sub

    Private Sub rtbName_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles rtbName.MouseDown

        RemoveToolTip()

    End Sub

    Private Sub RemoveToolTip()
        tltAutoText.Hide(rtbName)  'there's also a RemoveAll method. Not sure what the difference is. Neither method triggers the Disposed event for the tooltip, 
        'so I need a procedure for all this.
        For Each c As Control In TabsWereOn
            c.TabStop = True
        Next
        TabsWereOn = New List(Of Control)
    End Sub


    Private Shared Sub tltAutoText_Popup(ByVal sender As Object, ByVal e As System.Windows.Forms.PopupEventArgs) Handles tltAutoText.Popup
        TurnOffTabStops(frmMDI)
    End Sub

    Protected Overridable Sub RichTextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbName.TextChanged

        SetSizes()

        If rtbNameSet And Not rtbName.Modified Then   'remove grey background

            With rtbName
                Dim OS As Integer = .SelectionStart
                Dim OL As Integer = .SelectionLength
                .SelectAll()
                .SelectionBackColor = .BackColor
                .Select(OS, OL)
            End With

        End If
        If Not ParentCanvas() Is Nothing Then ParentCanvas.Dirty = True
        LEntity.Name = rtbName.Text 'am just going in this direction, i.e. no event is raised by the entity when this happens.
        UnderlineCorporateSuffixesInRTB()

    End Sub

    Shared TabsWereOn As New List(Of Control)

    Shared Sub TurnOffTabStops(ByVal con As Control)

        'Turn off/on tabbing facility so tab key can be used to accept autotext.
        'Has to be iterative to deal with controls being in a table layout panel.
        For Each c As Control In con.Controls
            If c.TabStop Then TabsWereOn.Add(c)
            c.TabStop = False
            TurnOffTabStops(c)
        Next

    End Sub

    Private Sub UnderlineCorporateSuffixesInRTB()

        'Looks to see whether the end is a listed suffix, and if so shows it underlined to _
        ' indicate that it has been recognised. There must be a space before the suffix and may _
        ' be spaces after it. The check is case-sensitive.

        Static OldSuffix As String 'Store of old suffix. Can't rely on LegalEntity.IncorpSuffix because LegalEntity may be shared.

        If Not LEntity Is Nothing Then

            Dim b As Boolean
            Dim ss() As String
            Dim S As String = ""

            With rtbName

                If Not LEntity.Incorp Is Nothing And Not .Text = "" Then

                    Dim c As List(Of String)
                    c = LEntity.Incorp.EntityTypes
                    ss = Split(RTrim$(.Text), " ")

                    If Not c Is Nothing Then
                        For Each S In c
                            If ss(UBound(ss)) = S Then
                                b = True
                                Exit For    '******
                            End If
                        Next
                    End If

                    If Not b Then S = ""

                End If

                'Ensure the end is underlined if necessary.
                'Unfortunately can't refer to TextRTF here as that will reset the rtb's undo _
                'buffer. So rely on OldSuffix to tell me if there's _
                'already some underlining. Can't just underline what I need and disunderline the rest, _
                'because that would lead to a lot of flickering as I keep resetting the SelStart and SelLength.
                'This is surprisingly hard and maybe impossible to do perfectly.

                If OldSuffix <> "" Or S <> "" Then

                    Dim OS As Long
                    Dim OL As Long

                    OS = .SelectionStart
                    OL = .SelectionLength

                    Dim Disunderline As Boolean
                    Dim Underline As Boolean

                    If OldSuffix <> S Then
                        If S <> "" Then Underline = True 'will need to underline the new suffix.
                        If OldSuffix <> "" Then Disunderline = True 'will need to disunderline the old suffix.
                    End If

                    If Disunderline Then    'disunderline the whole thing.
                        .Select(0, Len(.Text))
                        .SelectionFont = New Font(.Font, FontStyle.Regular)
                    End If

                    If Underline Then   'underline the suffix.
                        .Select(InStrRev(.Text, S) - 1, Len(S))
                        .SelectionFont = New Font(.Font, FontStyle.Underline)
                        .Select(.SelectionStart + .SelectionLength, Len(.Text) - .SelectionStart)
                        .SelectionFont = New Font(.Font, FontStyle.Regular)   'prevent spaces subsequently typed after the suffix appearing underlined.
                    End If

                    If Underline Or Disunderline Then
                        .Select(OS, OL)
                        .ClearUndo() 'user can't undo underlining - the app will just replace it.
                    End If

                End If

                LEntity.IncorpSuffix = S
                OldSuffix = S

            End With

        End If

    End Sub
#End Region

#Region "Textbox Macrobutton style"

    Friend rtbNameSet As Boolean

    Private Sub RichTextBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles rtbName.MouseClick

        If Not rtbName.Modified Then rtbName.SelectAll() 'select all like in macrobuttons in word so user doesn't have to delete it all.
    End Sub

    'Tiny problem here is that I can't set the backcolour of the initial text in a textbox. Could use a richtextbox 
    'but then would lose AutoComplete facility.
    Private Sub txtBranch_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtBranch.MouseClick

        If txtBranch.Text = conBranchBrackets Then txtBranch.SelectAll()
    End Sub

#End Region

#Region "Mouse Events"

    Private Sub Control_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txtBranch.DragEnter, rtbName.DragEnter, jcbJur.DragEnter, Me.DragEnter
        If Not frmMDI.tsiNonRepSec.Checked Then
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    Private Sub Control_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown

        SetZOrder()

        If e.Button = Windows.Forms.MouseButtons.Left Then

            MouseDownControl = Me

            If frmMDI.tsiSelect.Checked Then
                Me.Cursor = Cursors.SizeAll
                ControlDownPoint = New Point(e.X, e.Y)
            End If
            'If this one's not selected, then it seems natural for any that are selected to cease to be.
            If Not Selected Then
                For Each u As Control In Me.Parent.Controls
                    If TypeOf u Is EntityControl Then CType(u, EntityControl).Selected = False
                Next
                frmMDI.SetPRGObjects()
            End If

        End If

    End Sub

    Friend Sub TestCursor(ByRef e As PartyCanvas.CursorEventArgs)
        If Me.Bounds.Contains(e.Location) Then e.OverControl = Me

    End Sub

    Private Sub Control_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter

        If BaseLineModule.BLDrawing Is Nothing Then Me.Cursor = frmMDI.EntityCursor

    End Sub

    Private Sub Control_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove

        If e.Button = Windows.Forms.MouseButtons.Left Then

            'If users drops down the combo drop down and then clicks on the tablelayoutpanel, mousemove will occur without a preceding 
            'mousedown. So need to have ControlDownPoint as nullable and check for a value here.



            If ControlDownPoint.HasValue Then
                Dim ToRefresh As Boolean    'don't want to reformat the canvas unless necessary.
                For Each p As EntityControl In SelectedEntityControls()
                    With p
                        Dim p2 As Point = New Point(.Left + e.X - ControlDownPoint.Value.X, .Top + e.Y - ControlDownPoint.Value.Y)
                        If Not .Location = p2 Then
                            .Location = p2
                            ToRefresh = True
                        End If

                    End With
                Next

                If ToRefresh Then RaiseEvent Reformat()
            End If

            'Reset the printarea here? Movement is a bit juddery if I do.

        End If

    End Sub

    Friend Function SelectedEntityControls() As List(Of EntityControl)

        'Return a List of all selectedEntityControls in the parent canvas, to include Me as well, cos this is what the menu or other action relates to.
        SelectedEntityControls = New List(Of EntityControl)
        For Each p As Control In Parent.Controls
            If TypeOf p Is EntityControl Then
                If CType(p, EntityControl).Selected Or p Is Me Then SelectedEntityControls.Add(p)
            End If
        Next

    End Function

    Private Sub Control_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp

        If e.Button = Windows.Forms.MouseButtons.Left Then
            MouseDownControl = Nothing

            Dim pc As PartyCanvas = TryCast(Me.Parent, PartyCanvas)

            If Not pc Is Nothing Then
                With pc
                    .SetPrintArea(False) 'don't do this while moving as it slows drawing down a lot.
                    .Refresh()
                    If frmMDI.tsiEntity.Checked Then    'Allow controls to be added on top. Alternative requires me to have the No cursor over the controls, which doesn't look good.
                        CType(.Parent, frmBase).picCanvas_MouseUp(sender, New MouseEventArgs(e.Button, e.Clicks, e.X + Me.Left, e.Y + Me.Top, e.Delta))
                    End If
                End With
            End If

            ControlDownPoint = Nothing
        End If
    End Sub

#End Region

#Region "Sizing"

    Friend Sub SetSizes()

        'Can do everything by having a table layout panel with two rows and 
        'two columns, setting column widths automatically and having 
        'everything fully docked and autosizing. But it's fiddly!

        Dim MaxW As Long

        Dim Z As Long
        If txtBranch.Visible Then Z = Math.Max(CTW(txtBranch), CTW(txtBranch, "London"))

        MaxW = Math.Max(CTW(jcbJur), Math.Max(CTW(rtbName), 20) + Z + IIf(txtBranch.Visible, rtbName.Margin.Right, 0))

        'Following seems to work OK without having to undock all controls and redock them.
        SuspendLayout()  'avoids a flicker.
        jcbJur.Width = MaxW
        txtBranch.Width = Z
        rtbName.Width = MaxW - Z
        ResumeLayout()

    End Sub

    Private Function CTW(ByVal C As Control, Optional ByVal s As String = Nothing) As Integer
        'Control Text Width
        If s = Nothing Then s = C.Text
        Dim G As Graphics = CreateGraphics()
        CTW = G.MeasureString(s, C.Font).Width

        If TypeOf C Is TextBoxBase Then
            With CType(C, TextBoxBase)
                CTW += .Width - .ClientRectangle.Width + 3
            End With

        ElseIf TypeOf C Is ComboBox Then
            CTW += SystemInformation.VerticalScrollBarWidth + 5 'ClientRectangle just returns the Bounds for a combobox.
        End If
        G.Dispose()
    End Function

    Private Sub Control_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        PPWidth = Me.Width
        If Created And Not Parent Is Nothing Then Parent.Refresh()
    End Sub

    Private Sub ReturnArea(ByRef R As Rectangle)
        R = Rectangle.Union(R, Bounds)
    End Sub

    Friend Function Mid() As Point
        'Return the middle of this control.
        With ActiveBounds()
            Return New Point(.Left + .Width / 2, .Top + .Height / 2)
        End With
    End Function
#End Region

#Region "Jurisdiction"

    Private Sub jcbJur_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles jcbJur.MouseEnter

        'If I don't do the following I get the entity control's underlying cursor showing through. I want the ibeam cursor 
        'and the arrow cursor when over the dropdown for this. So setting back to default is the way to go. No similar 
        'problem for the rich text boxes. Clearly I need to reset things when the cursor goes back over the other parts of the control.
        'MouseLeave event seems to be not reliable so don't use that.
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub jcbJur_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles jcbJur.SelectedIndexChanged

        'Happens after TextChanged event
        LEntity.Incorp = jcbJur.SelectedItem
        UnderlineCorporateSuffixesInRTB()
    End Sub

    Private Sub jcbJur_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles jcbJur.TextChanged

        SetSizes()
        If Not ParentCanvas() Is Nothing Then ParentCanvas.Dirty = True
        'validity of existing underlining may have changed.
    End Sub

    Private Sub LEntity_JurnChanged() Handles LEntity.JurnChanged

        If Not LEntity.Incorp Is Nothing Then If Me.jcbJur.Items.Contains(LEntity.Incorp) Then Me.jcbJur.SelectedItem = LEntity.Incorp

    End Sub
    Public Sub JurnChanged()

        'Keeps the control in check with the jurisdiction of the entity. 
        If jcbJur.SelectedItem IsNot LEntity.Incorp Then jcbJur.SelectedItem = LEntity.Incorp

    End Sub
#End Region

#Region "Context menu"

    Private Function SomeSelectedNotBank() As Boolean

        For Each p As EntityControl In Me.SelectedEntityControls
            If Not p.LEntity.Bank Then SomeSelectedNotBank = True
        Next

    End Function

    Protected Overridable Sub cmsEntityControl_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsEntityControl.Opening

        mniShowBranch.Checked = txtBranch.Visible

        'mniEntityAlign relates to alignment of just one EntityControl, so not appropriate if more than one is selected.
        If Me.SelectedEntityControls.Count = 1 Then
            Me.mniEntityAlign.Visible = True

            Me.cmsAlign.Items.Clear()

            For Each c As TrancheCollection In ParentCanvas.StanConCols
                c.AddAlignMenuItem(Me.LEntity, Me.cmsAlign)
            Next

            If Me.cmsAlign.Items.Count = 0 Then Me.cmsAlign.Items.Add("(none)")
        Else
            Me.mniEntityAlign.Visible = False
        End If


    End Sub

    Friend Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mniEntityDelete.Click

        Dim bOK As Boolean
        If sender Is Nothing Then
            bOK = True
        Else
            Dim sdesc As String
            If Me.SelectedEntityControls.Count > 1 Then
                sdesc = "the selected entities"
            Else
                sdesc = LEntity.Name
            End If
            bOK = MsgBox("Are you sure you wish to delete " & sdesc & "?", MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation, My.Application.Info.Title) = MsgBoxResult.Yes
        End If

        If bOK Then

            'Delete the control(s).
            With ParentCanvas()
                For Each p As EntityControl In Me.SelectedEntityControls
                    .Controls.Remove(p)    'n.b. can't refer to ParentForm after this.
                    RemoveHandler .CanvasMouseMove, AddressOf p.TestCursor
                    RemoveHandler p.MouseMove, AddressOf .GlobalMouseMoveHandler
                    RemoveHandler .GettingUsedArea, AddressOf p.ReturnArea
                    p.LEntity.Parent = Nothing
                Next
                .Dirty = True
                .Refresh()
            End With

            frmMDI.SetDrawButtonEnablement()

        End If

    End Sub

    Private Sub mniShowBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mniShowBranch.Click

        txtBranch.Visible = Not txtBranch.Visible
        If Not txtBranch.Visible Then

            '     LParty.BranchJurisdiction = Nothing
            txtBranch.Text = conBranchBrackets 'I think the record should go.
        End If
        SetColumnSpan(rtbName, IIf(txtBranch.Visible, 1, 2))
        SetSizes()

    End Sub

    Private Sub cmsAlign_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsAlign.ItemClicked
        If TypeOf e.ClickedItem.Tag Is TrancheCollection Then
            Dim OtherEntity As EntityControl = Nothing
            Dim sc As TrancheCollection = e.ClickedItem.Tag
            If sc.FromEntity Is Me.LEntity Then OtherEntity = sc.ToEntity.Parent
            If sc.ToEntity Is Me.LEntity Then OtherEntity = sc.FromEntity.Parent
            If Not OtherEntity Is Nothing Then  'shouldn't be
                If sc.Orientation = BaseLine.Bearing.East Or sc.Orientation = BaseLine.Bearing.West Then
                    Me.Top = OtherEntity.Top
                Else
                    Me.Left = OtherEntity.Left
                End If
                ParentCanvas.Refresh()
            End If
        End If
    End Sub

#End Region

#Region "PowerPoint"

    Friend PPShapes As List(Of String)

    Friend Sub AddPPShape(ByVal ppp As PowerPoint.Slide)

        'Draw a box on PPSlide to represent the Control.
        Dim PPText As String = Me.rtbName.Text & IIf(txtBranch.Visible And txtBranch.Text <> conBranchBrackets, ", " & txtBranch.Text, "") & vbCrLf & Me.jcbJur.Text   'text to go in the box.

        With ppp.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeRectangle, _
        ActiveBounds.Left, ActiveBounds.Top, 100, ActiveBounds.Height)  '100 is a pre-estimate. I need the Shape's TextRange object to get the necessary size.

            PPFillText(.TextFrame, PPText, PowerPoint.PpParagraphAlignment.ppAlignCenter)

            .Width = Math.Max(Width / CType(Parent, PartyCanvas).ScaleFactor, .TextFrame.TextRange.BoundWidth * 1.1)
            PPWidth = .Width

            'Don't set otherwise as then text appears a bit low.
            If jcbJur.Text <> "" Then .TextFrame.TextRange.ParagraphFormat.SpaceWithin = 1.2

            Dim C As Color = Color.White

            .Fill.ForeColor.RGB = RGB(C.R, C.G, C.B)

            PPShapes = New List(Of String)  'need to clear it each time.
            PPShapes.Add(.Name)

        End With

    End Sub

    Friend PPWidth As Single

    Friend Function ActiveBounds() As Rectangle
        'Returns the Bounds of the control or, if a slide is being made, the bounds of the related powerpoint shape.
        If Not Parent Is Nothing Then
            With CType(Parent, PartyCanvas)
                Return New Rectangle(.ScaleXZero + Left / .ScaleFactor, .ScaleYZero + Top / .ScaleFactor, PPWidth, Height / .ScaleFactor)
            End With
        End If
    End Function

    Friend Function ClearBounds() As Rectangle
        'Return the area outside which collateral must be drawn to be clear of control and any other collateral already drawn there.
        ClearBounds = Rectangle.FromLTRB(ActiveBounds.Left - ControlMargin() * ArcsLeft, ActiveBounds.Top - ControlMargin() * ArcsTop, ActiveBounds.Right + ControlMargin() * ArcsRight, ActiveBounds.Bottom + ControlMargin() * ArcsBottom)
    End Function

    Private Function ControlMargin() As Single
        Return 20 / ParentCanvas.ScaleFactor
    End Function

#End Region

#Region "Drawing"

    Friend Sub Draw(ByVal g As Graphics)

        'For drawing on a PrintDocument or a copied image.
        With g
            .FillRectangle(Brushes.White, Bounds)  'clear the area.
            .DrawRectangle(Pens.Black, Bounds)
            Dim ss As New System.Drawing.StringFormat
            ss.Alignment = StringAlignment.Center
            ss.LineAlignment = StringAlignment.Center
            .DrawString(rtbName.Text & vbCrLf & jcbJur.Text, jcbJur.Font, Brushes.Black, Bounds, ss)
        End With

    End Sub

    Friend Sub DrawControlToBitmap(ByVal eg As Graphics, ByVal X As Long, ByVal Y As Long, Optional ByVal C As Control = Nothing)

        'Recursive routine to draw controls to the canvas when I want them to have no functionality.
        'This control is not visible in frmOptions.
        'Doesn't work for richtextboxes unfortunately.
        If C Is Nothing Then C = Me

        If C.Width <> 0 And C.Height <> 0 Then
            Dim b As New Bitmap(C.Width, C.Height)

            'Richtextbox doesn't work so I have SampleTB to stand in for this.
            If Not TypeOf C Is RichTextBox Then C.DrawToBitmap(b, New Rectangle(0, 0, C.Width, C.Height))

            eg.DrawImage(b, X, Y)

            For Each c2 As Control In C.Controls
                DrawControlToBitmap(eg, X + c2.Left, Y + c2.Top, c2)
            Next
        End If

    End Sub
#End Region

#Region "Textbox menu"

    Private Sub cmsTextBox_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsTextBox.Opening

        With tscBranchJur
            JurisdictionSpace.SetItems(.Items)
            .Visible = cmsTextBox.SourceControl Is txtBranch
            '   .SelectedItem = LParty.BranchJurisdiction
            If .SelectedItem Is Nothing Then .Text = "Branch Jurisdiction"
        End With
        zspFour.Visible = cmsTextBox.SourceControl Is txtBranch
        mniBranchJur.Visible = cmsTextBox.SourceControl Is txtBranch

    End Sub

    Private Sub cmsTextBox_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripDropDownClosingEventArgs) Handles cmsTextBox.Closing
        If cmsTextBox.SourceControl Is txtBranch Then
            '       LParty.BranchJurisdiction = tscBranchJur.SelectedItem
        End If
    End Sub
    Private Sub cmsTextBox_Opened(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmsTextBox.Opened

        mniUndo.Enabled = CurrentT.CanUndo
        mniCut.Enabled = CurrentT.SelectedText <> ""
        mniCopy.Enabled = mniCut.Enabled
        mniDelete.Enabled = mniCut.Enabled
        mniPaste.Enabled = My.Computer.Clipboard.ContainsText

    End Sub

    Private Sub mniCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniCopy.Click
        CurrentT.Copy()
    End Sub

    Private Sub mniCut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniCut.Click
        CurrentT.Cut()
    End Sub

    Private Sub mniDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniDelete.Click
        If Not CurrentT() Is Nothing Then CurrentT.SelectedText = ""
    End Sub

    Private Sub mniPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniPaste.Click
        'Don't use Paste method because any underlining/colouring etc. in the clipboard text will then appear in the richtextbox.
        If My.Computer.Clipboard.ContainsText Then CurrentT.SelectedText = Clipboard.GetText(TextDataFormat.Text)
    End Sub

    Private Sub mniUndo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniUndo.Click
        If CurrentT.CanUndo Then CurrentT.Undo()
    End Sub

    Private Function CurrentT() As TextBoxBase
        Return cmsTextBox.SourceControl
    End Function

#End Region

#Region "Branch"

    Private Sub tscBranchJur_DropDownClosed(ByVal sender As Object, ByVal e As System.EventArgs) Handles tscBranchJur.DropDownClosed
        cmsTextBox.Close()
    End Sub

    Private Sub txtBranch_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBranch.Resize
        'Hack: Because the box isnt resized until _after_
        'new text is typed, the text is scrolled to the left , 
        'out of view. This code brings it back into view and reduces flickering.
        With txtBranch
            Dim selection As Integer = .SelectionStart
            .SelectionStart = 0
            .SelectionStart = selection
        End With

    End Sub


    Private Sub txtBranch_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBranch.TextChanged

        SetSizes()
        'If Not LParty Is Nothing Then
        '    LParty.Branch = txtBranch.Text
        '    For Each j As IJurisdiction In JurisdictionSpace.Jurisdictions.Values
        '        If j.FinancialCentres.Contains(txtBranch.Text) Then LParty.BranchJurisdiction = j
        '    Next

        'End If
        If Not ParentCanvas() Is Nothing Then ParentCanvas.Dirty = True
    End Sub

#End Region

#Region "Misc"

    Private Sub Control_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Enter
        SetZOrder()    'This event triggers for the control and its constituent controls, which GotFocus doesn't.
        CType(ParentCanvas.Parent, Form).Activate()
    End Sub

    Friend Sub SetZOrder()

        BringToFront()
        For Each p As Control In Me.ParentCanvas.Controls
            'Bring redmarkers to front.
            If TypeOf p Is PictureBox Then p.BringToFront()
        Next

    End Sub

    Public Property Selected() As Boolean
        Get
            Return mSelected
        End Get
        Set(ByVal value As Boolean)
            mSelected = value
            rtbName.Enabled = Not value
            txtBranch.Enabled = Not value
            jcbJur.Enabled = Not value
        End Set
    End Property

    Private Sub EntityControl_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move

        LEntity.ControlPoint = Me.Location
        'Refreshing constituent controls prevents red boxes from persisting over them if selectedBL is to or from the entity.
        Me.rtbName.Refresh()
        Me.jcbJur.Refresh()
        If Me.txtBranch.Visible Then Me.txtBranch.Refresh()
    End Sub

    Friend Sub convertrtbtotxtb()

        txtSample = New TextBox
        Me.Controls.Remove(rtbName)
        txtSample.Location = rtbName.Location
        txtSample.Size = rtbName.Size
        Me.Controls.Add(txtSample, 0, 0)
        txtSample.Text = rtbName.Text

    End Sub

    Friend Function ParentCanvas() As PartyCanvas
        Return Parent
    End Function

    Private Sub EntityControl_ParentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ParentChanged
        'Not designing this to go in any other type of control than PartyCanvas.

        If Not Parent Is Nothing Then
            Debug.Assert(TypeOf Parent Is PartyCanvas)
            AddHandler ParentCanvas.GettingUsedArea, AddressOf ReturnArea
        End If
    End Sub

#End Region

    Private Sub EntityControl_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        If Not ParentCanvas() Is Nothing Then ParentCanvas.Dirty = True
    End Sub

    Private Sub mniEntityStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniEntityStop.Click
        Stop
    End Sub

    Private Sub rtbName_ModifiedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbName.ModifiedChanged

        LEntity.bNameModified = rtbName.Modified

    End Sub

    Friend Sub DeleteIfNotUsed()

        Dim bUsed As Boolean = False
        With ParentCanvas()
            For Each c As TrancheCollection In .StanConCols
                If c.ToEntity Is LEntity Or c.FromEntity Is LEntity Then bUsed = True
            Next
        End With
        If Not bUsed Then DeleteToolStripMenuItem_Click(Nothing, Nothing) 'don't get any "are you sure?" prompt, because the sender is nothing.
    End Sub

End Class
