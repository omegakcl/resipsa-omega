
<Serializable()> Friend Class TransactionCon

    'A connection representing part of a new transaction. Must have Consideration.
    Inherits Connection

    Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal FEC As EntityControl)
        MyBase.New(c, bUserDraw, FEC)
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Private pConsideration As TransactionCon
    Public Property Consideration() As TransactionCon
        Get
            Return pConsideration
        End Get
        Set(ByVal value As TransactionCon)
            pConsideration = value
        End Set
    End Property

    Protected Overrides Function TrancheBranchPoint() As Single
        Return 0.8
    End Function

    Protected Overrides Function XInset() As Integer
        Return 12 / MyBase.Parent.ScaleFactor
    End Function

    Friend Overrides Function YInset() As Integer
        Return 12 / MyBase.Parent.ScaleFactor
    End Function

    Friend Overrides Sub PutIntoParentCol(ByVal TE As Entity)

        MyBase.PutIntoParentCol(TE)
        If Consideration.ParentCol Is Nothing Then Consideration.PutIntoParentCol(Me.FromEntity) 'need to avoid getting caught in a loop.

    End Sub

    Protected Overrides Function SetToEntityRelatedProperties(ByVal ToControl As EntityControl) As Boolean

        'Not needed by EntityToEntityCon cos ExistingCon doesn't need it cos can't be added before anything.
        'TODO - allow this to add Me before Cash. Doesn't do that currently. Obviously not possible if already has a non-cash before it. 
        'And think about the Cash presumption. If I'm adding cash before something then the presumption must be that the other thing is a funding.

        For Each c As BaseLine In Me.Parent.SelectedBLs
            Dim bAdd As Boolean
            If Not c.Inst Is Nothing Then If TypeOf c.Inst Is CashPayment Then bAdd = True
            If TypeOf c Is SwapCon Then bAdd = True
            'If bAdd Then Me.PassThroughs.Add(c)
            'CType(c, EntityToEntityCon).Underlyings.Add(Me)
        Next

        Consideration = New TransactionCon(Me.Parent, False, ToControl)
        Consideration.SetPrevious()
        Consideration.TargetSet = True
        Consideration.Consideration = Me

        Return True

        'Dim tempConsidNextBL As EntityToEntityCon = Nothing 'Consideration's nextBL. Store this temporarily while drawing.
        'Dim tempConsidPrevBL As EntityToEntityCon = Nothing

        'If frmMDI.tsiDrawPassThroughNew.Checked Then
        '    With Me.Parent.SelectedBLs
        '        Dim cnext As EntityToEntityCon = .UniqueSelectedConFrom(OverControl, False, True)
        '        If Not PossibleNextPrevCombination(Me.Underlyings, cnext) Then Return False 'Previous for Me is Funding/Pass-through and Next would be Cash so just don't draw it.
        '        If Not cnext Is Nothing Then cnext.SetPrevious(Me)
        '        tempConsidPrevBL = .UniqueSelectedConTo(OverControl, False, False)
        '        tempConsidNextBL = .UniqueSelectedConFrom(Me.FromEntity.Parent, False, Not TypeOf Me Is SwapCon) 'could be set earlier but no need cos not drawn until now. Swap can be before noncash.
        '        If Not PossibleNextPrevCombination(tempConsidPrevBL, tempConsidNextBL) Then Return False
        '    End With

        '    If Not (Me.Underlyings Is Nothing And Me.Passthroughs Is Nothing And tempConsidPrevBL Is Nothing And tempConsidNextBL Is Nothing) Then SetToEntityRelatedProperties = True
        'Else
        '    SetToEntityRelatedProperties = True
        'End If

        'If SetToEntityRelatedProperties Then
        '    Consideration = New TransactionCon(Me.Parent, False, OverControl)
        '    Consideration.SetPrevious(tempConsidPrevBL)
        '    If Not tempConsidNextBL Is Nothing Then tempConsidNextBL.SetPrevious(Consideration)
        '    Consideration.TargetSet = True
        '    Consideration.Consideration = Me
        'End If

    End Function

    Private Function PossibleNextPrevCombination(ByVal pPrev As Connection, ByVal pNext As Connection) As Boolean

        If pNext Is Nothing Then Return True
        If TypeOf pNext.Inst Is CashPayment Then
            If pPrev Is Nothing Then
                Return True
            Else
                Return TypeOf pPrev.Inst Is CashPayment
            End If
        Else
            Return False    'should not be the case because UniqueSelectedStanConFrom only returns Cash.
        End If

    End Function

    Friend Overrides Sub UnSetTarget()

        MyBase.UnSetTarget()

        If Consideration IsNot Nothing Then
            With Consideration
                .Consideration = Nothing
                .SetPrevious()
                .UnSetTarget()
                .Parent = Nothing
            End With
            Consideration = Nothing
        End If

    End Sub

    Protected MadeReal As Boolean
    Friend Overrides Sub MakeReal()

        MyBase.MakeReal()

        'A bit awkward to avoid getting stuck in a loop here.
        MadeReal = True
        If Not Consideration.MadeReal Then
            If ParentCol.Count = 1 Then ParentCol.LeftTabCon = True 'This instance will already be in there, if it's the only one, set this to be True.
            Consideration.MakeReal()
        End If

    End Sub

    Friend Overrides Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument

        Dim k As EToEInstrument = MyBase.GetNewInst(bUserDraw, FEC)

        'The cash assumption.
        If Not TypeOf k Is PassThrough And Not bUserDraw And Not MadeReal Then k = New CashPayment

        Return k

    End Function

    Friend Overrides ReadOnly Property Existing() As Boolean
        Get
            Return False
        End Get

    End Property

    Friend Overrides Function OffsetInteger() As Integer

        'Offset this for existing cons and other transactioncons in the same collection.
        If ParentCol IsNot Nothing And Me.ToEntity IsNot Nothing Then
            Dim h As TrancheCollection = Parent.GetStanConColl(Me.FromEntity, Me.ToEntity, GetType(ExistingCon))
            If Not h Is Nothing Then
                'if there's no text showing then I don't need to offset for the outermost item.
                Dim inst As Instrument = h.Item(h.Count - 1).Inst
                Dim s As String = ""
                If Not inst Is Nothing Then s = inst.ToString
                If s = "" Then
                    OffsetInteger = h.Count - 1
                Else
                    OffsetInteger = h.Count
                End If

            End If

        End If

        OffsetInteger += MyBase.OffsetInteger    'and offset it for the things in its own collection.

    End Function

    Friend Overrides Sub Delete()
        MyBase.Delete()

        If Not Consideration Is Nothing Then
            Consideration.Consideration = Nothing
            Consideration.Delete()
            Consideration = Nothing
        End If

    End Sub

    Friend Overrides Function CanPassThroughExisting() As Boolean
        Return False
    End Function

    Friend Overrides Function UserDraw(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) As Graphics
        UserDraw = MyBase.UserDraw(sender, e)
        If Not UserDraw Is Nothing Then
            If Not Consideration Is Nothing Then
                Consideration.Draw(UserDraw, Nothing)
            End If

        End If
    End Function

#Region "Post Completion Diagram"

    Friend Sub MakeExisting() Handles Parent.ConvertToExisting

        'Note this is in TransactionCon unlike all the others which are in StandardCon. I don't want to convert an ExistingCon.
        Dim b As Boolean = False
        b = Not Inst.ExistsPostCompletion
        If Not b Then
            Dim h As New ExistingCon(Parent, False, Me.FromEntity.Parent)
            h.PutIntoParentCol(ToEntity)
            h.MakeReal()
            h.Inst = Inst

            For Each ff As CreditSupportCon In Parent.CredSupCollection
                If ff.SupportedObligation Is Me Then ff.SupportedObligation = h

            Next
            For Each ss As SecurityArc In Parent.SecurityCollection

                If ss.ToConnection Is Me Then ss.ToConnection = h
            Next

        End If
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
