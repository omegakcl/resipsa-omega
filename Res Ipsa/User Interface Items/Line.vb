Imports Microsoft.Office
Imports Microsoft.Office.Interop

<Serializable(), CLSCompliant(False)> Public MustInherit Class Line

    'An arrow leading from an EntityControl.

    'CLSCompliance set to false because the PowerPoint slide object is not CLS compliant. Think this only matters for multi-language code.

    Inherits BaseLine

    'How am I going to relate different Instruments to each other?
    'Can have any number of fundings. Each relates to one Cash payment.
    'Can show all or a selected one - that follows through to next arrow, _
    'so must have same number of payments at next arrow.
    'Swaps each relate to one payment, but don't need to have the same number. They 
    'just relate to what's showing when they're added.
    'Pass-throughs don't form a collection - just show while appropriate, and hide if not.
    'Otherwise the user has to keep adding a new one each time a new funding is added.

    'User can display just one or all items, cash being added in the latter case.

    'Considered using a generic type but I need to override some functions so I can set 
    'the current terms. List(Of T) doesn't allow its functions to be overriden. Don't 
    'really want a HashTable because I don't want to store terms by a key.
    <NonSerialized()> Friend Orientation As Bearing

    Shared Function BearingVertical(ByVal x As Bearing)
        Return x = Bearing.North Or x = Bearing.South
    End Function

    Protected MustOverride Sub SetDrawToCoords(ByRef R As Rectangle, ByRef MinW As Long)

    Protected Overridable Sub AddArrowHeads(ByVal gpcs As System.Drawing.Graphics, ByVal clr As Color, ByRef BasePoint As Point, ByRef EndPoint As Point)

        BasePoint = Point.Truncate(Path.PathPoints(0))
        Dim ArrowHeadSize As Integer = 10
        If TypeOf Me.Inst Is CashPayment Then ArrowHeadSize = 8 'I want to distinguish Cash from Fundings, and this seems the best way. I tried showing Fundings with a double thickness, but it didn't look that good.
        DrawArrowHead(gpcs, clr, Path.PathPoints(Path.PathPoints.Length - 2), Path.GetLastPoint, EndPoint, TypeOf Me Is SwapCon, ArrowHeadSize)

    End Sub

    Friend Overrides Function CanPassThroughNew() As Boolean
        Return True
    End Function

    Protected Overridable Function XInset() As Integer
        Return FromEntity.Parent.ActiveBounds.Width / 2
    End Function

    Friend Overridable Function YInset() As Integer
        Return FromEntity.Parent.ActiveBounds.Height / 2
    End Function

    Friend MustOverride ReadOnly Property Toentity() As Entity

    Friend Overrides Sub Draw(ByVal gpcs As System.Drawing.Graphics, ByVal MousePos As Nullable(Of Point), Optional ByVal ppp As PowerPoint.Slide = Nothing)

        MyBase.Draw(gpcs, MousePos)

        Dim rectDrawTo As Rectangle      ' rectangle to be drawn to. May have no size, so effectively just a point.
        If FromEntity.Parent Is Nothing Then Exit Sub
        Dim MinW As Integer = FromEntity.Parent.ActiveBounds.Width 'Minimum width of two controls. Necessary in order to keep arrows parallel where one 
        'control is wider than the other.

        If MousePos.HasValue Then       'MousePos has to be Nullable. Can't use Point.IsEmpty cos this will be True if cursor goes over origin.
            rectDrawTo = New Rectangle(MousePos, New Size(0, 0))
        Else
            SetDrawToCoords(rectDrawTo, MinW)
        End If

        Dim MaxGap As Long

        With FromEntity.Parent.ActiveBounds
            'Work out which side to draw the arrow from.
            MaxGap = .Top - rectDrawTo.Bottom
            Orientation = Bearing.North
            If rectDrawTo.Top - .Bottom > MaxGap Then
                MaxGap = rectDrawTo.Top - .Bottom
                Orientation = Bearing.South
            End If
            If rectDrawTo.Left - .Right > MaxGap Then
                MaxGap = rectDrawTo.Left - .Right
                Orientation = Bearing.East
            End If
            If .Left - rectDrawTo.Right > MaxGap Then
                MaxGap = .Left - rectDrawTo.Right
                Orientation = Bearing.West
            End If

            'It would seem logical to adjust my figures by one pixel so as to be outside the control, but when I do that, 
            'there's a one pixel gap showing to the right and bottom of all controls. So don't bother - it looks OK. Note 
            'however that I have to make an adjustment in SetRedEnds for this.

            Select Case Orientation
                Case Bearing.West
                    Path.AddLine(.Left, .Bottom - YInset(), rectDrawTo.Right, IIf(rectDrawTo.Height > YInset(), rectDrawTo.Bottom - YInset(), rectDrawTo.Bottom))

                Case Bearing.East
                    Path.AddLine(.Right, .Top + YInset(), rectDrawTo.Left, IIf(rectDrawTo.Height > YInset(), rectDrawTo.Top + YInset(), rectDrawTo.Top))

                Case Bearing.South
                    Path.AddLine(.Left + MinW - XInset(), .Bottom, IIf(rectDrawTo.Width > XInset(), rectDrawTo.Left + MinW - XInset(), rectDrawTo.Left), rectDrawTo.Top)

                Case Bearing.North
                    Path.AddLine(.Left + XInset(), .Top, IIf(rectDrawTo.Width > XInset(), rectDrawTo.Left + XInset(), rectDrawTo.Left), rectDrawTo.Bottom)

            End Select

            'Easiest to set the end of the path wrongly for credit support and then change it.

            If FirstInCollection() Then
                RedPoints.Add(Path.PathPoints(0))
                RedPoints.Add(Path.PathPoints(1))
            End If

            MainStartPoint = Path.PathPoints(0)
            MainEndPoint = Path.PathPoints(Path.PathPoints.Length - 1)

            SetTrancheOffset(gpcs)

        End With

        If TypeOf Parent.Parent Is frmOptions Then  'Actual PartyControls are off the bottom, need to adjust this to fit the copies.
            Path.Transform(New Drawing2D.Matrix(1, 0, 0, 1, 0, -Parent.Height))
        End If

        If gpcs IsNot Nothing Then

            Dim pn As Pen = LinePen()

            SetLineWidthAndStyle(pn)

            'If I'm drawing a wider line then I need to draw it short so that it doesn't extend over the arrowhead.
            'Could test for the path being wider, but no need - do it all the time. Need it for swaps too, with hollow arrowhead.
            Dim ShortPath As New System.Drawing.Drawing2D.GraphicsPath

            Dim BP As Point
            Dim EP As Point
            If Path.PointCount <> 0 And FirstInCollection() Then
                AddArrowHeads(gpcs, LinePen.Color, BP, EP)

                'Rebuild the Path, using the new co-ordinates. I'm assuming it's made up of straight lines.
                If Path.PointCount = 2 Then
                    ShortPath.AddLine(EP, BP)
                Else
                    For i As Integer = 1 To Path.PathPoints.Length - 2
                        If i = 1 Then
                            ShortPath.AddLine(BP, Path.PathPoints(i))
                        Else
                            AddLineToPath(ShortPath, Path.PathPoints(i))
                        End If
                    Next
                    AddLineToPath(ShortPath, EP)

                End If

            Else
                ShortPath = Path
            End If

            gpcs.DrawPath(pn, ShortPath)

        ElseIf ppp IsNot Nothing Then
            'Adds zero sized rectangles to act as connection sites at the end of each _
            'arrow and connects a connector to these to represent the arrow.

            'Another point is that I could use the existing connection sites where single arrows are _
            'drawn between entities. But I think it's easier for the user to understand if I'm _
            'consistent.

            DoPPLine(ppp)

        End If

        TextRect = Nothing

        'If it's part of a tranche then just rotate it to sit against the line.
        Dim b As Boolean
        Dim pe As Connection = TryCast(Me, Connection)
        If Not pe Is Nothing Then
            With pe
                If Not .ParentCol Is Nothing Then If .ParentCol.Count > 1 Then b = True
            End With
        End If

        If Not Inst Is Nothing Then If Not Inst.ToString = "" Then DrawTextByLine(gpcs, ppp, Rotate(Orientation, -1), TextStartPoint, TextEndPoint, b)

        DrawingDone()

        'Set the positions that collateral will be drawn from and to. There should never need to be more than 2 or 3
        'securityarcs attached to a Connection anyway. 1.5 works well, cos it ensures that if there are lots of arcs
        'from a connection and lots to it, where they overlap at the middle they will all have different attachment points.
        Dim st As New List(Of SecurityArc)
        Dim ss As New List(Of SecurityArc)
        For Each s As SecurityArc In Parent.SecurityCollection
            If TypeOf s Is LineArc Then If CType(s, LineArc).FromConnection Is Me Then st.Add(s) 'don't use AreTheSame because could be an ExistingCon and a TransactionCon
            If s.ToConnection Is Me Then ss.Add(s)
        Next
        Dim n As Integer
        For n = 0 To st.Count - 1
            st.Item(n).DrawFromPos = LinePointType.ColBase - n * 1.5
        Next
        DrawFromPos = LinePointType.ColBase - n * 1.5
        For n = 0 To ss.Count - 1
            ss.Item(n).DrawToPos = LinePointType.SecSupBase + n * 1.5
        Next
        DrawToPos = LinePointType.SecSupBase + n * 1.5

    End Sub

    Friend Overrides Function LinePen() As System.Drawing.Pen

        Dim sec As SecurityArc = TryCast(BLDrawing, SecurityArc)
        If Not sec Is Nothing Then
            If sec.ValidObligation(Me) Then Return New Pen(Color.Red)
        End If

        Return MyBase.LinePen()

    End Function

    Friend Overrides Function TextColour() As System.Drawing.Color
        If Not Inst Is Nothing Then
            If Not Inst.TypeIsEnabled(Inst.GetType) Then Return BaseLine.conBadSupportColour
        End If

        Return MyBase.TextColour()
    End Function

    Friend Overrides Sub SetLineWidthAndStyle(Optional ByRef pn As System.Drawing.Pen = Nothing, Optional ByVal pc As PowerPoint.LineFormat = Nothing)

        If Not pc Is Nothing Then
            If Existing() Then
                pc.DashStyle = Core.MsoLineDashStyle.msoLineDash
            Else
                pc.DashStyle = Core.MsoLineDashStyle.msoLineSolid
            End If

        End If
        MyBase.SetLineWidthAndStyle(pn, pc)


    End Sub

    Protected Friend Overridable Function FirstInCollection() As Boolean
        Return True
    End Function

    Protected Overridable Function TextStartPoint() As PointF
        Return MainStartPoint
    End Function
    Protected Overridable Function TextEndPoint() As PointF
        Return MainEndPoint
    End Function

    <CLSCompliant(False)> Protected MustOverride Sub DoPPLine(ByVal ppp As PowerPoint.Slide)

    Protected Overridable Sub SetTrancheOffset(ByVal gpcs As Graphics)

    End Sub

    Protected Overrides Function DrawingCursor() As System.Windows.Forms.Cursor
        Return frmMDI.EntityCursor
    End Function

    <CLSCompliant(False)> Protected Function CreatePPConnection(ByVal ppp As PowerPoint.Slide, ByVal pc As PowerPoint.Shape, Optional ByVal EndEC As EntityControl = Nothing) As PowerPoint.Shape
        'Make any powerpoint connections for this line. Unfortunately no way to connect a line to a given point on another. Could 
        'try and be very clever and group a credit support line to a rectangle which is invisible and extends over the supported line
        'but that will only work when the user moves the original ractangle if the angle of the supported line remains the same.

        'Add connection points for the arrow.

        'Could give second one a dimension of 0,0 and then would not _
        'need to worry about making the Fill invisible. Only (v. minor) _
        'issue then is that if user ungroups the whole diagram, and then _
        'selects everything to drag it, the connection point won't _
        'be caught by the selecting process _
        'If it's on a line (i.e. credit support), so will remain where it is.    
        'For first one need to have dimension of 1,1 rather than 0,0 while the _
        'second shape has this dimension, or else get kinks in _
        'vertical lines.   

        Dim d As Integer = 0
        If EndEC IsNot Nothing Then d = 1

        CreatePPConnection = ppp.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeRectangle, Path.PathPoints(d).X, Path.PathPoints(d).Y, 1, 1)
        With CreatePPConnection
            If EndEC Is Nothing Then
                Me.FromEntity.Parent.PPShapes.Add(.Name)
            Else
                EndEC.PPShapes.Add(.Name)
            End If
            .Line.Visible = False    'otherwise will be visible if entity is ungrouped.
            .Fill.Visible = False
        End With

    End Function

    'These two points are the start and end of the main parts of a Connection. If it's a tranche then they're the corner points between which the 
    'central part of the arrow is drawn. I use them both for determining where to draw credit support and collateral to and for positioning text.
    Protected MainStartPoint As PointF
    Protected MainEndPoint As PointF

    Friend Enum LinePointType   'enumeration values have to be integers.
        FullLength = 24
        TrancheEnd = 21
        ColBase = 16
        MidPoint = 12
        SecSupBase = 8
        CredSupBase = 6
        TrancheBase = 3
    End Enum

    Friend Function LinePoint(ByVal s As LinePointType) As Point
        Return LinePoint(CSng(s / LinePointType.FullLength))
    End Function

    Friend Function LinePoint(ByVal s As Single) As Point

        'Returns a point along the line. As Single for benefit of TextStart/EndPoint
        Return New Point(MainStartPoint.X * (1 - s) + MainEndPoint.X * s, MainStartPoint.Y * (1 - s) + MainEndPoint.Y * s)

    End Function

    Friend MustOverride ReadOnly Property FromEntity() As Entity

    Friend Overrides Function UserDraw(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) As Graphics

        If TypeOf sender Is EntityControl Then   'should be
            Dim ec As EntityControl = CType(sender, EntityControl)
            Dim h As New Point(e.X + ec.Left, e.Y + ec.Top)

            If ec.Bounds.Contains(h) Then '    'Doesn't look good if you draw when cursor is over EntityControl.
                Parent.GetGraphics()
            Else
                Dim e2 As New MouseEventArgs(e.Button, e.Clicks, h.X, h.Y, e.Delta)
                Return MyBase.UserDraw(sender, e2)
            End If
        End If

    End Function

    Friend Overridable Function OffsetInteger() As Integer
        Return 0
    End Function

    Protected Overrides Sub EntityDeleted(ByVal ent As EntityControl)
        If AreTheSame(ent, Me.FromEntity) Then Me.Delete()
    End Sub

    Public Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal FEC As EntityControl)
        MyBase.New(c, bUserDraw, FEC)
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub
    Friend MustOverride Function BeneficiaryIfSecured() As Entity

    Friend Function IsSecurable(ByVal Collateral As Line, ByVal bExisting As Boolean) As Boolean



    End Function

End Class


