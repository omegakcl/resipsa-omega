<Serializable()> Friend Class EntityArc

    'Security drawn from an Entity. I did do away with this for a while because it doesn't really 
    'represent anything other than security. But security and fixed/floating charges are so important in the 
    'context of finance that I think they need representing.

    Inherits SecurityArc

    Friend FromEntity As Entity

    Sub New(ByVal c As PartyCanvas, ByVal E As Entity, ByVal bExisting As Boolean, ByVal bUserDraw As Boolean)
        MyBase.new(c, bExisting, bUserDraw)
        Me.FromEntity = E
        mExisting = bExisting
    End Sub

    Friend Overrides Function ValidObligation(ByVal ProposedSupportedLine As Line) As Boolean
        'Whether this security could be granted in support of h

        'Person granting security must have benefit of what it's granted over.

        If Not MyBase.ValidObligation(ProposedSupportedLine) Then Return False

        If Not ProposedSupportedLine.FromEntity Is Me.FromEntity Then Return False

        'Add more reasons why security can't be given...
        Return True
    End Function

    Friend Overrides Function GetPoints(ByVal Points As System.Collections.Generic.List(Of System.Drawing.Point), ByRef ArrowHeadPoint As Point, ByRef VertTextAdj As Integer) As Boolean

        'Numbering sectors 1-8 from top, clockwise.

        Const Margin = 20

        If ToConnection Is Nothing Then ArrowHeadPoint = New Point(Point.Add(ArrowHeadPoint, New Size(FromEntity.Parent.Location))) 'Please can I get rid of this fudge!

        If FromEntity.Parent.Bounds.Contains(ArrowHeadPoint) Then Return False

        With ArrowHeadPoint

            If .X > FromEntity.Parent.Mid.X Then

                If .Y < FromEntity.Parent.Mid.Y Then
                    If FromEntity.Parent.Top - .Y > .X - FromEntity.Parent.Right Then   'sector 1
                        Points.Add(New Point(FromEntity.Parent.Right, FromEntity.Parent.Mid.Y))
                        If .X > Points(0).X + Margin Then
                            Points.Add(New Point(.X, Points(0).Y))
                        Else
                            Points.Add(New Point(Points(0).X + Margin, Points(0).Y))
                            Points.Add(New Point(Points(1).X, .Y))
                        End If
                    Else 'sector 2
                        Points.Add(New Point(FromEntity.Parent.Mid.X, FromEntity.Parent.Top))
                        If .Y < Points(0).Y - Margin Then
                            Points.Add(New Point(Points(0).X, .Y))
                        Else
                            Points.Add(New Point(Points(0).X, Points(0).Y - Margin))
                            Points.Add(New Point(.X, Points(1).Y))
                        End If
                    End If
                Else
                    If .Y - FromEntity.Parent.Bottom > .X - FromEntity.Parent.Right Then   'sector 4
                        Points.Add(New Point(FromEntity.Parent.Right, FromEntity.Parent.Mid.Y))
                        If .X > Points(0).X + Margin Then
                            Points.Add(New Point(.X, Points(0).Y))
                        Else
                            Points.Add(New Point(Points(0).X + Margin, Points(0).Y))
                            Points.Add(New Point(Points(1).X, .Y))
                        End If
                    Else    'sector 3

                        Points.Add(New Point(FromEntity.Parent.Mid.X, FromEntity.Parent.Bottom))
                        If .Y > Points(0).Y + Margin Then

                            Points.Add(New Point(Points(0).X, .Y))
                        Else
                            Points.Add(New Point(Points(0).X, Points(0).Y + Margin))
                            Points.Add(New Point(.X, Points(1).Y))
                        End If
                    End If

                End If

            Else

                If .Y > FromEntity.Parent.Mid.Y Then
                    If .Y - FromEntity.Parent.Bottom > FromEntity.Parent.Left - .X Then   'sector 5
                        Points.Add(New Point(FromEntity.Parent.Left, FromEntity.Parent.Mid.Y))
                        If .X < Points(0).X - Margin Then
                            Points.Add(New Point(.X, Points(0).Y))
                        Else
                            Points.Add(New Point(Points(0).X - Margin, Points(0).Y))
                            Points.Add(New Point(Points(1).X, .Y))
                        End If
                    Else 'sector 6
                        Points.Add(New Point(FromEntity.Parent.Mid.X, FromEntity.Parent.Bottom))
                        If .Y > Points(0).Y + Margin Then
                            Points.Add(New Point(Points(0).X, .Y))
                        Else
                            Points.Add(New Point(Points(0).X, Points(0).Y + Margin))
                            Points.Add(New Point(.X, Points(1).Y))
                        End If
                    End If
                Else
                    If FromEntity.Parent.Top - .Y > FromEntity.Parent.Left - .X Then   'sector 8
                        Points.Add(New Point(FromEntity.Parent.Left, FromEntity.Parent.Mid.Y))
                        If .X < Points(0).X - Margin Then
                            Points.Add(New Point(.X, Points(0).Y))
                        Else
                            Points.Add(New Point(Points(0).X - Margin, Points(0).Y))
                            Points.Add(New Point(Points(1).X, .Y))
                        End If
                    Else    'sector 7
                        Points.Add(New Point(FromEntity.Parent.Mid.X, FromEntity.Parent.Top))
                        If .Y < Points(0).Y - Margin Then
                            Points.Add(New Point(Points(0).X, .Y))
                        Else
                            Points.Add(New Point(Points(0).X, Points(0).Y - Margin))
                            Points.Add(New Point(.X, Points(1).Y))
                        End If
                    End If

                End If

            End If
        End With

        Return True


    End Function


End Class
