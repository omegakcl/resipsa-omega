
<Serializable()> Friend Class ExistingCon

    Inherits Connection
    Friend Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal FEC As EntityControl)
        MyBase.New(c, bUserDraw, FEC)
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Protected Overrides Function SetToEntityRelatedProperties(ByVal OverControl As EntityControl) As Boolean

        'Am not allowing ExistingCons to be added before other ones cos then a Funding will have to become a passthrough.
        Return True

    End Function

    Friend Overrides ReadOnly Property Existing() As Boolean
        Get
            Return True
        End Get
    End Property

    Friend Overrides Property Inst() As Instrument
        Get
            Return MyBase.Inst
        End Get
        Set(ByVal value As Instrument)
            If Not value Is Nothing Then If Not value.ExistsPostCompletion Then Exit Property
            MyBase.Inst = value
        End Set
    End Property

    Protected Overrides Function XInset() As Integer
        XInset = MyBase.XInset()
        'If you've got ExistingCons either way between two Entities you need to move them both out a bit so they don't overlap.
        If Parent.GetStanConColl(Me.ToEntity, Me.FromEntity, Me.GetType) IsNot Nothing Then XInset = XInset - 3
    End Function

    Friend Overrides Function YInset() As Integer
        YInset = MyBase.YInset()
        If Parent.GetStanConColl(Me.ToEntity, Me.FromEntity, Me.GetType) IsNot Nothing Then YInset = YInset - 2
    End Function
End Class
