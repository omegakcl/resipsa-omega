Imports Microsoft.Office.Interop
Imports Microsoft.Office


<Serializable()> Friend Class SwapCon
    Inherits TransactionCon
    Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal bExisting As Boolean, ByVal FEC As EntityControl)

        MyBase.New(c, bUserDraw, FEC)
        mExisting = bExisting
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Protected Overrides Function TrancheBranchPoint() As Single
        Return 0.64
    End Function

    Friend Overrides ReadOnly Property Existing() As Boolean
        Get
            Return mExisting
        End Get

    End Property

    'Can't do the same as my outline arrows in powerpoint, so will substitute open arrow heads.
    Friend Overrides Function BeginArrowHeadStyle() As Core.MsoArrowheadStyle
        Return Core.MsoArrowheadStyle.msoArrowheadNone        'can't have heads on both ends because then there's nothing to distinguish the two.
    End Function
    Friend Overrides Function EndArrowHeadStyle() As Core.MsoArrowheadStyle
        Return Core.MsoArrowheadStyle.msoArrowheadOpen
    End Function

    'Protected Overrides Sub AddArrowHeads(ByVal gpcs As System.Drawing.Graphics, ByVal clr As Color, ByRef BasePoint As Point, ByRef EndPoint As Point)
    '    '  BaseLine.DrawArrowHead(gpcs, clr, Path.PathPoints(Path.PathPoints.Length - 2), Path.GetLastPoint, EndPoint, True)
    '    'Was adding two arrowheads. It looks nice, but surely there should be an indication of which way the funding payment is going? Perhaps an existing swap should show larger arrowheads
    '    'for coupons and repayment and a new swap should show smaller ones for the intial exchange of principal, as this is a cash payment?
    '    BaseLine.DrawArrowHead(gpcs, clr, Path.PathPoints(1), Path.PathPoints(0), BasePoint, True)

    'End Sub

    Friend Overrides Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument

        Return New SwapLeg()

    End Function

    Friend Sub GenerateConfirm()

        Dim r As New frmConfirmation("", Inst, Consideration.Inst, Nothing)
        r.ShowDialog()

    End Sub

    Protected Overrides Function SetToEntityRelatedProperties(ByVal OverControl As EntityControl) As Boolean
        If Not OverControl Is Nothing Then
            Consideration = New SwapCon(Me.Parent, False, Existing, OverControl)
            Consideration.TargetSet = True
            Consideration.Consideration = Me
            Return True
        End If
    End Function

    Friend Overloads Property Consideration() As SwapCon
        Get
            Return MyBase.Consideration
        End Get
        Set(ByVal value As SwapCon)
            MyBase.Consideration = value
        End Set
    End Property

    Friend Overrides Function OffsetInteger() As Integer

        'Offset this for existing cons and other swapcons in the same collection and trancons.
        'Equivalent to proc inherited from TransactionCon, but I need to offset even if there's no text.
        If ParentCol IsNot Nothing And Me.ToEntity IsNot Nothing Then
            Dim h As TrancheCollection = Parent.GetStanConColl(Me.FromEntity, Me.ToEntity, GetType(TransactionCon))
            If Not h Is Nothing Then OffsetInteger = h.Count
        End If

        OffsetInteger = OffsetInteger + MyBase.OffsetInteger    'and offset it for the things in its own collection.

    End Function

    'Draw these a little further out than TransactionCons so that the arrow heads are not entirely obscured by those of the transactioncons.
    Protected Overrides Function XInset() As Integer
        Return 8 / MyBase.Parent.ScaleFactor
    End Function

    Friend Overrides Function YInset() As Integer
        Return 8 / MyBase.Parent.ScaleFactor
    End Function

End Class
