Imports Microsoft.Office.Interop
Imports Microsoft.Office

<Serializable()> Friend Class CreditSupportCon
    Inherits Line
    Friend SupportedObligation As Connection

    Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, ByVal FEC As EntityControl, ByVal bExisting As Boolean)
        MyBase.New(c, bUserDraw, FEC)
        mExisting = bExisting
        pFromEntity = FEC.LEntity
    End Sub

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    Friend Overrides ReadOnly Property ToEntity() As Entity
        Get
            Return Me.SupportedObligation.ToEntity
        End Get
    End Property

    Public Overrides Function ToString() As String

        'Necessary for displaying in propertygrid.
        If Inst.ToString = "" Then     'For the basic types.
            Return FromEntity.ToString & " Credit Support"  'need to keep this short for the propertygrid.
        Else
            Return Inst.ToString
        End If

    End Function

    Friend Overrides Sub OtherBaseLineDeleted(ByVal BL As BaseLine)
        If BL Is Me.SupportedObligation Then Me.Delete()
    End Sub

    Dim pFromEntity As Entity
    Friend Overrides ReadOnly Property FromEntity() As Entity
        Get
            Return pFromEntity
        End Get

    End Property

    Protected Overrides Sub DoPPLine(ByVal ppp As PowerPoint.Slide)

        With ppp.Shapes

            Dim pc As PowerPoint.Shape = .AddConnector(Core.MsoConnectorType.msoConnectorStraight, Path.PathPoints(0).X, Path.PathPoints(0).Y, Path.PathPoints(1).X - Path.PathPoints(0).X, Path.PathPoints(1).Y - Path.PathPoints(0).Y)
            pc.ConnectorFormat.BeginConnect(CreatePPConnection(ppp, pc), 1)

            SetArrowStyle(pc.Line)

        End With
    End Sub

    Friend Overrides Function LinePen() As System.Drawing.Pen

        'Check for credit support of things which aren't supportable, e.g. subparticipation has changed to sale.

        If Not SupportedObligation Is Nothing Then
            If Not SupportedObligation.Inst.Supportable Then Return New Pen(conBadSupportColour)
        End If

        Return MyBase.LinePen()

    End Function

    Friend Overrides Sub MakeReal()
        MyBase.MakeReal()
        If Not Parent.CredSupCollection.Contains(Me) Then Parent.CredSupCollection.Add(Me) 'will contain Me if deserializing.
    End Sub

    Friend Overrides Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument

        Return New CreditSupport()

    End Function

    Friend Overrides Function BeneficiaryIfSecured() As Entity
        Return Me.SupportedObligation.ToEntity
    End Function

    Protected Overrides Sub SetDrawToCoords(ByRef R As Rectangle, ByRef MinW As Long)

        If SupportedObligation IsNot Nothing Then R = New Rectangle(Me.SupportedObligation.LinePoint(LinePointType.CredSupBase), New Size(0, 0))

    End Sub

    Friend Overrides Sub Delete()
        MyBase.Delete()
        Parent.CredSupCollection.Remove(Me)
        SupportedObligation = Nothing
    End Sub
    Friend Overrides Function CanPassThroughExisting() As Boolean
        Return False
    End Function
    Friend Overrides Function CanPassThroughNew() As Boolean
        Return False
    End Function

    Friend Overrides Sub SetTarget(ByVal sender As Control, ByVal e As PartyCanvas.CursorEventArgs)

        If TypeOf sender Is EntityControl Then   'should be, but just to avoid any error.
            Me.SupportedObligation = Nothing
            TargetSet = False
            If TypeOf e.OverBL Is Line Then
                Dim j As Line = e.OverBL

                Dim bNoGood As Boolean  'avoid adding credit support by one of the parties to the transaction
                Dim ece As Connection = CType(j, Connection)
                If Not ece Is Nothing Then
                    If ece.ToEntity Is FromEntity Then bNoGood = True
                End If
                If AreTheSame(FromEntity, j.FromEntity) Then bNoGood = True
                If Not j.Inst.Supportable Then bNoGood = True

                If Me.Existing And Not j.Existing Then bNoGood = True

                If Not bNoGood And TypeOf j Is Connection Then

                    SupportedObligation = j
                    TargetSet = True

                End If
            End If

            MyBase.SetTarget(sender, e)
        End If

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
