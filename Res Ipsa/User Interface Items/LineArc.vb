<Serializable()> Friend Class LineArc

    'Security drawn from one Line to another.

    Inherits SecurityArc

    Friend FromConnection As Line

    Sub New(ByVal c As PartyCanvas, ByVal b As Line, ByVal bExisting As Boolean, ByVal bUserDraw As Boolean)
        MyBase.new(c, bExisting, bUserDraw)
        Me.FromConnection = b
        mExisting = bExisting
    End Sub

    'These two functions exist for the benefit of the dlls. See "Dual Property Structure" Word doc.
    Public Function Collateral() As FromEInstrument
        Return FromConnection.Inst
    End Function

    Friend Overrides Function ValidObligation(ByVal ProposedSupportedLine As Line) As Boolean
        'Whether this security could be granted in support of h

        'Person granting security must have benefit of what it's granted over.

        If Not MyBase.ValidObligation(ProposedSupportedLine) Then Return False

        If FromConnection.BeneficiaryIfSecured IsNot ProposedSupportedLine.FromEntity Then Return False

        If FromConnection.Inst IsNot Nothing Then If Not FromConnection.Inst.ExistsPostCompletion Then Return False

        'Add more reasons why security can't be given...
        Return True
    End Function

    Friend Overrides Function GetPoints(ByVal Points As System.Collections.Generic.List(Of System.Drawing.Point), ByRef ArrowHeadPoint As Point, ByRef VertTextAdj As Integer) As Boolean

        Points.Add(FromConnection.LinePoint(CSng(IIf(UserDrawn, FromConnection.DrawFromPos, Me.DrawFromPos))))

        Dim SquarePoint As Point

        Dim EntityControlToAvoid As EntityControl

        If Line.BearingVertical(FromConnection.Orientation) Then

            SquarePoint = New Point(ArrowHeadPoint.X, Points(0).Y)

            'Find which control is more to the left and which to the right. Easiest way is to make initial guess and correct if wrong.
            Dim Upper As EntityControl = FromConnection.FromEntity.Parent
            Dim Lower As EntityControl = FromConnection.Toentity.Parent

            If Not (Upper Is Nothing Or Lower Is Nothing) Then
                If Upper.Top > Lower.Top Then
                    Dim pc As EntityControl = Upper
                    Upper = Lower
                    Lower = pc
                End If

                EntityControlToAvoid = IIf(ArrowHeadPoint.Y < Points(0).Y, Upper, Lower)

                With EntityControlToAvoid
                    If (ArrowHeadPoint.Y > .Top And ArrowHeadPoint.Y > Points(0).Y) Or (ArrowHeadPoint.Y < .Bottom And ArrowHeadPoint.Y < Points(0).Y) Then
                        If ArrowHeadPoint.X <= .Mid.X And ArrowHeadPoint.X > .ClearBounds.Left Then
                            Points.Add(New Point(.ClearBounds.Left, SquarePoint.Y))
                            Points.Add(New Point(.ClearBounds.Left, ArrowHeadPoint.Y))
                            VertTextAdj = .ArcsLeft - 2
                            If Not UserDrawn() Then .ArcsLeft = .ArcsLeft + 1
                        ElseIf ArrowHeadPoint.X > .Mid.X And ArrowHeadPoint.X < .ClearBounds.Right Then
                            Points.Add(New Point(.ClearBounds.Right, SquarePoint.Y))
                            Points.Add(New Point(.ClearBounds.Right, ArrowHeadPoint.Y))
                            VertTextAdj = .ArcsRight - 2

                            If Not UserDrawn() Then .ArcsRight = .ArcsRight + 1
                        End If
                    End If

                End With

            End If

        Else

            'Add a point forming a square with the start point and the end point.
            SquarePoint = New Point(Points(0).X, ArrowHeadPoint.Y)

            'Find which control is more to the left and which to the right. Easiest way is to make initial guess and correct if wrong.
            Dim Lefter As EntityControl = FromConnection.FromEntity.Parent
            Dim Righter As EntityControl = FromConnection.Toentity.Parent
            If Lefter.Left > Righter.Left Then
                Dim pc As EntityControl = Lefter
                Lefter = Righter
                Righter = pc
            End If

            EntityControlToAvoid = IIf(ArrowHeadPoint.X < Points(0).X, Lefter, Righter) 'this is the party we're trying to avoid running over.

            With EntityControlToAvoid     'Don't be tempted to do away with RelControl and make this With IIf. If I do, then only public methods of EntityControl will be accessible, not Friend ones.
                If (ArrowHeadPoint.X > .Left And ArrowHeadPoint.X > Points(0).X) Or (ArrowHeadPoint.X < .Right And ArrowHeadPoint.X < Points(0).X) Then
                    If ArrowHeadPoint.Y <= .Mid.Y And ArrowHeadPoint.Y > .ClearBounds.Top Then
                        Points.Add(New Point(SquarePoint.X, .ClearBounds.Top))
                        Points.Add(New Point(ArrowHeadPoint.X, .ClearBounds.Top))
                        If Not UserDrawn() Then .ArcsTop = .ArcsTop + 1
                    ElseIf ArrowHeadPoint.Y > .Mid.Y And ArrowHeadPoint.Y < .ClearBounds.Bottom Then
                        Points.Add(New Point(SquarePoint.X, .ClearBounds.Bottom))
                        Points.Add(New Point(ArrowHeadPoint.X, .ClearBounds.Bottom))

                        If Not UserDrawn() Then .ArcsBottom = .ArcsBottom + 1
                    End If
                End If
            End With

        End If

        'No reason to have the third point if just drawing between two legs of a right angle.
        Dim RightAngleDrawn As Boolean
        If Not ToConnection Is Nothing Then
            RightAngleDrawn = (Line.BearingVertical(FromConnection.Orientation) <> Line.BearingVertical(ToConnection.Orientation))
        End If

        If Points.Count = 1 Then
            'Don't need the SquarePoint if line between end points is horizontal or vertical. Important where 
            'line drawn between Connection and its consideration.
            If Not (ArrowHeadPoint.X = Points(0).X Or ArrowHeadPoint.Y = Points(0).Y) Then Points.Add(SquarePoint)
        ElseIf Hypot(Points(1), Points(2)) < 2 * Radius() Or RightAngleDrawn Then
            'If the length of a line which is reduced at both ends to make a curve is less then 
            'the length of two curves then the curves will overlap and the whole thing will look 
            'terrible. So in that case just give up and draw it as two limbs. Only applies when parties
            'are very close together.
            Points.RemoveAt(2)
            Points.RemoveAt(1)
            Points.Add(SquarePoint)
        End If

        Return True

    End Function

    Friend Overrides Sub OtherBaseLineDeleted(ByVal BL As BaseLine)
        If BL Is Me.FromConnection Then Me.Delete()
        MyBase.OtherBaseLineDeleted(BL)
    End Sub

    Friend Overrides Sub Delete()
        MyBase.Delete()
        Me.FromConnection = Nothing
    End Sub

End Class
