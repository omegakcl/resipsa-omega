Imports System.Math
Imports Microsoft.Office
Imports Microsoft.Office.Interop

<Serializable(), Editor(GetType(BaseLineDisplayer), GetType(System.Drawing.Design.UITypeEditor))> Public MustInherit Class BaseLine

    'The base class for Connections and SecurityArcs. Public because I expose it to show related instruments in the propertygrid.
    Inherits SerializableObject

    Private pInst As Instrument

    <NonSerialized()> Friend WithEvents Parent As PartyCanvas

    Friend TargetSet As Boolean 'whether, while user is drawing, the target has been set.

    <NonSerialized()> Private picRE As List(Of PictureBox)

    <NonSerialized()> Protected Path As Drawing2D.GraphicsPath

    <NonSerialized()> Friend TextRect As Nullable(Of Rectangle)
    <NonSerialized()> Friend TextTransform As Drawing2D.Matrix  'The transform matrix applied when the text was drawn. Relevent for rotated text.

    Protected Shared conBadSupportColour As Color = System.Drawing.SystemColors.GrayText  'If user draws a guarantee for a sub-participation say and later 
    'changes that to a sale, the guarantee is nonsensical. I think rather than deleting it, I'll just show it in a disabled colour.
    ' Add the default and de-serialization constructors

    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)

        MyBase.New(info, context)

    End Sub

    'There are three stages to the creation of a BaseLine. New, when the user starts drawing. Set as much as possible then. SetTarget as the user moves the cursor. Again, set as much 
    'as possible there. And MakeReal when the user lets the mouse up. Everything should have been set by then except for handlers.

    Sub New(ByVal c As PartyCanvas, ByVal bUserDraw As Boolean, Optional ByVal FEC As EntityControl = Nothing)

        Parent = c

        If bUserDraw Then
            AddHandler MouseDownControl.MouseMove, AddressOf UserDrawHandler
            AddHandler MouseDownControl.MouseUp, AddressOf HandleDrawingEnd

            EventsControl = MouseDownControl
        End If

        If Inst Is Nothing Then Inst = GetNewInst(bUserDraw, FEC) 'condition is necessary for deserialization.

    End Sub

    Friend Overridable Property Inst() As Instrument

        'Remember this will be Nothing while drawing and may be at other times depending on my whim!
        Get
            Return pInst
        End Get
        Set(ByVal value As Instrument)
            '  If value Is Nothing Then value = GetNewInst()
            pInst = value
            value.Parent = Me

            Dim ete As Connection = TryCast(Me, Connection)
            If Not ete Is Nothing Then
                With ete
                    If Not .ParentCol Is Nothing Then .ParentCol.Sort() 'this needs doing after the Instrument is set, and before the property grid is filled, hence I have it here.
                End With

            End If

            frmMDI.FillTreeView()
            If Me.Parent.SelectedBLs.Contains(Me) Then frmMDI.SetPRGObjects()
            Parent.Refresh()    'show the appropriate text.
            Parent.Dirty = True
        End Set

    End Property

    Protected MustOverride Function DrawingCursor() As Cursor

    Friend Overridable Sub SetTarget(ByVal sender As Control, ByVal e As PartyCanvas.CursorEventArgs)
        'If drawing, determine if the thing being drawn to is set. Also, set anything that can be on the basis of where drawn to.
        If Me.TargetSet Then
            sender.Cursor = frmMDI.crsAdd
        Else
            sender.Cursor = DrawingCursor()
        End If
    End Sub

    Private Sub UserDrawHandler(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)    'Procedure for drawing when the user is drawing.
        UserDraw(sender, e)
    End Sub

    Friend Overridable Function UserDraw(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) As Graphics
        'Mousemove handler for drawing.

        <NonSerialized()> Static p As Nullable(Of Point)  'Record of last place drawn at with mouse down. Sometimes get an error on loading - "can't be found". Hoping that marking it nonserialized will fix it - testing for IsStatic in deserialization doesn't seem to work.

        'Maybe split these two out into separate procedures and use AddHandler and RemoveHandler?
        If e.Button = MouseButtons.Left Then

            'This is aimed at preventing the drawing from happening if the mouse's position hasn't changed.
            'Without this, the creation of the graphics object seems to trigger the paint event and 
            'you get stuck in a loop, even if the mouse isn't moving. This uses up a lot of processor 
            'resources (noisy) and also causes drawing of collateral to begin as soon as the mouse goes down.
            'There may be a better way of fixing this, but it does work.
            Dim ToDraw As Boolean
            If p.HasValue Then
                ToDraw = p.Value <> e.Location
            Else
                ToDraw = True
            End If

            p = e.Location

            If ToDraw Then
                Dim h As New Nullable(Of Point)
                If Not TargetSet Then h = p
                UserDraw = Parent.GetGraphics
                Draw(UserDraw, h)
            End If

        Else
            p = Nothing
        End If

    End Function

    Friend Overridable Sub Draw(ByVal gpcs As System.Drawing.Graphics, ByVal MousePos As Nullable(Of Point), Optional ByVal ppp As PowerPoint.Slide = Nothing)

        Path = New Drawing2D.GraphicsPath
        RedPoints = New List(Of PointF)

    End Sub

    Friend Overridable Sub SetLineWidthAndStyle(Optional ByRef pn As System.Drawing.Pen = Nothing, Optional ByVal pc As PowerPoint.LineFormat = Nothing)

        If Not pn Is Nothing Then
            If Existing Then pn.DashPattern = New Single() {5 / pn.Width, 5 / pn.Width}
        End If

    End Sub

    Protected Sub DrawingDone()
        If Parent.SelectedBLs.Contains(Me) Then SetRedEnds()
    End Sub

    Private Sub HandleDrawingEnd(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)

        'Handles the relevant mouseup event, and also called when user presses Esc key.
        If e.Button = MouseButtons.Left Then UserDrawingDone(False)
        If Not Parent Is Nothing Then Me.Parent.Refresh() 'creating an instrument in MakeReal has made this necessary, for some reason, don't know why.
    End Sub

    <NonSerialized()> Protected Shared EventsControl As Control  'MouseDownControl will be nothing when UserDrawingDone so need to record this.

    Friend Overridable Sub UserDrawingDone(ByVal Cancel As Boolean)

        If Not EventsControl Is Nothing Then
            RemoveHandler EventsControl.MouseMove, AddressOf UserDrawHandler
            RemoveHandler EventsControl.MouseUp, AddressOf HandleDrawingEnd
            EventsControl.Cursor = Cursors.Default
            EventsControl = Nothing
        End If

        Parent.Image = Nothing

        BLDrawing = Nothing

        If TargetSet And Not Cancel Then
            MakeReal()
        Else
            ClearUpAfterNonCompletedDrawing()
            If TargetSet And Cancel Then
                SetTarget(MouseDownControl, New PartyCanvas.CursorEventArgs(New MouseEventArgs(MouseButtons.None, 0, 0, 0, 0)))
            End If
            Parent = Nothing  'I'd like this object to die, but if it doesn't the Canvas events will continue to fire unless I do this.
        End If

        frmMDI.SetDrawButtonEnablement()
        If Not frmMDI.bDrawButtonDoubleClicked Then frmMDI.tsiSelect.Checked = True

    End Sub

    Friend Overridable Sub ClearUpAfterNonCompletedDrawing()

    End Sub

    Friend Overridable Sub MakeReal()  'ensure that the information about this is stored somewhere appropriate. The overrides just add this to the appropriate collection. SetTarget should
        'do the other things necessary to set it up, like setting related connections.

        AddHandler Parent.GettingUsedArea, AddressOf ExtendUsedAreaRect
        AddHandler Parent.QueryBandingInclusion, AddressOf RubberBanded
        AddHandler Parent.CanvasMouseMove, AddressOf TestCursor
        AddHandler Parent.EntityRemoved, AddressOf EntityDeleted
        AddHandler BaseLineDeleted, AddressOf Parent.BaseLineDeleted
        AddHandler Parent.BaseLineDeletedEvent, AddressOf OtherBaseLineDeleted
        AddHandler Parent.FillNameList, AddressOf AddToListIfType
        picRE = New List(Of PictureBox)     'If I declare this as New it isn't created at this point if I'm deserializing.
        NewPicRE()
        NewPicRE()
        Parent.Dirty = True
        Parent.Refresh()

    End Sub

    Friend Overridable Sub Delete()
        RemoveHandler Parent.CanvasMouseMove, AddressOf TestCursor
        RemoveHandler Parent.GettingUsedArea, AddressOf ExtendUsedAreaRect
        RemoveHandler Parent.QueryBandingInclusion, AddressOf RubberBanded
        RemoveHandler Parent.EntityRemoved, AddressOf EntityDeleted
        RemoveHandler Parent.BaseLineDeletedEvent, AddressOf OtherBaseLineDeleted
        RaiseEvent BaseLineDeleted(Me)
        RemoveHandler BaseLineDeleted, AddressOf Parent.BaseLineDeleted
        RemoveHandler Parent.FillNameList, AddressOf AddToListIfType
        Parent.SelectedBLs.Remove(Me, True)
        picRE.Clear()
        Parent.Dirty = True
        '   Parent = Nothing   Very complicated if I want to to this here. It's hard to ensure it's late enough. Maybe it's not necessary.
    End Sub

    'For any BaseLine I want to have the concept of a basic instrument type. This is important for setting an initial instrument so that 
    'the related BaseLines can be shown and also as a source to find all the inheriting instrument classes for the menus. I want to do it when a baseline is created too, so 
    'that it can be drawn appropriately, e.g. depending on whether the previous is cash or a funding.
    Friend MustOverride Function GetNewInst(Optional ByVal bUserDraw As Boolean = False, Optional ByVal FEC As EntityControl = Nothing) As Instrument
    Friend Overridable Function GetBasicType() As Type
        'For determining the root type for menus. 
        Return GetNewInst.GetType
    End Function

    Protected Sub TestCursor(ByRef e As PartyCanvas.CursorEventArgs)   'test whether the cursor is over the line.

        If e.Button <> MouseButtons.Left Or TypeOf BLDrawing Is SecurityArc Or TypeOf BLDrawing Is CreditSupportCon Then    'don't change when "rubber-banding" or setting print area, but do when drawing collateral.
            If Not Path Is Nothing Then     'can be for frmOptions.
                'The facility to do this is the main reason for storing the arrows as a Path.
                If Path.IsOutlineVisible(e.Location, New Pen(Color.Black, 6)) Then

                    e.OverBL = Me

                ElseIf TextRect.HasValue Then
                    Dim P(0) As PointF
                    P(0) = e.Location

                    Dim i As Drawing2D.Matrix = TextTransform.Clone
                    i.Invert()
                    i.TransformPoints(P)

                    If TextRect.Value.Contains(Point.Truncate(P(0))) Then
                        e.OverBL = Me
                        e.OverText = True
                    End If
                End If
            End If
        End If

    End Sub

    Friend Overridable Function CanPassThroughExisting() As Boolean
        Return False
    End Function

    Friend Overridable Function CanPassThroughNew() As Boolean
        Return False
    End Function

    'These do slightly different things for SecurityArcs and Connections. For the former they show where 
    'they are to be drawn from and to on their related Connections. For the latter they show where a SecurityArc being 
    'drawn by the user is to be positioned on them.
    <NonSerialized()> Private pDrawToPos As Single
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Property DrawToPos() As Single
        Get
            Return pDrawToPos
        End Get
        Set(ByVal value As Single)
            pDrawToPos = Max(Min(Line.LinePointType.FullLength - 1, value), 1) / Line.LinePointType.FullLength
        End Set
    End Property

    <NonSerialized()> Private pDrawFromPos As Single
    <Browsable(False), EditorBrowsable(EditorBrowsableState.Never)> Public Property DrawFromPos() As Single
        Get
            Return pDrawFromPos
        End Get
        Set(ByVal value As Single)
            pDrawFromPos = Max(Min(Line.LinePointType.FullLength - 1, value), 1) / Line.LinePointType.FullLength
        End Set
    End Property

    Public Enum Bearing
        North
        South
        East
        West
    End Enum

    Protected Shared Sub AddLineToPath(ByRef P As Drawing2D.GraphicsPath, ByVal PNew As PointF)
        'I just want to add a point at the end of the path, but it seems I always have to add a line.
        If P.PointCount > 0 Then
            P.AddLine(P.PathPoints(P.PathPoints.Length - 1), PNew)
        End If
    End Sub

    Friend Sub RubberBanded(ByVal R As Rectangle, ByRef BLToSelect As BaseLine)
        If R.Contains(Point.Truncate(MidPoint)) Then
            BLToSelect = Me
            Parent.SelectedBLs.add(Me, True)
        End If

    End Sub

    Friend Function MidPoint() As PointF

        'Returns the mid point along the length of the Path. Don't confuse with LinePoint which just looks for a point along two given points 
        'in the Path.
        If Not Path Is Nothing Then
            With Path
                If .PointCount > 1 Then
                    Dim h As Integer
                    If .PathPoints.Length / 2 = Int(.PathPoints.Length / 2) Then
                        h = .PathPoints.Length / 2 - 1
                    Else
                        h = .PathPoints.Length / 2 - 0.5
                    End If
                    Return New PointF((.PathPoints(h).X + .PathPoints(h + 1).X) / 2, _
                    (.PathPoints(h).Y + .PathPoints(h + 1).Y) / 2)
                End If
            End With
        End If

    End Function

    Friend Function Rotate(ByVal b As Bearing, ByVal Clock90s As Integer) As Bearing

        If Clock90s < 0 Then Clock90s = Clock90s + 4
        Rotate = b
        For n As Integer = 1 To Clock90s
            Rotate = Rotate90Clock(Rotate)
        Next
    End Function

    Private Function Rotate90Clock(ByVal b As Bearing) As Bearing
        Select Case b
            Case Bearing.East
                Return Bearing.South
            Case Bearing.North
                Return Bearing.East
            Case Bearing.South
                Return Bearing.West
            Case Bearing.West
                Return Bearing.North
        End Select
    End Function

    Friend Function NewPicRE() As PictureBox

        Dim pic As New PictureBox
        With pic
            .BorderStyle = BorderStyle.FixedSingle
            .BackColor = Color.Red
            .Enabled = False    'Canvas or control underneath will handle mouse events - this has no effect on the appearance of pic.
            .Visible = False
        End With
        picRE.Add(pic)
        Return pic

    End Function

    Protected RedPoints As List(Of PointF)  'store of points where red markers are to appear to indicate that this BaseLine is selected.
    'I did use Path.SetMarkers to store this together with a PathIterator.NextMarker. But this returns the indices of points at the beginning 
    'and end of each section. For my arcs there are two points at the same place, one before and one after the marker while for connected lines 
    'there are not, and it all got very confusing. Easy enough just to store the locations.

    Friend Sub SetRedEnds()

        Dim Clear As Boolean

        Dim jk As Integer
        For jk = 0 To RedPoints.Count - 1
            Clear = True
            For Each U As EntityControl In Parent.EntityControls
                Dim RR As New Rectangle(U.Left + 1, U.Top + 1, U.Width - 2, U.Height - 2)   'See comment in Draw
                If RR.Contains(Point.Truncate(RedPoints(jk))) Then Clear = False
            Next

            Dim p As PictureBox
            If picRE.Count > jk Then
                p = picRE(jk)
            Else
                p = NewPicRE()
            End If

            With p
                If Clear Then
                    If CurrentBL Is Me Then
                        .Size = New Size(6, 6)  'make it a bit larger to identify it.
                    Else
                        .Size = New Size(5, 5)
                    End If
                    .Location = New Point(RedPoints(jk).X - .Width / 2, RedPoints(jk).Y - .Height / 2)

                    If Not Parent.Controls.Contains(p) Then
                        Parent.Controls.Add(p)
                        .BringToFront() 'this is potentially a problem because it causes the canvas to refresh, leading to a perpetual loop.
                    End If

                    .Visible = True
                Else
                    .Visible = False
                End If
            End With
        Next

        'Hide surplus picture boxes.
        For ji As Integer = jk To picRE.Count - 1
            picRE(ji).Visible = False
        Next

    End Sub

    Friend Sub HideRedEnds()
        For Each b As PictureBox In picRE
            b.Hide()
        Next
    End Sub

    Friend Shared ff As Font = New System.Drawing.Font("Arial", 10)    'the standard font for drawing on picCanvas.

    Friend Overridable Function LinePen() As Pen
        Return New Pen(Color.Black) 'don't return Pens.Black cos won't be able to change it.
    End Function

    Friend Overridable Function TextColour() As Color
        Return Color.Black
    End Function

    Friend Overridable Function TypeIsVisible(ByVal t As Type) As Boolean

        Return True

    End Function

    Protected mExisting As Boolean
    Friend Overridable ReadOnly Property Existing() As Boolean
        'I have different class types for existing and new with Connection but this doesn't 
        'seem necessary for credit support, swaps and security.
        Get
            Return mExisting

        End Get

    End Property

    Friend Sub DrawTextByLine(ByVal gpcs As Graphics, ByVal ppp As PowerPoint.Slide, ByVal DirectionFromLine As Bearing, ByVal P1 As PointF, ByVal P2 As PointF, ByVal AllowRotation As Boolean, Optional ByVal ppptextname As String = "")

        'Draw text by a line connected by P1 and P2. If I'm having tranches then I really need to rotate text because there is only 
        'one line in which to fit in the text and it can be at any angle. But if there are no tranches it seems better not to because 
        'the visual appearance of rotated text is not that great - it seems quite bitty.

        If Not ToString() = "" Then    'could be the case for cash.
            If gpcs Is Nothing Then gpcs = Parent.GetGraphics 'need to have this for powerpoint.

            Dim LineH As Integer = gpcs.MeasureString("ABC", ff, 2000).Height   'height of one line.

            Dim R As RectangleF
            Dim ss As New System.Drawing.StringFormat(StringFormatFlags.LineLimit) 'don't ever want a half line.  

            Dim m As Single = (P1.Y - P2.Y) / (P1.X - P2.X) 'y=mx +c
            Dim bShiftedOut As Boolean
            If AllowRotation Then
                Dim OriginalTransform As Drawing2D.Matrix = gpcs.Transform

                ss.Alignment = StringAlignment.Center
                ss.Trimming = StringTrimming.EllipsisCharacter 'otherwise it should all show.

                Dim degs As Single = 0 'degrees to rotate the text by.

                If P1.Y <> P2.Y Then
                    degs = 90 - Math.Atan(1 / m) / 2 / Math.PI * 360
                    If P2.Y < P1.Y Then degs = degs + 180
                    If DirectionFromLine = Bearing.South Then degs = degs + 180 'don't ever want it to appear upside down.
                End If

                Dim RotCent As Point    'rotation centre
                If DirectionFromLine = Bearing.South Then
                    RotCent = Point.Truncate(P2)
                Else
                    RotCent = Point.Truncate(P1)
                End If

                gpcs.TranslateTransform(RotCent.X, RotCent.Y)

                R = New Rectangle(0, 0, Hypot(Point.Truncate(P1), Point.Truncate(P2)), LineH)
                If DirectionFromLine <> Bearing.South Then R.Offset(0, -LineH)

                gpcs.RotateTransform(degs)
                DrawText(gpcs, ppp, R, ss, P1, P2, degs, DirectionFromLine, ppptextname)
                gpcs.Transform = OriginalTransform
            Else

                'Several things for shifting the text off the line. Difficulty is that if the line is vertical/horizontal 
                'I want to put the text at the side half way along. But if it's at an angle I want to position the adjacent corner
                'half way along. So I do the first and then if the text is overlapping, I adjust it.
                Dim c As Single = P2.Y - P2.X * m   'y=mx+c
                Dim DShift As Integer   'amount to shift the rectangle by.

                Dim s2() As String = ToString.Split(" ")
                Dim w As Integer    'width required to accommodate the longest word

                For n As Integer = 0 To s2.Length - 1
                    w = Math.Max(w, gpcs.MeasureString(s2(n), ff).Width)
                Next
                w = w + 5

                Select Case DirectionFromLine

                    Case Bearing.East, Bearing.West 'approach is to use all vertical space available. If that and w don't give enough room then widen as necessary.

                        R = New Rectangle((P1.X + P2.X) / 2, Math.Min(P1.Y, P2.Y), w, Math.Max(LineH, Math.Abs(P2.Y - P1.Y)))
                        ss.LineAlignment = StringAlignment.Center       'LineAlignment is vertical

                        If DirectionFromLine = Bearing.West Then ss.Alignment = StringAlignment.Far 'Alignment is horizontal, default is Near.

                        'Increase width if height isn't enough to fit it all in on the basis of width of the longest word.
                        Dim h As Integer = Math.Abs(P2.Y - P1.Y)
                        Dim a As Integer

                        Do
                            gpcs.MeasureString(ToString, ff, R.Size, ss, a, 0)
                            R.Width = R.Width + 2
                        Loop Until a = ToString.Length

                        If DirectionFromLine = Bearing.East Then
                            R.Offset(4, 0)
                        Else
                            R.Offset(-R.Width - 4, 0)
                        End If

                    Case Bearing.North, Bearing.South

                        Dim InitHeight As Integer = 200

                        R = New Rectangle(Math.Min(P1.X, P2.X), (P1.Y + P2.Y) / 2, Math.Abs(P2.X - P1.X), InitHeight)
                        If DirectionFromLine = Bearing.North Then R.Offset(0, -InitHeight)

                        If DirectionFromLine = Bearing.North Then ss.LineAlignment = StringAlignment.Far 'Default is Near

                        ss.Alignment = StringAlignment.Center

                        If R.Width < w And TypeOf Me Is Connection Then
                            bShiftedOut = True
                            'If not wide enough to show widest word, widen it up.
                            R.Offset((R.Width - w) / 2, 0)
                            R.Width = w

                            'Now need to move it out of the gap between the Entities.
                            'I don't think this is so necessary where the line is vertical since the user has then made the gap unreasonably small so it's not surprising it won't work.

                            If DirectionFromLine = Bearing.North Then
                                R.Height = Math.Max(LineH, R.Height - CType(Me, Line).YInset) 'nb although converting to Connection this will look at the YInset for the object's type, not for Connection.
                            Else
                                R.Offset(0, CType(Me, Line).YInset)
                            End If

                        End If

                End Select

                If Not bShiftedOut Then
                    If Abs(m) > 0.2 And P1.X <> P2.X Then   'don't do for low values of m because it gets very jittery if I do.
                        'Adjust text up or down if it's overlapping.
                        SetGraphicsTextRect(gpcs, R, ss)

                        Dim a As Integer = Math.Min(TextRect.Value.Bottom, Max(P1.Y, P2.Y)) 'just look at the region the line is actually drawn in. Otherwise text can fly out to left or right.

                        'co-ordinates of edges of text rectangle on the line.
                        Dim TopX As Integer = (TextRect.Value.Top - c) / m
                        Dim BottomX As Integer = (a - c) / m
                        Dim RightY As Integer = m * TextRect.Value.Right + c
                        Dim LeftY As Integer = m * TextRect.Value.Left + c

                        If DirectionFromLine = Bearing.East Or (DirectionFromLine = Bearing.North And m > 0) Or (DirectionFromLine = Bearing.South And m < 0) Then
                            DShift = Max(Max(TopX, BottomX) - TextRect.Value.Left, 0)
                        Else
                            DShift = -(TextRect.Value.Right - Min(TopX, BottomX))
                        End If

                        If DShift <> 0 Then R.Offset(DShift / 2, 0) 'just half because I'm adjusting it in two directions.

                        If DirectionFromLine = Bearing.South Or (DirectionFromLine = Bearing.East And m < 0) Or (DirectionFromLine = Bearing.West And m > 0) Then
                            DShift = Max(Max(LeftY, RightY) - TextRect.Value.Top, 0)
                        Else
                            DShift = Min(-(TextRect.Value.Bottom - Min(LeftY, RightY)), 0)
                        End If

                        If DShift <> 0 Then R.Offset(0, DShift / 2)

                    End If
                End If

                DrawText(gpcs, ppp, R, ss, Nothing, Nothing, , , ppptextname)
            End If

            If Not ppp Is Nothing Then gpcs = Nothing

        End If

    End Sub

    Private Sub SetGraphicsTextRect(ByVal gpcs As Graphics, ByVal R As RectangleF, ByVal ss As System.Drawing.StringFormat)
        Dim hj As SizeF = gpcs.MeasureString(ToString, ff, New SizeF(R.Width, R.Height), ss)
        TextRect = GetTextRect(R, ss, hj)
    End Sub

    Friend Sub DrawText(ByVal gpcs As Graphics, ByVal ppp As PowerPoint.Slide, ByVal R As RectangleF, ByVal ss As System.Drawing.StringFormat, ByVal P1 As PointF, ByVal P2 As PointF, Optional ByVal degs As Single = -999, Optional ByVal DirectionFromLine As Bearing = Bearing.North, Optional ByRef ppptextname As String = "")

        'Procedure to add in text.

        If ppp Is Nothing And Not gpcs Is Nothing Then
            'Don't use TextRenderer - results are different in PrintDocument1 and Canvas.
            'Comment in help files "Note The DrawText methods of TextRenderer are not supported for printing. You should always use the DrawString methods of the Graphics class."

            'Other alternative is to create a path/use the existing path and the GetBounds function of that.
            'But the AddString method of a path uses an emSize rather than a font size and also draws the characters as 
            'outlines for some reason. So forget that.

            gpcs.DrawString(Inst.ToString, ff, New SolidBrush(TextColour), R, ss)

            SetGraphicsTextRect(gpcs, R, ss)
            TextTransform = gpcs.Transform

        ElseIf Not ppp Is Nothing Then

            Dim PPA As PowerPoint.PpParagraphAlignment
            Select Case ss.Alignment
                Case StringAlignment.Near
                    PPA = PowerPoint.PpParagraphAlignment.ppAlignLeft   'default value for TextRange object
                Case StringAlignment.Center
                    PPA = PowerPoint.PpParagraphAlignment.ppAlignCenter
                Case StringAlignment.Far
                    PPA = PowerPoint.PpParagraphAlignment.ppAlignRight
            End Select

            With ppp.Shapes.AddTextbox(Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 100, 100)
                PPFillText(.TextFrame, ToString, PPA)
                .Width = .TextFrame.TextRange.BoundWidth * 2  'OK for this to be more than needed, cos of way alignment works.
                'Making the textframe's autosize property doesn't seem to help.
                .Height = .TextFrame.TextRange.BoundHeight 'I think that using the actual size here is OK, but need to watch.
                'AutoSize property doesn't seem to work - it seems that all textframes within a group have _
                'to have the same setting for this, but I don't want parties to be autosized to their text - _
                'they look too small.

                If degs = -999 Then     'for rotated text which I draw with tranches.
                    Dim jk As SizeF = New SizeF(.Width, .Height)
                    Dim PPRect As Rectangle = GetTextRect(R, ss, jk)
                    .Left = PPRect.Left
                    .Top = PPRect.Top
                Else
                    .Width = Hypot(Point.Truncate(P1), Point.Truncate(P2))
                    .Height = TrancheWidth(Nothing)
                    .Rotation = degs
                    .Left = (P1.X + P2.X) / 2
                    .Top = (P1.Y + P2.Y) / 2

                    Select Case DirectionFromLine
                        Case Bearing.North
                            .IncrementLeft(-.Width / 2)
                            .IncrementTop(-.Height)
                        Case Bearing.South
                            .IncrementLeft(-.Width / 2)
                        Case Bearing.East
                            'Powerpoint does something funny with repositioning as part of the rotation here. Following seems to work OK.
                            .IncrementLeft(-.Width / 2 + .Height / 2)
                            .IncrementTop(-.Height / 2)
                        Case Bearing.West
                            .IncrementLeft(-.Width / 2 - .Height / 2)
                            .IncrementTop(-.Height / 2)
                    End Select
                    .TextFrame.WordWrap = Core.MsoTriState.msoFalse
                End If

                'Prefer not to set the background white, because, although it's possible that the shape _
                'could overlap a line, it's hard for me to set its height correctly, so effect is not good.
                .TextFrame.TextRange.Font.Color.RGB = TextColour.ToArgb

                'Return the name of the textbox for grouping with a securityarc.Not for connections because
                'I can never group a connector with anything because then that takes priority over _
                'its connection and if I move the object it's connected to, connection will be lost.
                'And I don't want to group text with an Entity as it won't keep its position in _
                'relation to the Connector.
                ppptextname = .Name

            End With

        End If

    End Sub

    Private Function GetTextRect(ByVal R As RectangleF, ByVal ss As System.Drawing.StringFormat, ByVal hj As SizeF) As Rectangle

        'Find the position of a text rectangle of given size using a given aligment in a given rectangle.

        Dim RL As Integer
        Select Case ss.Alignment
            Case StringAlignment.Near
                RL = R.Left
            Case StringAlignment.Center
                RL = R.Left + R.Width / 2 - hj.Width / 2
            Case StringAlignment.Far
                RL = R.Right - hj.Width
        End Select

        Dim RT As Integer
        Select Case ss.LineAlignment
            Case StringAlignment.Near
                RT = R.Top
            Case StringAlignment.Center
                RT = R.Top + R.Height / 2 - hj.Height / 2
            Case StringAlignment.Far
                RT = R.Bottom - hj.Height
        End Select

        Return New Rectangle(RL, RT, hj.Width, hj.Height)

    End Function

    Function TrancheWidth(ByVal gpcs As Graphics) As Integer

        'get the width of the gap between tranches. This needs to be a bit more than the height of a line of text.
        Dim p As Boolean
        If gpcs Is Nothing Then
            gpcs = Parent.Parent.CreateGraphics
            p = True
        End If
        TrancheWidth = gpcs.MeasureString("ABC", BaseLine.ff, 1000).Height * 1.33 'length of the projection. Needs to be enough to fit a line of text in.
        If p Then gpcs.Dispose()

    End Function

    Friend Sub ExtendUsedAreaRect(ByRef R As Rectangle)
        'Extend the used area to cover the path and text for this baseline.
        If Not Path Is Nothing Then
            If Path.PointCount > 1 Then   '>0 would be OK too.

                R = Rectangle.Union(R, Rectangle.Truncate(Path.GetBounds))

                If TextRect.HasValue Then

                    With TextRect.Value
                        ExtendRectangleForPoint(R, GetPointInCanvas(.Left, .Top))
                        ExtendRectangleForPoint(R, GetPointInCanvas(.Left, .Bottom))
                        ExtendRectangleForPoint(R, GetPointInCanvas(.Right, .Top))
                        ExtendRectangleForPoint(R, GetPointInCanvas(.Right, .Bottom))
                    End With
                End If

            End If
        End If
    End Sub

    Private Function GetPointInCanvas(ByVal X As Integer, ByVal Y As Integer) As Point

        Dim Q(0) As PointF
        Q(0) = New Point(X, Y)

        TextTransform.TransformPoints(Q)

        Return Point.Truncate(Q(0))

    End Function

    Private Sub ExtendRectangleForPoint(ByRef RR As Rectangle, ByVal P As Point)
        RR = Rectangle.Union(RR, New Rectangle(P.X, P.Y, 0, 0))
    End Sub

    Friend Sub SetArrowStyle(ByVal pc As PowerPoint.LineFormat, Optional ByVal bNoHeads As Boolean = False)

        SetLineWidthAndStyle(, pc)  'seems to need to be first or pass-through arrowheads are too big.

        If Not bNoHeads Then
            With pc

                'Might be some need to make arrows larger when entities appear larger, but the following seems to be OK at most sizes.

                .EndArrowheadStyle = EndArrowHeadStyle()
                .BeginArrowheadStyle = BeginArrowHeadStyle()

                If .Weight > 2 Then
                    .EndArrowheadWidth = Core.MsoArrowheadWidth.msoArrowheadNarrow
                    .EndArrowheadLength = Core.MsoArrowheadLength.msoArrowheadLengthMedium
                Else
                    .EndArrowheadWidth = Core.MsoArrowheadWidth.msoArrowheadWidthMedium
                    If TypeOf Me.Inst Is CashPayment Then
                        .EndArrowheadLength = Core.MsoArrowheadLength.msoArrowheadLengthMedium
                    Else
                        .EndArrowheadLength = Core.MsoArrowheadLength.msoArrowheadLong
                    End If
                End If

            End With
        End If

    End Sub
    Friend Overridable Function BeginArrowHeadStyle() As Core.MsoArrowheadStyle
        Return Core.MsoArrowheadStyle.msoArrowheadNone
    End Function
    Friend Overridable Function EndArrowHeadStyle() As Core.MsoArrowheadStyle
        Return Core.MsoArrowheadStyle.msoArrowheadTriangle
    End Function

    Shared Sub DrawArrowHead(ByVal gpcs As Graphics, ByVal clr As Color, ByVal ArrowHeadSource As PointF, ByVal ArrowHeadPoint As PointF, ByRef BasePoint As Point, ByVal bOutline As Boolean, Optional ByVal ArrowHeadSize As Integer = 10)

        'Shared because for swap I draw two arrowheads so hard to make this sufficiently general. That means I need to send the 
        'colour here rather than use LinePen.

        Dim DX As Long = ArrowHeadSource.X - ArrowHeadPoint.X
        Dim DY As Long = ArrowHeadSource.Y - ArrowHeadPoint.Y

        Dim L As Single = Hypot(DX, DY) 'Having decimal accuracy is necessary here. Otherwise if DX and DY are both 1,
        'L will be 1 when it should be 1.41, and you get an oversized arrow

        If L <> 0 Then
            Dim PF(2) As System.Drawing.PointF
            PF(0) = ArrowHeadPoint
            Dim Multiplier As Single = ArrowHeadSize / L
            BasePoint = New Point(PF(0).X + Multiplier * DX, PF(0).Y + Multiplier * DY)
            Multiplier = Multiplier * 0.4
            For n As Integer = 1 To 2
                Multiplier = -Multiplier
                PF(n) = Point.Add(BasePoint, New Size(Multiplier * DY, -Multiplier * DX))
            Next
            If bOutline Then
                gpcs.DrawPolygon(New Pen(clr), PF)
            Else
                gpcs.FillPolygon(New SolidBrush(clr), PF)
            End If

        End If

    End Sub

    Event BaseLineDeleted(ByVal BL As BaseLine) 'send a message up to the parent so it can raise an event and tell all other baselines that this one has been deleted.
    Friend Overridable Sub OtherBaseLineDeleted(ByVal BL As BaseLine)
        'Gives this baseline the opportunity to delete itself in response to another baseline being deleted.
    End Sub

    Protected Overridable Sub EntityDeleted(ByVal ent As EntityControl)
        'Gives this baseline the opportunity to delete itself in response to an entity being deleted.
    End Sub

    Friend Sub AddToListIfType(ByVal aa As List(Of INameObject), ByVal tp As Type)

        If Not Inst Is Nothing Then If Inst.GetType Is tp Then aa.Add(Inst)

    End Sub

End Class

Friend Class BaseLineDisplayer

    'For selecting Instruments through the grid. This has the undesired effect of not displaying the properties in grey, even though they are readonly, but never mind.
    Inherits System.Drawing.Design.UITypeEditor

    Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle
        Return System.Drawing.Design.UITypeEditorEditStyle.Modal  'Show the ellipsis. I'd rather show an arrow or something if I could, but I can't figure out how to do it.
    End Function

    Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

        'To do this I have had to make BaseLine public. Doesn't matter because all its methods are still Friend. Properties just relate to the BaseLine rather than any inheriting class so that I only 
        'need to make BaseLine public. Alternative is to have a different type of TypeEditor for each property that wants to do this, 
        'and find the appropriate BaseLine from the context.Instance.
        Dim bl As BaseLine = TryCast(value, BaseLine)
        If Not bl Is Nothing Then    'shouldn't be
            bl.Parent.SelectedBLs.Clear()
            CurrentBL = value
            SelectIEP(frmMDI.GridItemsCol)
        End If

    End Function

    Private Sub SelectIEP(ByVal gi As GridItemCollection)
        For Each g As GridItem In gi
            If Not g.PropertyDescriptor Is Nothing Then If g.PropertyDescriptor.Attributes.Item(GetType(IEPAttribute)) IsNot Nothing Then g.Select()
            SelectIEP(g.GridItems)
        Next
    End Sub

End Class