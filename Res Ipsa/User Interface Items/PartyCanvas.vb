Imports System.Math
Imports System.Reflection
Imports Microsoft.Office.Interop
Imports Microsoft.Office

'Have this as a separate class so I can put one in frmOptions.

<Serializable()> Public Class PartyCanvas

    Inherits PictureBox

    Friend CanvasDownPoint As Nullable(Of Point)    'Point on picCanvas where mouse went down, for rubberbanding.

    Friend Dirty As Boolean     'Whether changes have been made, so in need of saving.

    Private components As System.ComponentModel.IContainer      'Added automatically to deal with the context menu strip.
    Friend WithEvents mniDummy1 As System.Windows.Forms.ToolStripMenuItem

    Friend WithEvents cmsEdit As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniFormatAlign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument

    Friend WithEvents cmsPrintArea As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuFilePrintAreaShowHide As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFilePrintAreaReset As System.Windows.Forms.ToolStripMenuItem

    Friend WithEvents cmsInstrument As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniInstrumentConvert As System.Windows.Forms.ToolStripMenuItem
    Friend zspMatchedFunding As ToolStripSeparator
    Friend WithEvents mniMatchedFunding As System.Windows.Forms.ToolStripMenuItem
    Friend zspInstrumentDelete As ToolStripSeparator
    Friend WithEvents mniInstrumentDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsBaseline As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniBaseLineDelete As System.Windows.Forms.ToolStripMenuItem

    Private bMenuDismiss As Boolean     'Flag to resolve the problem that collateral starts being drawn if a menu is dismissed without any item being selected.

    'Following properties are only set to non-default values for powerpoint slide creation.
    Friend ScaleFactor As Single = 1
    Friend ScaleXZero As Integer = 0
    Friend ScaleYZero As Integer = 0

    Friend WithEvents mnuEditSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsMatchedFunding As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mniInstrumentInclude As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniInstrumentExclude As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents zspIncludeExclude As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuEditCopy As System.Windows.Forms.ToolStripMenuItem

    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.mniDummy1 = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsEdit = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mniFormatAlign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditSelectAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditCopy = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.cmsPrintArea = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuFilePrintAreaShowHide = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilePrintAreaReset = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsInstrument = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mniInstrumentConvert = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsBaseline = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.zspMatchedFunding = New System.Windows.Forms.ToolStripSeparator
        Me.mniMatchedFunding = New System.Windows.Forms.ToolStripMenuItem
        Me.zspInstrumentDelete = New System.Windows.Forms.ToolStripSeparator
        Me.mniBaseLineDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mniInstrumentDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.zspIncludeExclude = New System.Windows.Forms.ToolStripSeparator
        Me.mniInstrumentInclude = New System.Windows.Forms.ToolStripMenuItem
        Me.mniInstrumentExclude = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsMatchedFunding = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsEdit.SuspendLayout()
        Me.cmsPrintArea.SuspendLayout()
        Me.cmsInstrument.SuspendLayout()
        Me.cmsBaseline.SuspendLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mniDummy1
        '
        Me.mniDummy1.Name = "mniDummy1"
        Me.mniDummy1.Size = New System.Drawing.Size(120, 22)
        Me.mniDummy1.Text = "Dummy"
        '
        'cmsEdit
        '
        Me.cmsEdit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mniFormatAlign, Me.mnuEditSelectAll, Me.mnuEditCopy})
        Me.cmsEdit.Name = "cmsFormat"
        Me.cmsEdit.Size = New System.Drawing.Size(206, 70)
        '
        'mniFormatAlign
        '
        Me.mniFormatAlign.Image = Global.LegalObjectsModel.My.Resources.Resources.Align
        Me.mniFormatAlign.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mniFormatAlign.Name = "mniFormatAlign"
        Me.mniFormatAlign.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.mniFormatAlign.Size = New System.Drawing.Size(205, 22)
        Me.mniFormatAlign.Text = "Ali&gn All"
        '
        'mnuEditSelectAll
        '
        Me.mnuEditSelectAll.Name = "mnuEditSelectAll"
        Me.mnuEditSelectAll.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.mnuEditSelectAll.Size = New System.Drawing.Size(205, 22)
        Me.mnuEditSelectAll.Text = "Select &All Entities"
        '
        'mnuEditCopy
        '
        Me.mnuEditCopy.Image = Global.LegalObjectsModel.My.Resources.Resources.Copy
        Me.mnuEditCopy.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mnuEditCopy.Name = "mnuEditCopy"
        Me.mnuEditCopy.Size = New System.Drawing.Size(205, 22)
        Me.mnuEditCopy.Text = "&Copy Image"
        '
        'PrintDocument1
        '
        '
        'cmsPrintArea
        '
        Me.cmsPrintArea.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFilePrintAreaShowHide, Me.mnuFilePrintAreaReset})
        Me.cmsPrintArea.Name = "cmsPrintArea"
        Me.cmsPrintArea.Size = New System.Drawing.Size(210, 48)
        '
        'mnuFilePrintAreaShowHide
        '
        Me.mnuFilePrintAreaShowHide.CheckOnClick = True
        Me.mnuFilePrintAreaShowHide.Enabled = False
        Me.mnuFilePrintAreaShowHide.Image = Global.LegalObjectsModel.My.Resources.Resources.PrintArea
        Me.mnuFilePrintAreaShowHide.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mnuFilePrintAreaShowHide.Name = "mnuFilePrintAreaShowHide"
        Me.mnuFilePrintAreaShowHide.Size = New System.Drawing.Size(209, 22)
        Me.mnuFilePrintAreaShowHide.Text = "Show/Hide Print &Area"
        '
        'mnuFilePrintAreaReset
        '
        Me.mnuFilePrintAreaReset.Name = "mnuFilePrintAreaReset"
        Me.mnuFilePrintAreaReset.Size = New System.Drawing.Size(209, 22)
        Me.mnuFilePrintAreaReset.Text = "&Reset to Page Dimensions"
        '
        'cmsInstrument
        '
        Me.cmsInstrument.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mniInstrumentConvert, Me.mniInstrumentDelete, Me.zspIncludeExclude, Me.mniInstrumentInclude, Me.mniInstrumentExclude})
        Me.cmsInstrument.Name = "cmsInstrument"
        Me.cmsInstrument.Size = New System.Drawing.Size(136, 98)
        '
        'mniInstrumentConvert
        '
        Me.mniInstrumentConvert.DropDown = Me.cmsBaseline
        Me.mniInstrumentConvert.Name = "mniInstrumentConvert"
        Me.mniInstrumentConvert.Size = New System.Drawing.Size(135, 22)
        Me.mniInstrumentConvert.Text = "Change to"
        '
        'cmsBaseline
        '
        Me.cmsBaseline.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.zspMatchedFunding, Me.mniMatchedFunding, Me.zspInstrumentDelete, Me.mniBaseLineDelete})
        Me.cmsBaseline.Name = "cmsBaseline"
        Me.cmsBaseline.ShowItemToolTips = False
        Me.cmsBaseline.Size = New System.Drawing.Size(168, 60)
        '
        'zspMatchedFunding
        '
        Me.zspMatchedFunding.Name = "zspMatchedFunding"
        Me.zspMatchedFunding.Size = New System.Drawing.Size(164, 6)
        '
        'mniMatchedFunding
        '
        Me.mniMatchedFunding.Name = "mniMatchedFunding"
        Me.mniMatchedFunding.Size = New System.Drawing.Size(167, 22)
        Me.mniMatchedFunding.Text = "Matched Funding"
        '
        'zspInstrumentDelete
        '
        Me.zspInstrumentDelete.Name = "zspInstrumentDelete"
        Me.zspInstrumentDelete.Size = New System.Drawing.Size(164, 6)
        '
        'mniBaseLineDelete
        '
        Me.mniBaseLineDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Italic)
        Me.mniBaseLineDelete.Image = Global.LegalObjectsModel.My.Resources.Resources.Delete
        Me.mniBaseLineDelete.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mniBaseLineDelete.Name = "mniBaseLineDelete"
        Me.mniBaseLineDelete.Size = New System.Drawing.Size(167, 22)
        Me.mniBaseLineDelete.Text = "Delete"
        '
        'mniInstrumentDelete
        '
        Me.mniInstrumentDelete.Image = Global.LegalObjectsModel.My.Resources.Resources.Delete
        Me.mniInstrumentDelete.ImageTransparentColor = System.Drawing.Color.Fuchsia
        Me.mniInstrumentDelete.Name = "mniInstrumentDelete"
        Me.mniInstrumentDelete.Size = New System.Drawing.Size(135, 22)
        Me.mniInstrumentDelete.Text = "Delete"
        '
        'zspIncludeExclude
        '
        Me.zspIncludeExclude.Name = "zspIncludeExclude"
        Me.zspIncludeExclude.Size = New System.Drawing.Size(132, 6)
        '
        'mniInstrumentInclude
        '
        Me.mniInstrumentInclude.Name = "mniInstrumentInclude"
        Me.mniInstrumentInclude.Size = New System.Drawing.Size(135, 22)
        Me.mniInstrumentInclude.Text = "Include"
        '
        'mniInstrumentExclude
        '
        Me.mniInstrumentExclude.Name = "mniInstrumentExclude"
        Me.mniInstrumentExclude.Size = New System.Drawing.Size(135, 22)
        Me.mniInstrumentExclude.Text = "Exclude"
        '
        'cmsMatchedFunding
        '
        Me.cmsMatchedFunding.Name = "cmsMatchedFunding"
        Me.cmsMatchedFunding.Size = New System.Drawing.Size(61, 4)
        '
        'PartyCanvas
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.cmsEdit.ResumeLayout(False)
        Me.cmsPrintArea.ResumeLayout(False)
        Me.cmsInstrument.ResumeLayout(False)
        Me.cmsBaseline.ResumeLayout(False)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Public Sub New()
        MyBase.New()
        InitializeComponent()

        'I want to mark those toolstrip items which are created at design time so they don't get deleted before I add in all the instrument types.
        For Each i As ToolStripItem In Me.cmsBaseline.Items
            i.Tag = "x"
        Next

        Me.BackColor = My.Settings.CanvasBackColour 'can put this in InitializeComponent, but then get error if I try to view designer.
        Me.PrintDocument1.DefaultPageSettings.Landscape = True    'default

        SetMarginsToPrintArea()  'not correct by default

        mniMatchedFunding.DropDown = cmsMatchedFunding    'don't set at design time or cmsBaseLine will appear at top left of screen first time it is shown.

    End Sub

    Friend Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)

    End Sub
#Region "Entities"
    Friend Function AddEC(ByVal P As Point, ByVal AddHandlers As Boolean, Optional ByVal Entity1 As Object = Nothing) As EntityControl

        Dim bNewEntity As Boolean
        If Entity1 Is Nothing Then
            Entity1 = New Entity
            bNewEntity = True
        End If

        Dim EC As EntityControl = New EntityControl(Entity1)

        If bNewEntity Then
            'I want to set the name before adding the control into the canvas, as otherwise if the user puts it to the 
            'right, it can overlap the right edge until the name changes. The horizontal scrollbar appears (and remains) 
            'unneccesarily.
            Dim aa As New List(Of INameObject)
            aa.Add(Entity1)
            For Each fc As EntityControl In Me.EntityControls
                If TypeOf fc.LEntity Is INameObject Then aa.Add(fc.LEntity)
            Next

            EC.rtbName.Text = GetName(aa, Entity1, Entity1.GetType)

        End If

        EC.rtbNameSet = True

        With EC
            .Location = P
            AddHandler .Reformat, AddressOf Me.ReformatHandler
            .Visible = False    'otherwise it will appear at design time size for an instant.

            Controls.Add(EC)
            .SetZOrder()

            'TODO Make this the default jurisdiction.
            'Get an error I don't understand if I do this for an existing entity. Need to set elsewhere.
            If bNewEntity Then .LEntity.Incorp = JurisdictionSpace.Jurisdictions.Item(PDJ.UKEngWales)

            .Visible = True
            Refresh()

            'Possible to add a party to the non active form. Form ought to be activated in this case.
            If TypeOf Me.Parent Is Form Then CType(Me.Parent, Form).Activate()

        End With

        If AddHandlers Then
            AddHandler EC.MouseMove, AddressOf GlobalMouseMoveHandler
            AddHandler CanvasMouseMove, AddressOf EC.TestCursor
        End If

        EC.jcbJur.ClearSelection()  'setting name seems to reselect all of jurisdiction text so need to do this at very end.

        frmMDI.SetDrawButtonEnablement()

        Dirty = True

        Refresh()

        Return EC

    End Function
    Event EntityRemoved(ByVal ent As EntityControl)

    Private Sub PartyCanvas_ControlRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles Me.ControlRemoved

        If TypeOf e.Control Is EntityControl Then RaiseEvent EntityRemoved(e.Control)

    End Sub

    Friend Function EntityControls() As List(Of EntityControl)

        'This saves me from doing For Each p as EntityControl in Controls which is a bit risky in case something else added there.
        EntityControls = New List(Of EntityControl)
        For Each c As Control In Me.Controls
            If TypeOf c Is EntityControl Then EntityControls.Add(c)
        Next
        Return EntityControls

    End Function

    Private Sub mnuEditSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEditSelectAll.Click

        For Each c As EntityControl In EntityControls()
            c.Selected = True
        Next
        frmMDI.SetPRGObjects()

    End Sub

#End Region

#Region "BaseLine Storage"

    Friend SelectedBLs As New SelectedCol  'collection of baselines which are selected in this canvas.

    ''' <remarks>Serialization Object</remarks>
    ''' I did have these in a nested class, but it made the code longer and meant that I couldn't show these relationships in the class diagram.
    ''' The only advantage was that serialisation was simpler.
    Friend SecurityCollection As New List(Of SecurityArc)
    ''' <remarks>Serialization Object</remarks>
    Friend CredSupCollection As New List(Of CreditSupportCon)
    ''' <remarks>Serialization Object</remarks>
    Friend StanConCols As New List(Of TrancheCollection)

    Friend Sub UnpackSerialData()

        For Each s As SecurityArc In Me.SecurityCollection
            s.Parent = Me
            s.MakeReal()
        Next
        For Each c As CreditSupportCon In Me.CredSupCollection
            c.Parent = Me
            c.MakeReal()
        Next
        For Each k As TrancheCollection In Me.StanConCols
            For Each j As Connection In k
                j.Parent = Me
                j.MakeReal()
            Next
        Next

    End Sub

    Friend Function GetStanConColl(ByVal F As Entity, ByVal T As Entity, ByVal ty As Type) As TrancheCollection
        For Each h As TrancheCollection In Me.StanConCols
            If h.FromEntity Is F And h.ToEntity Is T And h.BaseType Is ty Then Return h
        Next
    End Function

    Friend Sub DrawLines(ByVal gp As Graphics, ByVal ppp As PowerPoint.Slide)

        For Each c As TrancheCollection In Me.StanConCols
            c.DrawLines(gp, ppp)
            DoPPEvents(ppp)
        Next

        For Each c2 As CreditSupportCon In Me.CredSupCollection
            c2.Draw(gp, Nothing, ppp)
        Next

        For Each sa As SecurityArc In SecurityCollection
            sa.Draw(gp, New Point, ppp)
        Next

    End Sub

    Friend Function GetSecArc(ByVal collat As Line, ByVal SecuredCon As Line, Optional ByVal insttype As Type = Nothing) As SecurityArc
        'Find a SecurityArc over collat in support of SecuredCon. Maybe I should have a SecurityCollection object and have this as a function.
        For Each s As SecurityArc In SecurityCollection
            If TypeOf s Is LineArc Then
                Dim p As LineArc = s

                If p.FromConnection Is collat And p.FromConnection Is SecuredCon Then
                    If insttype Is Nothing Then
                        Return s
                    Else
                        If insttype Is s.GetType Then Return s
                    End If
                End If
            End If
        Next
    End Function

#End Region

#Region "Canvas Size"
    Private Sub PartyCanvas_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles Me.ControlAdded

        If TypeOf e.Control Is EntityControl Then

            mnuFilePrintAreaShowHide.Enabled = True
            RaiseEvent Reformat()

            SetPrintArea(True)
            Refresh()


            Dirty = True
        End If
    End Sub
    Event GettingUsedArea(ByRef R As Rectangle)

    Friend Function GetUsedArea(Optional ByVal IgnorePrintArea As Boolean = False) As Rectangle

        'Gets a rectangle which represents where the contents of picCanvas are.
        'Used for PowerPoint, printing and setting canvas size. Co-ordinates are in the frame of 
        'reference of the canvas, not its parent.

        Dim R As New Rectangle
        Dim ee As List(Of EntityControl) = EntityControls()

        If ee.Count <> 0 Then R = ee.Item(0).Bounds 'otherwise left and top could be stuck at zero.

        RaiseEvent GettingUsedArea(R)

        'Ensure the print area is included if shown.
        If ShowPrintArea() And Not IgnorePrintArea Then R = Rectangle.Union(PrintArea, R)

        'Allow an extra pixel on each side so that it's clear that a control is completely within the screen.
        R = New Rectangle(R.Left - 1, R.Top - 1, R.Width + 2, R.Height + 2)

        Return R

    End Function

    Event Reformat()
    Friend Sub ReformatHandler()
        RaiseEvent Reformat()
    End Sub

#End Region

#Region "Canvas Style"

    Friend Sub SetControlColour(ByVal Clr As System.Drawing.Color)

        For Each f As EntityControl In EntityControls()
            f.BackColor = Clr
            f.Refresh()
        Next
        Refresh()

    End Sub

    Friend Sub SetControlStyle(ByVal Flat As Boolean)

        For Each f As EntityControl In EntityControls()
            f.SetStylex(Flat)
        Next
        Refresh()

    End Sub
#End Region

#Region "Drawing"

    Friend Sub PartyCanvas_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        'Will get an error at this point if powerpoint dll is not installed on user's computer - deployment issue.
        Me.Draw(e.Graphics)
    End Sub

    Friend Function GetGraphics() As Graphics
        'Create an image to persist

        Image = New Bitmap(Width, Height)

        Return Graphics.FromImage(Image)

    End Function

    Friend Sub Draw(Optional ByVal gp As Graphics = Nothing, Optional ByVal PrintDoc As Boolean = False, Optional ByVal ppp As PowerPoint.Slide = Nothing, Optional ByVal bCopy As Boolean = False)

        'important here cos if drawing fails, user will just see the control white with a red cross and border.
        On Error Resume Next
        If PrintDoc Or bCopy Then
            gp.DrawRectangle(Pens.Black, PrintArea)
            gp.Clip = New Region(PrintArea) 'Don't draw anything outside print area.
        End If

        For Each U As EntityControl In EntityControls()

            If Not ppp Is Nothing Then
                U.AddPPShape(ppp)
                DoPPEvents(ppp)
            Else
                If Not TypeOf Parent Is frmBase Then
                    U.DrawControlToBitmap(gp, U.Left, U.Top - Height)
                End If
            End If
        Next

        For Each U As EntityControl In EntityControls()
            U.ArcsBottom = 1
            U.ArcsLeft = 1
            U.ArcsRight = 1
            U.ArcsTop = 1
        Next

        DrawLines(gp, ppp)

        If PrintDoc Or bCopy Then
            'Important to do this last so any baselines drawn under a party or overlapping parts of parties are obscured.
            For Each U As EntityControl In EntityControls()
                U.Draw(gp)
            Next
        End If

        DoPPEvents(ppp)

        'Draw the print area if appropriate.
        If TypeOf Me.Parent Is frmBase And Not PrintDoc And Not bCopy And ppp Is Nothing And ShowPrintArea() Then DrawPrintArea(gp)

        'Haven't included this for a simple copy of the image. Had problems getting the scale right, and I 
        'think the user would need the option to turn it on and off which seems unecessary bother from the point of 
        'view of the visual interface.
        If ShowTitleWhenPrinting And PrintDoc Then
            'If the size of the print area is small, I don't want really big text for the title. In particular there's a risk that this 
            'could overlap an entity. So take the transform off and add
            'the title at a standard size at the foot of the rectangle. This seems to be OK even if there's just one entity in the diagram.
            Dim ss As New System.Drawing.StringFormat
            ss.LineAlignment = StringAlignment.Far
            ss.Alignment = StringAlignment.Center

            Dim j As Single() = gp.Transform.Elements
            gp.ResetTransform()

            Dim PA2 As Rectangle = New Rectangle(PrintArea.Left * j(0) + j(4), PrintArea.Top * j(3) + j(5), PrintArea.Width * j(0), PrintArea.Height * j(3) - 8)
            gp.DrawString(PrintDocument1.DocumentName, New System.Drawing.Font("Arial", 16), Brushes.Black, PA2, ss)

        End If

    End Sub
#End Region

#Region "cmsBaseLine"
    Private Sub cmsBaseline_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsBaseline.Opening

        Dim q As New List(Of ToolStripItem)
        Me.SelectedBLs.Clear()
        CurrentBL = MouseOverBL    'am not selecting by right click so need to do this.

        With cmsBaseline

            Dim bConvertOptions As Boolean  'whether this is being shown as options to convert the existing instrument to.
            bConvertOptions = Not .OwnerItem Is Nothing 'alternative is for it to be cmsInstrument in which case I want them visible.
            'Note that OwnerItem and DropDown properties are not complementary. The former is to do with what is actually displayed, the 
            'latter is to do with what is set at design time. e.g. a menu can be shown as a context menu in its own right while 
            'still being the dropdown menu of another menu. In this case the parent menu is not the "owner" of the sub menu even though 
            'the sub menu is its dropdown.

            For Each h As ToolStripItem In .Items
                If TypeOf h.Tag Is String Then
                    h.Visible = Not bConvertOptions
                Else
                    q.Add(h)
                End If
            Next

            For Each k As ToolStripItem In q
                .Items.Remove(k)
            Next

            Cursor = Cursors.Default 'in case user moves mouse off menu while it's showing.
            FillToolStripWithInheritors(.Items, .Tag)

            If .Tag Is GetType(PassThrough) Then
                Me.mniMatchedFunding.Visible = True
                Me.zspMatchedFunding.Visible = True
                FillToolStripWithInheritors(Me.cmsMatchedFunding.Items, GetType(Debt))  'do this before cmsMatchedFunding.Opening so that arrow appears against mniMatchedFunding from the start.
                If Not .OwnerItem Is Me.mniInstrumentConvert Then
                    .Items.Add(Me.zspIncludeExclude)
                    .Items.Add(Me.mniInstrumentInclude)
                    .Items.Add(Me.mniInstrumentExclude)
                End If
            Else
                Me.mniMatchedFunding.Visible = False
                Me.zspMatchedFunding.Visible = False
            End If

            For Each mni As ToolStripItem In .Items
                If TypeOf mni Is ToolStripMenuItem Then
                    If Not CurrentBL.Inst Is Nothing Then
                        If mni.Tag Is CurrentBL.Inst.GetType Then
                            If bConvertOptions Then
                                mni.Enabled = False 'can't convert something to what it already is.
                            Else
                                CType(mni, ToolStripMenuItem).Checked = True    'show what has already been selected.
                            End If
                        End If

                    End If
                End If
            Next

        End With

    End Sub

    Private Sub cmsBaseline_Closed(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripDropDownClosedEventArgs) Handles cmsBaseline.Closed
        bMenuDismiss = True
        If cmsBaseline.Items.Contains(Me.zspIncludeExclude) Then
            With Me.cmsInstrument.Items
                .Add(Me.zspIncludeExclude)
                .Add(Me.mniInstrumentInclude)
                .Add(Me.mniInstrumentExclude)
            End With

        End If
    End Sub

    Private Sub mniBaseLineDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniBaseLineDelete.Click

        If Not CurrentBL Is Nothing Then
            CurrentBL.Delete()
            CurrentBL = Nothing
            Refresh()
            frmMDI.SetDrawButtonEnablement()
        End If

    End Sub

    Private Sub cmsBaseLine_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsBaseline.ItemClicked, cmsMatchedFunding.ItemClicked

        'Create the new instrument and send it as an argument to the MechanismItemClicked event.

        If TypeOf e.ClickedItem.Tag Is Type And Not CurrentBL Is Nothing Then
            Dim converting As Boolean = cmsBaseline.OwnerItem Is mniInstrumentConvert

            Dim OldObj As Instrument = CurrentBL.Inst

            Dim t As Type = e.ClickedItem.Tag

            If t.IsSubclassOf(GetType(Instrument)) Then

                Dim NewObj As Instrument = Activator.CreateInstance(t)
                'Should I ever want to pass parameters to the constructor, here's the code I'll need.
                '(t, Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic, Nothing, New Object() {param1, param2}, Nothing)

                Dim sGivenName As String = NewObj.Name

                'Loop to find the first shared parent type of t and source.
                Dim sharedbaseclass As Type = t
                Do Until sharedbaseclass.BaseType Is Nothing Or CurrentBL.Inst.GetType.IsSubclassOf(sharedbaseclass)
                    sharedbaseclass = sharedbaseclass.BaseType
                Loop

                'I think that fields are enough, but I'd better give this more consideration.
                Do Until sharedbaseclass Is GetType(Object) 'loop through inherited classes. Using the FlattenHierarchy flag 
                    'is no good since this only returns public fields. 

                    'Loop through each field in the shared base class and copy the value from
                    'the source object to the new object.
                    For Each field As Reflection.FieldInfo In sharedbaseclass.GetFields(Reflection.BindingFlags.NonPublic Or Reflection.BindingFlags.DeclaredOnly Or Reflection.BindingFlags.Instance)
                        If converting Or field.GetCustomAttributes(GetType(BaseLine), True) IsNot Nothing Then
                            field.SetValue(NewObj, field.GetValue(OldObj))
                        End If
                    Next

                    sharedbaseclass = sharedbaseclass.BaseType
                Loop
                'If the user changed the name himself then I want to copy the name over to the NewObj.
                ' This will happen above. But if he didn't then I want to revert to my Auto generated name.
                If Not CurrentBL.Inst.bUserNameSet Then
                    NewObj.Name = sGivenName
                    NewObj.bUserNameSet = False   'cos above will make it True.
                End If

                If Not converting And sender Is cmsMatchedFunding Then 'should have security for it and should be limited recourse.
                    Dim ec As Connection = TryCast(CurrentBL, Connection)
                    If Not ec Is Nothing Then    'should'nt be
                        Dim prev As Connection = CType(ec.Inst, PassThrough).parent.Underlyings.Item(0).Underlying 'think I have to insist that there's only one item here.
                        If GetSecArc(prev, CurrentBL, GetType(FixedCharge)) Is Nothing Then
                            Dim s As SecurityArc = New LineArc(Me, prev, False, False)
                            s.ToConnection = CurrentBL
                            s.MakeReal()
                            s.Inst = New FixedCharge()
                        End If
                        NewObj.AddTerms = "Recourse limited to " & prev.ToString

                    End If
                End If

                CurrentBL.Inst = NewObj

            End If
        End If

    End Sub

    Friend Event BaseLineDeletedEvent(ByVal BL As BaseLine)
    Friend Sub BaseLineDeleted(ByVal BL As BaseLine)
        RaiseEvent BaseLineDeletedEvent(BL)
    End Sub
#End Region

#Region "cmsInstrument"

    Private Sub cmsInstrument_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsInstrument.Opening
        Cursor = Cursors.Default
        Me.mniInstrumentConvert.Enabled = Me.mniInstrumentConvert.DropDown.Tag IsNot Nothing
        Me.SelectedBLs.Clear()
        CurrentBL = MouseOverBL    'am not selecting by right click so need to do this.
    End Sub
    Private Sub cmsInstrument_Closed(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripDropDownClosedEventArgs) Handles cmsInstrument.Closed
        bMenuDismiss = True
    End Sub

    Private Sub mniInstrumentDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniInstrumentDelete.Click
        If Not CurrentBL Is Nothing Then
            CurrentBL.Inst = Nothing
        End If
    End Sub
#End Region

#Region "Mouse Movement"

    'A big issue. Maybe I should have in frmBase, because this is of no relevance to frmOptions but I want to remove handlers from within the
    'EntityControls and they can only refer to this.
    Friend Event CanvasMouseMove(ByRef e As CursorEventArgs)   'Can make this as EventHandler(Of CursorEventArgs) to make it look like 
    'any other event in having a sender argument first. But I see no need.

    ' This class extends the MouseEventArgs and allows a handler to specify that
    ' a crosshair should be used.
    Public Class CursorEventArgs
        Inherits MouseEventArgs

        Friend OverBL As BaseLine
        Friend OverText As Boolean

        Friend OverControl As EntityControl

        Friend Sub New(ByVal e As MouseEventArgs)
            MyBase.New(e.Button, e.Clicks, e.X, e.Y, e.Delta)
        End Sub
    End Class

    Friend Sub picCanvas_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove

        'Don't want mouse to have any effect in the partycanvas control so maybe this should be in frmBase. No effects currently so 
        'won't bother moving it.

        Const conLineMargin As Integer = 5   'number of pixels away from a line that the cursor is considered to be 
        'over it, so that user gets feedback without the cursor having to be exactly on it.

        If e.Button = Windows.Forms.MouseButtons.None Then
            If frmMDI.tsiEntity.Checked Then Me.Cursor = frmMDI.crsAdd
            bMenuDismiss = False 'look for a mouse movement without the button down to show this isn't following on immediately from the dismissal of the menu.
        End If

        'When control key is down for selecting multiple baselines this could interfere with that. So don't do this if control key is down.

     If e.Button = Windows.Forms.MouseButtons.Left And MouseDownControl Is Me Then

            Select Case Cursor

                Case Cursors.SizeWE

                    PASizeEW(PASide(0), e.X)
                    DrawPrintArea(GetGraphics)

                Case Cursors.SizeNS

                    PASizeNS(PASide(1), e.Y)
                    DrawPrintArea(GetGraphics)

                Case Cursors.SizeNESW, Cursors.SizeNWSE

                    PASizeEW(PASide(0), e.X)
                    PASizeNS(PASide(1), e.Y)
                    DrawPrintArea(GetGraphics)

                Case Else

                    Dim ProposedCollateral As BaseLine = CurrentBL

                    If (frmMDI.tsiDrawSecurityExisting.Checked Or frmMDI.tsiDrawSecurityNew.Checked) And _
                    BLDrawing Is Nothing And TypeOf CurrentBL Is Line And _
                    ProposedCollateral IsNot MouseOverBL And Not ProposedCollateral Is Nothing _
                    And Not bMenuDismiss And Not Control.ModifierKeys = Keys.Control Then

                        If ProposedCollateral.Inst.ExistsPostCompletion Then
                            If ProposedCollateral.Existing Or frmMDI.tsiDrawSecurityNew.Checked Then
                                BLDrawing = New LineArc(Me, CurrentBL, frmMDI.tsiDrawSecurityExisting.Checked, True) 'mouse must have moved off SelectedBL to start drawing.

                                ContextMenuStrip = Nothing
                            End If
                        End If
                    End If

                    If CanvasDownPoint.HasValue Then

                        Dim f As New Pen(Color.Black)
                        f.DashStyle = Drawing2D.DashStyle.Dash

                        GetGraphics.DrawRectangle(f, RubberBand(e.Location))
                        f.Dispose()
                    End If

            End Select

        End If

        'If over the rectangle, show a cursor to reflect how the rectangle can be resized.
        Dim OverLeft As Boolean
        Dim OverTop As Boolean
        Dim OverRight As Boolean
        Dim OverBottom As Boolean

        If ShowPrintArea() And BLDrawing Is Nothing And Not CanvasDownPoint.HasValue Then
            With PrintArea
                If e.Y > .Top - conLineMargin And e.Y < .Bottom + conLineMargin Then
                    OverLeft = e.X > .Left - conLineMargin And e.X < .Left + conLineMargin
                    OverRight = e.X > .Right - conLineMargin And e.X < .Right + conLineMargin
                End If
                If e.X > .Left - conLineMargin And e.X < .Right + conLineMargin Then
                    OverTop = e.Y > .Top - conLineMargin And e.Y < .Top + conLineMargin
                    OverBottom = e.Y > .Bottom - conLineMargin And e.Y < .Bottom + conLineMargin
                End If

                If OverLeft Then PASide(0) = Line.Bearing.West
                If OverRight Then PASide(0) = Line.Bearing.East
                If OverTop Then PASide(1) = Line.Bearing.North
                If OverBottom Then PASide(1) = Line.Bearing.South

                If (OverLeft And OverTop) Or (OverRight And OverBottom) Then
                    Cursor = Cursors.SizeNWSE
                ElseIf (OverLeft And OverBottom) Or (OverRight And OverTop) Then
                    Cursor = Cursors.SizeNESW
                ElseIf OverLeft Or OverRight Then
                    Cursor = Cursors.SizeWE
                ElseIf OverTop Or OverBottom Then
                    Cursor = Cursors.SizeNS
                End If

                If OverTop Or OverBottom Or OverLeft Or OverRight Then ContextMenuStrip = Me.cmsPrintArea

            End With
        End If

        If Not (OverLeft Or OverTop Or OverRight Or OverBottom) Then GlobalMouseMoveHandler(sender, e)

    End Sub

    Friend Sub GlobalMouseMoveHandler(ByVal sender As Object, ByVal e As MouseEventArgs)

        'Handles mouse movement on picCanvas and also on controls.

        If TypeOf sender Is EntityControl And BLDrawing Is Nothing And sender Is MouseDownControl Then
            'Start drawing from control.

            If e.Button = Windows.Forms.MouseButtons.Left Then

                'Can use Activator.CreateInstance for this sort of thing to avoid repetition, but don't like way it's late bound.
                'This rather parallels EntityCursor in frmMDI. I have three things that are related - toolbuttons, cursors and types of BaseLine. _
                'Would be good to package them up somehow. But since I want to show the cursor before I actually create the
                'BaseLine, and I can't have a Shared Inheritable property, I don't think there's any alterntive.
                If frmMDI.tsiDrawCredSupNew.Checked Or frmMDI.tsiDrawCredSupExisting.Checked Then
                    BLDrawing = New CreditSupportCon(Me, True, sender, frmMDI.tsiDrawCredSupExisting.Checked)
                ElseIf frmMDI.tsiDrawFundingNew.Checked Or frmMDI.tsiDrawPassthroughNew.Checked Then
                    BLDrawing = New TransactionCon(Me, True, sender)
                ElseIf frmMDI.tsiDrawFundingExisting.Checked Or frmMDI.tsiDrawPassThroughExisting.Checked Then
                    BLDrawing = New ExistingCon(Me, True, sender)
                ElseIf frmMDI.tsiDrawSwapNew.Checked Or frmMDI.tsiDrawSwapExisting.Checked Then
                    BLDrawing = New SwapCon(Me, True, frmMDI.tsiDrawSwapExisting.Checked, sender)
                ElseIf frmMDI.tsiNonRepSec.Checked Then
                    BLDrawing = New EntityArc(Me, CType(sender, EntityControl).LEntity, False, True)
                End If

            End If

        Else

            If TypeOf sender Is EntityControl Then   'adjust e to piccanvas co-ordinates
                With CType(sender, Control)
                    e = New MouseEventArgs(e.Button, e.Clicks, .Left + e.X, .Top + e.Y, e.Delta)
                End With
            End If

            Dim EventArgs As New CursorEventArgs(e)

            ' Raise the event so that handlers can do their thing.
            RaiseEvent CanvasMouseMove(EventArgs)
            MouseOverBL = EventArgs.OverBL

            '** Build action of cursor must be "Embedded Resource" for the following to work.
            'Set that by selecting the cursor resource in the Solution Explorer window, not the Resources tab of the Project.

            If BLDrawing Is Nothing Then

                If Not frmMDI.tsiEntity.Checked Then    'should be add cursor
                    If EventArgs.OverBL Is Nothing Then
                        If sender Is Me Then Cursor = Cursors.Default
                        ContextMenuStrip = Me.cmsEdit
                    Else
                        Dim t As Type = EventArgs.OverBL.GetBasicType

                        If EventArgs.OverText Then
                            Cursor = frmMDI.crsText
                            ContextMenuStrip = cmsInstrument
                            'Following may need restoring after contextmenuchanged event.
                            Me.mniInstrumentConvert.DropDown = cmsBaseline
                            cmsBaseline.OwnerItem = Me.mniInstrumentConvert
                            Me.mniInstrumentInclude.Visible = (t Is GetType(PassThrough))
                            Me.mniInstrumentExclude.Visible = (t Is GetType(PassThrough))
                            'I could do away with the context menu for the instrument and have buttons in the tabstrip. But I think this is 
                            'more intuitive - if it's here it's clear that it's the instrument that's being deleted/converted. If it's on the 
                            'tabstrip there's a question of whether it might be the baseline, at least in the case of deletion.
                        Else
                            Cursor = frmMDI.crsXHair
                            If t Is Nothing Then
                                ContextMenuStrip = Nothing
                            Else
                                ContextMenuStrip = cmsBaseline
                            End If

                        End If
                        cmsBaseline.Tag = t

                    End If
                End If
            Else
                BLDrawing.SetTarget(sender, EventArgs)
            End If

        End If

    End Sub
#End Region

#Region "Printing"

    Friend ShowTitleWhenPrinting As Boolean = True 'whether to show title at bottom of print page.

    Friend Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        With PrintDocument1.DefaultPageSettings

            Dim f As Single = NumberOfRectsThatWillFitIntoBiggerRect(PrintArea, PrintingArea)

            With e.Graphics
                .TranslateTransform(PrintingArea.X, PrintingArea.Y)
                .ScaleTransform(f, f)
                .TranslateTransform(-PrintArea.Left, -PrintArea.Top)
            End With

            Draw(e.Graphics, True)

        End With

    End Sub

    Private Sub PrintDocument1_QueryPageSettings(ByVal sender As Object, ByVal e As System.Drawing.Printing.QueryPageSettingsEventArgs) Handles PrintDocument1.QueryPageSettings

        Dim R As Rectangle = GetUsedArea(True)
        SetPrintingArea()

        If ShowPrintArea() And (R.Left < PrintArea.Left Or R.Top < PrintArea.Top _
        Or R.Bottom > PrintArea.Bottom Or R.Right > PrintArea.Right) Then

            Select Case MsgBox("One or more parties are outside the print area and will " & _
            "be obscured. Reset the print area?", MsgBoxStyle.Exclamation Or vbYesNo, My.Application.Info.Title)

                Case vbYes

                    SetPrintArea(False)
                    Refresh()

            End Select

        End If
    End Sub

    Friend Sub SetMarginsToPrintArea()

        'Set the smallest margins possible, taking into account the print area.
        With PrintDocument1.DefaultPageSettings
            Dim pa As RectangleF = .PrintableArea
            Dim ps As Printing.PaperSize = .PaperSize
            .Margins = New Printing.Margins(pa.Left, ps.Width - pa.Right, pa.Top, ps.Height - pa.Bottom)
        End With
    End Sub

    Private Sub mnuFilePrintAreaShowHide_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFilePrintAreaShowHide.CheckStateChanged
        CType(Me.Parent, frmBase).tsbPrintArea.Checked = Me.mnuFilePrintAreaShowHide.Checked
        Refresh()
    End Sub

    Private Sub mnuFilePrintAreaReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFilePrintAreaReset.Click
        SetPrintArea(False)
        Refresh()
    End Sub

    Private Sub cmsPrintArea_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsPrintArea.Opening
        Cursor = Cursors.Default
    End Sub

    Private Sub DrawPrintArea(ByVal g As Graphics)
        g.DrawRectangle(Pens.AntiqueWhite, PrintArea)
    End Sub

    Friend Sub SetPrintArea(ByVal EnlargeOnly As Boolean)

        'Sets the Print Area big enough to fit all entities, with the same proportions as the Printable Area.
        'Called (1) when the user adds an entity (EnlargeOnly), (2) when the user selects mnuFilePrintAreaReset, _
        '(3) when he selects Yes after being prompted to reset the area on printing, and (4) on loading.
        Dim D As Rectangle

        If EntityControls.Count = 0 Then
            'Try to cover all visible area, ten pixels in.
            D = New Rectangle(10, 10, Me.Parent.ClientRectangle.Width - 20, Me.Parent.ClientRectangle.Height - 20)
        Else

            D = GetUsedArea(True)   'won't be perfect if in print preview cos of rounding in drawing.

        End If

        With PrintingArea

            Dim F As Single = 0.9 * NumberOfRectsThatWillFitIntoBiggerRect(D, PrintingArea)

            If F > 0 Then PrintArea = New Rectangle((D.Left + D.Right) / 2 - .Width / 2 / F, (D.Top + D.Bottom) / 2 - .Height / 2 / F, .Width / F, .Height / F)
        End With

    End Sub

    Dim PrintingArea As Rectangle

    Friend Sub SetPrintingArea()

        'Return the area of the paper that printing is to take place on. This takes half a second. I don't know why, but try to do it as little as possible.
        With PrintDocument1.DefaultPageSettings
            Dim m As Printing.Margins = .Margins
            Dim s As Rectangle = .Bounds
            PrintingArea = New Rectangle(m.Left, m.Top, s.Width - m.Left - m.Right, s.Height - m.Top - m.Bottom)
        End With

    End Sub

    Private Function NumberOfRectsThatWillFitIntoBiggerRect(ByVal D As Rectangle, ByVal R2 As Rectangle) As Single
        Return Min(R2.Width / D.Width, R2.Height / D.Height)
    End Function

    Friend Function ShowPrintArea() As Boolean
        Return mnuFilePrintAreaShowHide.Checked
    End Function

    Friend PrintArea As Rectangle 'The area of picCanvas to represent the print area on a sheet of paper to be printed on.

    Private PASide(1) As Line.Bearing 'Record of where mouse has gone down on PrintArea.

    Private Sub PASizeNS(ByVal Side As Line.Bearing, ByVal Y As Single)

        'Resize the print area in a vertical direction.

        If Side = Line.Bearing.South And Y < PrintArea.Top Then
            Side = Line.Bearing.North
            PASide(1) = Line.Bearing.North
        ElseIf Side = Line.Bearing.North And Y > PrintArea.Bottom Then
            Side = Line.Bearing.South
            PASide(1) = Line.Bearing.South
        End If

        If Side = Line.Bearing.North Then
            PrintArea = New Rectangle(PrintArea.Left, Y, PrintArea.Width, PrintArea.Bottom - Y)
        Else
            PrintArea = New Rectangle(PrintArea.Left, PrintArea.Top, PrintArea.Width, Y - PrintArea.Top)
        End If

    End Sub

    Private Sub PASizeEW(ByVal Side As Line.Bearing, ByVal X As Single)

        'Resize the print area in a horizontal direction.

        If Side = Line.Bearing.East And X < PrintArea.Left Then
            Side = Line.Bearing.West
            PASide(0) = Line.Bearing.West
        ElseIf Side = Line.Bearing.West And X > PrintArea.Right Then
            Side = Line.Bearing.East
            PASide(0) = Line.Bearing.East
        End If

        If Side = Line.Bearing.East Then
            PrintArea = New Rectangle(PrintArea.Left, PrintArea.Top, X - PrintArea.Left, PrintArea.Height)
        Else
            PrintArea = New Rectangle(X, PrintArea.Y, PrintArea.Right - X, PrintArea.Height)
        End If

    End Sub

#End Region

#Region "Rubberbanding"

    Friend Sub QueryRB(ByRef BLToSelect As BaseLine, ByVal P As Point)
        RaiseEvent QueryBandingInclusion(RubberBand(P), BLToSelect)
    End Sub

    Friend Event QueryBandingInclusion(ByVal RB As Rectangle, ByRef BL As BaseLine)

    Friend Function RubberBand(ByVal P As Point) As Rectangle

        'Returns a rectangle representing the rubberband that's been drawn.
        If CanvasDownPoint.HasValue Then
            With CanvasDownPoint.Value
                RubberBand = New Rectangle(Min(P.X, .X), Min(P.Y, .Y), Abs(.X - P.X), Abs(.Y - P.Y))
            End With
        End If

    End Function

#End Region

#Region "Entity Alignment"
    Friend Sub mniFormatAlign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mniFormatAlign.Click

        'Align all entitycontrols. Problems are: when two controls are on the same side of another and get aligned so that there lines are on top
        'each other. Try to deal with this with AlignSpotsOccupied. 
        For Each ec As EntityControl In Me.EntityControls
            ec.bAligned = False
            ReDim ec.AlignSpotsOccupied(Line.Bearing.West)
        Next

        Dim bStopLooking As Boolean
        Do
            bStopLooking = True
            For Each ec2 As EntityControl In Me.EntityControls
                If Not ec2.bAligned Then
                    AlignAround(ec2)    'start with an arbitrary control.
                    bStopLooking = False
                    Exit For
                End If
            Next

        Loop Until bStopLooking  'can't assume that first call to AlignAround will get all entitycontrols, cos
        'might have separate "chains", so loop until everything is got.

        Refresh()

    End Sub

    Private Sub AlignAround(ByVal ec As EntityControl)

        ec.bAligned = True

        'Align everything which is connected to ec. Am probably aligning some controls more than once in certain circumstances, 
        'but that shouldn't matter.
        Dim FirstControl As EntityControl = Nothing
        For Each EToMove As EntityControl In Me.EntityControls

            Dim h As TrancheCollection = GetStanConColl(ec.LEntity, EToMove.LEntity, GetType(ExistingCon))
            If h Is Nothing Then h = GetStanConColl(ec.LEntity, EToMove.LEntity, GetType(TransactionCon))
            If h Is Nothing Then h = GetStanConColl(ec.LEntity, EToMove.LEntity, GetType(SwapCon))

            If Not h Is Nothing Then
                If h.Orientation = BaseLine.Bearing.East Or h.Orientation = BaseLine.Bearing.West Then
                    If Not EToMove.bAligned Then
                        TryAlignTop(ec, EToMove)
                        If Not EToMove.bAligned Then TryAlignLeft(ec, EToMove)
                    Else    'If EToMove is already aligned, maybe we can align ec?
                        TryAlignTop(EToMove, ec)
                        If Not ec.bAligned Then TryAlignLeft(EToMove, ec)
                    End If

                Else
                    If Not EToMove.bAligned Then
                        TryAlignLeft(ec, EToMove)
                        If Not EToMove.bAligned Then TryAlignTop(ec, EToMove)
                    Else
                        TryAlignLeft(EToMove, ec)
                        If Not ec.bAligned Then TryAlignTop(EToMove, ec)

                    End If

                End If
                If FirstControl Is Nothing Then FirstControl = EToMove

            End If
        Next

    End Sub

    Private Sub TryAlignLeft(ByVal ec As EntityControl, ByVal EToMove As EntityControl)

        If (Not ec.AlignSpotsOccupied(Line.Bearing.North) And EToMove.Top < ec.Top) Then
            MoveEntity(EToMove, ec.Left, EToMove.Top)
            If EToMove.bAligned Then
                ec.AlignSpotsOccupied(Line.Bearing.North) = True
                EToMove.AlignSpotsOccupied(Line.Bearing.South) = True
            End If
        ElseIf (Not ec.AlignSpotsOccupied(Line.Bearing.South) And EToMove.Top > ec.Top) Then
            MoveEntity(EToMove, ec.Left, EToMove.Top)
            If EToMove.bAligned Then
                ec.AlignSpotsOccupied(Line.Bearing.South) = True
                EToMove.AlignSpotsOccupied(Line.Bearing.North) = True
            End If
        End If

    End Sub

    Private Sub TryAlignTop(ByVal ec As EntityControl, ByVal EToMove As EntityControl)

        'Idea is to try to align the tops, but not if there's a control that has already had its top aligned with 
        'ec and on the same side.
        If (Not ec.AlignSpotsOccupied(Line.Bearing.West) And EToMove.Left < ec.Left) Then
            MoveEntity(EToMove, EToMove.Left, ec.Top)
            If EToMove.bAligned Then
                ec.AlignSpotsOccupied(Line.Bearing.West) = True
                EToMove.AlignSpotsOccupied(Line.Bearing.East) = True
            End If
        ElseIf (Not ec.AlignSpotsOccupied(Line.Bearing.East) And EToMove.Left > ec.Left) Then
            MoveEntity(EToMove, EToMove.Left, ec.Top)
            If EToMove.bAligned Then
                ec.AlignSpotsOccupied(Line.Bearing.East) = True
                EToMove.AlignSpotsOccupied(Line.Bearing.West) = True
            End If
        End If

    End Sub

    Private Sub MoveEntity(ByVal ECToMove As EntityControl, ByVal NewLeft As Integer, ByVal NewTop As Integer)

        'Procedure to prevent a control being moved on top of another. It looks so bad when this happens, I might as well forget moving it.
        Dim Cancel As Boolean

        For Each c As EntityControl In Me.EntityControls
            If Not c Is ECToMove Then
                Dim r As New Rectangle(c.Left - c.Width, c.Top - c.Height, c.Width * 2, c.Height * 2)
                If r.Contains(NewLeft, NewTop) Then Cancel = True
            End If
        Next
        If Not Cancel Then
            ECToMove.Location = New Point(NewLeft, NewTop)
            ECToMove.bAligned = True
        End If

    End Sub

#End Region

#Region "Misc"

    Event EditCopyClicked(ByVal sender As Object, ByVal e As System.EventArgs)
    Private Sub mnuEditCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEditCopy.Click
        RaiseEvent EditCopyClicked(sender, e)
    End Sub

    Private Sub PartyCanvas_ContextMenuStripChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ContextMenuStripChanged
        'If the contextmenu has an owner item then it sometimes appears at the top left of the screen. So if I ever want to use
        'a menu as a submenu and as a top level context menu then I'd better set it as the submenu before I want it to be such.
        If Not Me.ContextMenuStrip Is Nothing Then Me.ContextMenuStrip.OwnerItem = Nothing
        If Me.ContextMenuStrip IsNot Nothing And TypeOf Me.Parent Is frmOptions Then Me.ContextMenuStrip = Nothing
    End Sub


    Private Sub picCanvas_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick

        '   If IsIDE() Then Stop

    End Sub

    Friend Event FillNameList(ByVal aa As List(Of INameObject), ByVal tp As Type)

    Friend Function GetInstrumentName(ByVal i As Instrument) As String
        Dim aa As New List(Of INameObject)
        RaiseEvent FillNameList(aa, i.GetType)
        Return GetName(aa, i, i.GetType)

    End Function

#End Region

#Region "Generate Post Completion Diagram"
    Friend Event PostCompletionInitialisation()
    Friend Event PostCompletionClearUp()
    Friend Event Complete()
    Friend Event ConvertToExisting()

    Friend Sub ConvertToPostCompletion()

        'Converts this file to post completion. TODO swaps.

        RaiseEvent PostCompletionInitialisation()   'set a temporary record of the ToEntity for each conncetion.
        RaiseEvent Complete()                       'adjust the temporary record if subject to a pass-through
        RaiseEvent PostCompletionClearUp()          'if the temporary record is not the current ToEntity, put this into the appropriate parent collection.
        RaiseEvent ConvertToExisting()              'convert TransactionCons into Existing ones.

        'get rid of all TransactionCons.

        Dim tt As New List(Of TrancheCollection)
        For Each j As TrancheCollection In StanConCols
            If Not j.BaseType Is GetType(TransactionCon) Then tt.Add(j)
        Next

        StanConCols = tt

        'Remove any entity that doesn't have an arrow to it or from it.
        For Each ec2 As EntityControl In EntityControls()
            ec2.DeleteIfNotUsed()
        Next

        Refresh()

    End Sub

#End Region

End Class



