'This represents the portion of a Financing which is passed through. Need a reference to the Financing and the relevant portion. Maybe
'a boolean too to reflect if the whole of it is passed through so changes in issue size are carried through.

<Serializable(), TypeConverter(GetType(UnderlyingConverter))> Public Class UnderlyingBaseLine
    Private pNomVal As New Cash    'I need to store the proportion of Fundings that are passed through. 
    <DisplayName("Nominal Value")> Public Overridable Property NomVal() As Cash
        Get
            Return pNomVal
        End Get
        Set(ByVal value As Cash)     'only want this showing if value has been set for the underlying. And currency can't be set here.  
            If InApp Then
                pNomVal = value
            End If
        End Set
    End Property

    Private pInst As Connection
    <DisplayName("Instrument")> Public ReadOnly Property Underlying() As Connection     'this has to be a connection of which the Instrument is a Funding.
        Get
            Return pInst
        End Get

    End Property

    Public Sub New(ByVal p As Connection)
        pInst = p
    End Sub

    Public Overrides Function ToString() As String

        'This will appear in the property grid.
        ToString = Underlying.ToString
        Dim t As Funding = TryCast(Underlying.Inst, Funding)
        If Not t Is Nothing Then
            If t.NomAmount.Amount.HasValue And NomVal.Amount.HasValue Then
                If Not (t.NomAmount.Amount.Value = NomVal.Amount.Value And t.NomAmount.Suffix = NomVal.Suffix) Then
                    ToString = NomVal.ToString & " " & ToString
                End If
            End If
        End If

    End Function

End Class

<Serializable()> Public Class UnderlyingsCol
    Inherits List(Of UnderlyingBaseLine)
    'need to do something so available UnderlyingBaseLines are listed and can be added here. Message is "Constructor not defined" when user presses Add button.
End Class
