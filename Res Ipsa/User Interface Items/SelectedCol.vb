Public Class SelectedCol
    'Collection of selected baselines. I need to allow the user to select more than one baseline so that:
    '(a) he can select both a TransactionCon and its consideration to reflect that both will be relevant for a swap and 
    '(b) he can select two existing obligations to set up an asset swap.
    '(c) so he can change the same properties for a lot of objects together, using the SelectedObjects property of the PropertyGrid.

    'Each picCanvas has one, and also each tab of the property grid is related to one. 

    Inherits List(Of BaseLine)

    Friend Function Last() As BaseLine
        If Me.Count > 0 Then Return Me.Item(Me.Count - 1) 'for when I need an arbitrary selected BL.
    End Function
    Friend Overloads Sub add(ByVal value As BaseLine, ByVal bUpdate As Boolean)
        MyBase.Add(value)
        If bUpdate Then frmMDI.SetPRGObjects()
    End Sub
    Friend Overloads Sub Remove(ByVal value As BaseLine, ByVal bUpdate As Boolean)
        MyBase.Remove(value)
        value.HideRedEnds()

        If bUpdate Then frmMDI.SetPRGObjects()
    End Sub

    Friend Overloads Sub Clear()
        'If I use mybase.Clear or List.Clear this won't raise the removeevents.
        For n As Integer = Count - 1 To 0 Step -1
            Remove(Me.Item(n), False)
        Next
        CurrentBL = Nothing
        frmMDI.SetPRGObjects()
    End Sub

    Friend Sub SwitchToConsideration()
        'for use when the user selects the other tab in the tabstrip.

        If SharedParentCol() IsNot Nothing Then   'should be the case.
            Dim temp As New List(Of BaseLine)   'need this so list is not changed during enumeration.
            For Each b As BaseLine In Me
                If TypeOf b Is TransactionCon Then  'should be
                    temp.Add(b)
                End If
            Next
            For Each b As BaseLine In temp
                add(CType(b, TransactionCon).Consideration, False)
                Me.Remove(b, False)
            Next
            frmMDI.SetPRGObjects()
            Dim tc As TransactionCon = TryCast(CurrentBL, TransactionCon)
            If Not tc Is Nothing Then CurrentBL = tc.Consideration
        End If

    End Sub

    Friend Function InstArray() As Instrument()

        'Return an array of all instruments represented by the list, for display in the property grid.
        'If any selected baseline has no instrument then it won't be represented in the property grid.
        Dim Insts As New List(Of Instrument)

        For Each b As BaseLine In Me

            Insts.Add(b.Inst)

        Next
        Return Insts.ToArray    'doesn't like this when there's no instrument there.

    End Function

    Friend Function LeftTab() As Boolean

        'Should there be a Left tab for a shared parent collection?
        If Not SharedParentCol() Is Nothing Then If SharedParentCol.LeftTabCon Then Return True

    End Function

    Friend Function RightTab() As Boolean

        'Should there be a Right tab for a shared parent collection?
        If Not SharedParentCol() Is Nothing Then If Not SharedParentCol.LeftTabCon Then Return True

    End Function

    Friend Function SharedParentCol() As TrancheCollection

        'If all BaseLines share the same parent collection, and are TransactionCons, I want to be able to show two tabs in the propertygrid and switch between them.
        'So identify that collection here.
        SharedParentCol = Nothing
        For Each b As BaseLine In Me
            If TypeOf b Is TransactionCon Then
                If SharedParentCol Is Nothing Then
                    SharedParentCol = CType(b, TransactionCon).ParentCol
                Else
                    If Not CType(b, TransactionCon).ParentCol Is SharedParentCol Then
                        SharedParentCol = Nothing
                        Exit For
                    End If
                End If
            Else
                SharedParentCol = Nothing
                Exit For
            End If
        Next

    End Function

    Private Function NotCreated(ByVal b As Connection) As Boolean

        'Need to know if Next or Prev has already been set. If it has, but is being drawn then it doesn't count. Otherwise 
        'it will get set and then unset immediately.
        If b Is Nothing Then Return True
        If b Is BLDrawing Then Return True
        If TypeOf b Is TransactionCon Then
            If CType(b, TransactionCon).Consideration Is BLDrawing Then Return True
        End If

    End Function

    Friend Function GetEToEConsReEntity(ByVal E As Entity, ByVal bToCons As Boolean, ByVal bExistingOrNew As Boolean) As UnderlyingsCol
        'Return all standard connections which point to or from E. So that I know what a pass-through or swap is being drawn in relation to.
        GetEToEConsReEntity = New UnderlyingsCol
        For Each s As BaseLine In Me
            Dim ec As Connection = TryCast(s, Connection)
            If Not ec Is Nothing Then
                Dim bGoAhead As Boolean = False
                If bToCons Then
                    'Want arrows to the entity.
                    bGoAhead = ec.ToEntity Is E
                Else
                    'Want arrows from the entity.
                    bGoAhead = ec.FromEntity Is E
                End If
                If bGoAhead And (TypeOf ec Is ExistingCon Or bExistingOrNew) Then GetEToEConsReEntity.Add(New UnderlyingBaseLine(s))
            End If
        Next
    End Function

End Class